using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Text;
using Scorer.Common.Database.Models;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using Scorer.Common.Database.Models.UserManagement;

namespace Scorer.Common.Database.Database
{
    public class ScorerDb : DbContext, IScorerDb
    {
        public ScorerDb()
            : base("DefaultConnection")
        {

        }
        public virtual DbSet<UploadedFile> UploadFiles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }      
        public virtual DbSet<ELMAH_Error> ELMAH_Error { get; set; }   
        public virtual DbSet<Region> Regions { get; set; }   

        public static ScorerDb Create()
        {
            return new ScorerDb();
        }

        public int SaveChangesWithErrors()
        {
            try
            {
                return SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb, ex
                );
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
    }
}