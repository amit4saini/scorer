﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using Scorer.Common.Database.Models.UserManagement;
using Scorer.Common.Database.Models;

namespace Scorer.Common.Database.Database
{
    public interface IScorerDb : IDisposable, IObjectContextAdapter
    {
        int SaveChangesWithErrors();

        DbSet<User> Users { get; set; }

        DbSet<Role> Roles { get; set; }

        DbSet<UserRole> UserRoles { get; set; }

        DbSet<UserLogin> UserLogins { get; set; }

        DbSet<RolePermission> RolePermissions { get; set; }
        DbSet<UploadedFile> UploadFiles { get; set; }    

        DbSet<Country> Countries { get; set; }

        DbSet<State> States { get; set; }       

        DbSet<Notification> Notifications { get; set; }
       
        DbSet<ELMAH_Error> ELMAH_Error { get; set; }
        DbSet<Region> Regions { get; set; }
    }
}

