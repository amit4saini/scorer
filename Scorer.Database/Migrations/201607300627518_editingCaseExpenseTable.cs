namespace Kreller.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingCaseExpenseTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StandardExpenses", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.StandardExpenses", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.StandardExpenses", "EditedBy", "dbo.Users");
            DropForeignKey("dbo.CaseExpenses", "ExpenseId", "dbo.StandardExpenses");
            DropIndex("dbo.CaseExpenses", new[] { "ExpenseId" });
            DropIndex("dbo.StandardExpenses", new[] { "CreatedBy" });
            DropIndex("dbo.StandardExpenses", new[] { "EditedBy" });
            DropIndex("dbo.StandardExpenses", new[] { "DeletedBy" });
            AddColumn("dbo.CaseExpenses", "ExpensePaymentType", c => c.Int(nullable: false));
            AddColumn("dbo.CaseExpenses", "InvoiceApproved", c => c.Boolean(nullable: false));
            AddColumn("dbo.Vendors", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.CaseExpenses", "ExpenseId");
            DropTable("dbo.StandardExpenses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StandardExpenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceName = c.String(),
                        Currency = c.String(),
                        Amount = c.Double(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        EditedOn = c.DateTime(nullable: false),
                        EditedBy = c.Guid(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CaseExpenses", "ExpenseId", c => c.Int(nullable: false));
            DropColumn("dbo.Vendors", "Cost");
            DropColumn("dbo.CaseExpenses", "InvoiceApproved");
            DropColumn("dbo.CaseExpenses", "ExpensePaymentType");
            CreateIndex("dbo.StandardExpenses", "DeletedBy");
            CreateIndex("dbo.StandardExpenses", "EditedBy");
            CreateIndex("dbo.StandardExpenses", "CreatedBy");
            CreateIndex("dbo.CaseExpenses", "ExpenseId");
            AddForeignKey("dbo.CaseExpenses", "ExpenseId", "dbo.StandardExpenses", "Id");
            AddForeignKey("dbo.StandardExpenses", "EditedBy", "dbo.Users", "Id");
            AddForeignKey("dbo.StandardExpenses", "DeletedBy", "dbo.Users", "Id");
            AddForeignKey("dbo.StandardExpenses", "CreatedBy", "dbo.Users", "Id");
        }
    }
}
