using Scorer.Common.Database.Database;
using Scorer.Common.Database.Enums;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using Scorer.Common.Database.Models.UserManagement;
using Scorer.Common.Database.Models;

namespace Scorer.Common.Database.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;
    using MigrationSeedData;

    internal sealed class Configuration : DbMigrationsConfiguration<ScorerDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ScorerDb context)
        {
            var adminId = AddAdminRole(context);
            var userId = AddUserRole(context);
            AddUsersAndUserRoles(context, userId, adminId);
            AddRegions(context);
            AddCountries(context);
            AddStates(context);        
        }
     
        private void AddUsersAndUserRoles(ScorerDb context, Guid userRoleId, Guid adminRoleId)
        {
            var userId = Guid.Parse("47a839cb-23e9-e511-82af-6036dd4de34b");
            var adminId = Guid.Parse("aff6fcea-23e9-e511-82af-6036dd4de34b");   
            if (!context.Users.Any())
            {
                context.Users.AddOrUpdate(
                new User
                {
                    FirstName = "UserFirstName",
                    LastName = "UserLastName",
                    UserName = "user@vicesoftware.com",
                    Id = userId,
                    PasswordHash = "AGYKIqMx5vKUPK5Zch4jz1kEqZ+8LX7QMjUuW/YhAO6A0QR9VJQDvZIQZdRV4nG70A==",
                    SecurityStamp = "cae554ed-f13e-433e-aba5-62f6a0aeb025",
                    Email = "user@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                },
                new User
                {
                    FirstName = "AdminFirstName",
                    LastName = "AdminLastName",
                    UserName = "admin@vicesoftware.com",
                    Id = adminId,
                    PasswordHash = "AMCvvQQAqe/yuE5ZTyaohOWYxny4o6QlA/fn1lDhTJrjlogBrJubIc1HzpZ8L7/grA==",
                    SecurityStamp = "0c767b3d-de4e-4210-a1bc-63be37e50934",
                    Email = "admin@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                }               
            );
            }
            if (!context.UserRoles.Any())
            {
                context.UserRoles.AddOrUpdate(
                new UserRole
                {
                    RoleId = userRoleId,
                    UserId = userId
                },
                new UserRole
                {
                    RoleId = adminRoleId,
                    UserId = adminId
                }              
            );
            }            
        }      

        private static Guid AddAdminRole(ScorerDb context)
        {
            var roleName = Roles.Admin.ToString();
            var roleId = Guid.Parse("1aa58124-c50f-4773-ad62-694fad3ff939");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_VENDOR },    
              };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }        

        private static Guid AddUserRole(ScorerDb context)
        {
            string roleName;
            Guid roleId;
            roleName = Roles.User.ToString();
            roleId = Guid.Parse("cb7b51f1-9e76-4986-b829-dc19b85c2329");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission =
                  new List<RolePermission> {  
             };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }

        private void AddRegions(ScorerDb context)
        {
            if (!context.Regions.Any())
            {
                var regionList = new RegionList();
                var regions = regionList.Region();
                foreach (var region in regions)
                {
                    if (context.Regions.Count(x => x.RegionName == region) == 0)
                    {
                        context.Regions.AddOrUpdate(new Region { RegionName = region });
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddCountries(ScorerDb context)
        {
            if (!context.Countries.Any())
            {
                var countryList = new CountryList();
                var countries = countryList.Country();
                foreach (var country in countries)
                {
                    var countryDetails = country.Split('~');
                    var countryName = countryDetails[0];
                    var regionName = countryDetails[1];
                    var region = context.Regions.FirstOrDefault(x => x.RegionName == regionName);
                    if (region != null)
                    {
                        if (context.Countries.Count(x => x.CountryName == countryName) == 0)
                        {
                            context.Countries.AddOrUpdate(new Country { CountryName = countryName, RegionId = region.RegionId });
                        }
                    }
                }
                context.SaveChanges();
            }
        }


        private void AddStates(ScorerDb context)
        {
            if (!context.States.Any())
            {
                var stateList = new StateList();
                var states = stateList.States();
                foreach (var state in states)
                {
                    var stateDetails = state.Split('~');
                    var stateName = stateDetails[0];
                    var countryName = stateDetails[1];

                    var country = context.Countries.FirstOrDefault(x => x.CountryName == countryName);
                    if (country != null)
                    {
                        if (context.States.Count(x => x.StateName == stateName) == 0)
                        {
                            context.States.AddOrUpdate(new State { StateName = stateName, CountryId = country.CountryId });
                        }
                    }
                }
                context.SaveChanges();
            }
        }
    }
}

