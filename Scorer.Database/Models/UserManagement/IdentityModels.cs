﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Scorer.Common.Database.Database;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scorer.Common.Database.Models.UserManagement
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser<Guid, UserLogin, UserRole, UserClaim>
    {
        public User()
        {
            CreationDateTime = DateTime.UtcNow;       
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }        

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime LastEditedDateTime { get; set; }
        public Guid? ProfilePicKey { get; set; }
        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }


    }

    public class UserIdentityStore : UserStore<User, Role, Guid,
        UserLogin, UserRole, UserClaim>
    {
        public UserIdentityStore(ScorerDb context)
            : base(context)
        {
        }
    }

    public class RoleStore : RoleStore<Role, Guid, UserRole>
    {
        public RoleStore(ScorerDb context)
            : base(context)
        {
        }
    }
}