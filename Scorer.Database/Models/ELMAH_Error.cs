﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scorer.Common.Database.Models
{
    public class ELMAH_Error
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ErrorId { get; set; }
        public string Application { get; set; }
        public string Host { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string User { get; set; }
        public int StatusCode { get; set; }
        public DateTime TimeUtc { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Sequence { get; set; }
        [Column(TypeName = "ntext")]
        public string AllXml { get; set; }
    }
}
