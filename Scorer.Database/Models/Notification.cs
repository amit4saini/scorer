﻿using Scorer.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Scorer.Common.Database.Models
{
    public class Notification
    {
        public Notification()
        {
            CreatedOn = DateTime.Now;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User ForUserId { get; set; }
        public string NotificationText { get; set; }
        [DefaultValue(false)]
        public bool IsViewed { get; set; }
        public string URL { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
    }
}
