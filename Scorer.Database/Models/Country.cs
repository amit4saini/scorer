using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scorer.Common.Database.Models
{
    public class Country
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Scorer.Common.Database.Models.Region Region { get; set; }

    }
}