﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scorer.Common.Database.Models
{
    public class UploadedFile
    {
        [Key]     
        public Guid FileId { get; set; }

        public string FileName { get; set; }    

        public virtual string FileUrl { get; set; } 
        public string FileContent { get; set; }
        public string FileType { get; set; }
  
        public DateTime CreateDateTime { get; set; }

        [ForeignKey("UserId")]
        public UserManagement.User User
        {
            get; set;
        }
        public Guid UserId { get; set; }

        public string AzureKey { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        public DateTime? DeleteDateTime { get; set; }
        public string DeletedByUser { get; set; }    
    }
}
