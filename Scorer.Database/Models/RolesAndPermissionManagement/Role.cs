using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scorer.Common.Database.Models.RolesAndPermissionManagement
{
    public class Role : IdentityRole<Guid, UserRole>
    {
        public Role() { }
        public Role(string name) { Name = name; }

        public IList<RolePermission> RolePermissions { get; set; }
    }
}