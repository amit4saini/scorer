﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scorer.Common.Database.Models.RolesAndPermissionManagement
{
    public class UserClaim : IdentityUserClaim<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserClaimId { get; set; }
    }
}