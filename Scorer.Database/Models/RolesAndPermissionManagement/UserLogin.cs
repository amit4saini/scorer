using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scorer.Common.Database.Models.RolesAndPermissionManagement
{
    public class UserLogin : IdentityUserLogin<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserLoginId { get; set; }   
    }
}