﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scorer.Common.Database.Enums;

namespace Scorer.Common.Database.Models.RolesAndPermissionManagement
{
    public class RolePermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RolePermissionId { get; set; }

        public Guid RoleId { get; set; }

        public Permission Permission { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }
    }
}
