﻿using System.Collections.Generic;

namespace Scorer.Common.Database.MigrationSeedData
{
    class RegionList
    {
        public List<string> Region()
        {
            var regions = new List<string>{
                      "Africa",
                      "Australia-Oceana",
                      "Central America/Caribbean",
                      "Central Asia",
                      "East/South East Asia",
                      "Europe",
                      "Middle East",
                      "North America",
                      "South America",
                      "South Asia",
                 };
            return regions;
        }
    }
}
