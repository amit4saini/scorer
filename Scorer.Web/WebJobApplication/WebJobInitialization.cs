﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scorer.Web.DependencyResolution;
using Scorer.Web.App_Start;

namespace WebJobApplication
{
    public class WebJobInitialization
    {
        public void AutomapperInitialization()
        {
            AutomapperConfig.Configure();
        }
    }
}
