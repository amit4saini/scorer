﻿using AutoMapper;
using Scorer.Web.DependencyResolution;

namespace Scorer.Web.App_Start
{
    public static class AutomapperConfig
    {
        public static void Configure()
        {
            var profiles = IoC.Container.GetAllInstances<Profile>();
            Mapper.Initialize(cfg => {
                foreach (var profile in profiles)
                    cfg.AddProfile(profile);
            });
        }
    }
}