using System.Web.Http;
using Scorer.Web.DependencyResolution;
using StructureMap;
using WebApi.StructureMap;

namespace Scorer.Web
{
    public static class Bootstrapper
    {
        public static Container Initialize()
        {
            GlobalConfiguration.Configuration.UseStructureMap(c =>
            {
                c.AddRegistry<DefaultRegistry>();
            });

            // This should return the container created above:
            // https://groups.google.com/forum/#!topic/structuremap-users/S7nBib95zh0
            return new Container();  
        }
    }
}