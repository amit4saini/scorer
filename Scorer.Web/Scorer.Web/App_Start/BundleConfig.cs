﻿using System.Web.Optimization;
using System.Configuration;
using Scorer.Common.Infrastructure.Utility;

namespace Scorer.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/Content/static/js").Include(
                "~/Content/lib/jquery/jquery.min.js",
                "~/Content/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js",
                "~/Scripts/js/main.js", 
                "~/Content/lib/bootstrap/dist/js/bootstrap.min.js",
                "~/Content/lib/jquery.bootstrap-touchspin/jquery.bootstrap-touchspin.js"));

            bundles.Add(new StyleBundle("~/Content/static/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/lib/stroke-7/style.css",
                "~/Content/lib/font-awesome/css/font-awesome.min.css",
                "~/Content/css/style.css",
                "~/Content/css/custom.css",
                "~/Content/lib/jquery.nanoscroller/css/nanoscroller.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css"                    ));

            bundles.Add(new StyleBundle("~/angular/app/css").Include(
                     "~/Assets/lib/stroke-7/style.css",
                     "~/Assets/lib/jquery.nanoscroller/css/nanoscroller.css",
                     "~/Content/select.min.css",
                     "~/Assets/css/style.css",
                     "~/Assets/css/custom.css",
                     "~/Content/angular-toastr.min.css",
                     "~/Assets/lib/font-awesome/css/font-awesome.min.css",
                       "~/Content/angular-bootstrap-calendar.min.css",
                     "~/Assets/lib/textAngular/textAngular.css",
                     "~/Content/pivot.min.css"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/app")
               .Include("~/Scripts/jquery-2.2.0.min.js",
               "~/Scripts/tinymce/jquery.tinymce.min.js",
               "~/Scripts/tinymce/tinymce.min.js",
         "~/Scripts/angular.js",
         "~/Scripts/AngularUI/tinymce.js",
         "~/Content/lib/bootstrap/dist/js/bootstrap.min.js",
      "~/Scripts/toastr.min.js",
       "~/Scripts/jquery-ui.js",
       "~/Scripts/angular-resource.js",
       "~/Scripts/angular-animate.js",
       "~/Scripts/AngularUI/ui-router.js",
       "~/Scripts/angular-ui/ui-bootstrap.js",
       "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
       "~/Scripts/angular-ui/angular-file-upload.min.js",
       "~/Scripts/angular-toastr.tpls.min.js",
       "~/Scripts/angular-cookies.js",
       "~/Scripts/angular-route.js",
       "~/Scripts/angular-sanitize.js",
       "~/Scripts/spin.min.js",
       "~/Scripts/angular-spinner.min.js",
       "~/Scripts/ng-file-upload-master/dist/ng-file-upload.min.js",
       "~/Scripts/select.min.js",
       "~/Scripts/jquery.bootstrap-touchspin.js ",
       "~/Scripts/textAngular-rangy.min.js",
       "~/Scripts/textAngular-sanitize.min.js",
       "~/Scripts/textAngular.min.js",
       "~/Scripts/moment.min.js",
       "~/Scripts/angular-filter.min.js",
       "~/Scripts/angular-bootstrap-calendar-tpls.min.js",
       "~/Scripts/ment.io/dist/mentio.js",
       "~/Scripts/ment.io/dist/templates.js",
       "~/Scripts/tinymce/tinymce-additional.min.js",
       "~/Scripts/pivot.min.js")
       .IncludeDirectory("~/Webapp", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/participan_t/app")
              .Include("~/Scripts/jquery-2.2.0.min.js",
        "~/Scripts/angular.js",
     "~/Scripts/toastr.min.js",
      "~/Scripts/jquery-ui.js",
      "~/Scripts/angular-resource.js",
      "~/Scripts/angular-animate.js",
      "~/Scripts/AngularUI/ui-router.js",
      "~/Scripts/angular-ui/ui-bootstrap.js",
      "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
      "~/Scripts/angular-ui/angular-file-upload.min.js",
      "~/Scripts/angular-toastr.tpls.min.js",
      "~/Scripts/angular-cookies.js",
      "~/Scripts/angular-route.js",
      "~/Scripts/angular-sanitize.js",
      "~/Scripts/spin.min.js",
      "~/Scripts/angular-spinner.min.js",
      "~/Scripts/ng-file-upload-master/dist/ng-file-upload.min.js",
      "~/Scripts/select.min.js",
      "~/Scripts/jquery.bootstrap-touchspin.js ",
      "~/Scripts/textAngular-rangy.min.js",
      "~/Scripts/textAngular-sanitize.min.js",
      "~/Scripts/textAngular.min.js",
      "~/Scripts/moment.min.js",
      "~/Scripts/angular-filter.min.js",
      "~/Scripts/angular-bootstrap-calendar-tpls.min.js")
      .IncludeDirectory("~/ParticipantApp", "*.js", true));

            // adding query string to urls after every change
            if (ConfigurationManager.AppSettings["Localhost"].Equals("false"))
            {
                bundles.GetBundleFor("~/bundles/jquery").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/bundles/jqueryval").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/bundles/modernizr").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/Content/static/js").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/Content/static/css").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/bundles/bootstrap").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/Content/css").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/angular/app/css").Transforms.Add(new FileHashVersionBundleTransformService());
                bundles.GetBundleFor("~/bundles/app").Transforms.Add(new FileHashVersionBundleTransformService());
            }
        }
    }
}
