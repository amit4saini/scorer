using System.Collections.Generic;

namespace Scorer.Web.ViewModels.UserManagement
{
    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}