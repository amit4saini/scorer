﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Scorer.Web.App_Start;
using Newtonsoft.Json.Serialization;

namespace Scorer.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // TODO: Prashanth - I'm returning the container eventhough it's not used just
            // to demonstrate that it can be done. i beleive we will need this
            // for attribute injection
            //CREATING ISSUE IN JS,NEEDS WORK. WILL BE DONE LATER
            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            var container = Bootstrapper.Initialize();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutomapperConfig.Configure();
        }

        
    }
}
