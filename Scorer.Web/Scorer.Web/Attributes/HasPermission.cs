﻿using System;
using System.Linq;
using Scorer.Common.Database.Enums;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using Scorer.Core.Domain;
using Scorer.Web.DependencyResolution;

namespace Scorer.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class HasPermission : AuthorizationFilterAttribute
    {
            private Permission[] _requiredPermissions;
            private IUserService _userService;

        public HasPermission(Permission[] requiredPermissions)
            : base()
        {
            _requiredPermissions = requiredPermissions;
             _userService = IoC.Container.GetInstance<IUserService>();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (_userService.UserPermissions.Intersect(_requiredPermissions).Any())
            {
                base.OnAuthorization(actionContext);
                return;
            }
            SendUnAuthorizedUserResponse(actionContext);        
        }

        void SendUnAuthorizedUserResponse(HttpActionContext actionContext)
        {            
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);                
        }      
     }    
  }

