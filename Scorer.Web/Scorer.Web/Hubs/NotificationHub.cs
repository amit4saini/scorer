﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Scorer.Common.Database.Database;
using Scorer.Core.Domain;

namespace Scorer.Web.Hubs
{
    public interface INotificationHub
    {
        void Send(string name, List<string> message);
    }

    public class NotificationHub : Hub, INotificationHub
    {
        private IUserService _userService;
        public NotificationHub(IScorerDb scorerDb, IUserService userService)
        { 
            _userService = userService;
        }
        public void Send(string userId, List<string> message)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            // Call the addNewMessageToPage method to update clients.
            hubContext.Clients.User(userId).sendNotification(message);
        }
    }
}