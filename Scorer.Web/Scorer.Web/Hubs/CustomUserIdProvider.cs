﻿using Scorer.Core.Domain;
using Scorer.Common.Database.Database;
using Microsoft.AspNet.SignalR;

namespace Scorer.Web.Hubs
{
    public class CustomUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            var errorHandler = new ErrorHandlerService();
            var scorerDb = new ScorerDb();
            var _userService = new UserService(scorerDb, errorHandler);
            return _userService.UserId.ToString();
        }
    }
}