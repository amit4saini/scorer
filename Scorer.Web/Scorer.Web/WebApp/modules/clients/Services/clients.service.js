﻿(function () {
    'use strict';
    angular.module('client').factory('Clients', ['$http', 'toastr', '$uibModal', 'usSpinnerService','$window','ErrorHandlerService','$location','$anchorScroll','$stateParams', '$global',
function ($http, toastr, $uibModal, usSpinnerService, $window, ErrorHandlerService, $location, $anchorScroll, $stateParams, $global) {
    var vm = this;

        function init() {
            vm.defaultPageSize = $window.defaultPageSize;
            vm.isClientSearchActive = false;
            vm.allPages = [];
            fetchAndStoreClientsList(0);
        }

        function clientFormInit() {
            vm.isClientContactUserBeingAdded = false;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.allPages = [];
            vm.states = [];
            vm.clientId = $stateParams.clientId;
            vm.showValidationMessage = false;
            vm.showClientValidationMessage = false;
            vm.isClientValid = false;
            vm.newClientContactUser = [];
            vm.clientContactBeingEdited = false;
            fetchAndStoreIndustryList();
            fetchAndStoreIndustrySubCategoriesList();
            fetchAndStoreCountryList();
            fetchAndStoreSalesRepresentativeList(function () {
                vm.clientId != "newclient" ? getClientDetails(vm.clientId) : vm.salesRepresentative = undefined;
            });
            if (vm.clientId != "newclient") {
                vm.isClientBeingEdited = true;
                vm.clientContactUser = {};
            }
            else {
                vm.client = {};
                vm.industry = undefined;
                vm.industrySubCategory = undefined;
                vm.country = undefined;
                vm.state = undefined;
                vm.isClientBeingEdited = false;
                vm.client.clientName = "";
                vm.client.clientContactUsersList = [];
            }
        }

        function resetSearchResult()
        {
            vm.isClientSearchActive = false;
            fetchAndStoreClientsList(0);
        }

        function editClientForm(client)
        {
            $location.path("/case-management/clients/" + client.clientId);
        }
        
        function addBusinessUnit(newClientContactUser,index)
        {
            if (newClientContactUser) {
                newClientContactUser.contactUserBusinessUnits.push({ businessUnit: "", accountCode: "" })
                vm.newClientContactUser[index] = newClientContactUser;
            }
            else {
                vm.clientContactUser.contactUserBusinessUnits.push({ businessUnit: "", accountCode: "" });
            }  
        }

        function removeBusinessUnit(clientContactUserIndex, contactUserBusinessUnitIndex)
        {
            if (vm.isClientContactUserBeingAdded) {
                vm.newClientContactUser[clientContactUserIndex].contactUserBusinessUnits.splice(contactUserBusinessUnitIndex, 1);
            } 
            else {
                vm.clientContactUser.contactUserBusinessUnits.splice(contactUserBusinessUnitIndex, 1);
            }
        }

        function addClientForm()
        {
            vm.client = {};
            $location.path("/case-management/clients/newclient");
        }

        function validateClient() {
            if (vm.client.clientName == "" || vm.client.clientName == undefined || vm.client.salesRepresentativeId == undefined){
                vm.showClientValidationMessage = true;
                return false;
            }
            else {
                vm.showClientValidationMessage = false;
                return true;
            } 
        }

        function saveClient() {
            if (!validateClient()) { return; }
            vm.isClientContactUserAdded = false;
            if (vm.client.clientContactUsersList.length == 0 && vm.newClientContactUser.length == 0) {
                toastr.error("Please add a contact");
                return;
            }
            else {
                for (var count = 0; count < vm.client.clientContactUsersList.length; count++) {
                    if (vm.client.clientContactUsersList[count].isDeleted == false) {
                        vm.isClientContactUserAdded = true;
                    }
                }
            }
            if (!vm.isClientContactUserAdded && vm.newClientContactUser.length == 0) {
                toastr.error("Please add a contact");
                return;
            }
            if (!addContactToList()) { return true; }
            vm.client.clientContactUsers = vm.client.clientContactUsersList;
            for (var count = 0; count < vm.client.clientContactUsers.length; count++)
            {
                if (vm.client.clientContactUsers[count].country != null)
                    vm.client.clientContactUsers[count].countryId = vm.client.clientContactUsers[count].country.countryId;
                if (vm.client.clientContactUsers[count].state != null)
                    vm.client.clientContactUsers[count].stateId = vm.client.clientContactUsers[count].state.stateId;               
            }
            if (!vm.isClientBeingEdited) {
                saveNewClient(vm.client, function (response, status) {
                    toastr.success("Client added");
                    vm.isClientContactUserBeingAdded = false;
                    vm.showValidationMessage = false;
                    vm.newClientContactUser = [];
                    editClientForm(response);
                },
                    function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else {
                saveEditedClient(vm.client, function (response, status) {
                    toastr.success("Client edited");
                    clientFormInit();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); })
            }
        }

        function saveNewClient(client, cbSuccess, cbError)
        {
            $http.post('/api/client', client)
                  .success(function (response, status) {
                      cbSuccess(response, status);
                  }).error(function (response, status) {
                      cbError(response, status);
                  });
        }

        function saveEditedClient(client, cbSuccess, cbError)
        {
            $http.put('/api/client/' + client.clientId, client)
                .success(function (response, status) {
                    cbSuccess(response, status);
                }).error(function (response, status) {
                    cbError(response, status);
                });
        }
       
        function fetchAndStoreCountryList() {
            vm.getExistingCountries(function (response, status) { vm.countries = response; },
                function (response, status) { vm.countries = []; ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function onCountrySelected(clientContactUser, index, $item) {
            clientContactUser.state = undefined;
            if (vm.isClientContactUserBeingAdded) {
                clientContactUser.countryName = $item.countryName;
                vm.newClientContactUser[index] = clientContactUser;
            }
            else {
                clientContactUser.countryName = $item.countryName;
                vm.client.clientContactUsersList[index] = clientContactUser;
            }
            vm.getExistingStatesByCountryId($item.countryId, function (response, status) { vm.states = response; },
                function (response, status) { vm.states = []; ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function onStateSelected(clientContactUser, index, $item) {
            if (vm.isClientContactUserBeingAdded)
                vm.newClientContactUser.stateName = $item.stateName;
            else {
                clientContactUser.stateName = $item.stateName;
                vm.client.clientContactUsersList[index] = clientContactUser;
            }
        }

        function refreshResults($select) {           
            var search = $select.search,
              list = angular.copy($select.items),
              FLAG = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.clientId !== FLAG;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    clientId: FLAG,
                    clientName: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }

        function clear($event, $select) {            
            $event.stopPropagation();
            $select.selected = undefined;
            $select.search = undefined;
            $select.activate();
        }

        function fetchAndStoreClientsList(pageIndex) {
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '?$skip=' + skipCount + "&$orderby=LastEditedDateTime desc";
            getExistingClients(options, function (response, status) {
                vm.clients = response.items.
                map(function (item) {
                    item.clientContactUsers[0].contactName = item.clientContactUsers[0].contactName.split(" ");
                    item.clientContactUsers[0].contactName[0] = item.clientContactUsers[0].contactName[0];
                    for (var i = 2 ; i < item.clientContactUsers[0].contactName.length ; i++)
                        item.clientContactUsers[0].contactName[1] = item.clientContactUsers[0].contactName[1] + " " + item.clientContactUsers[0].contactName[i];
                    return item;
                });
                vm.totalNumberOfClients = response.count;
                vm.allPages.length = Math.ceil(vm.totalNumberOfClients / vm.defaultPageSize);
            }, function (response, status) {
                vm.clients = [];
                vm.totalNumberOfClients = 0;
                ErrorHandlerService.DisplayErrorMessage(response, status);
            });
        }

        function getExistingClients(options, cbSuccess, cbError)
        {
            $http.get('/api/clients' + options)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function searchClients(searchQuery)
        {
            if (searchQuery == "" || searchQuery == undefined)
            {
                return;
            }
            vm.isClientSearchActive = true;
            vm.clientSearchQuery = searchQuery;
            getClientsByKeyword(0);
        }

        function getClientsByKeyword(pageIndex)
        {
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '&$top=' + vm.defaultPageSize + '&$skip=' + skipCount + "&$orderby=LastEditedDateTime desc";
            getExistingClientsByKeyword(vm.clientSearchQuery, options, function (response, status) {
                vm.clients = response.items;
                vm.totalNumberOfClients = response.count;
                vm.allPages.length = Math.ceil(vm.totalNumberOfClients / vm.defaultPageSize);
            }, function (response, status) {
                vm.clients = [];
                vm.totalNumberOfClients = 0;
                ErrorHandlerService.DisplayErrorMessage(response, status);
            });
        }

        function getExistingClientsByKeyword(keyword, options, cbSuccess, cbError)
        {
            $http.get('/api/search/clients?keyword=' + keyword + options)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function deleteClient(clientId) {
            deleteExistingClient(clientId, function (response, status) {
                toastr.success("Client removed");
                vm.clients.length < 1 ? fetchAndStoreClientsList(vm.pageIndex - 1) : fetchAndStoreClientsList(vm.pageIndex);
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingClient(clientId, cbSuccess, cbError)
        {
            $http.delete('/api/client/' + clientId)
               .success(function (response, status) {
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
        }

        function getClientDetails(clientId) {
            getClientByClientId(clientId, function (response, status) {
                vm.client = response;
                vm.client.lastEditedDateTime = new Date(vm.client.lastEditedDateTime);
                vm.client.clientContactUsersList = response.clientContactUsers;
                getCasesAssociatedWithCurrentClient(0);
                if (vm.client.industryId == null) {
                    vm.industry = undefined;
                }
                else {
                    vm.industry = { name: vm.client.industryName, id: vm.client.industryId };
                }
                if (vm.client.industrySubCategoryId == null) {
                    vm.industrySubCategory = undefined;
                }
                else {
                    vm.industrySubCategory = { name: vm.client.industrySubCategoryName, id: vm.client.industrySubCategoryId };
                }
                if (vm.client.salesRepresentativeId == null) {
                    vm.salesRepresentative = undefined;
                }
                else {
                    for (var i = 0; i < vm.salesRepresentativeList.length; i++) {
                        if (vm.salesRepresentativeList[i].id == vm.client.salesRepresentativeId) {
                            vm.salesRepresentative = vm.salesRepresentativeList[i];
                            vm.salesRepDisplayName = vm.salesRepresentative.firstName + " " + vm.salesRepresentative.lastName;
                        }
                    }
                }
                if (vm.client.reports == undefined) {
                    vm.client.reports = [];
                }
                vm.clientDisplayName = vm.client.clientName;
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            
        }

        function getClientByClientId(clientId, cbSuccess, cbError) {
            $http.get('/api/client/' + clientId)
               .success(function (response, status) {
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
        }

        function fetchAndStoreIndustryList() {
            vm.getExistingIndustries(function (response, status) { vm.industries = response; },
                function (response, status) { vm.industries = []; ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function onIndustrySelected($item) {
            vm.client.industryId = $item.id;
        }

        function fetchAndStoreIndustrySubCategoriesList() {
            vm.getExistingIndustrySubcategories(function (response, status) { vm.industrySubCategories = response; },
                function (response, status) { vm.industrySubCategories = []; ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function onSubCategorySelected($item) {
            vm.client.industrySubCategoryId = $item.id;
        }

        function fetchAndStoreSalesRepresentativeList(cbGetClient) {
            var options = "?$skip=0&$filter=Role/Name eq 'Sales'"
            vm.getExistingUsers(options, function (response, status) { vm.salesRepresentativeList = response.items; cbGetClient(); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.salesRepresentativeList = []; });
        }

        function onsalesRepresentativeSelected($item) {
            vm.client.salesRepresentativeId = $item.id;
        }
    
        function addClientContactUser() {
            vm.isClientContactUserBeingAdded = true;
            vm.newClientContactUser.push({ id: -1, isDeleted: false, contactUserBusinessUnits: [{businessUnit:"",accountCode:""}]});
        }
     
        function openAddClientContactUserModal() {
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/clients/views/add-contact-modal.view.html',
                size: 'lg'
            });       
            vm.modalInstance.result.then(function () {
            }, function () {
                //The modal closed by click on on any other part of the screen
            });
        }
        
        function closeModal() {
            vm.clientContactUser = {};
            vm.states = {};
            vm.isContactBeingEdited = false;
            vm.showValidationMessage = false;
            vm.modalInstance.dismiss('cancel');
        }
        function cancelClientContactUserChanges(newClientContactUser, index) {
            if (newClientContactUser) {
                vm.newClientContactUser.splice(index, 1);
            }
            else {
                vm.clientContactUser = {};
                vm.client.clientContactUsersList[vm.editIndex] = vm.clientContactUserEdited;
                vm.showValidationMessage = false;
                vm.clientContactBeingEdited = false;
            }
        }

        function validateClientContactUser(contactUser) {
            if (!contactUser.contactName) {
                vm.showValidationMessage = true;
                return false;
            }
            if (contactUser.contactUserBusinessUnits.length > 0) {
                for (var count = 0; count < contactUser.contactUserBusinessUnits.length; count++) {
                    if (contactUser.contactUserBusinessUnits[count].businessUnit == "" || contactUser.contactUserBusinessUnits[count].businessUnit == undefined || contactUser.contactUserBusinessUnits[count].accountCode == "" || contactUser.contactUserBusinessUnits[count].accountCode == undefined) {
                        vm.showValidationMessage = true;
                        return false;
                    }
                }
            }
            else {
                contactUser.contactUserBusinessUnits.push({ businessUnit: "", accountCode: "" });
                vm.showValidationMessage = true;
                return false;
            }
            return true;
        }

        function addContactToList() {
            if (vm.newClientContactUser.length > 0) {
                for (var index = 0 ; index < vm.newClientContactUser.length; index++) {
                    if (!validateClientContactUser(vm.newClientContactUser[index])) { return false; }
                    vm.client.clientContactUsersList.push(vm.newClientContactUser[index]);
                }
                vm.newClientContactUser = [];
            }
            if (vm.clientContactUser.id != undefined) {
                if (!validateClientContactUser(vm.clientContactUser)) { return false; }
                vm.client.clientContactUsersList[vm.editIndex] = vm.clientContactUser;
            }
            return true;
        }

        function editClientContactUser(contactUser, $index) {
            vm.clientContactBeingEdited = true;
            vm.clientContactUser = angular.copy(contactUser);
            vm.clientContactUserEdited = angular.copy(vm.client.clientContactUsersList[$index]);
            if (contactUser.country == null) {
                vm.clientContactUser.country = undefined;
            }
            else {
                onCountrySelected(vm.clientContactUser, $index, vm.clientContactUser.country);
            }
            if (contactUser.state == null) {
                vm.clientContactUser.state = undefined;
            }
            else {
                vm.clientContactUser.state = contactUser.state;
            }
            vm.isContactBeingEdited = true;
            if (vm.clientContactUser.contactUserBusinessUnits == undefined)
            vm.clientContactUser.contactUserBusinessUnits = [];
            vm.editIndex = $index;
        }

        function deleteAddedContactUser($index) {
            if (vm.client.clientContactUsersList[$index].id){
                vm.client.clientContactUsersList[$index].isDeleted = true;
            }
            else {
                vm.client.clientContactUsersList.splice($index, 1);
            }
        }

        function getCasesAssociatedWithCurrentClient(pageIndex) {
            vm.clientCasesPageIndex = pageIndex;
            var skipCount = vm.clientCasesPageIndex * vm.defaultPageSize;
            var currentClientId = $stateParams.clientId;
            var options = "?$skip=" + skipCount + "&$filter=IsDeleted eq false&$orderby=LastEditedDateTime desc";
            getCasesByClientId(currentClientId, options, function (response, status) {
                vm.clientCases = response.items;
                vm.totalNumberOfClientCases = response.count;
                vm.allPages.length = Math.ceil(vm.totalNumberOfClientCases / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.clientCases = [];
                vm.totalNumberOfClientCases = 0;
            });
        }

        function getCasesByClientId(clientId, options, cbSuccess, cbError)
        {
            $http.get("/api/client/" + clientId + "/cases" + options)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function openCaseDetails(selectedCase) {
            $location.path('/case-management/cases/' + selectedCase.caseNumber+'/caseDetails');
        }

        vm.init = init;
        vm.saveClient = saveClient;
        vm.onCountrySelected = onCountrySelected;
        vm.fetchAndStoreClientsList = fetchAndStoreClientsList;
        vm.fetchAndStoreCountryList = fetchAndStoreCountryList;
        vm.refreshResults = refreshResults;
        vm.clear = clear;
        vm.deleteClient = deleteClient;
        vm.getClientDetails = getClientDetails;
        vm.getExistingClients = getExistingClients;
        vm.fetchAndStoreIndustryList = fetchAndStoreIndustryList;
        vm.onIndustrySelected = onIndustrySelected;
        vm.onSubCategorySelected = onSubCategorySelected;
        vm.fetchAndStoreIndustrySubCategoriesList = fetchAndStoreIndustrySubCategoriesList;
        vm.fetchAndStoreSalesRepresentativeList = fetchAndStoreSalesRepresentativeList;
        vm.onsalesRepresentativeSelected = onsalesRepresentativeSelected;
        vm.getCasesAssociatedWithCurrentClient = getCasesAssociatedWithCurrentClient;
        vm.addBusinessUnit = addBusinessUnit;
        vm.removeBusinessUnit = removeBusinessUnit;
        vm.openCaseDetails = openCaseDetails;
        vm.getExistingClientsByKeyword = getExistingClientsByKeyword;
        vm.addClientContactUser = addClientContactUser;
        vm.deleteAddedContactUser = deleteAddedContactUser;
        vm.editClientForm = editClientForm;
        vm.addClientForm = addClientForm;
        vm.clientFormInit = clientFormInit;
        vm.openAddClientContactUserModal = openAddClientContactUserModal;
        vm.closeModal = closeModal;
        vm.saveNewClient = saveNewClient;
        vm.saveEditedClient = saveEditedClient;
        vm.addContactToList = addContactToList;
        vm.editClientContactUser = editClientContactUser;
        vm.deleteExistingClient = deleteExistingClient;
        vm.searchClients = searchClients;
        vm.getClientsByKeyword = getClientsByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.getClientByClientId = getClientByClientId;
        vm.getCasesByClientId = getCasesByClientId;
        vm.getExistingCountries = $global.getExistingCountries;
        vm.getExistingStatesByCountryId = $global.getExistingStatesByCountryId;
        vm.getExistingIndustries = $global.getExistingIndustries;
        vm.getExistingIndustrySubcategories = $global.getExistingIndustrySubcategories;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.onStateSelected = onStateSelected;
        vm.cancelClientContactUserChanges = cancelClientContactUserChanges;
        return vm;
    }

    ]);
})();