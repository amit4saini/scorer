'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise('not-found');

    // Home state routing
      $stateProvider
          .state('dashboard', {
              url: '/case-management/dashboard',
              templateUrl: '/webapp/modules/dashboard/views/dashboard.html' 
          });
  }
]);
