'use strict';

// Setting up route
angular.module('calendar').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
        .state('calendar', {
              url: '/case-management/calendar',
              templateUrl: '/webapp/modules/calendar/views/mwl-calendar.view.html'
          });
  }
]);
