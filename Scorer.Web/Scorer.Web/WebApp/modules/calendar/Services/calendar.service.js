﻿(function () {
    'use strict';
    angular.module('calendar').factory('Calendar', ['CaseTasks', 'CaseHeader', '$uibModal', '$http', 'toastr', 'ErrorHandlerService', 'Enums', 'calendarConfig', '$global', '$stateParams', '$location',
    function (CaseTasks, CaseHeader, $uibModal, $http, toastr, ErrorHandlerService, Enums, calendarConfig, $global, $stateParams, $location) {
        var vm = this;
        
        function caseCalendarInit()
        {
            setViewAndTemplates();
            vm.caseNumber = $stateParams.caseNumber;
            var options = "caseFilter";
            vm.getCaseFromCaseNumber(vm.caseNumber, getCaseAssociatedTasks);
        }

        function userCalendarInit()
        {
            if ($location.path().indexOf("/case-management/calendar") > -1)
                $global.isDashboardView = false;
            else
                $global.isDashboardView = true;
            vm.isDashboardView = $global.isDashboardView;
            vm.isSearchButtonPressed = true;
            setViewAndTemplates();
            getAllCases();
            if (vm.roleName == 'Admin' || vm.roleName == 'Supervisor')
            {
                getAllUsers();
            }
            if (vm.roleName != 'Admin' && vm.roleName != 'Supervisor')
            {
                getAssignedTasks();
            }
        }

        function setViewAndTemplates()
        {            
            vm.currentUser = $global.user;
            vm.calendarView = "month";
            vm.viewDate = new Date();
            vm.events = [];                
            calendarConfig.allDateFormats.angular.date.weekDay = 'EEE';
            calendarConfig.templates.calendarMonthCell = 'webapp/modules/calendar/templates/customMonthCell.html';
            calendarConfig.templates.calendarYearView = 'webapp/modules/calendar/templates/customYearView.html';
            calendarConfig.templates.calendarWeekView = 'webapp/modules/calendar/templates/customWeekView.html';
            calendarConfig.templates.calendarDayView = 'webapp/modules/calendar/templates/customDayView.html';
        }

        function getCaseAssociatedTasks() {
            vm.case = CaseHeader.case;
            vm.cases = [];
            vm.cases.push(vm.case);
            var options = '?&$orderby=Status,DueDate,CompletionDate';
            vm.getExistingCaseAssociatedTasks(vm.case.caseId, options,
                function (tasks, response, status) {
                    vm.tasks = tasks;
                    getEventsFromTasks();
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.tasks = [];
                });
        }

        function filterCasesByKeyword()
        {
            vm.cases = vm.allCases.slice();
            vm.areAllCasesSelected = false;
            toggleAllCases();
            if (vm.caseSearchQuery == undefined || vm.caseSearchQuery == "") {}
            else
            {
                for (var index = 0; index < vm.cases.length; index++)
                {
                    if(vm.cases[index].nickName.toLowerCase().search(vm.caseSearchQuery.toLowerCase()) < 0 && vm.cases[index].caseNumber.toLowerCase().search(vm.caseSearchQuery.toLowerCase()) < 0)
                    {
                        vm.cases.splice(index, 1);
                        index--;
                    }
                }
            }
        }

        function filterUsersByKeyword()
        {
            vm.users = vm.allUsers.slice();
            vm.areAllUsersSelected = false;
            toggleAllUsers();
            if (vm.userSearchQuery == undefined || vm.userSearchQuery == "") {}
            else
            {
                for (var index = 0; index < vm.users.length; index++)
                {
                    if (vm.users[index].firstName.toLowerCase().search(vm.userSearchQuery.toLowerCase()) < 0 && vm.users[index].lastName.toLowerCase().search(vm.userSearchQuery.toLowerCase()) < 0)
                    {
                        vm.users.splice(index, 1);
                        index--;
                    }
                }
            }
        }

        function showMyTasks()
        {
            vm.areAllCasesSelected = false;
            vm.areAllUsersSelected = false;
            toggleAllCases();
            toggleAllUsers();
            getAssignedTasks();
        }

        function matchDates(viewDateTime, eventDateTime)
        {

            var viewDate = new Date(viewDateTime).getDate();
            var eventDate = new Date(eventDateTime).getDate();
            var viewMonth = new Date(viewDateTime).getMonth();
            var eventMonth = new Date(eventDateTime).getMonth();
            if (viewDate == eventDate && viewMonth == eventMonth)
                return true;
            else
                false;
        }

        function groupEvents(cell)
        {           
            cell.groups = {};
            cell.events.forEach(function (event) {
                cell.groups[event.type] = cell.groups[event.type] || [];
                cell.groups[event.type].push(event);
            });
        };

        function getAllCases()
        {
            var options = "?$skip=0&$filter=IsDeleted eq false";
            vm.getExistingCases(options,
                function (response, status) {
                vm.allCases = response.items;
                for(var eachCase of vm.allCases)
                {
                    eachCase.isSelected = true;
                }
                vm.numberOfSelectedCases = vm.allCases.length;
                vm.cases = vm.allCases.slice();
                vm.areAllCasesSelected = true;               
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allCases = [];
                vm.cases = [];
                vm.areAllCasesSelected = true;
            });
        }

        function getAllUsers()
        {
            var options = "?$skip=0";
            vm.getExistingUsers(options,
                function (response, status) {
                    vm.allUsers = response.items;
                    for(var eachUser of vm.allUsers)
                    {
                        eachUser.isSelected = true;
                    }
                    vm.numberOfSelectedUsers = vm.allUsers.length;
                    vm.users = vm.allUsers.slice();
                    vm.areAllUsersSelected = true;
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.allUsers = [];
                    vm.users = [];
                    vm.areAllUsersSelected = true;
                });
        }

        function toggleCase(eachCase)
        {
            if(eachCase.isSelected == true)
            {
                vm.numberOfSelectedCases++;
                if(vm.numberOfSelectedCases == vm.cases.length)
                {
                    vm.areAllCasesSelected = true;
                }
            }
            else
            {
                vm.numberOfSelectedCases--;
                vm.areAllCasesSelected = false;
            }
        }

        function toggleAllCases()
        {           
                if (vm.areAllCasesSelected == true) {
                    for(var eachCase of vm.cases)
                    {
                        eachCase.isSelected = true;
                    }
                    vm.numberOfSelectedCases = vm.cases.length;
                }
                else {
                    for(var eachCase of vm.cases)
                    {
                        eachCase.isSelected = false;
                    }
                    vm.numberOfSelectedCases = 0;
                }              
        }

        function toggleUser(eachUser)
        {
            if(eachUser.isSelected == true)
            {
                vm.numberOfSelectedUsers++;
                if(vm.numberOfSelectedUsers == vm.users.length)
                {
                    vm.areAllUsersSelected = true;
                }
            }
            else
            {
                vm.numberOfSelectedUsers--;
                vm.areAllUsersSelected = false;
            }
        }

        function toggleAllUsers()
        {           
                if (vm.areAllUsersSelected == true) {
                    for(var user of vm.users)
                    {
                        user.isSelected = true;
                    }
                    vm.numberOfSelectedUsers = vm.users.length;
                }
                else {
                    for(var user of vm.users)
                    {
                        user.isSelected = false;
                    }
                    vm.numberOfSelectedUsers = 0;
                }                       
        }

        function showTasksForSelectedDateAndType(selectedDate, taskType)
        {           
            vm.isDateModalSelected = true;
            if (selectedDate._d != undefined)
                vm.selectedDate = new Date(selectedDate._d);
            else
                vm.selectedDate = new Date(selectedDate);
            vm.selectedTasks = [];
            for(var eachTask of vm.tasks)
            {
                var eachTaskDueDate = eachTask.dueDate;
                if (eachTaskDueDate.getFullYear() == vm.selectedDate.getFullYear() && eachTaskDueDate.getMonth() == vm.selectedDate.getMonth() && eachTaskDueDate.getDate() == vm.selectedDate.getDate()) {
                    vm.selectedTasks.push(eachTask);
                }
            }
            openTasksModal(taskType);
        }

        function showTasksForSelectedMonthAndType(selectedDate, taskType)
        {            
            vm.isDateModalSelected = false;
            vm.selectedDate = new Date(selectedDate._d);
            vm.selectedTasks = [];
            for(var eachTask of vm.tasks)
            {
                var eachTaskDueDate = eachTask.dueDate;
                if (eachTaskDueDate.getFullYear() == vm.selectedDate.getFullYear() && eachTaskDueDate.getMonth() == vm.selectedDate.getMonth()) {
                    vm.selectedTasks.push(eachTask);
                }
            }
            openTasksModal(taskType);
        }

        function openTasksModal(taskType)
        {
            switch(taskType)
            {
                case 'inverse': vm.selectedTaskStatus = 0; break;
                case 'warning': vm.selectedTaskStatus = 1; break;
                case 'success': vm.selectedTaskStatus = 2; break;
            }
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/calendar/views/tasks-list-modal.view.html',
                windowClass: 'task_modal',
                size: 'lg'
            });
        }

        function closeModal()
        {
            vm.modalInstance.dismiss('cancel');
        }

        function getTasksByCaseIdAndUserId()
        {
            var selectedCaseCount = 0;
            var selectedUserCount = 0;
            vm.casesAndUsersIdList = {};
            vm.casesAndUsersIdList.casesIdList = [];
            vm.casesAndUsersIdList.usersIdList = [];
            vm.isAssignedTasksSearchActive = false;
            for(var eachCase of vm.cases)
            {
                if (eachCase.isSelected) {
                    vm.casesAndUsersIdList.casesIdList.push(eachCase.caseId);
                    selectedCaseCount++;
                }
            }
            for(var eachUser of vm.users)
            {
                if (eachUser.isSelected) {
                    vm.casesAndUsersIdList.usersIdList.push(eachUser.id);
                    selectedUserCount++;
                }
            }

            if (selectedCaseCount == 0 && selectedUserCount == 0) {
                toastr.error("Please select a case or user");
                return;
            }
            getExistingTasksByCaseIdAndUserId(vm.casesAndUsersIdList,
                function (response, status) {
                    vm.tasks = response;
                    for (var index = 0; index < vm.tasks.length; index++) {
                        vm.tasks[index].dueDate = new Date(vm.tasks[index].dueDate);
                        if (vm.tasks[index].completionDate != null)
                            vm.tasks[index].completionDate = new Date(vm.tasks[index].completionDate);
                    }
                    getEventsFromTasks();
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.tasks = [];
                });
        }

        function getExistingTasksByCaseIdAndUserId(casesAndUsersIdList, cbSuccess, cbError)
        {
            $http.post("/api/task/getTasksForCasesAndUsers", casesAndUsersIdList)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function getAssignedTasks() {            
            vm.isAssignedTasksSearchActive = true;
            var options = "?$skip=0&$filter=IsDeleted eq false&$orderby=Status,DueDate,CompletionDate";
            vm.getExistingUserAssignedTasks(vm.currentUser.userId, options,
                function (tasks, response, status) {
                    vm.tasks = tasks;
                    getEventsFromTasks();
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.tasks = [];
                });
        }

        function getRecentTasks() {
            getRecentTasksForCurrentUser(vm.currentUser.userId, function (tasks, response, status) {                
                vm.tasks = tasks;
                getEventsFromTasks();
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.tasks = [];
            });
        }

        function getRecentTasksForCurrentUser(userId, cbSuccess, cbError) {
            $http.get("api/Task/" + userId + "/RecentTasks")
                    .success(function (response, status) {
                        var tasks = vm.processTasks(response, false);
                        cbSuccess(tasks, response, status);
                    }).error(function (response, status) { cbError(response, status); });
        }

        function getEventsFromTasks()
        {
            vm.events = [];
            for(var index = 0; index < vm.tasks.length; index++)
            {
                var event = {};
                event.title = vm.tasks[index].title;
                switch(vm.tasks[index].status)
                {
                    case Enums.TaskStatus.NotStarted: event.type = 'inverse'; break;
                    case Enums.TaskStatus.InProgress: event.type = 'warning'; break;
                    case Enums.TaskStatus.Completed: event.type = 'success'; break;
                    default: event.type = 'special';
                }
                event.startsAt = vm.tasks[index].dueDate;
                event.allDay = true;
                vm.events.push(event);                
            }           
        }

        function goToCaseDetails(selectedCase) {
            closeModal();
            $location.path('/case-management/cases/' + selectedCase.caseNumber + '/caseDetails');
        }

        vm.caseCalendarInit = caseCalendarInit;
        vm.userCalendarInit = userCalendarInit;
        vm.setViewAndTemplates = setViewAndTemplates;

        vm.getTasksByCaseIdAndUserId = getTasksByCaseIdAndUserId;
        vm.getAssignedTasks = getAssignedTasks;
        vm.getEventsFromTasks = getEventsFromTasks;

        vm.getAllCases = getAllCases;
        vm.getAllUsers = getAllUsers;
        vm.toggleCase = toggleCase;
        vm.toggleUser = toggleUser;
        vm.toggleAllCases = toggleAllCases;
        vm.toggleAllUsers = toggleAllUsers;

        vm.openTasksModal = openTasksModal;
        vm.closeModal = closeModal;
        vm.groupEvents = groupEvents;
        vm.showMyTasks = showMyTasks;
        vm.showTasksForSelectedDateAndType = showTasksForSelectedDateAndType;
        vm.showTasksForSelectedMonthAndType = showTasksForSelectedMonthAndType;
        vm.matchDates = matchDates;
        vm.filterCasesByKeyword = filterCasesByKeyword;
        vm.filterUsersByKeyword = filterUsersByKeyword;
        vm.getCaseAssociatedTasks = getCaseAssociatedTasks;
        vm.getExistingTasksByCaseIdAndUserId = getExistingTasksByCaseIdAndUserId;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.getExistingCaseAssociatedTasks = $global.getExistingCaseAssociatedTasks;
        vm.getExistingUserAssignedTasks = $global.getExistingUserAssignedTasks;
        vm.getExistingCases = $global.getExistingCases;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.goToTaskComments = CaseTasks.goToTaskComments;
        vm.goToCaseDetails = goToCaseDetails;
        vm.getRecentTasks = getRecentTasks;
        vm.roleName = $global.roleName;
        vm.processTasks = $global.processTasks;       
        return vm;
    }
    ]);
})();
