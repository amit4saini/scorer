﻿'use strict';

// Setting up route
angular.module('analytics').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
        .state('analytics', {
            url: '/case-management/analytics',
            templateUrl: '/webapp/modules/analytics/views/analytics.view.html'
        });
  }
]);
