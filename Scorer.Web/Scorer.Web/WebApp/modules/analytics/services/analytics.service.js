﻿(function () {
    'use strict';
    angular.module('analytics').factory('Analytics', ['$uibModal', '$http', 'toastr', 'ErrorHandlerService', 'Enums', '$global', '$stateParams', '$location', '$window',
    function ($uibModal, $http, toastr, ErrorHandlerService, Enums, $global, $stateParams, $location, $window) {
        var vm = this;
        
        function init()
        {
            getCaseAnalytics(function (caseAnalytics) {
                if (!caseAnalytics)  //for users who can't view analytics
                    return;
                fetchCaseAnalytics(caseAnalytics);
                fetchTaskAnalytics(caseAnalytics);
                fetchExpenseAnalytics(caseAnalytics);
                fetchTimesheetAnalytics(caseAnalytics);
            });
            vm.selectedAnalytics = vm.analyticsTypeEnum['Case'];
            //getAllClients(function (clientIdList)
            //{ getClientAnalytics(clientIdList, function (clientAnalytics) { convertClientAnalytics(clientAnalytics); }); });
            //getUserAnalytics(vm.currentUser.userId, function (userAnalytics) { convertUserAnalytics(userAnalytics); });
        }

        function fetchTaskAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.taskAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.tasks = caseItem.tasks.map(function (taskItem) {
                    //create pascalcase keys of task object to display in pivot-table
                    taskItem.Id = taskItem.taskId;
                    taskItem.CaseNumber = caseItem.caseNumber;
                    taskItem.CaseNickname = caseItem.nickName;
                    taskItem.Client = caseItem.client.clientName;
                    taskItem.Country = caseItem.primaryReportCountryName || "NA";
                    taskItem.Title = taskItem.title;
                    taskItem.Status = Object.keys(vm.taskStatusEnum)[taskItem.status];
                    taskItem.Priority = Object.keys(vm.taskPriorityEnum)[taskItem.priority];
                    taskItem.DueDate = new Date(taskItem.dueDate).toLocaleDateString();
                    taskItem.CompletionDate = (taskItem.completionDate ? new Date(taskItem.completionDate).toLocaleDateString() : "NA");
                    taskItem.AssignedTo = taskItem.assignedToUserDetails.firstName + " " + taskItem.assignedToUserDetails.lastName;
                    taskItem.CreatedBy = taskItem.createdByUserDetails.firstName + " " + taskItem.createdByUserDetails.lastName;
                    taskItem.CreatedDate = new Date(taskItem.createdOn).toLocaleDateString();
                    taskItem.OriginalDueDate = new Date(taskItem.originalDueDate).toLocaleDateString();
                    //delete all camelcase keys of task object
                    var keysList = Object.keys(taskItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete taskItem[keysList[key]];
                    return taskItem;
                });
                vm.taskAnalytics = vm.taskAnalytics.concat(caseItem.tasks);
            });
            vm.taskRows = ["Id", "Title", "CreatedBy", "CreatedDate"];
            vm.taskCols = ["CaseNumber"];
            displayAnalytics(vm.taskAnalytics, vm.taskRows, vm.taskCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Task"]);
        }

        function fetchExpenseAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.expenseAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.expenses = caseItem.expenses.map(function (expenseItem) {
                    //create pascalcase keys of expense object to display in pivot-table
                    expenseItem.Id = expenseItem.id;
                    expenseItem.CaseNumber = caseItem.caseNumber;
                    expenseItem.CaseNickname = caseItem.nickName;
                    expenseItem.Client = caseItem.client.clientName;
                    expenseItem.Country = caseItem.primaryReportCountryName || "NA";
                    expenseItem.Quantity = Math.trunc(expenseItem.quantity * 100) / 100;
                    expenseItem.Cost = Math.trunc(expenseItem.cost * 100) / 100;               
                    expenseItem.CreatedDate = new Date(expenseItem.creationDateTime).toLocaleDateString();
                    expenseItem.CreatedBy = expenseItem.createdByUser.firstName + " " + expenseItem.createdByUser.lastName;
                    expenseItem.VendorName = expenseItem.vendor ? expenseItem.vendor.vendorName : "NA";
                    expenseItem.OriginalCurrencyCost = expenseItem.originalCurrencyCost || 0;
                    //delete all camelcase keys of expense object
                    var keysList = Object.keys(expenseItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete expenseItem[keysList[key]];
                    return expenseItem;
                })
                vm.expenseAnalytics = vm.expenseAnalytics.concat(caseItem.expenses);
            });
            vm.expenseRows = ["Id", "Quantity", "Cost", "CreatedBy", "CreatedDate"];
            vm.expenseCols = ["CaseNumber"];
            displayAnalytics(vm.expenseAnalytics, vm.expenseRows, vm.expenseCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Expense"]);
        }

        function fetchTimesheetAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.timesheetAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.timesheets = caseItem.timesheets.map(function (timesheetItem) {
                    //create pascalcase keys of timesheet object to display in pivot-table
                    timesheetItem.Id = timesheetItem.id;
                    timesheetItem.CaseNumber = caseItem.caseNumber;
                    timesheetItem.CaseNickname = caseItem.nickName;
                    timesheetItem.Client = caseItem.client.clientName;
                    timesheetItem.Country = caseItem.primaryReportCountryName || "NA";
                    timesheetItem.Description = timesheetItem.description.description;
                    timesheetItem.endTime = (timesheetItem.endTime ? new Date(timesheetItem.endTime).getHours() + new Date(timesheetItem.endTime).getMinutes() / 60.00 : null);
                    timesheetItem.startTime = new Date(timesheetItem.startTime).getHours() + new Date(timesheetItem.startTime).getMinutes() / 60.00;
                    timesheetItem.Hours = Math.trunc((timesheetItem.endTime ? timesheetItem.endTime - timesheetItem.startTime : 0) * 100) / 100;
                    timesheetItem.Cost = Math.trunc(timesheetItem.cost * 100) / 100;
                    timesheetItem.CreatedDate = new Date(timesheetItem.creationDateTime).toLocaleDateString();
                    timesheetItem.CreatedBy = timesheetItem.createdByUser.firstName + " " + timesheetItem.createdByUser.lastName;
                    timesheetItem.TimesheetUser = timesheetItem.user.firstName + " " + timesheetItem.user.lastName;
                    //delete all camelcase keys of expense object
                    var keysList = Object.keys(timesheetItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete timesheetItem[keysList[key]];
                    return timesheetItem;
                });
                vm.timesheetAnalytics = vm.timesheetAnalytics.concat(caseItem.timesheets);
            });
            vm.timesheetRows = ["Id", "Hours", "Cost", "CreatedBy", "CreatedDate"];
            vm.timesheetCols = ["CaseNumber"];
            displayAnalytics(vm.timesheetAnalytics, vm.timesheetRows, vm.timesheetCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Timesheet"]);
        }

        function stringify(listName, list)
        {
            var stringResult = "";
            for(var index in list)
            {
                stringResult += ((index == 0) ? "?" : "&") + listName + "=" + list[index];
            }
            return stringResult;
        }

        function getAllClients(cbSuccess)
        {
            var options = '?$skip=0&$filter=IsDeleted eq false';
            getExistingClients(options, function (response, status) {
                vm.clientIdList = response.items.map(function (item) { return item.clientId; });
                cbSuccess(vm.clientIdList);
            },
            function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getExistingClients(options, cbSuccess, cbError)
        {
            $http.get("api/clients" + options)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getCaseAnalytics(cbSuccess)
        {
            getCaseAnalyticsForSelectedCases(function (response, status) {
                cbSuccess(response.items);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getCaseAnalyticsForSelectedCases(cbSuccess, cbError)
        {
            $http.get("api/Analytics/Cases")
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getClientAnalytics(clientIdList, cbSuccess)
        {
            getClientAnalyticsForSelectedClients(clientIdList, function (response, status) { cbSuccess(response.items); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getClientAnalyticsForSelectedClients(clientIdList, cbSuccess, cbError)
        {
            $http.get("api/Analytics/Clients" + stringify("clientIdList", clientIdList))
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getUserAnalytics(userId, cbSuccess)
        {
            getUserAnalyticsByUserId(userId, function (response, status) { cbSuccess(response); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getUserAnalyticsByUserId(userId, cbSuccess, cbError)
        {
            $http.get("api/User/" + userId + "/Analytics")
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function fetchCaseAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.caseAnalytics = analytics.map(function (caseItem) {
                //create pascalcase keys of case object to display in pivot-table
                caseItem.Client = caseItem.client.clientName;
                caseItem.Country = caseItem.primaryReportCountryName || "NA";
                caseItem.CaseNumber = caseItem.caseNumber;
                caseItem.Speed = Object.keys(vm.speedEnum)[caseItem.speed];
                caseItem.Nickname = caseItem.nickName;
                caseItem.Margin = Math.trunc(caseItem.margin * 100) / 100;
                caseItem.IsNFC = caseItem.isNFCCase;
                caseItem.CaseStatus = (caseItem.caseStatus ? caseItem.caseStatus.status : "NA");
                caseItem.ActualRetail = Math.trunc((caseItem.actualRetail || 0) * 100) / 100;
                caseItem.CreatedBy = caseItem.createdByUser.firstName + " " + caseItem.createdByUser.lastName;
                caseItem.CreatedDate = new Date(caseItem.creationDateTime).toLocaleDateString();
                caseItem.ClosedBy = (caseItem.closedByUser ? caseItem.closedByUser.firstName + " " + caseItem.closedByUser.lastName : "NA");
                caseItem.ClosedDate =(caseItem.closingDateTime ? new Date(caseItem.closingDateTime).toLocaleDateString(): "NA");
                caseItem.CaseAge = Math.trunc((caseItem.caseAge || 0) * 100) / 100;
                //delete all camelcase keys of case object
                var keysList = Object.keys(caseItem);
                for (var key in keysList)
                    if (keysList[key][0] == keysList[key][0].toLowerCase())
                        delete caseItem[keysList[key]];
                return caseItem;
            });
            vm.caseRows = ["CaseNumber", "Nickname", "CreatedBy", "CreatedDate"];
            vm.caseCols = ["Client"];
            displayAnalytics(vm.caseAnalytics, vm.caseRows, vm.caseCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Case"]);
        }

        function convertClientAnalytics(clientAnalytics)
        {
            vm.clientAnalytics = clientAnalytics.map(function (item) {
                item.ClientName = item.clientName;
                item.Country = item.country != null ? item.country.countryName : "NA";
                item.ClientContactUser = item.case.clientContactUser != null ? item.case.clientContactUser.contactName : "NA";
                item.AssignedToUser = item.case.assignedToUser != null ? item.case.assignedToUser.firstName + " " + item.case.assignedToUser.lastName : "NA";
                item.CaseNumber = item.case.caseNumber;
                item.CaseNickName = item.case.nickName;
                item.Margin = item.margin;
                delete item['clientName'];
                delete item['margin'];
                delete item['country'];
                delete item['case'];
                return item;
            });
            displayClientAnalytics(vm.clientAnalytics);
        }

        function convertUserAnalytics(userAnalytics)
        {
            var item = userAnalytics;
            vm.userAnalytics = [];
            var userAnalyticsModel = {
                UserName : item.userName,
                CasesCreated : item.totalCasesCreatedByAUser,
                DelayedTasks : item.listOfTaskDelayed.length,
                Timesheets : item.listOfTimesheet.length,
                Expenses : item.listOfCaseExpense.length,
            }
            vm.userAnalytics.push(userAnalyticsModel);
            displayUserAnalytics(vm.userAnalytics);
        }

        vm.speedEnum = Enums.Speed;
        vm.taskStatusEnum = Enums.TaskStatus;
        vm.taskPriorityEnum = Enums.TaskPriority;
        vm.analyticsTypeEnum = Enums.AnalyticsType;
        vm.currentUser = $global.user;
        vm.init = init;
        vm.getCaseAnalytics = getCaseAnalytics;
        vm.getClientAnalytics = getClientAnalytics;
        vm.getUserAnalytics = getUserAnalytics;
        vm.getCaseAnalyticsForSelectedCases = getCaseAnalyticsForSelectedCases;
        vm.getClientAnalyticsForSelectedClients = getClientAnalyticsForSelectedClients;
        vm.getUserAnalyticsByUserId = getUserAnalyticsByUserId;
        vm.getAllClients = getAllClients;
        vm.getExistingCases = $global.getExistingCases;
        vm.fetchCaseAnalytics = fetchCaseAnalytics;
        vm.fetchTaskAnalytics = fetchTaskAnalytics;
        vm.fetchExpenseAnalytics = fetchExpenseAnalytics;
        vm.fetchTimesheetAnalytics = fetchTimesheetAnalytics;
        vm.convertClientAnalytics = convertClientAnalytics;
        vm.convertUserAnalytics = convertUserAnalytics;
        vm.displayAnalytics = displayAnalytics;
        return vm;
    }
    ]);
})();
