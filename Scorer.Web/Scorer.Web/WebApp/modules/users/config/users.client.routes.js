'use strict';

// Setting up route
angular.module('users').config(['$stateProvider','$urlRouterProvider',
  function ($stateProvider,$urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

    // Users state routing
    $stateProvider
      .state('settings', {
        //abstract: true,
        url: '/case-management/settings',
        templateUrl: 'Webapp/modules/users/views/settings/settings.client.view.html',
        //data: {
        //  roles: ['user', 'admin']
        //}
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'Webapp/modules/users/views/settings/edit-profile.client.view.html'
      })
      .state('users', {
          url: '/case-management/users',
        templateUrl: 'Webapp/modules/users/views/user-client.view.html'
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'Webapp/modules/users/views/settings/change-password.client.view.html'
      })
      .state('settings.accounts', {
        url: '/accounts',
        templateUrl: 'Webapp/modules/users/views/settings/manage-social-accounts.client.view.html'
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'Webapp/modules/users/views/settings/change-profile-picture.client.view.html'
      })
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'Webapp/modules/users/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'Webapp/modules/users/views/authentication/signup.client.view.html'
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'Webapp/modules/users/views/authentication/signin.client.view.html'
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'Webapp/modules/users/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'Webapp/modules/users/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'Webapp/modules/users/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'Webapp/modules/users/views/password/reset-password.client.view.html'
      });
  }
]);
