(function () {
    'use strict';
    // Users service used for communicating with the users REST endpoint
    angular.module('users').factory('Users', ['$http', 'usSpinnerService', 'ErrorHandlerService', '$uibModal', 'toastr','$window', '$global',
      function ($http, usSpinnerService, ErrorHandlerService, $uibModal, toastr, $window, $global) {
          var vm = this;
          
          function init() {
              vm.defaultPageSize = $window.defaultPageSize;
              vm.showUserValidationMessage = false;
              vm.allPages = [];
              vm.isUserBeingEdited = false;
              vm.isUserSearchActive = false;
              fetchAndStoreUsersList(0);
              fetchAndStoreRolesList();
          }

          function fetchAndStoreUsersList(pageIndex) {
              vm.pageIndex = pageIndex;
              var skipCount = vm.pageIndex * vm.defaultPageSize;
              var options = "?$skip=" + skipCount + "&$top=" + vm.defaultPageSize + "&$filter=Role/Name ne 'NFCUser' &$orderby=Role/Name,UserName ";
              vm.getExistingUsers(options, function (response, status) {
                  vm.users = response.items;
                  vm.totalNumberOfUsers = response.count;
                  vm.allPages.length = Math.ceil(vm.totalNumberOfUsers / vm.defaultPageSize);
              }, function (response, status) {
                  ErrorHandlerService.DisplayErrorMessage(response, status);
                  vm.users = [];
                  vm.totalNumberOfUsers = 0;
              });
          }

          function editUser(userId) {
              getUserDetails(userId);
              openEditUserModal();
          }

          function openEditUserModal() {
              vm.isUserBeingEdited = true;
              vm.modalInstance = $uibModal.open({
                  templateUrl: '/webapp/modules/users/views/add-user.client.view.html',
                  size: 'lg'
              });
              fetchAndStoreRolesList();
              vm.modalInstance.result.then(function () {
              }, function () {
                  vm.isUserBeingEdited = false;
              });
          }

          function fetchAndStoreRolesList() {
              vm.getExistingRoles("", function (response, status) { vm.roles = response.items; },
                  function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); vm.roles = []; });
          }

          function onRoleSelected($item) {
              vm.user.role = $item;
          }

          function clear($event, $select) {
              $event.stopPropagation();
              $select.selected = undefined;
              $select.search = undefined;
              $select.activate();
          }

          function getUserDetails(userId)
          {
              getUserByUserId(userId, function (response, status) { vm.user = response.data; },
                  function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); vm.user = {}; });
          }

          function getUserByUserId(userId, cbSuccess, cbError) {
              $http.get('api/user/' + userId)
              .success(function (response, status) {
                  cbSuccess(response, status);
              }).error(function (response, status) {
                  cbError(response, status);
              });
          }

          function closeModal() {
              vm.modalInstance.dismiss('cancel');
          }

          function deleteUser(userId)
          {
              deleteExistingUser(userId, function (response, status) {
                  toastr.success("User removed");
                  vm.users.splice(userId, 1);
                  if (vm.users.length < 1)
                      vm.isUserSearchActive ? getUsersByKeyword(vm.pageIndex - 1) : fetchAndStoreUsersList(vm.pageIndex - 1);
                  else
                      vm.isUserSearchActive ? getUsersByKeyword(vm.pageIndex) : fetchAndStoreUsersList(vm.pageIndex);
              }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); });
          }

          function deleteExistingUser(userId, cbSuccess, cbError) {
              $http.delete('/api/user/' + userId)
                 .success(function (response, status) {
                     cbSuccess(response, status);
                 }).error(function (response, status) {
                     cbError(response, status);
                 });
          }

          function openAddUserModal() {
              vm.user = {}
              vm.showUserValidationMessage = false;
              vm.modalInstance = $uibModal.open({
                  templateUrl: '/webapp/modules/users/views/add-user.client.view.html',
                  size: 'lg'
              });
          }

          function saveUser(isValid) {
              vm.showUserValidationMessage = true;
              if (!isValid) { return; }
              if (!vm.isUserBeingEdited) {
                  vm.invitation = {
                      user: vm.user,
                      iciCaseUser: {},
                      participantUser: {},
                      nfcCaseUser: {}
                  };
                  vm.inviteNewUser(vm.invitation, function (response, status) {
                      toastr.success("Invitation sent");
                      vm.isUserSearchActive ? getUsersByKeyword(vm.pageIndex) : fetchAndStoreUsersList(vm.pageIndex);
                      vm.closeModal();
                  }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
              }
              else {
                  vm.saveEditedUser(vm.user, function (response, status) {
                      toastr.success("User edited");
                      vm.isUserSearchActive ? getUsersByKeyword(vm.pageIndex) : fetchAndStoreUsersList(vm.pageIndex);
                      vm.closeModal();
                  }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); })
              }
          }
          
          function searchUsers(searchQuery) {
              if (searchQuery == "" || searchQuery == undefined) {
                  return;
              }
              vm.isUserSearchActive = true;
              vm.searchQuery = searchQuery
              getUsersByKeyword(0);
          }

          function getUsersByKeyword(pageIndex)
          {
              vm.pageIndex = pageIndex;
              var skipCount = vm.pageIndex * vm.defaultPageSize;
              var options = "&$skip=" + skipCount + "&$top=" + vm.defaultPageSize + "&$filter=Role/Name ne 'Participant' and Role/Name ne 'ICIUser'";
              getExistingUsersByKeyword(vm.searchQuery, options, function (response, status) {
                  vm.users = response.items;
                  vm.totalNumberOfUsers = response.count;
                  vm.allPages.length = Math.ceil(vm.totalNumberOfUsers / vm.defaultPageSize);
              }, function (response, status) {
                  ErrorHandlerService.DisplayErrorMessage(response, status);
                  vm.users = [];
                  vm.totalNumberOfUsers = 0;
              });
          }

          function getExistingUsersByKeyword(keyword, options, cbSuccess, cbError) {
              $http.get("api/search/users?keyword=" + keyword + options)
                  .success(function (response, status) {
                      cbSuccess(response, status);
                  })
                  .error(function (response, status) {
                      cbError(response, status);
                  });
          }

          function resetSearchResult() {
              vm.searchQuery = "";
              vm.isUserSearchActive = false;
              fetchAndStoreUsersList(0);
          }

          vm.init = init;
          vm.clear = clear;
          vm.editUser = editUser;
          vm.fetchAndStoreUsersList = fetchAndStoreUsersList;
          vm.fetchAndStoreRolesList = fetchAndStoreRolesList;
          vm.openEditUserModal = openEditUserModal;
          vm.closeModal = closeModal;
          vm.onRoleSelected = onRoleSelected;
          vm.deleteUser = deleteUser;
          vm.openAddUserModal = openAddUserModal;
          vm.saveUser = saveUser;
          vm.getUserDetails = getUserDetails;
          vm.searchUsers = searchUsers;
          vm.resetSearchResult = resetSearchResult;
          vm.getUsersByKeyword = getUsersByKeyword;
          vm.getUserByUserId = getUserByUserId;
          vm.deleteExistingUser = deleteExistingUser;
          vm.getExistingUsersByKeyword = getExistingUsersByKeyword;
          vm.getExistingUsers = $global.getExistingUsers;
          vm.getExistingRoles = $global.getExistingRoles;
          vm.inviteNewUser = $global.inviteNewUser;
          vm.saveEditedUser = $global.saveEditedUser;
          return vm;
      }
    ]);
})();
