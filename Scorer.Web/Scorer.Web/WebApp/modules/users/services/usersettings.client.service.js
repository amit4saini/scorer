﻿(function () {
    'use strict';
    // Users service used for communicating with the users REST endpoint
    angular.module('users').factory('UserSettings', ['Header', '$http', 'usSpinnerService', 'ErrorHandlerService', '$uibModal', 'toastr', '$window', 'Enums', '$global',
      function (Header, $http, usSpinnerService, ErrorHandlerService, $uibModal, toastr, $window, Enums, $global) {
          var vm = this;
          
          function init() {
              vm.user = $global.user;
              vm.user.id = $global.user.userId;
              vm.isPictureUploaded = false;
              vm.actualPictureUrl = vm.user.profilePicUrl;
          }

          function getUploadedPictureUrl()
          {
              vm.downloadExistingFile(vm.uploadedPicId, Enums.DownloadFileType.ProfilePic,
                  function (response, status) { vm.user.profilePicUrl = response; },
                  function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
          }

          function saveUserDetails()
          {
              if (vm.isPictureUploaded == false) { toastr.error("Please upload a picture first"); return;}
              vm.user.profilePicKey = vm.uploadedPicId;
              vm.saveEditedUser(vm.user, function (response, status) {
                  Header.userProfilePicUrl = vm.user.profilePicUrl;
                  toastr.success("Profile picture saved");
              }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
          }

          function uploadProfilePicture(files)
          {
              if (files && files.length) {
                  for (var i = 0; i < files.length; i++) {
                      uploadProfilePictureToBlobStorage({ data: { file: files[i] } });
                  }
              }
          }

          function uploadProfilePictureToBlobStorage(file) {
              usSpinnerService.spin('spinner');
              vm.uploadNewFile(file, function (resp) {
                  vm.isPictureUploaded = true;
                  vm.uploadedPicId = resp.data[0].fileId;
                  usSpinnerService.stop('spinner');
                  toastr.success("Profile picture uploaded");
                  getUploadedPictureUrl();
              }, function (resp) {
                  usSpinnerService.stop('spinner');
                  ErrorHandlerService.DisplayErrorMessage(resp.data);
              });
          }

          function cancelChanges()
          {
              vm.user.profilePicUrl = vm.actualPictureUrl;
              vm.isPictureUploaded = false;
          }

          vm.init = init;
          vm.saveUserDetails = saveUserDetails;
          vm.uploadProfilePicture = uploadProfilePicture;
          vm.uploadProfilePictureToBlobStorage = uploadProfilePictureToBlobStorage;
          vm.getUploadedPictureUrl = getUploadedPictureUrl;
          vm.cancelChanges = cancelChanges;
          vm.downloadExistingFile = $global.downloadExistingFile;
          vm.uploadNewFile = $global.uploadNewFile;
          vm.saveEditedUser = $global.saveEditedUser;
        return vm;
      }
    ]);
})();
