'use strict';

// Setting up route
angular.module('case').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
          .state('cases', {
              url: '/case-management/cases',
              templateUrl: '/webapp/modules/cases/views/cases-list.view.html?ver=1234'
          })
          .state('discussions', {
              url: '/case-management/cases/:caseNumber/discussions',
              templateUrl: '/webapp/modules/cases/views/case-discussions-list.view.html'
          })
          .state('discussionMessages', {
              url: '/case-management/cases/:caseNumber/discussions/:discussionId',
              templateUrl: '/webapp/modules/cases/views/discussion-messages-list.view.html'
          })
          .state('caseExpenses', {
              url: '/case-management/cases/:caseNumber/expenses',
              templateUrl: '/webapp/modules/cases/views/case-expenses-list.view.html'
          })
          .state('timeSheet', {
              url: '/case-management/cases/:caseNumber/timesheets',
              templateUrl: '/webapp/modules/cases/views/case-timesheets-list.view.html'
          })
          .state('caseDetails', {
              url: '/case-management/cases/:caseNumber/caseDetails',
              templateUrl: '/webapp/modules/cases/views/case-details.view.html'
          })
          .state('files', {
              url: '/case-management/cases/:caseNumber/files',
              templateUrl: '/webapp/modules/cases/views/case-files-list.view.html'
          })
          .state('reports', {
              url: '/case-management/cases/:caseNumber/reports',
              templateUrl: '/webapp/modules/cases/views/case-reports-list.view.html'
          })
          .state('tasks', {
              url: '/case-management/cases/:caseNumber/tasks',
              templateUrl: '/webapp/modules/cases/views/case-tasks-list.view.html'
          })
          .state('taskComments', {
              url: '/case-management/cases/:caseNumber/task/:taskId',
              templateUrl: '/webapp/modules/cases/views/task-comments.view.html'
          })
          .state('caseHistory', {
              url: '/case-management/cases/:caseNumber/caseHistory',
              templateUrl: '/webapp/modules/cases/views/case-history-list.view.html'
          })
          .state('caseCalendar', {
              url: '/case-management/cases/:caseNumber/calendar',
              templateUrl: '/webapp/modules/cases/views/case-calendar.view.html'
          })
      .state('participants', {
          url: '/case-management/cases/:caseNumber/participants',
          templateUrl: '/webapp/modules/cases/views/case-participants-list.view.html'
      })
      .state('iciUsers', {
          url: '/case-management/cases/:caseNumber/users',
          templateUrl: '/webapp/modules/cases/views/case-users-list.view.html'
      })
      .state('nfcUsers', {
          url: '/case-management/cases/:caseNumber/nfcUsers',
          templateUrl: '/webapp/modules/cases/views/case-nfcUsers-list.view.html'
      });
  }
]);
