﻿(function () {
    'use strict';
    angular.module('case').factory('CaseHistory', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;

        function init() {
            vm.caseNumber = $stateParams.caseNumber;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.historyFiltersEnum = Enums.HistoryFilter;
            vm.getCaseFromCaseNumber(vm.caseNumber, fetchAndStoreCaseHistory, Enums.HistoryFilter.All);
        }

        function fetchAndStoreCaseHistory(historyFilterValue)
        {
            vm.case = CaseHeader.case;
            vm.activeFilterTabValue = historyFilterValue;
            getExistingCaseHistory(vm.case.caseId, historyFilterValue, function (response, status) {
                vm.allCaseHistory = response.items
                .map(function (item) {
                    item.creationDatetime = new Date(item.creationDatetime);
                    return item;
                });
                for (var i = 0 ; i < vm.allCaseHistory.length ; i++) {
                    vm.allCaseHistory[i].createDate = moment.utc(vm.allCaseHistory[i].creationDatetime).format("YYYY-MM-DD");
                }
                getCaseHistoryActionNames();
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getExistingCaseHistory(caseId, filterValue, cbSuccess, cbError) {
            $http.get("api/casehistory/" + caseId + "/case/" + filterValue)
             .success(function (response, status) {
                 cbSuccess(response, status);
             })
             .error(function (response, status) {
                 cbError(response, status);
             });
        }

        function setFilterActiveTabClass(selectedTabValue) {
            if (vm.activeFilterTabValue == selectedTabValue)
                return 'active';                            
        }

        function getHistoryFilterValues(value) {            
            switch (value) {
                case vm.historyFiltersEnum.Task: fetchAndStoreCaseHistory(Enums.HistoryFilter.Task); break;
                case vm.historyFiltersEnum.Expense: fetchAndStoreCaseHistory(Enums.HistoryFilter.Expense); break;
                case vm.historyFiltersEnum.Case_Status: fetchAndStoreCaseHistory(Enums.HistoryFilter.Case_Status); break;
                case vm.historyFiltersEnum.Discussion: fetchAndStoreCaseHistory(Enums.HistoryFilter.Discussion); break;
                case vm.historyFiltersEnum.All: fetchAndStoreCaseHistory(Enums.HistoryFilter.All); break;
                case vm.historyFiltersEnum.File: fetchAndStoreCaseHistory(Enums.HistoryFilter.File); break;
            }
        }

        function getCaseHistoryActionNames() {
            for (var i = 0 ; i < vm.allCaseHistory.length ; i++) {
                switch (vm.allCaseHistory[i].action) {
                    case Enums.Actions.CASE_ADDED: vm.allCaseHistory[i].actionText = "Added a case"; break;
                    case Enums.Actions.CASE_EDITED: vm.allCaseHistory[i].actionText = "Edited a case"; break;
                    case Enums.Actions.CASE_ARCHIVED: vm.allCaseHistory[i].actionText = "Archived a case"; break;
                    case Enums.Actions.CASE_UNARCHIVED: vm.allCaseHistory[i].actionText = "Unarchived a case"; break;
                    case Enums.Actions.CASE_EXPENSE_ADDED: vm.allCaseHistory[i].actionText = "Added a case expense of type"; break;
                    case Enums.Actions.CASE_EXPENSE_EDITED: vm.allCaseHistory[i].actionText = "Edited a case expense of type"; break;
                    case Enums.Actions.CASE_EXPENSE_DELETED: vm.allCaseHistory[i].actionText = "Deleted a case expense of type"; break;
                    case Enums.Actions.CASE_EXPENSE_APPROVED: vm.allCaseHistory[i].actionText = "Case expense approved of type"; break;
                    case Enums.Actions.CASE_TIMESHEET_ADDED: vm.allCaseHistory[i].actionText = "Added a timesheet"; break;
                    case Enums.Actions.CASE_TIMESHEET_EDITED: vm.allCaseHistory[i].actionText = "Edited a timesheet"; break;
                    case Enums.Actions.CASE_TIMESHEET_DELETED: vm.allCaseHistory[i].actionText = "Deleted a timesheet"; break;                   
                    case Enums.Actions.CASE_TASK_EDITED: vm.allCaseHistory[i].actionText = "Edited a task"; break;
                    case Enums.Actions.CASE_TASK_ADDED: vm.allCaseHistory[i].actionText = "Added a task"; break;
                    case Enums.Actions.CASE_TASK_ARCHIVED: vm.allCaseHistory[i].actionText = "Archived a task"; break;
                    case Enums.Actions.CASE_TASK_UNARCHIVED: vm.allCaseHistory[i].actionText = "Unarchived a task"; break;
                    case Enums.Actions.CASE_NOTES_ADDED: vm.allCaseHistory[i].actionText = "Added a note"; break;
                    case Enums.Actions.CASE_NOTES_EDITED: vm.allCaseHistory[i].actionText = "Edited a note"; break;
                    case Enums.Actions.CASE_NOTES_DELETED: vm.allCaseHistory[i].actionText = "Deleted a note"; break;
                    case Enums.Actions.CASE_PRINCIPAL_DELETED: vm.allCaseHistory[i].actionText = "Deleted a case principal"; break;
                    case Enums.Actions.CASE_PRINCIPAL_ADDED: vm.allCaseHistory[i].actionText = "Added a case principal"; break;
                    case Enums.Actions.CASE_ASSIGNED: vm.allCaseHistory[i].actionText = "Case is assigned"; break;
                    case Enums.Actions.CASE_DISCUSSION_ADDED: vm.allCaseHistory[i].actionText = "Added a discussion"; break;
                    case Enums.Actions.CASE_DISCUSSION_EDITED: vm.allCaseHistory[i].actionText = "Edited a discussion"; break;
                    case Enums.Actions.CASE_DISCUSSION_DELETED: vm.allCaseHistory[i].actionText = "Deleted a discussion"; break;
                    case Enums.Actions.CASE_DISCUSSION_ARCHIVED: vm.allCaseHistory[i].actionText = "Archive a discussion"; break;
                    case Enums.Actions.CASE_DISCUSSION_UNARCHIVED: vm.allCaseHistory[i].actionText = "Unarchive a discussion"; break;
                    case Enums.Actions.CASE_DISCUSSION_MESSAGE_ADDED: vm.allCaseHistory[i].actionText = "Added a discussion message"; break;
                    case Enums.Actions.CASE_DISCUSSION_MESSSAGE_EDITED: vm.allCaseHistory[i].actionText = "Edited a discussion message"; break;
                    case Enums.Actions.CASE_DISCUSSION_MESSAGE_DELETED: vm.allCaseHistory[i].actionText = "Deleted a discussion message"; break;
                    case Enums.Actions.CASE_DISCUSSION_MESSAGE_ARCHIVED: vm.allCaseHistory[i].actionText = "Archive a discussion message"; break;
                    case Enums.Actions.CASE_DISCUSSION_MESSAGE_UNARCHIVED: vm.allCaseHistory[i].actionText = "Unarchive a discussion message"; break;
                    case Enums.Actions.CASE_REPORT_ADDED: vm.allCaseHistory[i].actionText = "Report added"; break;
                    case Enums.Actions.CASE_STATUS_CHANGED: vm.allCaseHistory[i].actionText = "Case status changed"; break;
                    case Enums.Actions.CASE_PARTICIPANT_ADDED: vm.allCaseHistory[i].actionText = "Case participant added"; break;
                    case Enums.Actions.CASE_PARTICIPANT_EDITED: vm.allCaseHistory[i].actionText = "Case participant edited"; break;
                    case Enums.Actions.CASE_ICI_USER_ADDED: vm.allCaseHistory[i].actionText = "Case ici user added"; break;
                    case Enums.Actions.CASE_ICI_USER_EDITED: vm.allCaseHistory[i].actionText = "Case ici user edited"; break;
                    case Enums.Actions.CASE_SUBSTATUS_CHANGED: vm.allCaseHistory[i].actionText = "Case substatus changed"; break;
                    case Enums.Actions.CASE_NFC_USER_ADDED: vm.allCaseHistory[i].actionText = "Case nfc user added"; break;
                    case Enums.Actions.CASE_NFC_USER_EDITED: vm.allCaseHistory[i].actionText = "Case nfc user edited"; break;
                }
            }

        }

        vm.init = init;
        vm.fetchAndStoreCaseHistory = fetchAndStoreCaseHistory;
        vm.getCaseHistoryActionNames = getCaseHistoryActionNames;
        vm.getHistoryFilterValues = getHistoryFilterValues;
        vm.setFilterActiveTabClass = setFilterActiveTabClass;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.getExistingCaseHistory = getExistingCaseHistory;
        vm.roleName = $global.roleName;
        return vm;
    }
    ]);
})();