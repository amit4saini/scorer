﻿(function () {
    'use strict';
    angular.module('case').factory('CaseTasks', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;

        vm.tinyMceOptions = {           
            menubar: 'file edit format',
            plugins:[ 'mention link'],
            mentions: {                           
            },
            statusbar: false,
            default_link_target: "_blank"
        };
        
        function init() {
            setCommonVariables();
            vm.caseNumber = $stateParams.caseNumber;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.assignToUser = undefined;
            vm.isTaskBeingEdited = false;
            vm.isCompletedTaskHidden = false;
            vm.isTaskSearchActive = false;
            vm.getCaseFromCaseNumber($stateParams.caseNumber, function (response, status) {               
                vm.currentCaseId = CaseHeader.case.caseId;
                fetchAndStoreTasks(0);
                getCaseRelatedUsers(vm.currentCaseId);               
            }, function () { });                              
        }

        function getCaseRelatedUsers(caseId) {                               
            vm.getExistingCaseUsers(caseId, function (response, status) {
                vm.usersForTagging = response.items
                    .filter(function (item) { return item.role.name != "Participant"; })
                    .map(function (item) { item.name = item.firstName + " " + item.lastName + " " + item.email; return item; });
                vm.tinyMceOptions.mentions.source = vm.usersForTagging;
            }, function () { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }
        
        function setCommonVariables()
        {
            vm.task = {};
            vm.roleName = $global.roleName;
            fetchAndStoreUsersList();
            vm.datePickerOptions = { 'minDate': new Date(), 'showWeeks': false };
            vm.currentUser = $global.user;
        }

        function taskCommentsInit()
        {
            setCommonVariables();
            vm.isFileSearchActive = false;
            vm.currentTaskId = $stateParams.taskId;
            vm.isEditableTask = false;
            vm.taskPriorityEnum = Enums.TaskPriority;
            vm.taskStatusEnum = Enums.TaskStatus;
            vm.canUserEditTask = $global.user.permissions.indexOf("CASE_READ_ALL") > -1;
            vm.currentUserEmail = $global.user.email;
            vm.currentUser.id = $global.user.userId;           
            vm.taskStatus = [
                { 'type': 'Not Started', 'value': 0 },
                { 'type': 'In Progress', 'value': 1 },
                { 'type': 'Completed', 'value': 2 }
            ];
            vm.eventStatus = {
                'inverse': 0,
                'warning': 1,
                'success': 2
            };
            vm.getCaseFromCaseNumber($stateParams.caseNumber, function () { getTaskDetails(vm.currentTaskId) });
        }
        function getTaskDetails(taskId) {
            getSelectedTask(taskId,
                function (response, status) {
                    vm.task = response;
                    vm.task.originalDueDate = new Date(vm.task.originalDueDate);
                    vm.taskFilesCount = vm.task.taskFiles.length;
                    vm.task.priority = vm.task.priority.toString();
                   vm.task.status = vm.task.status.toString();
                   vm.task.dueDate = new Date(vm.task.dueDate);
                    if (vm.task.taskFiles == null)
                        vm.task.taskFiles = [];
                    getTaskComments();
                    getCaseRelatedUsers(vm.task.caseId);
                    vm.isFileSearchActive ? getTaskFilesByKeyword() : getTaskAssociatedFiles(vm.currentTaskId);
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.task = {};
                });
        }

        function getSelectedTask(taskId, cbSuccess, cbError) {
            $http.get("api/task/" + taskId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function editTask(task) {
            vm.taskToEdited = angular.copy(task);
            if (task.createdBy == vm.currentUser.id || vm.currentUser.permissions.indexOf("CASE_UPDATE_ALL") > -1 || task.assignedTo == vm.currentUser.id)
                vm.isEditableTask = true;
            else
                vm.isEditableTask = false;
        }

        function getTaskAssociatedFiles(taskId, cbSuccess) {
            getSelectedTaskFiles(taskId,
                function (response, status) {
                    vm.searchedTaskFiles = response
                    .map(function (item) { item.createDateTime == new Date(item.createDateTime); return item;});
                    vm.allTaskAssociatedFilesList = response
                    .map(function (item) { item.createDateTime == new Date(item.createDateTime); return item; });
                    cbSuccess ? cbSuccess(): null;
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.searchedTaskFiles = [];
                })
        }
        function getSelectedTaskFiles(taskId, cbSuccess, cbError) {
            $http.get("api/TaskFiles/" + taskId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function downloadTaskFile(fileId) {
            vm.downloadExistingFile(fileId, Enums.DownloadFileType.TaskFile,
                function (response, status) { window.location.href = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function updateTaskStatus(selectedTask, status) {
            selectedTask.status = status;
            if (selectedTask.status == 2 && selectedTask.completionDate == undefined) { selectedTask.completionDate = new Date(); }
            else if (selectedTask.status != 2) { selectedTask.completionDate = null; }
            vm.saveEditedTask(selectedTask, function (response, status) { toastr.success("Task edited"); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status) });           
        }

        function filterUsersByRole(user) {
            var roleName = user.role.name;           
            if (roleName == "ICIUser")
                return "ICI-Users";
            else
                return "Users";
        }

        function resetSearchResult()
        {
            vm.isTaskSearchActive = false;
            vm.isCompletedTaskHidden = false;
            fetchAndStoreTasks(0);
        }

        function searchTasks(searchQuery)
        {
            if(searchQuery == "" || searchQuery == undefined)
            {
                return;
            }
            vm.searchQuery = searchQuery;
            vm.isTaskSearchActive = true;
            getTasksByKeyword(0);
        }

        function getTasksByKeyword(pageIndex)
        {
            vm.allTaskPages = [];
            if (vm.isCompletedTaskHidden) { vm.filter = "&$filter=Status ne 'COMPLETED'"; }
            else { vm.filter = ""; }
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '&$top=' + vm.defaultPageSize + '&$skip=' + skipCount + vm.filter + "&$orderby=DueDate,Status,CompletionDate";
            vm.getExistingTasksByKeyword(vm.searchQuery, vm.case.caseId, options,
                function (tasks, response, status) {
                    vm.allTasks = tasks;
                    CaseHeader.case.caseAssociatedTasksCount = response.allTasksCount;
                    vm.totalNumberOfTasks = response.result.count;
                    vm.allTaskPages.length = Math.ceil(vm.totalNumberOfTasks / vm.defaultPageSize);
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);;
                    vm.allTasks = [];
                }, true);
        }
        function goToTaskComments(task) {
            $location.path('/case-management/cases/' + task.caseNumber + '/task/' + task.taskId);
        }

        function getTaskComments()
        {
            vm.newTaskComment = {};
            vm.newTaskComment.taskCommentFiles = [];
            vm.taskComment = {};
            getExistingTaskComments(vm.currentTaskId,
                function (response, status) {
                    vm.taskComments = response
                    .map(function (item) {
                        item.creationDateTime = new Date(item.creationDateTime);
                        return item;
                    });
                    vm.newTaskComment.createdByUser = vm.currentUser;
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.taskComments = [];
                });
        }

        function getExistingTaskComments(taskId, cbSuccess, cbError)
        {
            $http.get("/api/taskcomment?TaskId=" + taskId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function postTaskComment(taskComment)
        {
            if (taskComment.id == null) {
                if ((vm.newTaskComment.comment == undefined || vm.newTaskComment.comment == "") && (vm.newTaskComment.taskCommentFiles.length == 0))
                { toastr.error("Please enter a comment"); return; }
                vm.newTaskComment.createdByUser = null;
                vm.newTaskComment.taskId = vm.currentTaskId;
                saveNewTaskComment(vm.newTaskComment,
                    function (response, status) {
                        toastr.success("Comment added");
                        CaseHeader.case.caseAllFilesCount += vm.newTaskComment.taskCommentFiles.length;
                        getTaskComments();
                    }, function (response, status) {
                        ErrorHandlerService.DisplayErrorMessage(response, status);
                    });
            }
            else {
                if ((vm.taskComment.comment == undefined || vm.taskComment.comment == "") && (vm.taskComment.taskCommentFiles.length == 0))
                { toastr.error("Please enter a comment or upload a file"); return; }
                vm.taskComment.createdByUser = null;
                saveEditedTaskComment(vm.taskComment,
                    function (response, status) {
                        toastr.success("Comment edited");
                        CaseHeader.case.caseAllFilesCount += vm.taskComment.taskCommentFiles.length - vm.originalTaskCommentFilesLength;
                        getTaskComments();
                    },
                    function (response, status) {
                        ErrorHandlerService.DisplayErrorMessage(response, status);
                    });
            }
        }

        function saveNewTaskComment(taskComment, cbSuccess, cbError)
        {
            $http.post("/api/taskcomment/", taskComment)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function saveEditedTaskComment(taskComment, cbSuccess, cbError)
        {
            $http.put("/api/taskcomment/" + taskComment.id, taskComment)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function deleteTaskComment(taskCommentId)
        {
            deleteExistingTaskComment(taskCommentId,
                function (response, status) {
                    toastr.success("Comment archived");
                    getTaskComments();
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                })
        }

        function deleteExistingTaskComment(taskCommentId, cbSuccess, cbError)
        {
            $http.delete("/api/taskcomment?id=" + taskCommentId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function fetchAndStoreTasks(pageIndex) {
            vm.case = CaseHeader.case;
            vm.allTaskPages = [];
            if (vm.isCompletedTaskHidden) { vm.filter = "&$filter=Status ne 'COMPLETED'";}
            else { vm.filter = "";}
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '?$top=' + vm.defaultPageSize + '&$skip=' + skipCount + vm.filter + "&$orderby=DueDate,Status,CompletionDate";
            vm.getExistingCaseAssociatedTasks(vm.case.caseId, options,
                function (tasks, response, status) {
                    vm.allTasks = tasks;
                    vm.totalNumberOfTasks = response.result.count;
                    CaseHeader.case.caseAssociatedTasksCount = response.allTasksCount;
                    vm.allTaskPages.length = Math.ceil(vm.totalNumberOfTasks / vm.defaultPageSize);
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.allTasks = [];
                });
        }

        function fetchAndStoreUsersList() {
            var options = "?$skip=0&$filter=Role/Name ne 'Participant'";
            vm.getExistingUsers(options,
                function (response, status) {
                    if (response.items)
                        vm.users = response.items;
                    else
                        vm.users = [];                  
                },
                function (response, status) {ErrorHandlerService.DisplayErrorMessage(response, status);vm.users = [];});
        }

        function openAddTaskModal() {
            vm.task = {};
            vm.task.taskFiles = [];
            vm.status = undefined;
            vm.assignToUser = undefined;
            vm.isTaskBeingAdded = true;          
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/cases/views/add-task-modal.view.html',
                size: 'md'
            });
        }
        
        function clearPreFilledValues() {
            vm.task = {};
            vm.task.taskFiles = [];
            vm.status = undefined;
            vm.assignToUser = undefined;
            if (CaseDetails.case != undefined && CaseDetails.case.caseId != undefined)
            {
                if (vm.pageIndex != undefined)
                {
                    vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : fetchAndStoreTasks(vm.pageIndex);
                }
                else
                {
                    vm.isTaskSearchActive ? getTasksByKeyword(0) : fetchAndStoreTasks(0);
                }
            }
        }

        function saveTask(isValid, isTaskBeingEdited) {
            if (!isTaskBeingEdited) {
                if (isValid == false) { return; }
                if (vm.task.dueDate == undefined) { toastr.error("Please select a date"); return; }
                vm.task.caseId = vm.case.caseId;              
                saveNewTask(vm.task, function (response, status) {
                    toastr.success("Task added"); closeModal();
                    CaseHeader.case.caseAllFilesCount += vm.task.taskFiles.length;
                    if (vm.cbSave == undefined) {
                        vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : fetchAndStoreTasks(vm.pageIndex);
                    }
                    getCaseRelatedUsers(vm.currentCaseId);
                },
                    function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else {
                vm.task.assignedTo = vm.task.assignedToUserDetails.id;
                vm.taskStatus = vm.task.status;
                if (vm.task.status == 2 && vm.task.completionDate == undefined) { vm.task.completionDate = new Date(); }
                else if (vm.task.status != 2) { vm.task.completionDate = null; }
                vm.saveEditedTask(vm.task, function (response, status) {
                    toastr.success("Task edited");
                    vm.isEditableTask = false;
                    getTaskDetails(vm.currentTaskId);
                    getCaseRelatedUsers(vm.task.caseId);
                },
                    function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
        }

        function saveNewTask(task, cbSuccess, cbError)
        {                 
            $http.post('/api/task/', task)
                    .success(function (response) {
                        cbSuccess(response, status);
                    })
               .error(function (response, status) {
                   cbError(response, status);
               });
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
        }

        function onUserSelected(item) {
            vm.task.assignedTo = item.id;
        }

        function onTaskStatus(item) {
            vm.task.status = item.value;
        }

        function deleteTask(taskId)
        {
            vm.deleteExistingTask(taskId,
                function (response, status) {
                    if (vm.currentTaskId != undefined) {
                        $window.history.back();
                    }
                    else {
                        vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : fetchAndStoreTasks(vm.pageIndex);
                    }
                    toastr.success("Task archived");
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }
		
        function uploadTaskFiles(files) {
            if (vm.currentTaskId != undefined) {
                vm.task.taskFiles = [];
            }
            vm.taskFilesUploaded = 0;           
            if (files && files.length) {
                vm.numberOfTaskFilesToBeUploaded = files.length;
                for (var i = 0; i < files.length; i++) {
                    vm.uploadTaskFilesToBlobStorage({ data: { file: files[i] } });
                } 
            }
        }

        function uploadTaskFilesToBlobStorage(file) {
            usSpinnerService.spin('spinner');
            vm.uploadNewFile(file, function (resp) {
                vm.taskFilesUploaded++;
                for (var i = 0; i < resp.data.length; i++) {
                    vm.task.taskFiles.push(resp.data[i]);
                }
                if (vm.task.taskFiles.length > 0) {
                    vm.showExistingTaskFiles = true;
                }
                if (vm.numberOfTaskFilesToBeUploaded == vm.taskFilesUploaded) {
                    vm.taskFilesUploaded = 0;
                    usSpinnerService.stop('spinner');
                    toastr.success("File(s) uploaded");
                    if ($stateParams.taskId != undefined) {
                        postTaskFiles(vm.task, function (response, status) {
                            vm.isFileSearchActive ? getTaskAssociatedFiles(vm.currentTaskId, function () { getTaskFilesByKeyword(vm.taskFileSearchQuery); }) : getTaskAssociatedFiles(vm.currentTaskId);
                        }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
                    }
                }
            }, function (resp) {
                usSpinnerService.stop('spinner');
                ErrorHandlerService.DisplayErrorMessage(resp.data);
            });
        }

        function deleteNewTaskAssociatedFile(file) {
            vm.task.taskFiles.splice(file, 1);
            if (vm.task.taskFiles.length == 0) {
                vm.showExistingTaskFiles = false;
            }
        }

        function deleteSelectedTaskAssociatedFile(fileId, taskId) {
            deletExistingTaskFile(fileId, taskId, function(response,status) {
                toastr.success("File Deleted");
                vm.allTaskAssociatedFilesList = vm.allTaskAssociatedFilesList.filter(function (item) { return item.fileId != fileId; });
                vm.isFileSearchActive ? getTaskFilesByKeyword(vm.taskFileSearchQuery) : vm.searchedTaskFiles = vm.allTaskAssociatedFilesList.slice();
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status);
            })
        }

        function deletExistingTaskFile(fileId, taskId, cbSuccess, cbError) {
            $http.delete('api/TaskFiles/'+ taskId +'/File/'+ fileId )
                   .success(function (response) {
                       cbSuccess(response, status);
                   })
              .error(function (response, status) {
                  cbError(response, status);
              });
        }
        
        function postTaskFiles(task,cbSuccess,cbError) {
            $http.post('api/TaskFiles',task)
                   .success(function (response) {
                       cbSuccess(response, status);
                   })
              .error(function (response, status) {
                  cbError(response, status);
              });
        }

        function deleteTaskCommentAssociatedFile(file, commentId) {
            if (commentId == -1) {
                for (var fileIndex = 0; fileIndex < vm.newTaskComment.taskCommentFiles.length; fileIndex++)
                    if (vm.newTaskComment.taskCommentFiles[fileIndex].fileId == file.fileId)
                    { vm.newTaskComment.taskCommentFiles.splice(fileIndex, 1); toastr.success("File archived"); return; }
            }
            else {
                var index = 0;
                for (; index < vm.taskComments.length; index++) {
                    if (vm.taskComments[index].id == commentId)
                        break;
                }
                for (var fileIndex = 0; fileIndex < vm.taskComments[index].taskCommentFiles.length; fileIndex++)
                    if (vm.taskComments[index].taskCommentFiles[fileIndex].fileId == file.fileId)
                    { vm.taskComments[index].taskCommentFiles.splice(fileIndex, 1); toastr.success("File archived"); return; }
            }
        }

        function editTaskComment(taskComment)
        {           
            vm.taskComment = taskComment;
            vm.taskComment.taskCommentFiles != undefined ? vm.originalTaskCommentFilesLength = vm.taskComment.taskCommentFiles.length : vm.originalTaskCommentFilesLength = 0
        }
       
        function openSelectedTaskCaseDetails(caseNumber) {

            vm.roleName == 'Admin' ? $location.path("case-management/cases/" + caseNumber + "/caseDetails") : $location.path("case-management/cases/" + caseNumber + "/tasks");
        }

        function getTaskFilesByKeyword(taskFileSearchQuery) {
            vm.taskFileSearchQuery = taskFileSearchQuery;
            if (vm.taskFileSearchQuery == "" || vm.taskFileSearchQuery == undefined) {
                return;
            }
            vm.isFileSearchActive = true;
            vm.searchedTaskFiles = vm.allTaskAssociatedFilesList.slice();
            for (var index = 0; index < vm.searchedTaskFiles.length; index++) {
                    if (vm.searchedTaskFiles[index].fileName.toLowerCase().search(vm.taskFileSearchQuery.toLowerCase()) < 0) {
                        vm.searchedTaskFiles.splice(index, 1);
                        index--;
                    }
            }
        }

        function resetTaskFileSearchResult() {
            vm.isFileSearchActive = false;
            vm.taskFileSearchQuery = "";
            vm.searchedTaskFiles = vm.allTaskAssociatedFilesList.slice();
        }

        function cancelEditTaskChanges(){
            vm.isEditableTask = false;
            vm.task = angular.copy(vm.taskToEdited);
        }
        
        function renameFile(file,index) {
            if (vm.fileName == undefined || vm.fileName == "") { toastr.error("Please enter file name"); return; }
            vm.selectedFileId = null;
            vm.file = Object.assign({}, file);
            vm.file.fileName = vm.fileName;
            vm.saveEditedFile(vm.file,
                function (response, status) {
                    toastr.success("File renamed");
                    vm.allTaskAssociatedFilesList[index] = vm.file;
                    vm.isFileSearchActive ? getTaskFilesByKeyword(vm.taskFileSearchQuery) : vm.searchedTaskFiles = vm.allTaskAssociatedFilesList.slice();
                },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function openExistingCaseFilesModal() {           
            vm.caseFiles = $global.case.files;
            for (var count = 0; count < vm.caseFiles.length; count++)
                vm.caseFiles[count].isSelected = false;
            vm.caseFilesModalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/cases/views/add-case-files-task-modal.view.html',
                size: 'lg'
            })
        }

        function closeAddFileModal() {
            vm.caseFilesModalInstance.dismiss('cancel');
        }

        function associateSelectedCaseFiles(files) {
            closeAddFileModal();                    
            for (var count = 0; count < vm.caseFiles.length; count++) {
                if (vm.caseFiles[count].isSelected) {                    
                    vm.task.taskFiles.push(vm.caseFiles[count]);                    
                        if (vm.task.taskFiles.length > 0) {
                            vm.showExistingTaskFiles = true;
                    }                    
                }                       
            }
            if ($stateParams.taskId)
                postTaskFiles(vm.task, function (response, status) {
                    getTaskAssociatedFiles(vm.currentTaskId);
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        vm.init = init;
        vm.getTaskFilesByKeyword = getTaskFilesByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.fetchAndStoreTasks = fetchAndStoreTasks;
        vm.openAddTaskModal = openAddTaskModal;
        vm.fetchAndStoreUsersList = fetchAndStoreUsersList;
        vm.closeModal = closeModal;
        vm.onUserSelected = onUserSelected;
        vm.saveTask = saveTask;
        vm.onTaskStatus = onTaskStatus;
        vm.deleteTask = deleteTask;
        vm.postTaskComment = postTaskComment;
        vm.deleteTaskComment = deleteTaskComment;
        vm.getTaskComments = getTaskComments;
        vm.searchTasks = searchTasks;
        vm.getTasksByKeyword = getTasksByKeyword;
        vm.resetTaskFileSearchResult = resetTaskFileSearchResult;
        vm.uploadTaskFiles = uploadTaskFiles;
        vm.uploadTaskFilesToBlobStorage = uploadTaskFilesToBlobStorage;
        vm.deleteNewTaskAssociatedFile = deleteNewTaskAssociatedFile;
        vm.deleteSelectedTaskAssociatedFile = deleteSelectedTaskAssociatedFile;
        vm.deleteTaskCommentAssociatedFile = deleteTaskCommentAssociatedFile;
        vm.filterUsersByRole = filterUsersByRole;
        vm.updateTaskStatus = updateTaskStatus;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.getExistingCaseAssociatedTasks = $global.getExistingCaseAssociatedTasks;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.getExistingTasksByKeyword = $global.getExistingTasksByKeyword;
        vm.uploadNewFile = $global.uploadNewFile;
        vm.saveEditedTask = $global.saveEditedTask;
        vm.deleteExistingTask = $global.deleteExistingTask;
        vm.taskCommentsInit = taskCommentsInit;
        vm.clearPreFilledValues = clearPreFilledValues;
        vm.editTaskComment = editTaskComment;       
        vm.getExistingTaskComments = getExistingTaskComments;
        vm.saveNewTaskComment = saveNewTaskComment;
        vm.saveEditedTaskComment = saveEditedTaskComment;
        vm.deleteExistingTaskComment = deleteExistingTaskComment;
        vm.saveNewTask = saveNewTask;
        vm.getExistingCaseUsers = $global.getExistingCaseUsers;
        vm.goToTaskComments = goToTaskComments;
        vm.downloadTaskFile = downloadTaskFile;
        vm.downloadExistingFile = $global.downloadExistingFile;
        vm.openSelectedTaskCaseDetails = openSelectedTaskCaseDetails;
        vm.editTask = editTask;
        vm.goToPreviousUrl = $global.goToPreviousUrl;
        vm.getSelectedTask = getSelectedTask;
        vm.getSelectedTaskFiles = getSelectedTaskFiles;
        vm.deletExistingTaskFile = deletExistingTaskFile;
        vm.postTaskFiles = postTaskFiles;
        vm.cancelEditTaskChanges = cancelEditTaskChanges;
        vm.renameFile = renameFile;
        vm.saveEditedFile = $global.saveEditedFile;
        vm.openExistingCaseFilesModal = openExistingCaseFilesModal;
        vm.associateSelectedCaseFiles = associateSelectedCaseFiles;
        vm.closeAddFileModal = closeAddFileModal;     
        return vm;
    }
    ]);
})();
