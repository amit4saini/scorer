﻿(function () {
    'use strict';
    angular.module('case').factory('Timesheets', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;
        function init()
        {
            getUsersList();
            vm.currentUser = $global.user;
            vm.currentUser.id = $global.user.userId;
            getAllTimesheetsDescriptions();
            vm.datePickerOptions = { 'showWeeks': false };
            vm.isTimesheetBeingAdded = false;
            vm.isTimesheetBeingEdited = false;
            vm.isEndTimeAdded = false;
            vm.newTimesheet = {};
            vm.timesheet = {};
            vm.caseNumber = $stateParams.caseNumber;
            vm.getCaseFromCaseNumber(vm.caseNumber, getTimesheets);
        }

        function canUserSelectTimeSheetPersonName() {           
            if (vm.currentUser.permissions.indexOf("ADD_TIMESHEET_COST") > -1)
                return true;
           else
               return false;          
        }

        function getTimesheets()
        {
            vm.case = CaseHeader.case;
            getExistingTimesheets(vm.case.caseId, function (response, status) {               
                if (angular.isArray(response))
                    vm.allTimesheets = response;
                else
                    vm.allTimesheets = [];               
                for (var index = 0; index < vm.allTimesheets.length; index++) {
                    if (vm.allTimesheets[index].endTime == null) {
                        vm.newTimesheet = vm.allTimesheets[index];
                        vm.newTimesheet.startTime = new Date(vm.newTimesheet.startTime);
                        vm.newDate = new Date(vm.newTimesheet.startTime);
                        vm.isTimesheetBeingAdded = true;
                        vm.isStartTimeAdded = true;
                        vm.allTimesheets.splice(index, 1);
                        break;
                    }
                    else {
                        vm.isTimesheetBeingAdded = false;
                        vm.isStartTimeAdded = false;
                        vm.newTimesheet = {};
                    }
                }
                if (vm.allTimesheets.length > 0)
                for (var index = 0; index < vm.allTimesheets.length; index++) {                   
                        vm.allTimesheets[index].editDate = new Date(vm.allTimesheets[index].startTime);
                        vm.allTimesheets[index].startTime = new Date(vm.allTimesheets[index].startTime);
                        vm.allTimesheets[index].endTime = new Date(vm.allTimesheets[index].endTime);                                                                      
                }
                calculateTotalCost();
                CaseHeader.case.caseAssociatedTimesheetsCount = vm.allTimesheets.length;
                $global.case.caseAssociatedTimesheetsCount = vm.allTimesheets.length;
                if(vm.isStartTimeAdded){
                    CaseHeader.case.caseAssociatedTimesheetsCount++;
                    $global.case.caseAssociatedTimesheetsCount++;
                }
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allTimesheets = [];
            });
        }

        function getExistingTimesheets(caseId, cbSuccess, cbError) {
            $http.get('api/case/'+ caseId +'/timesheets')
              .success(function (response, status) {
                  cbSuccess(response, status);
              })
              .error(function (response, status) {
                  cbError(response, status);
              });
        }

        function getAllTimesheetsDescriptions()
        {
            getExistingTimesheetsDescriptions(function (response, status) { vm.allTimesheetDescriptions = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.allTimesheetDescriptions = []; });
        }

        function getExistingTimesheetsDescriptions(cbSuccess, cbError) {
            $http.get('/api/timesheetDescription')
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function getHoursAndMinutes(timesheet)
        {
            var timing = {};
            timing.endHours = new Date(timesheet.endTime).getHours();
            timing.endMinutes = new Date(timesheet.endTime).getMinutes();
            timing.startHours = new Date(timesheet.startTime).getHours();
            timing.startMinutes = new Date(timesheet.startTime).getMinutes();
            return timing;
        }

        function calculateTotalCost(endTime) {
            if (endTime != undefined && endTime != "") 
                vm.isEndTimeAdded = true;
            else
                vm.isEndTimeAdded = false;
            vm.totalCost = 0;
            vm.totalHours = 0;          
            if (vm.timesheet != undefined && vm.timesheet.cost == undefined)
                vm.timesheet.cost = 0;
            if (vm.newTimesheet != undefined && vm.newTimesheet.cost == undefined)
                vm.newTimesheet.cost = 0;
            for (var i = 0 ; i < vm.allTimesheets.length; i++) {
                var timing = getHoursAndMinutes(vm.allTimesheets[i]);
                vm.allTimesheets[i].timeSpent = (timing.endHours + timing.endMinutes / 60.00) - (timing.startHours + timing.startMinutes / 60.00);               
                if (vm.allTimesheets[i].timeSpent >= 0 && vm.allTimesheets[i].id != vm.timesheet.id) {
                    if(vm.allTimesheets[i].cost != undefined && vm.allTimesheets[i].cost > 0)
                        vm.totalCost += (vm.allTimesheets[i].cost * vm.allTimesheets[i].timeSpent);
                    vm.totalHours += vm.allTimesheets[i].timeSpent;
                }
                
            }
            if (vm.isTimesheetBeingAdded == true) {
                var timing = getHoursAndMinutes(vm.newTimesheet);
                vm.newTimesheet.timeSpent = (timing.endHours + timing.endMinutes / 60.00) - (timing.startHours + timing.startMinutes / 60.00);
                if (vm.newTimesheet.timeSpent >= 0) {
                    if(vm.newTimesheet.cost != undefined && vm.newTimesheet.cost > 0)
                        vm.totalCost += (vm.newTimesheet.cost * vm.newTimesheet.timeSpent);
                    vm.totalHours += vm.newTimesheet.timeSpent;
                }               
            }
            if (vm.isTimesheetBeingEdited == true) {               
                var timing = getHoursAndMinutes(vm.timesheet);
                vm.timesheet.timeSpent = (timing.endHours + timing.endMinutes / 60.00) - (timing.startHours + timing.startMinutes / 60.00);
                if (vm.timesheet.timeSpent >= 0){
                    if(vm.timesheet.cost != undefined && vm.timesheet.cost > 0)
                        vm.totalCost += (vm.timesheet.cost * vm.timesheet.timeSpent);
                    vm.totalHours += vm.timesheet.timeSpent;
                }                                                    
            }
            
        }
      
        function addTimesheet() {            
            vm.isTimesheetBeingAdded = true;
            vm.newTimesheet = { startTime: "", endTime: "", cost:"" };
            vm.newTimesheet.user = vm.currentUser;
            vm.newDate = new Date();
            vm.selectedTimePicker = "";
            calculateTotalCost();        
        }

        function cancelTimesheetChanges(timesheet)
        {           
            if(timesheet.id==null)
            {
                vm.isTimesheetBeingAdded = false;
                vm.newTimesheet = {};
                vm.newDate = undefined;
                vm.selectedTimePicker = '';
            }
            else
            {
                vm.isTimesheetBeingEdited = false;
                vm.timesheet = {};
                vm.editDate = undefined;
                vm.selectedTimePicker = '';
            }           
            getTimesheets();
        }

        function getUsersList() {
            var options = "?$skip=0";
            vm.getExistingUsers(options, function (response, status) { vm.users = response.items; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.users = []; });
        }

        function getActualTime(timesheet,date)
        {
            var dateTime = {};
            dateTime.startTime = new Date(timesheet.startTime);
            dateTime.endTime = new Date(timesheet.endTime);
            var day = new Date(date).getDate();
            var month = new Date(date).getMonth();
            var year = new Date(date).getFullYear();
            dateTime.startTime.setDate(day);
            dateTime.startTime.setMonth(month);
            dateTime.startTime.setFullYear(year);
            dateTime.endTime.setDate(day);
            dateTime.endTime.setMonth(month);
            dateTime.endTime.setFullYear(year);
            return dateTime;
        }

        function combineDateTime(timesheet)
        {
            var index = 0;
            if (timesheet.id==null)
            {               
                var dateTime = getActualTime(vm.newTimesheet, vm.newDate);
                if (vm.newTimesheet.startTime != null && vm.newTimesheet.startTime != "")
                    vm.newTimesheet.startTime = dateTime.startTime;
                if (vm.newTimesheet.endTime != null && vm.newTimesheet.endTime != "")
                    vm.newTimesheet.endTime = dateTime.endTime;
            }
            else
            {               
                var dateTime = getActualTime(vm.timesheet, vm.editDate);
                if (vm.timesheet.startTime != null)
                    vm.timesheet.startTime = dateTime.startTime;
                if (vm.timesheet.endTime != null)
                    vm.timesheet.endTime = dateTime.endTime;
            }
        }

        function validateTimesheet(timesheet)
        {           
            if (timesheet.description == undefined || timesheet.description == null)
            { toastr.error("Please select a timesheet description"); return false; }
            if (timesheet.description.description == "Other (Manual)" && timesheet.customDescription == "" || timesheet.description.description == "Other (Manual)" && timesheet.customDescription == null)
            { toastr.error("Please enter a custom description"); return false; }
            if (timesheet.startTime == null || timesheet.startTime === "")
            { toastr.error("Please select a valid start time"); return false; }
            if ((timesheet.endTime != null || timesheet.endTime !== "") && Date.parse(timesheet.startTime) >= Date.parse(timesheet.endTime))
            {
                if (vm.isStartTimeAdded)
                {
                    vm.newTimesheet.endTime = timesheet.startTime;
                }
                else
                {
                    toastr.error("End time should be greater than start time");
                    return false;
                }
            }
            if (timesheet.cost == undefined)
                timesheet.cost = 0;
            return true;
        }

        function saveTimesheet(timesheet) {
            vm.selectedTimePicker = '';
            combineDateTime(timesheet);
            if (!validateTimesheet(timesheet)) { return; }          
            if (timesheet.id == null) {
                vm.newTimesheet = timesheet;
                if (vm.newTimesheet.description.description != "Other (Manual)") { vm.newTimesheet.customDescription = ""; }
                vm.newTimesheet.caseId = vm.case.caseId;
                vm.newTimesheet.userId = vm.newTimesheet.user.id;
                vm.newTimesheet.user = null;
                vm.newTimesheet.descriptionId = vm.newTimesheet.description.id;
                vm.newTimesheet.description = null;
                vm.isStartTimeAdded = true;  
                saveNewTimesheet(vm.newTimesheet, function (response, status) {
                    getTimesheets();
                    toastr.success("Timesheet added");
                    vm.getCaseFromCaseNumber(vm.caseNumber);
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else
            {
                vm.timesheet = timesheet;
                if (vm.timesheet.description.description != "Other (Manual)") { vm.timesheet.customDescription = ""; }
                vm.timesheet.userId = vm.timesheet.user.id;
                vm.timesheet.user = null;
                vm.timesheet.descriptionId = vm.timesheet.description.id;
                vm.timesheet.description = null;
                saveEditedTimesheet(vm.timesheet, function (response, status) {
                    cancelTimesheetChanges(vm.timesheet);
                    toastr.success("Timesheet edited");
                    vm.getCaseFromCaseNumber(vm.caseNumber);
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }

        }

        function saveNewTimesheet(timesheet, cbSuccess, cbError)
        {
            $http.post("/api/timesheet/", timesheet)
                    .success(function (response, status) {
                        cbSuccess(response, status);
                    })
                    .error(function (response, status) {
                        cbError(response, status);
                    });
        }

        function saveEditedTimesheet(timesheet, cbSuccess, cbError)
        {
            $http.put("/api/timesheet/" + timesheet.id, timesheet)
                   .success(function (response, status) {
                       cbSuccess(response, status);
                   })
                   .error(function (response, status) {
                       cbError(response, status);
                   });
        }

        function deleteTimesheet(timesheetId)
        {
            deleteExistingTimesheet(timesheetId, function (response, status) {
                getTimesheets();
                toastr.success("Timesheet removed");
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingTimesheet(timesheetId, cbSuccess, cbError) {
            $http.delete('/api/timesheet/' + timesheetId)
              .success(function (response, status) {
                  cbSuccess(response, status);
              }).error(function (response, status) {
                  cbError(response, status);
              });
        }

        function editTimesheet(timesheet) {
            getTimesheets();
            vm.isTimesheetBeingEdited = true;
            vm.timesheet = angular.copy(timesheet);
            vm.editDate = timesheet.editDate;
            vm.selectedTimePicker = "";
        }       

        function setTimesheetTime(startTime, endTime, timesheet) {
            if(startTime)
                !vm.newTimesheet.startTime ? timesheet.startTime = new Date() : null;
            else
                timesheet.endTime = new Date();
            saveTimesheet(timesheet);
        }

        vm.init = init;
        vm.cancelTimesheetChanges = cancelTimesheetChanges;

        vm.getTimesheets = getTimesheets;
        vm.getUsersList = getUsersList;
        vm.getAllTimesheetsDescriptions = getAllTimesheetsDescriptions;

        vm.addTimesheet = addTimesheet;
        vm.editTimesheet = editTimesheet;
        vm.saveTimesheet = saveTimesheet;
        vm.deleteTimesheet = deleteTimesheet;
        
        vm.calculateTotalCost = calculateTotalCost;
        vm.canUserSelectTimeSheetPersonName = canUserSelectTimeSheetPersonName;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.getExistingUsers = $global.getExistingUsers;      
        vm.getExistingTimesheets = getExistingTimesheets;
        vm.getExistingTimesheetsDescriptions = getExistingTimesheetsDescriptions;
        vm.deleteExistingTimesheet = deleteExistingTimesheet;
        vm.saveEditedTimesheet = saveEditedTimesheet;
        vm.saveNewTimesheet = saveNewTimesheet;
        vm.roleName = $global.roleName;      
        vm.setTimesheetTime = setTimesheetTime;
        return vm;
    }
    ]);
})();
