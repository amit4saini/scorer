﻿(function () {
    'use strict';
    angular.module('case').factory('Expenses', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;
        function init()
        {
            vm.roleName = $global.roleName;
            vm.expenseAdded = false;
            vm.isExpenseBeingEdited = false;
            vm.showValidationMessage = false;
            vm.newExpense = {};
            vm.focusOnExpense = [];
            vm.totalExpenseCost = 0;
            vm.paymentTypeEnum = Enums.ExpensePaymentType;
            vm.allPaymentTypes = [];          
            for (var key in vm.paymentTypeEnum) {
                vm.allPaymentTypes.push(key);
            }
            getAllVendors();
            vm.dropdownListType = Enums.DropdownListType;
            vm.caseNumber = $stateParams.caseNumber;
            vm.getCaseFromCaseNumber(vm.caseNumber, getExpenses);
        }

        function getAllVendors()
        {
            getExistingVendors(function (response, status) { vm.vendors = response; },
                function (response, status) { vm.vendors = []; ErrorHandlerService.DisplayErrorMessage(response, status); });           
        }

        function getExistingVendors(cbSuccess, cbError)
        {
            $http.get('api/Vendor')
            .success(function (response, status) {
                cbSuccess(response, status);
            }).error(function (response, status) {
                cbError(response, status);
            });
        }

        function calculateTotalExpenseCost() {
            vm.totalExpenseCost = 0;
            for (var index = 0; index < vm.allExpenses.length; ++index) {
                if (vm.allExpenses[index].quantity == undefined || vm.allExpenses[index].quantity < 0) { continue; }
                if (vm.allExpenses[index].cost == undefined || vm.allExpenses[index].cost < 0) { continue; }
                vm.totalExpenseCost += vm.allExpenses[index].quantity * vm.allExpenses[index].cost;
            }
            if (vm.expenseAdded == true && vm.newExpense.quantity != undefined && vm.newExpense.cost != undefined)
                if(vm.newExpense.quantity>=0 && vm.newExpense.cost>=0)
                vm.totalExpenseCost += vm.newExpense.quantity * vm.newExpense.cost;
        }

        function getExpenses()
        {
            vm.areAllInvoicesApproved = true;
            vm.case = CaseHeader.case;
            getExistingExpenses(vm.case.caseId, function (response, status) {
                vm.allExpenses = response;
                for (var index = 0; index < vm.allExpenses.length; ++index) { 
                    if (!vm.allExpenses[index].invoiceApproved && vm.allExpenses[index].expensePaymentType == vm.paymentTypeEnum.Invoice)
                        vm.areAllInvoicesApproved = false;
                    if (vm.allExpenses[index].expensePaymentType == vm.paymentTypeEnum.Account)
                        vm.allExpenses[index].expensePaymentType = "Account";
                    if (vm.allExpenses[index].expensePaymentType == vm.paymentTypeEnum.CreditCardPayment)
                        vm.allExpenses[index].expensePaymentType = "Credit Card Payment";
                    if (vm.allExpenses[index].expensePaymentType == vm.paymentTypeEnum.Invoice)
                        vm.allExpenses[index].expensePaymentType = "Invoice";
                    !vm.allExpenses[index].originalCurrencyCost ? vm.allExpenses[index].originalCurrencyCost = "" : null;
                    var originalCurrencyCost = vm.allExpenses[index].originalCurrencyCost.toString();
                    originalCurrencyCost = originalCurrencyCost.replace(/,/g, "");
                    originalCurrencyCost = originalCurrencyCost.split(" ");
                    if (originalCurrencyCost == "") {
                        vm.allExpenses[index].costValue = null;
                        vm.allExpenses[index].costUnit = null;
                    }
                    else {
                        vm.allExpenses[index].costValue = originalCurrencyCost[0];
                        vm.allExpenses[index].costUnit = originalCurrencyCost[1];
                    }
                }
                vm.totalExpenseCost = 0;
                if (vm.allExpenses == null) { vm.allExpenses = []; }
                for (var index = 0; index < vm.allExpenses.length; ++index) {
                    vm.focusOnExpense[index] = 0;    
                }
                CaseHeader.case.caseAssociatedExpensesCount = vm.allExpenses.length;
                $global.case.caseAssociatedExpensesCount = vm.allExpenses.length;
                calculateTotalExpenseCost();
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allExpenses = [];
            });
        }

        function getExistingExpenses(caseId, cbSuccess, cbError) {
            $http.get("api/case/" + caseId + "/caseExpenses")
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function addExpense() {
            vm.isExpenseBeingEdited = false;
            openExpenseModal();
            vm.expenseAdded = true;
        }

        function onEditExpense(expense) {
            vm.isExpenseBeingEdited = true;
            vm.expense = angular.copy(expense);
            openExpenseModal();
        }

        function openExpenseModal() {
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/cases/views/expense-modal-view.html',
                size: 'md'
            });
        }

        function deleteExpense(expenseId)
        {
            deleteExistingExpense(expenseId, function (response, status) {
                toastr.success("Case expense removed");
                getExpenses();
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingExpense(expenseId, cbSuccess, cbError) {
            $http.delete("/api/CaseExpense?id=" + expenseId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function validateOriginalCurrencyCost(expense)
        {
            !expense.originalCurrencyCost ? expense.originalCurrencyCost = "" : null;
            var originalCurrencyCost = expense.originalCurrencyCost.toString();
            if (originalCurrencyCost == "")
                return true;
            if (originalCurrencyCost.match(/[-!$%^&*()_+|~=`{}\[\]:";'<>?@\/]/))
                return false;
            if (!originalCurrencyCost.includes(" "))
                return false;
            originalCurrencyCost = originalCurrencyCost.split(" ");
            if (originalCurrencyCost.length > 2)
                return false;
            else {
                if (!originalCurrencyCost[0].match(/^-?(?:0|[1-9]\d{0,2}(\d{3})*)(?:\,\d+)*(?:\.\d+)?$/))
                    return false;
                if (originalCurrencyCost[0].match(/^[a-zA-Z]*$/))
                    return false;
                if (!originalCurrencyCost[1].match(/^[a-zA-Z]*$/))
                    return false;
            }
            return true;
        }

        function saveExpense(caseExpense)
        {
            var isValid = true;
            if (caseExpense == undefined) { vm.showValidationMessage = true; }
            if (caseExpense.vendor == undefined || caseExpense.vendor == "") {
                isValid = false;
                vm.showValidationMessage = true;
                return;
            }
            if (caseExpense.vendor.vendorName == undefined || caseExpense.vendor.vendorName == "") {
                isValid = false;
                vm.showValidationMessage = true;
                return;
            }
            if (caseExpense.expensePaymentType == undefined || caseExpense.expensePaymentType == "") {
                isValid = false;
                vm.showValidationMessage = true;
                return;
            }
            if (caseExpense.quantity == undefined || caseExpense.quantity == "" || caseExpense.quantity <= 0) {
                isValid = false;
                vm.showValidationMessage = true;
                document.getElementById("expense_quantity").focus();
                return;
            }
            if (caseExpense.originalCurrencyCost != undefined) {
                if (!validateOriginalCurrencyCost(caseExpense)) {
                    isValid = false;
                    toastr.error("Please enter valid original currency cost (i.e) 1000 GBP");
                    document.getElementById("expense_originalCurrencyCost").focus();
                    return;
                }
            }
            if (caseExpense.cost == undefined || caseExpense.cost == "" || caseExpense.cost == 0 || !vm.validateCurrency(caseExpense.cost)) {
                isValid = false;
                toastr.error("Please enter valid cost");
                document.getElementById("expense_cost").focus();
                return;
            }
            if (!isValid) return;
            caseExpense.vendorId = caseExpense.vendor.vendorId || null;
            
            if (caseExpense.vendor.vendorName != "Other") { caseExpense.vendorName = ""; }
            if (caseExpense.id == null) { vm.expenseAdded = false; caseExpense.caseId = vm.case.caseId; }
            for (var index = 0; index < vm.allExpenses.length; ++index) {
                if (vm.allExpenses[index].id == caseExpense.id)
                    vm.focusOnExpense[index] = 0;
               
            }
            if (caseExpense.id == null) {
                saveNewExpense(caseExpense, function (response, status) {
                    toastr.success("Case expense added");
                    closeExpenseModal();
                    getAllVendors();
                    getExpenses();
                    vm.getCaseFromCaseNumber(vm.caseNumber);
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else {
                saveEditedExpense(caseExpense, function (response, status) {
                    toastr.success("Case expense edited");                   
                    closeExpenseModal();
                    getAllVendors();
                    getExpenses();
                    vm.getCaseFromCaseNumber(vm.caseNumber);
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
        }

        function saveNewExpense(expense, cbSuccess, cbError)
        {
            $http.post('/api/CaseExpense', expense)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function saveEditedExpense(expense, cbSuccess, cbError)
        {
            $http.put('/api/CaseExpense/' + expense.id, expense)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function clearList($event, $select) {
            $event.stopPropagation();
            $select.selected = undefined;
            $select.search = undefined;
            $select.activate();
        }

        function refreshList($select, selectedType) {
            var search = $select.search,
              list = angular.copy($select.items),
              FLAG = -1;
            //remove newly added values
            list = list.filter(function (item) {
                return vm.dropdownListType[selectedType] == vm.dropdownListType["ExpenseType"] ? item.id !== FLAG : item.vendorId !== FLAG;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var newUserInput = {};
                vm.dropdownListType[selectedType] == vm.dropdownListType["ExpenseType"] ?
                newUserInput = { sourceName: search, id: FLAG, currency: "US", amount: 0 }
                : newUserInput = { vendorId: FLAG, vendorName: search };
                list.push(newUserInput);
                $select.items = list;
                $select.selected = newUserInput;
            }
        }

        function closeExpenseModal() {
            if (vm.modalInstance) {
                vm.modalInstance.dismiss('cancel');
                vm.expense = undefined;
                vm.showValidationMessage = false;
            }           
        }

        function updateAllExpenses(allExpenses) {
            vm.areAllInvoicesApproved = true;
          for (var count = 0; count < allExpenses.length; count++) {
                 allExpenses[count].invoiceApproved = true;
              }            
             updateExpensesList(allExpenses, function (response, status) {
             }, function (response, status) {
                 ErrorHandlerService.DisplayErrorMessage(response, status);
                 vm.areAllInvoicesApproved = false;
                 getExpenses();
             });
        }

        function updateExpensesList(allExpenses, cbSuccess, cbError) {
            $http.put('/api/CaseExpense', allExpenses)
             .success(function (response, status) {
                 cbSuccess(response, status);
             })
             .error(function (response, status) {
                 cbError(response, status);
             });
        }
        vm.init = init;       
        vm.getExpenses = getExpenses;
        vm.saveExpense = saveExpense;
        vm.addExpense = addExpense;
        vm.openExpenseModal = openExpenseModal;
        vm.closeExpenseModal = closeExpenseModal;
        vm.deleteExpense = deleteExpense;
        vm.calculateTotalExpenseCost = calculateTotalExpenseCost;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.getAllVendors = getAllVendors;
        vm.validateCurrency = $global.validateCurrency;
        vm.getExistingVendors = getExistingVendors;
        vm.getExistingExpenses = getExistingExpenses;
        vm.deleteExistingExpense = deleteExistingExpense;
        vm.saveEditedExpense = saveEditedExpense;
        vm.saveNewExpense = saveNewExpense;
        vm.onEditExpense = onEditExpense;
        vm.clearList = clearList;
        vm.refreshList = refreshList;
        vm.roleName = $global.roleName;
        vm.updateAllExpenses = updateAllExpenses;
        return vm;
    }
    ]);
})();
