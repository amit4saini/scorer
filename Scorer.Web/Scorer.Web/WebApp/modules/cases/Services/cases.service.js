﻿(function () {
    'use strict';
    angular.module('case').factory('Cases', ['$global', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams',
    function ($global, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams) {
        var vm = this;
        
        function init() {
            vm.isCaseSearchActive = false;
            vm.caseFilterOption = "All";
            vm.roleName = $global.roleName;
            vm.currentUser = $global.user;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
            vm.modalInstance = {};
            vm.reportPreviewModalInstance = {};
            vm.allSubjectTypes = Enums.SubjectType;
            vm.allPages = [];
            fetchAndStoreCasesList(0);
            fetchAndStoreClientsData();
            fetchAndStoreDiscountsList();
            fetchAllReportTypes();
            fetchAndStoreIndustryList();
            populateIndustrySubCategory();
            fetchAndStoreSubStatusList();
            fetchAndStoreCountryList();
        }

        function searchCases(searchQuery)
        {
            if(searchQuery == undefined || searchQuery == "")
            {
                return;
            }
            vm.searchQuery = searchQuery;
            vm.isCaseSearchActive = true;
            getCasesByKeyword(0);
        }

        function getCasesByKeyword(pageIndex)
        {
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '&$top=' + vm.defaultPageSize + '&$skip=' + skipCount + vm.filterQuery + "&$orderby=LastEditedDateTime desc";
            getExistingCasesByKeyword(vm.searchQuery, options, function (response, status) {
                vm.cases = response.items;
                vm.totalNumberOfCases = response.count;
                vm.allPages.length = Math.ceil(vm.totalNumberOfCases / vm.defaultPageSize);
            }, function (reponse, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.cases = [];
                vm.totalNumberOfCases = 0;
            });
        }

        function getExistingCasesByKeyword(keyword, options, cbSuccess, cbError)
        {
            $http.get('api/search/cases?keyword=' + keyword + options)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function resetSearchResult()
        {
            vm.isCaseSearchActive = false;
            vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
            vm.caseFilterOption = "All";
            fetchAndStoreCasesList(0);
        }

        function openAssignCaseModal(selectedCase)
        {
            vm.case = selectedCase;
            getAllUsers();
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/assign-case-modal.view.html',
                size: 'md'
            });
            vm.modalInstance.result.then(function () {
                vm.case = undefined;
            }, function () {
                //The modal closed by click on on any other part of the screen
            });
        }

        function getAllUsers()
        {
            var options = "?$skip=0&$filter=Role/Name eq 'IHIUser' or Role/Name eq 'Admin'";
            vm.getExistingUsers(options, function (response, status) {
                vm.users = response.items;
                getCaseRelatedICIUsers(vm.case.caseId);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.users = []; })
        }

        function fetchAndStoreDiscountsList() {
            getExistingDiscounts(function (response, status) { vm.discountOptions = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.discountOptions = []; });
        }

        function getExistingDiscounts(cbSuccess, cbError)
        {
            $http.get('/api/discounts')
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function openAddCaseModal() {
            vm.case = {};
            fetchAndStoreClientsData();
            getAllExistingCaseStatus();
            clearAddCaseFormPreFilledValues();
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/add-case-modal.view.html',
                size: 'lg'
            });           
            vm.modalInstance.result.then(function () {
                vm.case = undefined;
            }, function () {
                //The modal closed by click on on any other part of the screen
            });
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
        }

        function onNFCSelected() {
            toastr.success("NFC Selected");
        }

        function validateCaseFormData() {
            vm.showValidationMessage = true;
            var isValid = true;
            if (!vm.case.client || vm.case.client.clientName == null || vm.case.client.clientName == "") {
                isValid = false;
            }
            if(isValid)
            if (!vm.case.reports || vm.case.reports == null || vm.case.reports == "") {
                if (!vm.isNewReportAdded)
                    addNewReport();
                isValid = false;
            }
            if(isValid)
            for (var count = 0; count < vm.case.reports.length; count++) {
                if (!vm.case.reports[count]){
                    isValid = false;
                    break;
                }
            }
            if(isValid)
            if (!vm.case.reportsCountries || vm.case.reportsCountries == null || vm.case.reportsCountries == "") {
                isValid = false;
            }
            if(isValid)
            for (var count = 0; count < vm.case.reports.length; count++) {
                if (!vm.case.reportsCountries[count]) {
                    isValid = false;
                    break;
                }
            }
            if(isValid)
            for (var count = 0; count < vm.case.reports.length; count++) {
                if (vm.case.reports[count].isOtherReportType)
                {
                    if (!vm.case.otherReportTypes[count] || vm.case.otherReportTypes[count] == null || vm.case.otherReportTypes[count] == "") {
                        isValid = false;
                        break;
                    }
                }
            }
            if(isValid)
            if (!vm.case.reportSubject || vm.case.reportSubject == null || vm.case.reportSubject == "") {
                isValid = false;
            }
            if(isValid)
            if (vm.case.subjectType < 0){
                isValid = false;
            }
            if(isValid)
            if (vm.case.references == null || vm.case.references == undefined) {
                isValid = false;
            }
            if(isValid)
            if (vm.case.speed == null || vm.case.speed == undefined) {
                isValid = false;
            }
            if(isValid)
            if (vm.case.reputation == null || vm.case.reputation == undefined) {
                isValid = false;
            }
            if(isValid)
            if (vm.case.creditReport == null || vm.case.creditReport == undefined) {
                isValid = false;
            }
            if(isValid)
            if (vm.case.clientProvidedAttachments == null || vm.case.clientProvidedAttachments == undefined) {
                isValid = false;
            }
            if(isValid)
            if (vm.isReportCountryUSA) {
                if (vm.case.consentReceived == null || vm.case.consentReceived == undefined) {
                    isValid = false;
                }
                if(isValid)
                if (vm.case.agreementReceived == null || vm.case.agreementReceived == undefined) {
                    isValid = false;
                }
                if(isValid)
                if (vm.case.salaryGreaterThan75k == null || vm.case.salaryGreaterThan75k == undefined) {
                    isValid = false;
                }
            }
            if(isValid)
            if (vm.case.isProBonoGratis) {
                if (!vm.case.whyProBonoGratis || vm.case.whyProBonoGratis == null || vm.case.whyProBonoGratis == "") {
                    isValid = false;
                }
            }
            if(isValid)
            if (vm.case.subjectType == vm.allSubjectTypes.Company) {
                if (vm.case.isSpecificPrincipal == null || vm.case.isSpecificPrincipal == undefined) {
                    isValid = false;
                }
                if(isValid)
                if (vm.case.isSpecificPrincipal) {
                    for (var index = 0; index < vm.case.principalNames.length;index++)
                    {
                        if (!vm.case.principalNames[index].principalName || vm.case.principalNames[index].principalName == null || vm.case.principalNames[index].principalName == "") {
                            isValid = false;
                            break;
                        }
                    }
                }
            }
            if (!isValid)
            {
                $location.hash('edit_case_errors_msg');
                $anchorScroll();
            }
            return isValid;
        }

        function saveCaseOnAddCase(isValid) {
            if (isValid == false) { return; }
            if (vm.case.client) {
                vm.case.clientName = vm.case.client.clientName;
            }
            vm.case.statusId = vm.caseStatus.filter(function (item) { return item.status == 'NEW'; return; })[0].id;
            vm.saveNewCase(vm.case, function (response, status) {
                vm.closeModal();
                if (vm.pageIndex == undefined)
                    vm.pageIndex = 0;
                toastr.success("Case added");
                vm.caseFilterOption = "All";
                vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
                vm.isCaseSearchActive ? getCasesByKeyword(vm.pageIndex) : fetchAndStoreCasesList(vm.pageIndex);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveCase() {
            if (!validateCaseFormData()) { return; }
            if (vm.case.selectedDiscount) {
                vm.case.discount = vm.case.selectedDiscount.discountAmount;
            }
            if (vm.case.client) {
                vm.case.clientName = vm.case.client.clientName;
            }
            if (vm.case.subjectType != vm.allSubjectTypes.Company) {
                vm.case.isSpecificPrincipal = false;
            }
            if (vm.case.isSpecificPrincipal == false || vm.case.principalNames == null) {
                vm.case.principalNames = null;
                vm.case.numberOfPrincipals = 0;
            }
            else {
                vm.case.numberOfPrincipals = vm.case.principalNames.length;
            }
            if (vm.isReportCountryUSA == false) {
                vm.case.fcraCertificationEmailReceivedFromClient = false;
                vm.case.consentReceived = false;
                vm.case.agreementReceived = false;
                vm.case.stateOfEmploymentId = 0;
                vm.case.stateOfResidenceId = 0;
                vm.case.salaryGreaterThan75k = false;
            }
            if (vm.case.isProBonoGratis == false) {
                vm.case.whyProBonoGratis = null;
            }
            vm.saveEditedCase(vm.case, function (response, status) {
                vm.closeModal();
                toastr.success("Case edited");
                vm.caseFilterOption = "All";
                vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
                vm.isCaseSearchActive ? getCasesByKeyword(vm.pageIndex) : fetchAndStoreCasesList(vm.pageIndex);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function fetchAndStoreClientsData() {
            getExistingClients(function (response, status) {
                vm.clients = response;
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.clients = []; });
        }

        function getExistingClients(cbSuccess, cbError)
        {
            $http.get('/api/GetAllClients')
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function fetchAndStoreIndustryList() {
            vm.getExistingIndustries(function (response, status) { vm.industries = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.industries = []; });
        }

        function populateIndustrySubCategory() {
            vm.getExistingIndustrySubcategories(function (response, status) { vm.industrySubCategories = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.industrySubCategories = []; });
        }

        function fetchAndStoreCountryList() {
            vm.getExistingCountries(function (response, status) { vm.countries = response; vm.reportCountries = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.countries = []; vm.reportCountries = []; });
        }

        function refreshResults($select) {
            var search = $select.search,
              list = angular.copy($select.items),
              FLAG = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.clientId !== FLAG;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    clientId: FLAG,
                    clientName: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }       

        function refreshDiscounts($select) {
            var search = $select.search,
              list = angular.copy($select.items),
              FLAG = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.discountId !== FLAG;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    discountId: FLAG,
                    discountAmount: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }

        function clearAddCaseFormPreFilledValues() {
            vm.case.principalNames = [];
            vm.isNewPrincipalAdded = false;
            vm.showValidationMessage = false;
            vm.showExistingCaseFiles = false;
            vm.case.reports = [];
            vm.isNewReportAdded = false;
            vm.reportDetail = {};
            vm.case.otherReportTypes = [];
            vm.case.selectedDiscount = {};
            vm.isPrimary = false;
            vm.isReportCountryUSA = false;
            vm.isReportFileUploaded = false;
            vm.numberOfFilesToBeUploaded = 0;
            vm.filesUploaded = 0;
            vm.case.primaryReportFiles = [];
            vm.case.reportsCountries = [];
            vm.case.files = [];
            vm.case.client = undefined;
            vm.case.nickName = undefined;
            vm.case.clientName = undefined;
            vm.case.clientPO = undefined;
            vm.case.businessUnit = undefined;
            vm.case.accountCode = undefined;
            vm.case.contactName = undefined;
            vm.case.positionHeldbyContact = undefined;
            vm.case.phoneNumber = undefined;
            vm.case.emailAddress = undefined;
            vm.case.street = undefined;
            vm.case.city = undefined;
            vm.case.zipCode = undefined;
            vm.industry = undefined;
            vm.industrySubCategory = undefined;
            vm.case.salesRepresentativeName = undefined;
            vm.case.salesRepresentativeEmail = undefined;
            vm.region = undefined;
            vm.country = undefined;
            vm.state = undefined;
            vm.case.references = null;
            vm.case.speed = null;
            vm.case.reputation = null;
            vm.case.clientProvidedAttachments = null;
            vm.case.creditReport = null;
            vm.case.isNFCCase = null;
        }

        function onClientSelected($item) {
            vm.case.client = $item;          
            if (!$item.industryId) {
                vm.industry = undefined;
            }
            else {
                vm.case.client.industryId = $item.industryId;
                vm.industry = { name: $item.industryName, industryId: $item.industryId };
            }
            if (!$item.industrySubCategoryId) {
                vm.industrySubCategory = undefined;
            }
            else {
                vm.case.client.industrySubCategoryId = $item.industrySubCategoryId;
                vm.industrySubCategory = { name: $item.industrySubCategoryName, industrySubCategoryId: $item.industrySubCategoryId };
            }
            if ($item.countryId == undefined) {
                vm.country = undefined;
                vm.state = undefined;
            }

            else {
                vm.case.client.countryId = $item.countryId;
                vm.case.client.stateId = $item.stateId;
                vm.country = { countryName: $item.countryName, countryId: $item.countryId };
                vm.state = { stateName: $item.stateName, stateId: $item.stateId };
            }
            if ($item.salesRepresentative != null) {
                vm.case.client.salesRepresentativeName = $item.salesRepresentative.firstName + " " + $item.salesRepresentative.lastName;
            }
            else {
                vm.case.client.salesRepresentativeName = null;
            }
        }

        function clear($event, $select) {
            $event.stopPropagation();
            $select.selected = undefined;
            $select.search = undefined;
            $select.activate();
        }

        function assignCaseToUser(isValid) {
            if (!isValid) { return;}
            saveAssignedCase(vm.case.caseId, vm.case.assignedToUser.id, function (response, status) {
                toastr.success("Case assigned to " + vm.case.assignedToUser.firstName + " " + vm.case.assignedToUser.lastName);
                closeModal();
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveAssignedCase(caseId, userId, cbSuccess, cbError)
        {
            $http.post('/api/case/' + caseId + '/' + userId + '/user')
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function clearSelectedReportCountry($event, $select, $index) {
            if ($index == 0) {
                vm.isReportCountryUSA = false;
            }
            clear($event, $select);
        }

        function clearSelectedReport($event, $select, $index) {
            if ($index == 0) {
                vm.isOtherReportType = false;
            }
            clear($event, $select);
        }

        function clearSelectedPrincipal($index) {
            vm.case.principalNames.splice($index, 1);
        }

        function getFilterForEachOption(caseFilterOption) {
            switch (caseFilterOption) {
                case Enums.CaseFilter.New_Case: vm.filterQuery = "&$filter= SubStatus/Name eq 'Requested' and IsDeleted eq false"; vm.caseFilterOption = "New"; break;
                case Enums.CaseFilter.My_Cases: vm.filterQuery = "&$filter= AssignedTo eq (guid'" + vm.currentUser.userId + "') or CreatedBy eq (guid'" + vm.currentUser.userId + "') and IsDeleted eq false and SubStatus/Name ne 'Requested'"; vm.caseFilterOption = "My"; break;
                case Enums.CaseFilter.Completed: vm.filterQuery = "&$filter= SubStatus/Name eq 'Completed' and IsDeleted eq false and SubStatus/Name ne 'Requested'"; vm.caseFilterOption = "Completed"; break;
                case Enums.CaseFilter.Archived: vm.filterQuery = "&$filter=IsDeleted eq true and SubStatus/Name ne 'Requested'"; vm.caseFilterOption = "Archived"; break;
                case Enums.CaseFilter.All_Cases: vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'"; vm.caseFilterOption = "All"; break;
            }
            vm.isCaseSearchActive ? getCasesByKeyword(0) : fetchAndStoreCasesList(0);
        }

        function fetchAndStoreCasesList(pageIndex) {
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = '?$skip=' + skipCount + "&$top=" + vm.defaultPageSize + vm.filterQuery + "&$orderby=LastEditedDateTime desc";
            vm.getExistingCases(options, function (response, status) {
                vm.cases = response.items;
                vm.totalNumberOfCases = response.count;
                vm.allPages.length = Math.ceil(vm.totalNumberOfCases / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.cases = [];
                vm.totalNumberOfCases = 0;
            });
        }

        function deleteAddedReport(report) {
            vm.case.reports.splice(report, 1);
            vm.case.reportsCountries.splice(report, 1);
            if (vm.case.reports.length == 0) {
                vm.isNewReportAdded = false;
                vm.isPrimary = false;
            }
        }

        function fetchAllReportTypes() {
            getExistingReportTypes(function (response, status) { vm.reports = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.reports = []; });
        }

        function getExistingReportTypes(cbSuccess, cbError)
        {
            $http.get('/api/reports')
                .success(function (response, status) {
                    cbSuccess(response, status);
                }).error(function (response, status) {
                    cbError(response, status);
                });
        }

        function addNewReport() {
            vm.isNewReportAdded = true;
            vm.case.reports.length++;

        }
        function addNewPrincipal() {
            vm.isNewPrincipalAdded = true;
            vm.case.principalNames.push({ "id": null, "principalName": "" });
        }

        function onReportSelected($item, $index) {
            vm.case.reports[0].isPrimary = true;
            vm.isPrimary = true;
            vm.reportDetail = $item;
            if (vm.case.reports[$index].reportType == "Other") {
                vm.case.reports[$index].isOtherReportType = true;
            }
            else {
                vm.case.reports[$index].isOtherReportType = false;
            }
            if (vm.case.reportsCountries[$index]) {
                if (vm.case.reportsCountries[0].countryName == "United States" && vm.case.reports[0].reportType == "Pre-Employment") {
                    vm.isReportCountryUSA = true;
                }
                else {
                    if ($index == 0) {
                        vm.isReportCountryUSA = false;
                    }
                }
            }
            if (vm.case.reports[$index].reportType != "Other") {
                vm.reportPreviewModalInstance = $uibModal.open({
                    templateUrl: '/webapp/modules/cases/views/report-details-description-modal.view.html',
                    size: 'lg'
                });
            }
        }

        function closeReportPreviewModal() {
            vm.reportPreviewModalInstance.dismiss('cancel');
        }

        function archiveCase(caseId) {
            deleteExistingCase(caseId, function (response, status) {
                vm.caseFilterOption = "All";
                vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
                if (vm.cases.length < 1)
                    vm.isCaseSearchActive ? getCasesByKeyword(vm.pageIndex - 1) : fetchAndStoreCasesList(vm.pageIndex - 1);
                else
                    vm.isCaseSearchActive ? getCasesByKeyword(vm.pageIndex) : fetchAndStoreCasesList(vm.pageIndex);
                toastr.success("Case archived");
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingCase(caseId, cbSuccess, cbError)
        {
            $http.delete("api/case/" + caseId)
               .success(function (response, status) {
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
        }

        function unarchiveCase(selectedCase)
        {
            selectedCase.isdeleted = false;
            vm.saveEditedCase(selectedCase, function (response, status) {
                vm.caseFilterOption = "All";
                vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
                vm.isCaseSearchActive ? getCasesByKeyword(vm.pageIndex) : fetchAndStoreCasesList(vm.pageIndex);
                toastr.success("Case unarchived");
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function editCase(caseId) {
            getCaseDetails(caseId);            
            openEditCaseModal();
        }

        function openEditCaseModal() {
            vm.showValidationMessage = false;
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/edit-case-modal.view.html',
                size: 'lg'
            });        
            vm.modalInstance.result.then(function () {
            }, function () {
                //The modal closed by click on on any other part of the screen
            });
        }

        function getCaseDetails(caseId) {
            getExistingCaseByCaseId(caseId, function (response, status) {
                vm.case = response;
                vm.case.otherReportTypes = [];
                vm.case.reports = response.reports;
                vm.case.files = response.files;
                vm.case.selectedDiscount = { discountAmount: vm.case.discount };
                vm.case.reportsCountries = [];
                if (vm.case.principalNames == null) {
                    vm.case.principalNames = [];
                }
                if (vm.case.client != null) {
                    if (vm.case.client.salesRepresentative) {
                        vm.case.client.salesRepresentativeName = vm.case.client.salesRepresentative.firstName + " " + vm.case.client.salesRepresentative.lastName;
                    }
                    if (vm.case.client.industryId == 0 || vm.case.client.industryId == null) {
                        vm.industry = undefined;
                    }
                    else {
                        vm.industry = { name: response.client.industryName, id: response.client.industryId };
                    }
                    if (vm.case.client.industrySubCategoryId == 0 || vm.case.client.industrySubCategoryId == null) {
                        vm.industrySubCategory = undefined;
                    }
                    else {
                        vm.industrySubCategory = { name: response.client.industrySubCategoryName, id: response.client.industrySubCategoryId };
                    }
                    if (vm.case.clientContactUser != null) {
                        if (vm.case.clientContactUser.countryId == 0 || vm.case.clientContactUser.countryId == null) {
                            vm.country = undefined;
                        }
                        else {
                            vm.country = { countryName: response.clientContactUser.countryName, countryId: response.clientContactUser.countryId };
                        }
                        if (vm.case.clientContactUser.stateId == 0 || vm.case.clientContactUser.stateId == null) {
                            vm.state = undefined;
                        }
                        else {
                            vm.state = { stateName: response.clientContactUser.stateName, stateId: response.clientContactUser.stateId };
                        }
                    }
                }
                for (var i = 0; i < response.reports.length; i++) {
                    var existingReportCountry = {};
                    existingReportCountry.countryId = response.reports[i].countryId;
                    existingReportCountry.countryName = response.reports[i].countryName;
                    vm.case.reportsCountries.push(existingReportCountry);
                }
                if (vm.case.reports.length > 0) {
                    vm.isNewReportAdded = true;
                    vm.isPrimary = true;
                    if (vm.case.reports[0].countryName == "United States") {
                        if (vm.case.reports[0].reportType == "Pre-Employment")
                        { vm.isReportCountryUSA = true; }
                        fetchStatesListBasedOnCountryId(vm.case.reports[0].countryId);
                    }
                    else {
                        vm.isReportCountryUSA = false;
                    }
                    for (var i = 0; i < vm.case.reports.length; i++) {
                        if (vm.case.reports[i].countryId == 0)
                            vm.case.reportsCountries[i] = undefined;
                        if (vm.case.reports[i].isOtherReportType)
                            vm.case.otherReportTypes[i] = vm.case.reports[i].otherReportTypeName;
                    }
                } else {
                    vm.isNewReportAdded = false;
                    vm.isPrimary = false;
                    vm.case.reportsCountries[0] = undefined;
                }
                if (vm.case.files.length > 0) {
                    vm.showExistingCaseFiles = true;
                } else {
                    vm.showExistingCaseFiles = false;
                }
                if (vm.case.stateOfEmploymentId == 0 || vm.case.stateOfEmploymentId == null) {
                    vm.case.stateOfEmployment = undefined;
                }
                else {
                    vm.case.stateOfEmployment = { stateName: response.stateOfEmploymentName, stateId: response.stateOfEmploymentId };
                    onStateOfEmploymentSelected(vm.case.stateOfEmployment);
                }
                if (vm.case.stateOfResidenceId == 0 || vm.case.stateOfResidenceId == null) {
                    vm.case.stateOfResidence = undefined;
                }
                else {
                    vm.case.stateOfResidence = { stateName: response.stateOfResidenceName, stateId: response.stateOfResidenceId };
                    onStateOfResidenceSelected(vm.case.stateOfResidence);
                }
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getExistingCaseByCaseId(caseId, cbSuccess, cbError)
        {
            $http.get('/api/case/' + caseId)
               .success(function (response, status) {
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
        }

        function uploadFiles(files) {
            if (files && files.length) {
                vm.numberOfFilesToBeUploaded = files.length;
                for (var i = 0; i < files.length; i++) {
                    vm.uploadFilesToBlobStorage({ data: { file: files[i] } });
                }
            }
        }

        function uploadReportFiles(files) {
            if (files && files.length) {
                vm.numberOfFilesToBeUploaded = files.length;
                vm.isReportFileUploaded = true;
                for (var i = 0; i < files.length; i++) {
                    vm.uploadFilesToBlobStorage({ data: { file: files[i] } });
                }
            }
        }

        function uploadFilesToBlobStorage(file) {
            usSpinnerService.spin('spinner');
            vm.uploadNewFile(file, function (resp) {
                vm.filesUploaded++;
                for (var i = 0; i < resp.data.length; i++) {
                    if (vm.isReportFileUploaded) {
                        vm.case.primaryReportFiles.push(resp.data[i]);
                    }
                    else {
                        vm.case.files.push(resp.data[i]);
                    }
                }
                if (!vm.isReportFileUploaded) {
                    if (vm.case.files.length > 0) {
                        vm.showExistingCaseFiles = true;
                    }
                }

                if (vm.numberOfFilesToBeUploaded == vm.filesUploaded) {
                    vm.filesUploaded = 0;
                    usSpinnerService.stop('spinner');
                    vm.isReportFileUploaded = false;
                    toastr.success("File(s) uploaded");
                }
            }, function (resp) { usSpinnerService.stop('spinner'); ErrorHandlerService.DisplayErrorMessage(resp.data, resp.status); });
        }

        function deleteCaseAssociatedFile(file) {
            vm.case.files.splice(file, 1);
            if (vm.case.files.length == 0) {
                vm.showExistingCaseFiles = false;
            }
        }

        function onReportCountrySelected($item, $index) {
            if (vm.case.reportsCountries[0].countryName == "United States" && vm.case.reports[0].reportType == "Pre-Employment") {
                vm.isReportCountryUSA = true;
                fetchStatesListBasedOnCountryId(vm.case.reportsCountries[0].countryId);
            }
            else {
                if ($index == 0) {
                    vm.isReportCountryUSA = false;
                }
            }
        }

        function fetchStatesListBasedOnCountryId(countryId) {
            vm.getExistingStatesByCountryId(countryId, function (response, status) { vm.countrySpecificStates = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.countrySpecificStates = []; });
        }

        function onStateOfResidenceSelected($item) {
            vm.case.stateOfResidenceId = $item.stateId;
        }

        function onStateOfEmploymentSelected($item) {
            vm.case.stateOfEmploymentId = $item.stateId;
        }
        function downloadFile(fileId) {
            vm.downloadExistingFile(fileId, Enums.DownloadFileType.CaseFile, 
                function (response, status) { var fileUrl = response; window.location.href = fileUrl; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function openCaseDetails(selectedCase) {
            switch(vm.roleName)
            {
                case 'User': $location.path('/case-management/cases/' + selectedCase.caseNumber + '/participants'); break;
                case 'ICIUser': case 'NFCUser': $location.path('/case-management/cases/' + selectedCase.caseNumber + '/tasks'); break;
                default: $location.path('/case-management/cases/' + selectedCase.caseNumber + '/caseDetails');
            }
        }
       
        function onClientContactNameSelected($item) {        
            vm.case.clientContactUserId = $item.id;
            vm.case.clientContactUser = $item;
            if ($item.countryId == 0 || $item.countryId == null) {
                vm.country = undefined;
            }
            else {
                vm.country = { countryName: $item.countryName, countryId: $item.countryId };
            }
            if ($item.stateId == 0 || $item.stateId == null) {
                vm.state = undefined;
            }
            else {
                vm.state = { stateName: $item.stateName, stateId: $item.stateId };
            }
        }
      
        function fetchAndStoreSubStatusList() {
            vm.getExistingCaseSubStatusList(function (response, status) { vm.subStatusList = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.subStatusList = []; });
        }

        function onSubStatusSelected(item) {
            vm.case.subStatusId = item.id;
        }

        function filterUsersByRole(user) {
            var roleName = user.role.name;
            if (roleName == "ICIUser")
                return "ICI-Users";
            else
            {
                if(roleName == "IHIUser")
                    return "IHI-Users";

                else if (roleName == "Admin") {
                    return "Admin";
                }
            }

        }

        function getCaseRelatedICIUsers(caseId) {
            vm.getExistingCaseRelatedICIUsers(caseId, function (response, status) {
                vm.listOfICIUsers = response.map(function (item) { return item.user; });
                vm.users = vm.users.concat(vm.listOfICIUsers);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.listOfICIUsers = []; });
        }

        function getAllExistingCaseStatus() {
            vm.getExistingCaseStatus(function (response, status) {
                vm.caseStatus = response;},
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.caseStatus = []; });
        }

        vm.init = init;
        vm.openAddCaseModal = openAddCaseModal;
        vm.closeModal = closeModal;
        vm.onStateOfEmploymentSelected = onStateOfEmploymentSelected;
        vm.onStateOfResidenceSelected = onStateOfResidenceSelected;
        vm.saveCase = saveCase;
        vm.saveCaseOnAddCase = saveCaseOnAddCase;
        vm.onClientSelected = onClientSelected;
        vm.fetchAndStoreClientsData = fetchAndStoreClientsData;
        vm.fetchAndStoreCasesList = fetchAndStoreCasesList;
        vm.fetchAndStoreCountryList = fetchAndStoreCountryList;
        vm.fetchAndStoreIndustryList = fetchAndStoreIndustryList;
        vm.refreshResults = refreshResults;
        vm.refreshDiscounts = refreshDiscounts;
        vm.deleteAddedReport = deleteAddedReport;
        vm.addNewReport = addNewReport;
        vm.addNewPrincipal = addNewPrincipal;
        vm.fetchAllReportTypes = fetchAllReportTypes;
        vm.onReportSelected = onReportSelected;
        vm.clear = clear;
        vm.clearAddCaseFormPreFilledValues = clearAddCaseFormPreFilledValues;
        vm.editCase = editCase;
        vm.archiveCase = archiveCase;
        vm.unarchiveCase = unarchiveCase;
        vm.openEditCaseModal = openEditCaseModal;
        vm.getCaseDetails = getCaseDetails;
        vm.uploadFiles = uploadFiles;
        vm.uploadFilesToBlobStorage = uploadFilesToBlobStorage;
        vm.deleteCaseAssociatedFile = deleteCaseAssociatedFile;
        vm.closeReportPreviewModal = closeReportPreviewModal;
        vm.onReportCountrySelected = onReportCountrySelected;
        vm.fetchStatesListBasedOnCountryId = fetchStatesListBasedOnCountryId;
        vm.uploadReportFiles = uploadReportFiles;
        vm.getExistingDiscounts = getExistingDiscounts;
        vm.populateIndustrySubCategory = populateIndustrySubCategory;
        vm.getFilterForEachOption = getFilterForEachOption;
        vm.onNFCSelected = onNFCSelected;
        vm.assignCaseToUser = assignCaseToUser;
        vm.downloadFile = downloadFile;
        vm.onClientContactNameSelected = onClientContactNameSelected;
        vm.fetchAndStoreSubStatusList = fetchAndStoreSubStatusList;
        vm.onSubStatusSelected = onSubStatusSelected;
        vm.getExistingClients = getExistingClients;
        vm.fetchAndStoreDiscountsList = fetchAndStoreDiscountsList;
        vm.clearSelectedReportCountry = clearSelectedReportCountry;
        vm.clearSelectedPrincipal = clearSelectedPrincipal;
        vm.validateCaseFormData = validateCaseFormData;
        vm.openCaseDetails = openCaseDetails;
        vm.openAssignCaseModal = openAssignCaseModal;
        vm.getExistingReportTypes = getExistingReportTypes;
        vm.getAllUsers = getAllUsers;
        vm.searchCases = searchCases;
        vm.deleteExistingCase = deleteExistingCase;
        vm.getCasesByKeyword = getCasesByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.getExistingCasesByKeyword = getExistingCasesByKeyword;
        vm.getExistingCaseByCaseId = getExistingCaseByCaseId;
        vm.saveAssignedCase = saveAssignedCase;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.getExistingIndustries = $global.getExistingIndustries;
        vm.getExistingIndustrySubcategories = $global.getExistingIndustrySubcategories;
        vm.getExistingCountries = $global.getExistingCountries;
        vm.uploadNewFile = $global.uploadNewFile;
        vm.getExistingStatesByCountryId = $global.getExistingStatesByCountryId;
        vm.downloadExistingFile = $global.downloadExistingFile;
        vm.saveEditedCase = $global.saveEditedCase;
        vm.getExistingCaseSubStatusList = $global.getExistingCaseSubStatusList;
        vm.saveNewCase = $global.saveNewCase;
        vm.getExistingCases = $global.getExistingCases;
        vm.defaultPageSize = $window.defaultPageSize;
        vm.filterQuery = "&$filter=IsDeleted eq false and SubStatus/Name ne 'Requested'";
        vm.filterUsersByRole = filterUsersByRole;
        vm.getExistingCaseRelatedICIUsers = $global.getExistingCaseRelatedICIUsers;
        vm.getExistingCaseStatus = $global.getExistingCaseStatus;
        return vm;
    }
    ]);
})();
