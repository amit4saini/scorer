﻿(function () {
    'use strict';
    angular.module('case').factory('CaseDiscussions', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;

        vm.tinyMceOptions = {
            menubar: 'file edit format',
            plugins: ['mention link'],
            mentions: {
            },
            statusbar: false,
            default_link_target: "_blank"
        };

        function discussionsInit() {
            setCommonVariables();
            vm.allDiscussions = [];
            vm.allDiscussionPages = [];
            vm.discussion = {};
            vm.discussion.files = [];
            vm.showNotes = false;
            vm.isDiscussionSearchActive = false;           
            vm.getCaseFromCaseNumber(vm.caseNumber, function () {
                vm.case = CaseHeader.case;
                fetchAndStoreDiscussionsList(0);
                getCaseRelatedUsers(vm.case.caseId);
            });
        }

        function discussionMessagesInit() {
            setCommonVariables();
            vm.selectedDiscussion = {};
            vm.selectedDiscussion.id = $stateParams.discussionId;
            vm.editDiscussionMessageId = 0;
            vm.showValidationMessage = false;
            vm.newDiscussionMessage = { files: [] };
            vm.allDiscussionMessages = [];
            vm.allDiscussionMessagePages = [];
            vm.discussionMessageFilesUploaded = 0;
            vm.newDiscussionMessage.createdBy = vm.currentUser.id;
            vm.getCaseFromCaseNumber(vm.caseNumber, function () {
                vm.case = CaseHeader.case;
                getDiscussionDetails();
                getCaseRelatedUsers(vm.case.caseId);
            });
        }

        function setCommonVariables() {
            
            vm.caseNumber = $stateParams.caseNumber;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.currentUser = $global.user;
            vm.currentUser.id = $global.user.userId;
            vm.roleName = $global.roleName;
            vm.isDiscussionBeingEdited = false;
            vm.numberOfDiscussionFilesToBeUploaded = 0;
            vm.discussionFilesUploaded = 0;
            vm.showDiscussionValidationMessage = false;
            vm.listOfUserIds = [];
            vm.users = [];
            vm.areAllFieldsValid = false;
            vm.showExistingDiscussionFiles = false;
            vm.discussionAction = Enums.DiscussionAction;
        }

        function goToDiscussionsList() {
            vm.isDiscussionBeingEdited = false;
            $location.path('/case-management/cases/' + $stateParams.caseNumber + '/discussions');
        }

        function toggleDiscussionAsNote()
        {
            vm.isDiscussionBeingEdited = true;
            vm.isMarkedAsNote = true;
            vm.discussion = vm.selectedDiscussion;
            vm.discussion.isNotes = !vm.discussion.isNotes;
            postDiscussion(true, vm.discussion.isNotes ? vm.discussionAction['Marked as report'] : vm.discussionAction['Unmarked as report']);
        }

        function getDiscussionDetails()
        {
            getSelectedDiscussion(vm.selectedDiscussion.id,
                function (response, status) {
                    vm.selectedDiscussion = response;
                    vm.selectedDiscussion.creationDateTime = new Date(vm.selectedDiscussion.creationDateTime);
                    getAllDiscussionMessages();                                      
                }, function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.selectedDiscussion = {};
                });
        }

        function getSelectedDiscussion(discussionId, cbSuccess, cbError) {
            $http.get("api/discussion/" + discussionId)
            .success(function (response, status) {
                cbSuccess(response,status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function goToDiscussionMessages(discussion) {
            if(discussion.caseNumber == undefined)
                $location.path('/case-management/cases/' + vm.caseNumber + '/discussions/' + discussion.id);
            else
                $location.path('/case-management/cases/' + discussion.caseNumber + '/discussions/' + discussion.id);
        }

        function getAllDiscussionMessages()
        {
            var options = "?$skip=0&$orderby=CreationDateTime desc";
            getExistingDiscussionMessages(vm.selectedDiscussion.id, options,
                function (response, status) {
                    vm.allDiscussionMessages = response.items
                        .map(function (item) { item.creationDateTime = new Date(item.creationDateTime); return item; });
                },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.allDiscussionMessages = []; });
        }

        function getExistingDiscussionMessages(discussionId, options, cbSuccess, cbError) {
            $http.get("api/discussion/" + discussionId + "/discussionMessages" + options)
            .success(function (response, status) {
                cbSuccess(response, status)
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function saveDiscussionMessage()
        {
            vm.showValidationMessage = true;
            if (vm.newDiscussionMessage.message === "" || vm.newDiscussionMessage.message == undefined) {
                if (vm.newDiscussionMessage.files == undefined || vm.newDiscussionMessage.files.length == 0)
                    return;
            }
            vm.newDiscussionMessage.discussionId = vm.selectedDiscussion.id;
            postDiscussionMessage(vm.newDiscussionMessage,
                function (response, status) {
                    toastr.success("Message added");
                    getDiscussionDetails();
                    vm.newDiscussionMessage = { files: [] , createdBy: vm.currentUser.id};
                    vm.showValidationMessage = false;
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveEditedDiscussionMessage(message)
        {
            vm.showValidationMessage = true;
            if (message.message === "" || message.message == undefined) {
                if (message.files == undefined || message.files.length == 0)
                    return;
            }
            message.createdByUser = null;
            updateDiscussionMessage(message,
                function (response, status) {
                    toastr.success("Message edited");
                    getDiscussionDetails();
                    vm.editDiscussionMessageId = 0;
                    vm.showValidationMessage = false;
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function postDiscussionMessage(message, cbSuccess, cbError) {
            $http.post("/api/DiscussionMessages", message)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function updateDiscussionMessage(message, cbSuccess, cbError) {
            $http.put("/api/DiscussionMessages/" + message.id, message)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function deleteDiscussionMessage(message)
        {
            deleteExistingDiscussionMessage(message.id, function (response, status) { toastr.success("Message archived"); getDiscussionDetails(); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingDiscussionMessage(messageId, cbSuccess, cbError) {
            $http.delete("/api/DiscussionMessages/" + messageId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function editDiscussion() {
            vm.isDiscussionBeingEdited = true;
            fetchAndStoreUsersList(function () { 
            vm.discussion = vm.selectedDiscussion;
            vm.listOfUserIds = vm.selectedDiscussion.userIdList;
            if (vm.listOfUserIds.length == 0)
                vm.addUsers = false;
            vm.listOfParticipantIds = vm.selectedDiscussion.participantIdList;
            if (vm.discussion.discussionFiles != null)
                if (vm.discussion.discussionFiles.length > 0) {
                    vm.showExistingDiscussionFiles = true;
                }
            openEditDiscussionModal();
            });
        }

        function openEditDiscussionModal() {
            vm.isDiscussionView = true;
            openDiscussionModalWindow();
            vm.modalInstance.result.then(function () {
            }, function () {
                vm.isDiscussionBeingEdited = false;
            });
        }

        
        function uploadDiscussionMessageFiles(files, messageId) {
            if (files && files.length) {
                vm.numberOfDiscussionMessageFilesToBeUploaded = files.length;
                for (var i = 0; i < files.length; i++) {
                    vm.uploadDiscussionMessageFilesToBlobStorage({ data: { file: files[i] } }, messageId);
                }
            }
        }

        function uploadDiscussionMessageFilesToBlobStorage(file, messageId) {
            usSpinnerService.spin('spinner');
            vm.uploadNewFile(file, function (resp) {
                vm.discussionMessageFilesUploaded++;
                if (messageId == -1) {
                    for (var i = 0; i < resp.data.length; i++) {
                        vm.newDiscussionMessage.files.push(resp.data[i]);
                    }
                }
                else {
                    var index = 0;
                    for (; index < vm.allDiscussionMessages.length; index++) {
                        if (vm.allDiscussionMessages[index].id == messageId)
                            break;
                    }
                    for (var i = 0; i < resp.data.length; i++) {
                        vm.allDiscussionMessages[index].files.push(resp.data[i]);
                    }
                }
                if (vm.numberOfDiscussionMessageFilesToBeUploaded == vm.discussionMessageFilesUploaded) {
                    vm.discussionMessageFilesUploaded = 0;
                    usSpinnerService.stop('spinner');
                    toastr.success("File(s) uploaded");
                }
            }, function (resp) {
                usSpinnerService.stop('spinner');
                ErrorHandlerService.DisplayErrorMessage(resp.data);
            });
        }

        function deleteDiscussionMessageAssociatedFile(file, messageId) {
            if (messageId == -1) {
                for (var fileIndex = 0; fileIndex < vm.newDiscussionMessage.files.length; fileIndex++)
                    if (vm.newDiscussionMessage.files[fileIndex].fileId == file.fileId)
                    { vm.newDiscussionMessage.files.splice(fileIndex, 1); toastr.success("File archived"); return; }
            }
            else {
                var index=0;
                for (; index < vm.allDiscussionMessages.length; index++)
                {
                    if (vm.allDiscussionMessages[index].id == messageId)
                        break;
                }
                for (var fileIndex = 0; fileIndex < vm.allDiscussionMessages[index].files.length; fileIndex++)
                    if (vm.allDiscussionMessages[index].files[fileIndex].fileId == file.fileId)
                    { vm.allDiscussionMessages[index].files.splice(fileIndex, 1); toastr.success("File archived"); return; }
            }
        }

        function fetchAndStoreDiscussionsList(discussionPageIndex) {
            if (vm.showNotes == true)
            {
                vm.filter = "&$filter=IsNotes eq true";
            }
            else
            {
                vm.filter = "";
            }
            vm.currentRoute = $location.path().split("/")[4];
            if (vm.roleName == 'Participant') {
                if (vm.currentRoute == "discussions") {
                    vm.filter = "&$filter=IsNotes eq false";
                    vm.url = "/case-management/cases/" + vm.caseNumber + "/discussions";
                }
                else {
                    vm.filter = "&$filter=IsNotes eq true";
                    vm.url = "/case-management/cases/" + vm.caseNumber + "/reports";
                }
            }
            else {
                vm.url = "/case-management/cases/" + vm.caseNumber + "/discussions";
            }
            
            vm.discussionPageIndex = discussionPageIndex;
            var skipCount = vm.discussionPageIndex * vm.defaultPageSize;
            var options = '?$top=' + vm.defaultPageSize + '&$skip=' + skipCount + "&$orderby=LastEditedDateTime desc" + vm.filter;
            getExistingCaseAssociatedDiscussions(vm.case.caseId, options,
                function (response, status) {
                    vm.allDiscussions = response.result.items
                    .map(function (item) { item.lastEditedDateTime = new Date(item.lastEditedDateTime); return item;});
                    vm.numberOfDiscussions = response.result.count;
                    if (vm.roleName == 'Participant'){
                        if (vm.currentRoute != "reports") {
                            CaseHeader.case.caseAssociatedDiscussionsCount = response.result.items.map(function (item) { if (item.isNotes == false) return item; }).length;
                            $global.case.caseAssociatedDiscussionsCount = response.result.items.map(function (item) { if (item.isNotes == true) return item; }).length;
                        }
                    }
                    else {
                        CaseHeader.case.caseAssociatedDiscussionsCount = response.totalDiscussionCount;
                        $global.case.caseAssociatedDiscussionsCount = response.totalDiscussionCount;
                    }
                    vm.allDiscussionPages.length = Math.ceil(vm.numberOfDiscussions / vm.defaultPageSize);
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.allDiscussions = [];
                    vm.numberOfDiscussions = 0;
                });
        }

        function getExistingCaseAssociatedDiscussions(caseId, options, cbSuccess, cbError)
        {
            $http.get('api/case/' + caseId + '/discussions' + options)
               .success(function (response, status) {
                   cbSuccess(response, status);
               })
               .error(function (response, status) {
                   cbError(response, status);
               });
        }

        function openAddDiscussionModal() {           
            vm.isDiscussionView = true;
            fetchAndStoreUsersList();
            clearAddDiscussionFormPreFilledValues();
            openDiscussionModalWindow();
        }

        function openDiscussionModalWindow() {
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/cases/views/add-discussion-modal.view.html',
                size: 'md'
            });
        }

        function validateDiscussion() {
            vm.showDiscussionValidationMessage = true;
            return vm.areAllFieldsValid;
        }

        function postDiscussion(isValid, discussionAction) {
            vm.areAllFieldsValid = isValid;
            if (!validateDiscussion()) { return; }
            if (!vm.isMarkedAsNote) {
                vm.discussion.userIdList = [];
                vm.discussion.participantIdList = [];
                vm.discussion.participantIdList = vm.listOfParticipantIds;
                if (vm.addUsers && vm.roleName != 'Participant'){
                    vm.discussion.userIdList = vm.listOfUserIds;
                }  
                else {
                    if (vm.roleName == 'Participant') {
                        vm.discussion.participantIdList = [];
                        vm.discussion.userIdList.push(CaseHeader.case.client.salesRepresentativeId);
                        vm.discussion.participantIdList.push(vm.currentUser.id);
                    }
                }
            }

            if (!vm.isDiscussionBeingEdited) {
                vm.discussion.caseId = vm.case.caseId;
                saveNewDiscussion(vm.discussion,
                    function (response, status) {
                        toastr.success("Discussion " + Object.keys(vm.discussionAction)[discussionAction].toLowerCase());
                        if (vm.discussion.discussionFiles != undefined && vm.discussion.discussionFiles.length > 0) {
                            CaseHeader.case.caseAllFilesCount += vm.discussion.discussionFiles.length;
                            $global.case.caseAllFilesCount += vm.discussion.discussionFiles.length;
                        }
                        closeModal();
                        vm.isDiscussionSearchActive ? getDiscussionsByKeyword(vm.discussionPageIndex) : fetchAndStoreDiscussionsList(vm.discussionPageIndex);
                    }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else {
                saveEditedDiscussion(vm.discussion, function (response, status) {
                    toastr.success("Discussion " + Object.keys(vm.discussionAction)[discussionAction].toLowerCase());
                    closeModal();
                    clearAddDiscussionFormPreFilledValues();
                },function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
        }

        function saveNewDiscussion(discussion, cbSuccess, cbError) {
            $http.post('/api/discussion', discussion)
               .success(function (response, status) {
                   cbSuccess(response, status);
               })
               .error(function (response, status) {
                   cbError(response, status);
               });
        }

        function saveEditedDiscussion(discussion, cbSuccess, cbError) {
            $http.put('/api/discussion/' + discussion.id, discussion)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        function clearAddDiscussionFormPreFilledValues() {
            vm.discussion = {};
            vm.discussion.discussionFiles = [];
            vm.listOfUserIds = [];
            vm.showExistingDiscussionFiles = false;
            vm.showDiscussionValidationMessage = false;
            vm.isMarkedAsNote = false;
        }

        function uploadDiscussionFiles(files) {
            if (files && files.length) {
                vm.numberOfDiscussionFilesToBeUploaded = files.length;
                for (var i = 0; i < files.length; i++) {
                    vm.uploadDiscussionFilesToBlobStorage({ data: { file: files[i] } });
                }
            }
        }

        function fetchAndStoreUsersList(cbSuccess) {
            vm.addUsers = true;
            vm.listOfUserIds = [];
            vm.listOfParticipantIds = [];
            vm.listOfNFCUserIds = [];
            var options = "?$skip=0";
            vm.getExistingCaseUsers(vm.case.caseId, function (response, status) {
                vm.users = response.items.filter(function (item) { return item.role.name != 'ICIUser'; });
                vm.numberOfParticipants = vm.users.filter(function (item) { return item.role.name == 'Participant'; }).length;
                if (!vm.isDiscussionBeingEdited)
                    vm.listOfUserIds = vm.users.filter(function (item) { return item.role.name != 'Participant'; }).map(function (item) { return item.id; });
                getCaseRelatedICIUsers(vm.case.caseId);
                getCaseRelatedNFCUsers(vm.case.caseId)
                cbSuccess ? cbSuccess() : null;
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.users = [];
            });
        }

        function uploadDiscussionFilesToBlobStorage(file) {
            usSpinnerService.spin('spinner');
            vm.uploadNewFile(file, function (resp) {
                vm.discussionFilesUploaded++;
                for (var i = 0; i < resp.data.length; i++) {
                    vm.discussion.discussionFiles.push(resp.data[i]);
                }
                if (vm.discussion.discussionFiles.length > 0) {
                    vm.showExistingDiscussionFiles = true;
                }
                if (vm.numberOfDiscussionFilesToBeUploaded == vm.discussionFilesUploaded) {
                    vm.discussionFilesUploaded = 0;
                    usSpinnerService.stop('spinner');
                    toastr.success("File(s) uploaded");
                }
            }, function (resp) {
                usSpinnerService.stop('spinner');
                ErrorHandlerService.DisplayErrorMessage(resp.data);
            });
        }

        function deleteDiscussionAssociatedFile(file) {
            vm.discussion.discussionFiles.splice(file, 1);
            if (vm.discussion.discussionFiles.length == 0) {
                vm.showExistingDiscussionFiles = false;
            }
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
            vm.listOfParticipantIds = [];
            vm.isParticipantSelected = [];
            if ($stateParams.discussionId != undefined)
                getDiscussionDetails();
            vm.isDiscussionView = false;
        }

        function closeAddFileModal() {
            vm.caseFilesModalInstance.dismiss('cancel');
        }

        function deleteSelectedDiscussion(discussionAction)
        {
            deleteExistingDiscussion(vm.selectedDiscussion.id, function (response, status) {
                toastr.success("Discussion " + Object.keys(vm.discussionAction)[discussionAction].toLowerCase());
                $location.path('/case-management/cases/' + vm.caseNumber + '/discussions');
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function deleteExistingDiscussion(discussionId, cbSuccess, cbError) {
            $http.delete("/api/Discussion/" + discussionId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function discardDiscussionMessageChanges() {
            vm.editDiscussionMessageId = 0;
            vm.showValidationMessage = false;
            getAllDiscussionMessages();
        }

        function downloadFile(fileId) {
            vm.downloadExistingFile(fileId, Enums.DownloadFileType.DiscussionFile,
                function (response, status) { window.location.href = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function searchDiscussions(searchQuery) {
            if (searchQuery == "" || searchQuery == undefined) {
                return;
            }
            vm.isDiscussionSearchActive = true;
            vm.searchQuery = searchQuery
            getDiscussionsByKeyword(0);
        }

        function getDiscussionsByKeyword(discussionPageIndex) {
            if (vm.showNotes) {
                vm.filter = "&$filter=IsNotes eq true";
            }
            else {
                vm.filter = "";
            }
            vm.discussionPageIndex = discussionPageIndex;
            var skipCount = vm.discussionPageIndex * vm.defaultPageSize;
            var options="&$top=" + vm.defaultPageSize + "&$skip=" + skipCount + "&$orderby=LastEditedDateTime desc" + vm.filter;
            getExistingDiscussionsByKeyword(vm.searchQuery, vm.case.caseId, options, function (response, status) {
                vm.allDiscussions = response.result.items
                .map(function (item) { item.lastEditedDateTime = new Date(item.lastEditedDateTime); return item; });
                vm.numberOfDiscussions = response.result.count;
                CaseHeader.case.caseAssociatedDiscussionsCount = response.totalDiscussionCount;
                $global.case.caseAssociatedDiscussionsCount = response.totalDiscussionCount;
                vm.allDiscussionPages.length = Math.ceil(vm.numberOfDiscussions / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allDiscussions = [];
                vm.numberOfDiscussions = 0;
            });
        }

        function getExistingDiscussionsByKeyword(keyword, caseId, options, cbSuccess, cbError)
        {
            $http.get("api/search/discussions?keyword=" + keyword + '&caseId=' + caseId + options)
               .success(function (response, status) {
                   cbSuccess(response,status);
               })
               .error(function (response, status) {
                   cbError(response, status);
               });
        }

        function resetSearchResult() {
            vm.searchQuery = "";
            vm.isDiscussionSearchActive = false;
            fetchAndStoreDiscussionsList(0);
        }

        function openConfirmationModal(participantId) {
            vm.selectedParticipantId = participantId;
            vm.participantToBeAdded = vm.users.filter(function (item) { return item.id == vm.selectedParticipantId })[0];
            for (var index = 0; index < vm.listOfParticipantIds.length; index++) {
                if (vm.listOfParticipantIds[index] == vm.selectedParticipantId) {
                    vm.index = index;
                    vm.participantBeingAdded = true;
                    vm.modalInstanceConfirmation = $uibModal.open({
                        backdrop: 'static',
                        keyboard: false,
                        templateUrl: '/webapp/modules/cases/views/participant-add-confirmation-modal.html',
                        size: 'md'
                    })
                }
            }
        }

        function closeConfirmationModal(isParticipantAdded) {
            if (vm.participantBeingAdded && isParticipantAdded) {
                vm.listOfParticipantIds.splice(vm.index, 1);
            }
            vm.modalInstanceConfirmation.dismiss('cancel');
        }

        function getCaseRelatedUsers(caseId)
        {
            vm.getExistingCaseUsers(caseId, function (response, status) {
                vm.usersForTagging = response.items
                    .filter(function (item) { return item.role.name != "Participant"; })
                    .map(function (item) { item.name = item.firstName + " " + item.lastName + " " + item.email; return item; });
                vm.tinyMceOptions.mentions.source = vm.usersForTagging;
            }, function () { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function goToPreviousUrl() {
            $window.history.back();
        }

        function getCaseRelatedICIUsers(caseId)
        {
            vm.getExistingCaseRelatedICIUsers(caseId, function (response, status) {
                vm.listOfICIUsers = response.map(function (item) { return item.user; });
                vm.users = vm.users.concat(vm.listOfICIUsers);
                vm.numberOfICIUsers = vm.users.filter(function (item) { return item.role.name == 'ICIUser'; }).length;
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.listOfICIUsers = [];});
        }
        function getCaseRelatedNFCUsers(caseId) {
            getExistingCaseRelatedNFCUsers(caseId, function (response, status) {
                vm.listOfNFCUsers = response.map(function (item) { return item.user; });
                vm.users = vm.users.concat(vm.listOfNFCUsers);
                vm.numberOfNFCUsers = vm.users.filter(function (item) { return item.role.name == 'NFCUser'; }).length;
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.listOfICIUsers = []; });
        }

        function getExistingCaseRelatedNFCUsers(caseId, cbSuccess, cbError) {
            $http.get("api/NFCUserCaseAssociation/" + caseId)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function displayNotes(showOnlyNotes) {
            vm.showNotes = showOnlyNotes;
            showOnlyNotes ? vm.isDiscussionSearchActive ? getDiscussionsByKeyword(0) : fetchAndStoreDiscussionsList(0) : (
                vm.searchQuery = "",
                vm.isDiscussionSearchActive = false,
                vm.fetchAndStoreDiscussionsList(0)
            );

        }

        function renameFile(file) {
            if (vm.fileName == undefined || vm.fileName == "") { toastr.error("Please enter file name"); return; }
            vm.selectedFileId = null;
            vm.file = Object.assign({}, file);
            vm.file.fileName = vm.fileName;
            vm.saveEditedFile(vm.file,
                function (response, status) {
                    toastr.success("File renamed");
                    vm.isFileSearchActive ? getDiscussionsByKeyword(getFilesByKeyword) : getDiscussionDetails();
                },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function downloadMessageFile(url) {
            window.location.href = url;
        }

        function openExistingCaseFilesModal() {
            vm.caseFiles = $global.case.files;
            for (var count = 0; count < vm.caseFiles.length; count++)
                vm.caseFiles[count].isSelected = false;
            vm.caseFilesModalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/cases/views/add-case-files-discussion-modal.view.html',
                size: 'lg'
            })
        }

        function associateSelectedCaseFiles(files) {
            closeAddFileModal();                      
            if (vm.isDiscussionView) {
                for (var count = 0; count < vm.caseFiles.length; count++) {
                    if (vm.caseFiles[count].isSelected)
                        vm.discussion.discussionFiles.push(vm.caseFiles[count]);
                }
                if (vm.discussion.discussionFiles.length > 0) {
                    vm.showExistingDiscussionFiles = true;
                }
            }
            else {
                if (vm.discussionMessageId == -1) {
                    for (var count = 0; count < vm.caseFiles.length; count++) {
                        if (vm.caseFiles[count].isSelected)
                            vm.newDiscussionMessage.files.push(vm.caseFiles[count]);
                    }                  
                }
                else {                                      
                    for (var count = 0; count < vm.caseFiles.length; count++) {
                        if (vm.caseFiles[count].isSelected) {
                            var index = 0;
                            for (; index < vm.allDiscussionMessages.length; index++) {
                                if (vm.allDiscussionMessages[index].id == vm.discussionMessageId)
                                    vm.allDiscussionMessages[index].files.push(vm.caseFiles[count]);
                            }
                        }                            
                    }                                                                                                
                }                
            }
        }

        vm.closeAddFileModal = closeAddFileModal;
        vm.discussionsInit = discussionsInit;
        vm.discussionMessagesInit = discussionMessagesInit;
        vm.fetchAndStoreDiscussionsList = fetchAndStoreDiscussionsList;
        vm.openAddDiscussionModal = openAddDiscussionModal;
        vm.postDiscussion = postDiscussion;
        vm.clearAddDiscussionFormPreFilledValues = clearAddDiscussionFormPreFilledValues;
        vm.uploadDiscussionFiles = uploadDiscussionFiles;
        vm.uploadDiscussionFilesToBlobStorage = uploadDiscussionFilesToBlobStorage;
        vm.deleteDiscussionAssociatedFile = deleteDiscussionAssociatedFile;
        vm.fetchAndStoreUsersList = fetchAndStoreUsersList;
        vm.closeModal = closeModal;
        vm.discardDiscussionMessageChanges = discardDiscussionMessageChanges;
        vm.getAllDiscussionMessages = getAllDiscussionMessages;
        vm.saveDiscussionMessage = saveDiscussionMessage;
        vm.goToDiscussionMessages = goToDiscussionMessages;
        vm.saveEditedDiscussionMessage = saveEditedDiscussionMessage;
        vm.deleteDiscussionMessage = deleteDiscussionMessage;
        vm.deleteSelectedDiscussion = deleteSelectedDiscussion;
        vm.getDiscussionDetails = getDiscussionDetails;
        vm.uploadDiscussionMessageFiles = uploadDiscussionMessageFiles;
        vm.uploadDiscussionMessageFilesToBlobStorage = uploadDiscussionMessageFilesToBlobStorage;
        vm.deleteDiscussionMessageAssociatedFile = deleteDiscussionMessageAssociatedFile;
        vm.editDiscussion = editDiscussion;
        vm.openEditDiscussionModel = openEditDiscussionModal;
        vm.downloadFile = downloadFile;
        vm.searchDiscussions = searchDiscussions;
        vm.getDiscussionsByKeyword = getDiscussionsByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.openConfirmationModal = openConfirmationModal;
        vm.closeConfirmationModal = closeConfirmationModal;
        vm.toggleDiscussionAsNote = toggleDiscussionAsNote;
        vm.getExistingCaseUsers = $global.getExistingCaseUsers;
        vm.downloadExistingFile = $global.downloadExistingFile;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.uploadNewFile = $global.uploadNewFile;
        vm.goToDiscussionsList = goToDiscussionsList;
        vm.getSelectedDiscussion = getSelectedDiscussion;
        vm.getExistingDiscussionMessages = getExistingDiscussionMessages;
        vm.postDiscussionMessage = postDiscussionMessage;
        vm.updateDiscussionMessage = updateDiscussionMessage;
        vm.deleteExistingDiscussionMessage = deleteExistingDiscussionMessage;
        vm.getExistingDiscussionsByKeyword = getExistingDiscussionsByKeyword;
        vm.getCaseRelatedUsers = getCaseRelatedUsers;
        vm.goToPreviousUrl = goToPreviousUrl;
        vm.getCaseRelatedICIUsers = getCaseRelatedICIUsers;
        vm.getExistingCaseRelatedICIUsers = $global.getExistingCaseRelatedICIUsers;
        vm.displayNotes = displayNotes;
        vm.renameFile = renameFile;
        vm.saveEditedFile = $global.saveEditedFile;
        vm.downloadMessageFile = downloadMessageFile;
        vm.openExistingCaseFilesModal = openExistingCaseFilesModal;
        vm.associateSelectedCaseFiles = associateSelectedCaseFiles;
        vm.isDiscussionView = false;
        return vm;
    }
    ]);
})();