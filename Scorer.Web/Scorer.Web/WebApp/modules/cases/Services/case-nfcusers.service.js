﻿(function () {
    'use strict';
    angular.module('case').factory('CaseNFCUsers', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;

        function init() {
            vm.isNFCUserBeingEdited = false;
            vm.isExistingNFCUser = false;
            getAllNFCUsers();
            getRolesList();
            getCountriesList();
            vm.caseNumber = $stateParams.caseNumber;
            vm.getCaseFromCaseNumber(vm.caseNumber, function () {
                getNFCCaseUsers();
            });
        }

        function getNFCCaseUsers() {
            vm.case = CaseHeader.case;
            getExistingNFCUserCaseAssociationsByCaseId(vm.case.caseId,
                function (response, status) {
                    vm.allNFCCaseUsers = response;
                    vm.nfcCaseUsers = response;
                },
                    function (response, status) {
                        ErrorHandlerService.DisplayErrorMessage(response, status);
                        vm.allNFCCaseUsers = [];
                        vm.nfcCaseUsers = [];
                    });
        }

        function getExistingNFCUserCaseAssociationsByCaseId(caseId, cbSuccess, cbError) {
            $http.get("api/nfcUserCaseAssociation/" + caseId)
                .success(function (response, status) { cbSuccess(response, status); })
                .error(function (response, status) { cbError(response, status); });
        }

        function addNFCUser() {
            vm.nfcCaseUser = {
                isActive: true,
                user: undefined,
            };
            vm.isExistingNFCUser = false;
            vm.isNFCUserBeingEdited = false;
            openAddEditNFCUserModal();
        }

        function openAddEditNFCUserModal() {
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/add-nfcuser-modal.view.html',
                size: 'lg'
            });
        }

        function onNFCUserSelected(item) {
            vm.nfcCaseUser.user = item;
            vm.nfcCaseUser.nfcUserId = item.id;
            if (item.id != -1) {
                vm.isExistingNFCUser = true;
            }
            else {
                vm.isExistingNFCUser = false;
            }
        }

        function closeModal() {
            if (vm.modalInstance != undefined)
                vm.modalInstance.dismiss('cancel');
        }

        function refresh($select) {
            var search = $select.search,
              list = angular.copy($select.items),
              flag = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.id !== flag;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    id: -1,
                    email: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }

        function onCountrySelected(item) {
            vm.nfcCaseUser.user.userDetails.country = item;
            vm.nfcCaseUser.user.userDetails.countryId = item.countryId;
        }

        function getRolesList() {
            var options = "?$filter=Name eq 'NFCUser'";
            vm.getExistingRoles(options, function (response, status) {
                if (response.items)
                    vm.nfcRole = response.items[0];
                else
                    vm.nfcRole = {};
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getCountriesList() {
            vm.getExistingCountries(function (response, status) { vm.countries = response; }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.countries = [];
            });
        }

        function getAllNFCUsers() {
            var options = "?$skip=0&$filter=Role/Name eq 'NFCUser'";
            vm.getExistingUsers(options, function (response, status) {
                if (response.items){
                    vm.allNFCUsers = response.items;
                }
                else
                    vm.allNFCUsers = [];
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.nfcUsers = [];
            });
        }

        function sendInvitation(isValid) {
            if (!vm.emailRegex.test(vm.nfcCaseUser.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.nfcCaseUser.user.role = vm.nfcRole;
            vm.nfcCaseUser.caseId = vm.case.caseId;
            vm.nfcCaseUser.isBeingAdded = true;
            vm.invitation = {
                user: vm.nfcCaseUser.user,
                nfcCaseUser: vm.nfcCaseUser,
            };
            vm.inviteNewUser(vm.invitation, function (response, status) {
                toastr.success("Invitation sent");
                closeModal();
                getNFCCaseUsers();
                getAllNFCUsers();
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function editNFCUser(nfcCaseUser) {
            vm.isNFCUserBeingEdited = true;
            vm.nfcCaseUser = angular.copy(nfcCaseUser);
            if (vm.nfcCaseUser.user.userDetails == undefined)
                vm.nfcCaseUser.user.userDetails = {};
            vm.nfcCaseUser.user.userDetails.userId = vm.nfcCaseUser.user.id;
            openAddEditNFCUserModal();
        }

        function saveNFCUser(isValid, nfcUserAction) {
            if (!vm.emailRegex.test(vm.nfcCaseUser.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.nfcCaseUser.user.role = vm.nfcRole;
            vm.nfcCaseUser.caseId = vm.case.caseId;
            if (!vm.isNFCUserBeingEdited) {
                vm.nfcCaseUser.isBeingAdded = true;
                saveNewNFCUserCaseAssociation(vm.nfcCaseUser, function (response, status) {
                    toastr.success("NFC User " + Object.keys(vm.nfcUserAction)[nfcUserAction].toLowerCase());
                    closeModal();
                    getNFCCaseUsers();
                    getAllNFCUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
            else {
                saveEditedNFCUserCaseAssociation(vm.nfcCaseUser, function (response, status) {
                    toastr.success("NFC User " + Object.keys(vm.nfcUserAction)[nfcUserAction].toLowerCase());
                    closeModal();
                    getNFCCaseUsers();
                    getAllNFCUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
        }

        function saveNewNFCUserCaseAssociation(nfcUserCaseAssociation, cbSuccess, cbError) {
            $http.post('api/nfcUserCaseAssociation', nfcUserCaseAssociation)
                   .success(function (response, status) {
                       cbSuccess(response, status);
                   }).error(function (response, status) {
                       cbError(response, status);
                   });
        }

        function saveEditedNFCUserCaseAssociation(nfcUserCaseAssociation, cbSuccess, cbError) {
            $http.put('api/nfcUserCaseAssociation/' + nfcUserCaseAssociation.id, nfcUserCaseAssociation)
                    .success(function (response, status) {
                        cbSuccess(response, status);
                    }).error(function (response, status) {
                        cbError(response, status);
                    });
        }

        function toggleNFCUserStatus(nfcCaseUser) {
            vm.isNFCUserBeingEdited = true;
            vm.nfcCaseUser = nfcCaseUser;
            vm.nfcCaseUser.user.role = vm.nfcRole;
            vm.nfcCaseUser.isActive ? saveNFCUser(true, vm.nfcUserAction['Status set to active']) : saveNFCUser(true, vm.nfcUserAction['Status set to inactive']);
        }

        vm.init = init;
        vm.nfcUserAction = Enums.NFCUserAction;
        vm.emailRegex = $global.regex.email;
        vm.saveEditedNFCUserCaseAssociation = saveEditedNFCUserCaseAssociation;
        vm.saveNewNFCUserCaseAssociation = saveNewNFCUserCaseAssociation;
        vm.saveNFCUser = saveNFCUser;
        vm.getNFCCaseUsers = getNFCCaseUsers;
        vm.getExistingNFCUserCaseAssociationsByCaseId = getExistingNFCUserCaseAssociationsByCaseId;
        vm.addNFCUser = addNFCUser;
        vm.closeModal = closeModal;
        vm.onNFCUserSelected = onNFCUserSelected;
        vm.refresh = refresh;
        vm.onCountrySelected = onCountrySelected;
        vm.getExistingCountries = $global.getExistingCountries;
        vm.getCountriesList = getCountriesList;
        vm.getAllNFCUsers = getAllNFCUsers;
        vm.getRolesList = getRolesList;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.getExistingRoles = $global.getExistingRoles;
        vm.sendInvitation = sendInvitation;
        vm.toggleNFCUserStatus = toggleNFCUserStatus;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.inviteNewUser = $global.inviteNewUser;
        vm.editNFCUser = editNFCUser;
        vm.roleName = $global.roleName;
        return vm;
    }
    ]);
})();