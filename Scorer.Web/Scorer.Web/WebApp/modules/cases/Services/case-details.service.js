(function () {
    'use strict';
    angular.module('case').factory('CaseDetails', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global','CaseTasks',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global, CaseTasks) {
        var vm = this;

        function caseDetailsInit() {
            setCommonVariables();
            vm.isPrimary = false;
            vm.isReportCountryUSA = false;
            vm.isReportFileUploaded = false;     
            vm.isNewReportAdded = false;
            vm.showExistingCaseFiles = false;
            vm.isNewPrincipalAdded = false;
            vm.isCaseBeingEdited = false;
            if(vm.roleName == 'Sales'){
            vm.showValidationMessage = true;
            }
            else {
                vm.showValidationMessage = false;
            }
            vm.speedEnum = Enums.Speed;
            vm.allSubjectTypes = Enums.SubjectType;
            fetchAllReportTypes();
            fetchAndStoreDiscountsList();
            fetchAndStoreSubStatusList();
            fetchAndStoreCountriesListForReports();
            getAllExistingCaseStatus();
            vm.getCaseFromCaseNumber(vm.caseNumber, mapApiResponse);
        }

        function setCommonVariables()
        {
            vm.currentUser = $global.user;
            vm.downloadFileType = Enums.DownloadFileType;
            vm.caseNumber = $stateParams.caseNumber;
            vm.isCurrentUserSalesRep = false;
            vm.isCaseSentForApprovalOrNFCApprovedOrApproved = false;
        }

        function caseFilesInit()
        {
            setCommonVariables();
            vm.isFileSearchActive = false;
            vm.selectedFileId = null;
            vm.getCaseFromCaseNumber(vm.caseNumber, fetchAndStoreAllCaseRelatedfiles);
            vm.searchedFileId = $global.searchedFileId;
            if (vm.searchedFileId) { // move scroll bar to selected file
                var newHash = 'scroll_' + vm.searchedFileId;               
                if ($location.hash() !== newHash) {                  
                    $location.hash('scroll_' + vm.searchedFileId);
                } else {                  
                    $anchorScroll();
                }
            }             
        }

        function groupFilesByTaskName(item){           
            return item.taskName;
        }       

        function getFilesByKeyword()
        {
            if (vm.fileSearchQuery == "" || vm.fileSearchQuery == undefined)
            {
                return;
            }
            vm.isFileSearchActive = true;
            vm.allCaseRelatedFiles = vm.caseFilesList.slice();
            vm.allTaskRelatedFiles = vm.taskFilesList.slice();
            vm.allDiscussionRelatedFiles = vm.discussionFilesList.slice();
            vm.caseFilesCount = vm.caseFilesList.length;
            vm.taskFilesCount = vm.taskFilesList.length;
            vm.discussionFilesCount = vm.discussionFilesList.length;
            for (var index = 0; index < vm.allCaseRelatedFiles.length; index++) {
                if (vm.allCaseRelatedFiles[index].fileName.toLowerCase().search(vm.fileSearchQuery.toLowerCase()) < 0) {
                    vm.allCaseRelatedFiles.splice(index, 1);
                    index--;
                    vm.caseFilesCount--;
                }
            }
            for (var index = 0; index < vm.allTaskRelatedFiles.length; index++) {
                if (vm.allTaskRelatedFiles[index].fileName.toLowerCase().search(vm.fileSearchQuery.toLowerCase()) < 0) {
                    vm.allTaskRelatedFiles.splice(index, 1);
                    index--;
                    vm.taskFilesCount--;
                }
            }
            for (var index = 0; index < vm.allDiscussionRelatedFiles.length; index++) {
                if (vm.allDiscussionRelatedFiles[index].fileName.toLowerCase().search(vm.fileSearchQuery.toLowerCase()) < 0) {
                    vm.allDiscussionRelatedFiles.splice(index, 1);
                    index--;
                    vm.discussionFilesCount--;
                }
            }
        }

        function resetSearchResult()
        {
            vm.isFileSearchActive = false;
            vm.fileSearchQuery = "";
            vm.allCaseRelatedFiles = vm.caseFilesList.slice();
            vm.allTaskRelatedFiles = vm.taskFilesList.slice();
            vm.allDiscussionRelatedFiles = vm.discussionFilesList.slice();
            vm.caseFilesCount = vm.caseFilesList.length;
            vm.taskFilesCount = vm.taskFilesList.length;
            vm.discussionFilesCount = vm.discussionFilesList.length;
        }

        function saveCaseFileDetails()
        {
            saveCaseFileAssociations(vm.case,
                function (response, status) {
                    vm.isFileSearchActive ? fetchAndStoreAllCaseRelatedfiles(getFilesByKeyword) : fetchAndStoreAllCaseRelatedfiles();
                    vm.getCaseFromCaseNumber(vm.caseNumber, mapApiResponse);
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                });
        }

        function saveCaseFileAssociations(eachCase, cbSuccess, cbError) {
            $http.put("/api/caseFiles/" + eachCase.caseId, eachCase)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function renameFile(file)
        {
            if (vm.fileName == undefined || vm.fileName == "") { toastr.error("Please enter file name"); return; }
                vm.selectedFileId = null;
                vm.file = Object.assign({}, file);
                vm.file.fileName = vm.fileName;
                vm.saveEditedFile(vm.file,
                    function (response, status) {
                        toastr.success("File renamed");
                        vm.isFileSearchActive ? fetchAndStoreAllCaseRelatedfiles(getFilesByKeyword) : fetchAndStoreAllCaseRelatedfiles();
                    },
                    function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function uploadFiles(files)
        {
            vm.filesUploaded = 0;
            if (files && files.length) {
                vm.numberOfFilesToBeUploaded = files.length;
                for (var i = 0; i < files.length; i++) {
                    uploadFilesToBlobStorage({ data: { file: files[i] } });
                }
            }
        }

        function uploadFilesToBlobStorage(file)
        {
            usSpinnerService.spin('spinner');
            vm.uploadNewFile(file, function (resp) {
                vm.filesUploaded++;
                for (var i = 0; i < resp.data.length; i++) {
                    vm.case.files.push(resp.data[i]);
                }
                if (vm.case.files.length > 0) {
                    vm.showExistingCaseFiles = true;
                }
                if (vm.numberOfFilesToBeUploaded == vm.filesUploaded) {
                    vm.filesUploaded = 0;
                    usSpinnerService.stop('spinner');              
                    saveCaseFileDetails();
                    CaseHeader.case.caseAllFilesCount += vm.numberOfFilesToBeUploaded;                    
                    toastr.success("File(s) uploaded");
                }
            }, function (resp) {
                usSpinnerService.stop('spinner');
                ErrorHandlerService.DisplayErrorMessage(resp.data);
            });
        }
       
        function downloadFile(fileId, type) {
            vm.downloadExistingFile(fileId, type,
                function (response, status) { window.location.href = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function mapApiResponse() {
            vm.case = angular.copy(CaseHeader.case);
            fetchAndStoreUsersList();
            if (vm.case.client.salesRepresentativeId == vm.currentUser.userId) {
                vm.isCurrentUserSalesRep = true;
            }
            if (vm.case.caseStatus) {
                if (vm.case.caseStatus.status != 'NEW' && vm.case.caseStatus.status != 'PROCESSING') {
                    vm.isCaseSentForApprovalOrNFCApprovedOrApproved = true;
                }
            }
            vm.case.otherReportTypes = [];          
            vm.case.reportsCountries = [];
            vm.case.selectedDiscount = { discountAmount: vm.case.discount };
        
            if (!vm.case.files) {
                vm.case.files = [];
                vm.showExistingCaseFiles = false;
            }
            if (!vm.case.primaryReportFiles) {
                vm.case.primaryReportFiles = [];
            }
            if (!vm.case.reports) {
                vm.case.reports = [];
            }
            if (vm.case.principalNames == null || vm.case.principalNames == undefined) {
                vm.case.principalNames = [];
            }
            if (vm.case.client != null) {
                if (vm.case.client.salesRepresentative) {
                    vm.case.client.salesRepresentativeName = vm.case.client.salesRepresentative.firstName + " " + vm.case.client.salesRepresentative.lastName;
                }
                if (vm.case.client.industryId == 0 || vm.case.client.industryId == null) {
                    vm.industry = undefined;
                }
                else {
                    vm.industry = { name: vm.case.client.industryName, id: vm.case.client.industryId };
                }
                if (vm.case.client.industrySubCategoryId == 0 || vm.case.client.industrySubCategoryId == null) {
                    vm.industrySubCategory = undefined;
                }
                else {
                    vm.industrySubCategory = { name: vm.case.client.industrySubCategoryName, id: vm.case.client.industrySubCategoryId };
                }
                if (vm.case.clientContactUser != null) {
                    if (vm.case.clientContactUser.countryId == 0 || vm.case.clientContactUser.countryId == null) {
                        vm.country = undefined;
                    }
                    else {
                        vm.country = { countryName: vm.case.clientContactUser.countryName, countryId: vm.case.clientContactUser.countryId };
                    }
                    if (vm.case.clientContactUser.stateId == 0 || vm.case.clientContactUser.stateId == null) {
                        vm.state = undefined;
                    }
                    else {
                        vm.state = { stateName: vm.case.clientContactUser.stateName, stateId: vm.case.clientContactUser.stateId };
                    }
                }
            }
            for (var i = 0; i < vm.case.reports.length; i++) {
                var existingReportCountry = {};
                existingReportCountry.countryId = vm.case.reports[i].countryId;
                existingReportCountry.countryName = vm.case.reports[i].countryName;
                vm.case.reportsCountries.push(existingReportCountry);
            }
            if (vm.case.reports.length > 0) {
                vm.isNewReportAdded = true;
                vm.isPrimary = true;
                if (vm.case.reports[0].countryName == "United States") {
                    if (vm.case.reports[0].reportType == "Pre-Employment")
                    { vm.isReportCountryUSA = true; }
                    fetchStatesListBasedOnCountryId(vm.case.reports[0].countryId);
                }
                else {
                    vm.isReportCountryUSA = false;
                }
                for (var i = 0; i < vm.case.reports.length; i++) {
                    if (vm.case.reports[i].countryId == 0)
                        vm.case.reportsCountries[i] = undefined;
                    if (vm.case.reports[i].isOtherReportType)
                        vm.case.otherReportTypes[i] = vm.case.reports[i].otherReportTypeName;
                }
            } else {
                vm.isNewReportAdded = false;
                vm.isPrimary = false;
            }
            if (vm.case.files.length > 0) {
                vm.showExistingCaseFiles = true;
            } else {
                vm.showExistingCaseFiles = false;
            }
            if (vm.case.stateOfEmploymentId == 0 || vm.case.stateOfEmploymentId == null) {
                vm.case.stateOfEmployment = undefined;
            }
            else {
                vm.case.stateOfEmployment = { stateName: vm.case.stateOfEmploymentName, stateId: vm.case.stateOfEmploymentId };
                onStateOfEmploymentSelected(vm.case.stateOfEmployment);
            }
            if (vm.case.stateOfResidenceId == 0 || vm.case.stateOfResidenceId == null) {
                vm.case.stateOfResidence = undefined;
            }
            else {
                vm.case.stateOfResidence = { stateName: vm.case.stateOfResidenceName, stateId: vm.case.stateOfResidenceId };
                onStateOfResidenceSelected(vm.case.stateOfResidence);
            }
            if (vm.case.caseAcceptedByNFC == undefined) {
                vm.case.caseAcceptedByNFC = false;
            }
            if (vm.case.reportReceivedFromNFC == undefined) {
                vm.case.reportReceivedFromNFC = false;
            }
            if (vm.case.invoiceReceivedFromNFC == undefined) {
                vm.case.invoiceReceivedFromNFC = false;
            }
            if (vm.case.nfcDueDate != undefined) {
                vm.case.nfcDueDate = new Date(vm.case.nfcDueDate);
            }
            if (vm.case.nfcDueDate == undefined || vm.case.caseStatus && vm.case.caseStatus.status != "NFC_APPROVED") {
                vm.case.nfcDueDate = new Date();
            }
            if (vm.case.dateReportReceivedFromNFC != undefined) {
                vm.case.dateReportReceivedFromNFC = new Date(vm.case.dateReportReceivedFromNFC);
            }
            if (vm.case.dateInvoiceReceivedFromNFC != undefined) {
                vm.case.dateInvoiceReceivedFromNFC = new Date(vm.case.dateInvoiceReceivedFromNFC);
            }
        }

        function fetchAndStoreAllCaseRelatedfiles(cbSuccess)
        {
            vm.case = CaseHeader.case;
            getExistingCaseAssociatedFiles(vm.case.caseId,
                function (response, status) {
                    vm.allFilesList = response;
                    vm.caseFilesList = [];
                    vm.taskFilesList = [];
                    vm.discussionFilesList = [];
                    vm.allCaseRelatedFiles = vm.caseFilesList.slice();
                    vm.allTaskRelatedFiles = vm.taskFilesList.slice();
                    vm.allDiscussionRelatedFiles = vm.discussionFilesList.slice();
                    vm.caseFilesCount = 0;
                    vm.taskFilesCount = 0;
                    vm.discussionFilesCount = 0;
                    for(var file of vm.allFilesList)
                    {
                        if (file.fileType == vm.downloadFileType['CaseFile'])
                        {
                            vm.caseFilesList.push(file);
                            vm.allCaseRelatedFiles.push(file);
                            vm.caseFilesCount++;
                        }
                        else if (file.fileType == vm.downloadFileType['TaskFile'])
                        {
                            vm.taskFilesList.push(file)
                            vm.allTaskRelatedFiles.push(file);
                            vm.taskFilesCount++;
                        }
                        else //if(file.fileType == vm.downloadFileType['DiscussionFile'])
                        {
                            vm.discussionFilesList.push(file);
                            vm.allDiscussionRelatedFiles.push(file);
                            vm.discussionFilesCount++;
                        }
                    }
                    cbSuccess ? cbSuccess() : null;
                },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getExistingCaseAssociatedFiles(caseId, cbSuccess, cbError) {
            $http.get('api/caseAllFiles/' + caseId)
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
        }

        function changeCaseStatusToAvailable() {
            fetchAndStoreSubStatusList();
        }

        function fetchAndStoreSubStatusList() {
            vm.getExistingCaseSubStatusList(function (response, status) { vm.subStatusList = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.subStatusList = []; });
        }

        function saveCase()
        {
            vm.saveNewCase(vm.case,
                function (response, status) { toastr.success("Case saved successfully."); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }
        
        function goToSelectedDiscussionDetails(selectedDiscussion) {           
            $location.path("case-management/cases/" + vm.caseNumber + "/discussions/" + selectedDiscussion.discussionId);
        }

        function goToSelectedTaskDetails(selectedTask) {           
            getTaskDetailsByTaskId(selectedTask.taskId, function (response, status) {               
                vm.openTaskCommentsModal(response);
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });           
           
        }

        function getTaskDetailsByTaskId(taskId, cbSuccess, cbError) {
            $http.get("api/task/" + taskId)
          .success(function (response, status) {
              cbSuccess(response, status);
          })
          .error(function (response, status) {
              cbError(response, status);
          });
        }

        function updateCaseDetails(caseDetails, toastrMessage) {          
            if (caseDetails.selectedDiscount) {
                caseDetails.discount = caseDetails.selectedDiscount.discountAmount;
            }
            if (caseDetails.client) {
                caseDetails.clientName = caseDetails.client.clientName;
            }
            if (!validateCaseFormData()) { return; }
            if (caseDetails.subjectType != vm.allSubjectTypes.Company) {
                caseDetails.isSpecificPrincipal = false;
            }
            if (caseDetails.isSpecificPrincipal == false || caseDetails.principalNames == null) {
                caseDetails.principalNames = null;
                caseDetails.numberOfPrincipals = 0;
            }
            else {
                caseDetails.numberOfPrincipals = caseDetails.principalNames.length;
            }
            if (vm.isReportCountryUSA == false) {
                caseDetails.fcraCertificationEmailReceivedFromClient = false;
                caseDetails.consentReceived = false;
                caseDetails.agreementReceived = false;
                caseDetails.stateOfEmploymentId = 0;
                caseDetails.stateOfResidenceId = 0;
                caseDetails.salaryGreaterThan75k = false;
            }
            if (caseDetails.isProBonoGratis == false) {
                caseDetails.whyProBonoGratis = null;
            } 
            if (caseDetails.caseStatus.status == "NEW") {
                caseDetails.caseStatus = vm.caseStatus.filter(function (item) { return item.status == "PROCESSING"; })[0];
                caseDetails.statusId = caseDetails.caseStatus.id;
            }
           
            vm.saveEditedCase(caseDetails, function (response, status) {
                if (caseDetails.assignedTo && (vm.caseAssignedToUserId != caseDetails.assignedTo))
                    assignCaseToUser(caseDetails);
                toastr.success(toastrMessage || "Case edited.");
                vm.getCaseFromCaseNumber(vm.caseNumber, mapApiResponse);
                vm.isCaseBeingEdited = false;
                vm.showValidationMessage = false;
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function fetchAndStoreUsersList() {
            var options = "?$skip=0&$filter=Role/Name eq 'IHIUser' or Role/Name eq 'Admin'";
            vm.getExistingUsers(options, function (response, status) {
                vm.users = response.items;
                getCaseRelatedICIUsers(vm.case.caseId);
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.users = []; });
        }

        function editCase(caseDetails) {
            vm.isCaseBeingEdited = true;
            if (vm.isCurrentUserSalesRep) {
                vm.showValidationMessage = true;
            }
            vm.caseToBeEdited = angular.copy(caseDetails);
            vm.caseAssignedToUserId = caseDetails.assignedTo;
        }

        function onAssignToUserSelected(selectedItem) {           
            vm.case.assignedTo = selectedItem.id;
        }

        function assignCaseToUser(caseDetails) {
            saveAssignedCase(caseDetails.caseId, caseDetails.assignedTo,
                function (response, status) { toastr.success("Case assigned to " + caseDetails.assignedToUser.firstName + " " + caseDetails.assignedToUser.lastName); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveAssignedCase(caseId, userId, cbSuccess, cbError)
        {
            $http.post('/api/case/' + caseId + '/' + userId + '/user')
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }
        
        function cancelEditCaseChanges() {
            vm.isCaseBeingEdited = false;
            if (vm.roleName == 'Sales') {
                vm.showValidationMessage = true;
            }
            else {
                vm.showValidationMessage = false;
            }
            vm.isNewReportAdded = false;
            vm.case = angular.copy(vm.caseToBeEdited);
            CaseHeader.case = vm.case;
        }
        
        function onSubStatusSelected(item) {
            vm.case.subStatusId = item.id;
        }

        function addNewReport() {
            vm.isNewReportAdded = true;
            vm.case.reports.length = vm.case.reports.length + 1;
        }

        function addNewPrincipal() {
            vm.isNewPrincipalAdded = true;
            vm.case.principalNames.push({ "id": null, "principalName": "" });
        }

        function onReportSelected($item, $index) {
            vm.case.reports[0].isPrimary = true;
            vm.isPrimary = true;
            vm.reportDetail = $item;
            if (vm.case.reports[$index].reportType == "Other") {
                vm.case.reports[$index].isOtherReportType = true;
            }
            else {
                vm.case.reports[$index].isOtherReportType = false;
            }
            if (vm.case.reportsCountries[$index]) {
                if (vm.case.reportsCountries[0].countryName == "United States" && vm.case.reports[0].reportType == "Pre-Employment") {
                    vm.isReportCountryUSA = true;
                }
                else {
                    if ($index == 0) {
                        vm.isReportCountryUSA = false;
                    }
                }
            }
            if (vm.case.reports[$index].reportType != "Other") {
                vm.reportPreviewModalInstance = $uibModal.open({
                    templateUrl: '/webapp/modules/cases/views/report-details-description-modal.view.html',
                    size: 'lg',
                    windowClass: 'report-modal'
                });
            }
        }
        function onReportCountrySelected($item, $index) {
            if (vm.case.reportsCountries[0].countryName == "United States" && vm.case.reports[0].reportType == "Pre-Employment") {
                vm.isReportCountryUSA = true;
                fetchStatesListBasedOnCountryId(vm.case.reportsCountries[0].countryId);
            }
            else {
                if ($index == 0) {
                    vm.isReportCountryUSA = false;
                }
            }
        }
        function deleteAddedReport(report) {
            vm.case.reports.splice(report, 1);
            vm.case.reportsCountries.splice(report, 1);
            if (vm.case.reports.length == 0) {
                vm.isNewReportAdded = false;
                vm.isPrimary = false;
            }
        }

        function onStateOfResidenceSelected($item) {
            vm.case.stateOfResidenceId = $item.stateId;
        }

        function onStateOfEmploymentSelected($item) {
            vm.case.stateOfEmploymentId = $item.stateId;
        }

        function fetchAndStoreCountriesListForReports() {
            vm.getExistingCountries(function (response, status) { vm.reportCountries = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.reportCountries = []; });
        }

        function fetchAllReportTypes() {
            getExistingReportTypes(function (response, status) { vm.reports = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.reports = []; });
        }

        function getExistingReportTypes(cbSuccess, cbError) {
            $http.get('/api/reports')
                .success(function (response, status) {
                    cbSuccess(response, status);
                }).error(function (response, status) {
                    cbError(response, status);
                });
        }

        function closeReportPreviewModal() {
            vm.reportPreviewModalInstance.dismiss('cancel');
        }

        function fetchAndStoreDiscountsList() {
            getExistingDiscounts(function (response, status) { vm.discountOptions = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.discountOptions = []; });
        }

        function getExistingDiscounts(cbSuccess, cbError) {
            $http.get('/api/discounts')
            .success(function (response, status) {
                cbSuccess(response, status);
            })
            .error(function (response, status) {
                cbError(response, status);
            });
        }

        function validateCaseFormData() {
            var isValid = true;
            var isInvalidActualRetail = false;
            var isInvalidNFCBudget = false;
            if (!vm.case.client || vm.case.client.clientName == null || vm.case.client.clientName == "") {
                isValid = false;
            }
            if (!vm.case.nickName || vm.case.nickName == null || vm.case.nickName == "")
            {
                isValid = false;
            }
            if (isValid)
                if (!vm.case.reports || vm.case.reports == null || vm.case.reports == "") {
                    if (!vm.isNewReportAdded)
                        addNewReport();
                    isValid = false;
                }
            if (isValid)
                for (var count = 0; count < vm.case.reports.length; count++) {
                    if (!vm.case.reports[count]) {
                        isValid = false;
                        break;
                    }
                }
            if (isValid)
                if (!vm.case.reportsCountries || vm.case.reportsCountries == null || vm.case.reportsCountries == "") {
                    isValid = false;
                }
            if (isValid)
                for (var count = 0; count < vm.case.reports.length; count++) {
                    if (!vm.case.reportsCountries[count]) {
                        isValid = false;
                        break;
                    }
                }
            if (isValid)
                for (var count = 0; count < vm.case.reports.length; count++) {
                    if (vm.case.reports[count].isOtherReportType) {
                        if (!vm.case.otherReportTypes[count] || vm.case.otherReportTypes[count] == null || vm.case.otherReportTypes[count] == "") {
                            isValid = false;
                            break;
                        }
                    }
                }
            if (isValid)
                if (!vm.case.reportSubject || vm.case.reportSubject == null || vm.case.reportSubject == "") {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.subjectType ==null ) {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.references == null || vm.case.references == undefined) {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.speed == null || vm.case.speed == undefined) {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.reputation == null || vm.case.reputation == undefined) {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.creditReport == null || vm.case.creditReport == undefined) {
                    isValid = false;
                }
            if (isValid)
                if (vm.case.clientProvidedAttachments == null || vm.case.clientProvidedAttachments == undefined) {
                    isValid = false;
                }
            if (isValid)
                if (vm.isReportCountryUSA) {
                    if (vm.case.consentReceived == null || vm.case.consentReceived == undefined) {
                        isValid = false;
                    }
                    if (isValid)
                        if (vm.case.agreementReceived == null || vm.case.agreementReceived == undefined) {
                            isValid = false;
                        }
                    if (isValid)
                        if (vm.case.salaryGreaterThan75k == null || vm.case.salaryGreaterThan75k == undefined) {
                            isValid = false;
                        }
                }
            if (isValid)
                if (vm.case.isProBonoGratis) {
                    if (!vm.case.whyProBonoGratis || vm.case.whyProBonoGratis == null || vm.case.whyProBonoGratis == "") {
                        isValid = false;
                    }
                }
            if (isValid)
                if (vm.case.subjectType == vm.allSubjectTypes.Company) {
                    if (vm.case.isSpecificPrincipal == null || vm.case.isSpecificPrincipal == undefined) {
                        isValid = false;
                    }
                    if (isValid)
                        if (vm.case.isSpecificPrincipal) {
                            for (var index = 0; index < vm.case.principalNames.length; index++) {
                                if (!vm.case.principalNames[index].principalName || vm.case.principalNames[index].principalName == null || vm.case.principalNames[index].principalName == "") {
                                    isValid = false;
                                    break;
                                }
                            }
                        }
                }

            if (vm.case.actualRetail != 0) {
                if (!vm.validateCurrency(vm.case.actualRetail))
                {
                    isInvalidActualRetail = true;
                    isValid = false;
                }
            }

            if (isValid)
            {
                if (vm.case.caseStatus && vm.case.caseStatus.status == "NFC_APPROVED" && vm.isCurrentUserSalesRep) {
                    if (vm.case.nfcDueDate == undefined) {
                        isValid = false;
                    }
                    if (isValid && (vm.case.nfcBudget == null || vm.case.nfcBudget == undefined || vm.case.nfcBudget <= 0)) {
                        isValid = false;
                    }
                    if (isValid && !vm.validateCurrency(vm.case.nfcBudget)) {
                        isInvalidNFCBudget = true;
                        isValid = false;
                    }
                    if (isValid && vm.case.reportReceivedFromNFC && vm.case.dateReportReceivedFromNFC == undefined) {
                        isValid = false;
                    }
                    if (isValid && vm.case.invoiceReceivedFromNFC && vm.case.dateInvoiceReceivedFromNFC == undefined) {
                        isValid = false;
                    }
                }
            }
            if (!isValid) {
                $location.hash('edit_case_errors_msg');
                $anchorScroll();
            }
            vm.showValidationMessage = !isValid;
            vm.isInvalidActualRetail = isInvalidActualRetail;     
            vm.isInvalidNFCBudget = isInvalidNFCBudget;
            return isValid;
        }

        function removeSelectedPrincipal($index) {
            vm.case.principalNames.splice($index, 1);
        }

        function deleteCaseAssociatedFile(file) {           
            for (var index = 0; index < vm.case.files.length; index++) {
                if (vm.case.files[index].fileId == file.fileId) {
                     vm.case.files.splice(index, 1);
                     break;
                }
             }              
            if (vm.case.files.length == 0) {
                vm.showExistingCaseFiles = false;
            }          
            saveCaseFileDetails();
            CaseHeader.case.caseAllFilesCount--;        
            toastr.success("File archived");
        }      

        function fetchStatesListBasedOnCountryId(countryId) {
            vm.getExistingStatesByCountryId(countryId, function (response, status) { vm.countrySpecificStates = response; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.countrySpecificStates = []; });
        }

        function updateCaseForSalesUser(caseDetails)
        {
            if (caseDetails.selectedDiscount) {
                caseDetails.discount = caseDetails.selectedDiscount.discountAmount;
            }
            if (caseDetails.client) {
                caseDetails.clientName = caseDetails.client.clientName;
            }
            if (caseDetails.subjectType != vm.allSubjectTypes.Company) {
                caseDetails.isSpecificPrincipal = false;
            }
            if (caseDetails.isSpecificPrincipal == false || caseDetails.principalNames == null) {
                caseDetails.principalNames = null;
                caseDetails.numberOfPrincipals = 0;
            }
            else {
                caseDetails.numberOfPrincipals = caseDetails.principalNames.length;
            }
            if (vm.isReportCountryUSA == false) {
                caseDetails.fcraCertificationEmailReceivedFromClient = false;
                caseDetails.consentReceived = false;
                caseDetails.agreementReceived = false;
                caseDetails.stateOfEmploymentId = 0;
                caseDetails.stateOfResidenceId = 0;
                caseDetails.salaryGreaterThan75k = false;
            }
            if (caseDetails.isProBonoGratis == false) {
                caseDetails.whyProBonoGratis = null;
            }
            if (caseDetails.caseStatus.status == "NEW") {
                caseDetails.caseStatus = vm.caseStatus.filter(function (item) { return item.status == "PROCESSING"; })[0];
                caseDetails.statusId = caseDetails.caseStatus.id;
            }
            vm.saveEditedCase(caseDetails, function (response, status) {
                toastr.success("Case edited");
                vm.getCaseFromCaseNumber(vm.caseNumber);
                vm.isCaseBeingEdited = false;
                vm.showValidationMessage = true;
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function sendCaseForApproval(caseDetails)
        {
            caseDetails.caseStatus = vm.caseStatus.filter(function (item) { return item.status == "SENT_FOR_APPROVAL"; })[0];
            caseDetails.statusId = caseDetails.caseStatus.id;
            vm.isCaseSentForApprovalOrNFCApprovedOrApproved = true;
            updateCaseDetails(caseDetails, "Case sent for approval");
        }

        function approveCase(caseDetails)
        {
            caseDetails.caseStatus = (caseDetails.isNFCCase ? vm.caseStatus.filter(function (item) { return item.status == "NFC_APPROVED"; })[0] : vm.caseStatus.filter(function (item) { return item.status == "APPROVED"; })[0]);
            caseDetails.statusId = caseDetails.caseStatus.id;
            updateCaseDetails(caseDetails, caseDetails.isNFCCase ? "NFC-Case approved" : "Case approved");
        }

        function onNFCSelected(caseDetails) {
            caseDetails.isNFCCase = true;
            caseDetails.caseStatus = vm.caseStatus.filter(function (item) { return item.status == "NFC_APPROVED"; })[0];
            caseDetails.statusId = caseDetails.caseStatus.id;
            updateCaseDetails(caseDetails, "Sent to NFC");
        }

        function onNFCReportStatusChange(reportStatus)
        {
            reportStatus ? vm.case.dateReportReceivedFromNFC = new Date() : vm.case.dateReportReceivedFromNFC = undefined;
        }

        function onNFCInvoiceStatusChange(invoiceStatus)
        {
            invoiceStatus ? vm.case.dateInvoiceReceivedFromNFC = new Date() : vm.case.dateInvoiceReceivedFromNFC = undefined;
        }
        function onBusinessUnitSelected(item) {
            vm.case.businessUnitId = item.id;
        }

        function getCaseRelatedICIUsers(caseId) {
            vm.getExistingCaseRelatedICIUsers(caseId, function (response, status) {
                vm.listOfICIUsers = response.map(function (item) { return item.user; });
                if (vm.users) {
                    vm.users = vm.users.concat(vm.listOfICIUsers);
                    vm.numberOfICIUsers = vm.users.filter(function (item) { return item.role.name == 'ICIUser'; }).length;
                }
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.listOfICIUsers = []; });
        }

        function filterUsersByRole(user) {
            var roleName = user.role.name;
            if (roleName == "ICIUser")
                return "ICI-Users";
            else {
                if (roleName == "IHIUser")
                    return "IHI-Users";
                else if (roleName == "Admin")
                    return "Admin";
            }
        }
        function getAllExistingCaseStatus() {
            vm.getExistingCaseStatus(function (response, status) {
                vm.caseStatus = response;
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.caseStatus = []; });
        }

        function setActiveCaseStatusClass(currentTab) {
            if (vm.case && vm.case.caseStatus) {
                if (currentTab == "NEW") {
                if ("NEW" == vm.case.caseStatus.status)
                    return 'current';
                else if (vm.case.caseStatus.status != "NEW" )
                    return 'finish';
            }    
            else
                if (currentTab == "PROCESSING") {
                    if ("PROCESSING" == vm.case.caseStatus.status)
                        return 'current';
                    else if (vm.case.caseStatus.status != "NEW")
                        return 'finish';
                }
                else if (currentTab == "IN_PROCESS") {
                    if ("NEW" != vm.case.caseStatus.status &&  "PROCESSING" != vm.case.caseStatus.status && "QC_REVIEWS" != vm.case.caseStatus.status)
                        return 'current';
                    else if (vm.case.caseStatus.status == "QC_REVIEWS")
                        return 'finish';
                }
                else if (currentTab == "QC_REVIEWS") {
                    if ("QC_REVIEWS" == vm.case.caseStatus.status)
                        return 'current';
                    else
                        null;
                }
            }
        }

        function setCaseStatus(status) {
            vm.statusId = vm.caseStatus.filter(function (item) { return item.status == status; })[0].id;
            if (vm.statusId) {
                setAciveCaseStatus(vm.statusId, vm.case.caseId, function () { 
                    toastr.success("Status updated");
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); })
            }
        }

        function setAciveCaseStatus(statusId, caseId, cbSuccess, cbError) {
            $http.post('/api/CaseStatus?caseId=' + caseId + '&statusId=' + statusId)
                .success(function (response, status) {
                    cbSuccess(response, status);
                })
                .error(function (response, status) {
                    cbError(response, status);
                });
        }

        vm.caseDetailsInit = caseDetailsInit;
        vm.caseFilesInit = caseFilesInit;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.downloadFile = downloadFile;
        vm.uploadFiles = uploadFiles;
        vm.uploadFilesToBlobStorage = uploadFilesToBlobStorage;      
        vm.saveCaseFileDetails = saveCaseFileDetails;
        vm.fetchAndStoreAllCaseRelatedfiles = fetchAndStoreAllCaseRelatedfiles;
        vm.closeModal = closeModal;
        vm.renameFile = renameFile;
        vm.changeCaseStatusToAvailable = changeCaseStatusToAvailable;
        vm.saveCase = saveCase;
        vm.fetchAndStoreSubStatusList = fetchAndStoreSubStatusList;
        vm.getFilesByKeyword = getFilesByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.groupFilesByTaskName = groupFilesByTaskName;
        vm.downloadExistingFile = $global.downloadExistingFile;
        vm.uploadNewFile = $global.uploadNewFile;
        vm.getExistingCaseSubStatusList = $global.getExistingCaseSubStatusList;
        vm.saveNewCase = $global.saveNewCase;
        vm.goToSelectedTaskDetails = goToSelectedTaskDetails;
        vm.goToSelectedDiscussionDetails = goToSelectedDiscussionDetails;
        vm.updateCaseDetails = updateCaseDetails;
        vm.fetchAndStoreUsersList = fetchAndStoreUsersList;
        vm.onAssignToUserSelected = onAssignToUserSelected;
        vm.assignCaseToUser = assignCaseToUser;
        vm.editCase = editCase;
        vm.cancelEditCaseChanges = cancelEditCaseChanges;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.saveEditedCase = $global.saveEditedCase;
        vm.openTaskCommentsModal = CaseTasks.openTaskCommentsModal;
        vm.onSubStatusSelected = onSubStatusSelected;
        vm.addNewReport = addNewReport;
        vm.onReportSelected = onReportSelected;
        vm.deleteAddedReport = deleteAddedReport;
        vm.onStateOfEmploymentSelected = onStateOfEmploymentSelected;
        vm.onStateOfResidenceSelected = onStateOfResidenceSelected;
        vm.fetchAllReportTypes = fetchAllReportTypes;
        vm.closeReportPreviewModal = closeReportPreviewModal;
        vm.addNewPrincipal = addNewPrincipal;
        vm.validateCaseFormData = validateCaseFormData;
        vm.getExistingCountries = $global.getExistingCountries;
        vm.fetchAndStoreCountriesListForReports = fetchAndStoreCountriesListForReports;
        vm.getExistingStatesByCountryId = $global.getExistingStatesByCountryId;
        vm.deleteCaseAssociatedFile = deleteCaseAssociatedFile;
        vm.removeSelectedPrincipal = removeSelectedPrincipal;
        vm.saveCaseFileAssociations = saveCaseFileAssociations;
        vm.roleName = $global.roleName;
        vm.getTaskDetailsByTaskId = getTaskDetailsByTaskId;
        vm.validateCurrency = $global.validateCurrency;
        vm.saveEditedFile = $global.saveEditedFile;
        vm.getExistingCaseAssociatedFiles = getExistingCaseAssociatedFiles;
        vm.getTaskDetailsByTaskId = getTaskDetailsByTaskId;
        vm.saveAssignedCase = saveAssignedCase;
        vm.getExistingReportTypes = getExistingReportTypes;
        vm.getExistingDiscounts = getExistingDiscounts;
        vm.sendCaseForApproval = sendCaseForApproval;
        vm.approveCase = approveCase;
        vm.updateCaseForSalesUser = updateCaseForSalesUser;
        vm.onNFCReportStatusChange = onNFCReportStatusChange;
        vm.onNFCInvoiceStatusChange = onNFCInvoiceStatusChange;
        vm.onNFCSelected = onNFCSelected;
        vm.datePickerOptions = { 'showWeeks': false };
        vm.onReportCountrySelected = onReportCountrySelected;
        vm.onBusinessUnitSelected = onBusinessUnitSelected;
        vm.getExistingCaseRelatedICIUsers = $global.getExistingCaseRelatedICIUsers;
        vm.filterUsersByRole = filterUsersByRole;
        vm.getExistingCaseStatus = $global.getExistingCaseStatus
        vm.setActiveCaseStatusClass = setActiveCaseStatusClass;
        vm.setCaseStatus = setCaseStatus;
        return vm;
    }]);
})();