﻿(function () {
    'use strict';
    angular.module('case').factory('Participants', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;

        function init() {
            vm.isParticipantBeingEdited = false;
            vm.isExistingParticipant = false;
            vm.participantAction = Enums.ParticipantAction;
            getAllParticipantUsers();
            getRolesList();
            vm.caseNumber = $stateParams.caseNumber;
            vm.emailRegex = $global.regex.email;
            vm.getCaseFromCaseNumber(vm.caseNumber, getParticipantsList);
        }

        function toggleParticipantStatus(participant)
        {
            vm.isParticipantBeingEdited = true;
            vm.participant = participant;
            vm.participant.user.role = vm.participantRole;
            vm.participant.isActive ? saveParticipant(true, vm.participantAction['Status set to active']) : saveParticipant(true, vm.participantAction['Status set to inactive']);
        }

        function getParticipantsList()
        {
            vm.case = CaseHeader.case;
            getExistingParticipantCaseAssociationsByCaseId(vm.case.caseId,
                function (response, status) {
                    vm.participantsList = response;
                },
                function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.participantsList = [];
                });
        }

        function getExistingParticipantCaseAssociationsByCaseId(caseId, cbSuccess, cbError) {
            $http.get("api/participator/" + caseId)
                .success(function (response, status) { cbSuccess(response, status); })
                .error(function (response, status) { cbError(response, status); });
        }

        function addParticipant()
        {
            vm.participant = {
                isActive: true,
                user: {}
            };
            vm.isExistingParticipant = false;
            vm.isParticipantBeingEdited = false;
            vm.participant.user = undefined;
            openParticipantModal();
        }

        function openParticipantModal() {
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/add-participant-modal.view.html',
                size: 'lg'
            });
        }

        function sendInvitation(isValid) {
            if (!vm.emailRegex.test(vm.participant.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.participant.user.role = vm.participantRole;
            vm.participant.caseId = vm.case.caseId;
            vm.participant.isBeingAdded = true;
            vm.invitation = {
                user: vm.participant.user,
                iciCaseUser: {},
                participantUser: vm.participant
            };
            vm.inviteNewUser(vm.invitation, function (response, status) { toastr.success("Invitation sent"); closeModal(); getParticipantsList(); getAllParticipantUsers(); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveParticipant(isValid, participantAction) {
            if (!vm.emailRegex.test(vm.participant.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.participant.user.role = vm.participantRole;
            vm.participant.caseId = vm.case.caseId;
            if (!vm.isParticipantBeingEdited) {
                vm.participant.isBeingAdded = true;
                saveNewParticipantCaseAssociation(vm.participant, function (response, status) {
                    toastr.success("Participant " + Object.keys(vm.participantAction)[participantAction].toLowerCase());
                    closeModal();
                    getParticipantsList();
                    getAllParticipantUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
                }
                else {
                saveEditedParticipantCaseAssociation(vm.participant, function (response, status) {
                    toastr.success("Participant " + Object.keys(vm.participantAction)[participantAction].toLowerCase());
                    closeModal();
                    getParticipantsList();
                    getAllParticipantUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
                }
        }

        function saveNewParticipantCaseAssociation(participantCaseAssociation, cbSuccess, cbError)
        {
            $http.post('api/participator', participantCaseAssociation)
                        .success(function (response, status) {
                            cbSuccess(response, status);
                        }).error(function (response, status) {
                            cbError(response, status);
                        });
        }

        function saveEditedParticipantCaseAssociation(participantCaseAssociation, cbSuccess, cbError)
        {
            $http.put('api/participator/' + participantCaseAssociation.id, participantCaseAssociation)
                        .success(function (response, status) {
                            cbSuccess(response, status);
                        }).error(function (response, status) {
                            cbError(response, status);
                        });
        }

        function closeModal() {
            if(vm.modalInstance != undefined)
                vm.modalInstance.dismiss('cancel');
        }

        function getRolesList() {
            var options = "?$filter=Name eq 'Participant'";
            vm.getExistingRoles(options, function (response, status) {
                if (response.items)
                    vm.participantRole = response.items[0];
                else
                    vm.participantRole = {};
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function editParticipant(participant) {
            vm.isParticipantBeingEdited = true;
            vm.participant = angular.copy(participant);
            openParticipantModal();
        }

        function getAllParticipantUsers() {
            var options = "?$skip=0&$filter=Role/Name eq 'Participant'";
            vm.getExistingUsers(options, function (response, status) { vm.participants = response.items; },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); vm.participants = []; });
        }

        function onParticipantSelected(item) {
            vm.participant.user = item;
            vm.participant.userId = item.id;
            if (item.id != -1) {
                vm.isExistingParticipant = true;
            }
            else
            {
                vm.isExistingParticipant = false;
            }
            
        }

        function refresh($select) {
            var search = $select.search,
              list = angular.copy($select.items),
              flag = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.id !== flag;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    id: -1,
                    email: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }

        vm.init = init;
        vm.addParticipant = addParticipant;
        vm.getParticipantsList = getParticipantsList;
        vm.getAllParticipantUsers = getAllParticipantUsers;
        vm.openParticipantModal = openParticipantModal;
        vm.saveParticipant = saveParticipant;
        vm.sendInvitation = sendInvitation;
        vm.closeModal = closeModal;
        vm.getRolesList = getRolesList;
        vm.editParticipant = editParticipant;
        vm.refresh = refresh;
        vm.onParticipantSelected = onParticipantSelected;
        vm.toggleParticipantStatus = toggleParticipantStatus;
        vm.getExistingParticipantCaseAssociationsByCaseId = getExistingParticipantCaseAssociationsByCaseId;
        vm.saveNewParticipantCaseAssociation = saveNewParticipantCaseAssociation;
        vm.saveEditedParticipantCaseAssociation = saveEditedParticipantCaseAssociation;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.inviteNewUser = $global.inviteNewUser;
        vm.getExistingRoles = $global.getExistingRoles;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.roleName = $global.roleName;
        return vm;
    }
    ]);
})();