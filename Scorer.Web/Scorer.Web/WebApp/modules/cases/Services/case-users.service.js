﻿(function () {
    'use strict';
    angular.module('case').factory('CaseUsers', ['CaseHeader', '$http', 'toastr', '$uibModal', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams', '$global',
    function (CaseHeader, $http, toastr, $uibModal, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams, $global) {
        var vm = this;
        
        function init() {
            vm.isICIUserBeingEdited = false;
            vm.isExistingICIUser = false;
            vm.isUserSearchActive = false;
            getAllICIUsers();
            getRolesList();
            getCountriesList();
            vm.caseNumber = $stateParams.caseNumber;
            vm.getCaseFromCaseNumber(vm.caseNumber, function () {
                getICICaseUsers();
                getAllCaseRelatedUsers();
            });
        }

        function toggleICIUserStatus(iciCaseUser) {
            vm.isICIUserBeingEdited = true;
            vm.iciCaseUser = iciCaseUser;
            vm.iciCaseUser.user.role = vm.iciRole;
            vm.iciCaseUser.isActive ? saveICIUser(true, vm.iciUserAction['Status set to active']) : saveICIUser(true, vm.iciUserAction['Status set to inactive']);
        }

        function searchUsers(searchQuery)
        {
            if(searchQuery == '' || searchQuery == undefined)
            {
                return;
            }
            vm.iciCaseUsers = vm.allICICaseUsers.slice();
            vm.isUserSearchActive = true;
            vm.searchQuery = searchQuery.toLowerCase();
            for (var index = 0; index < vm.iciCaseUsers.length; index++)
            {
                var eachICICaseUser=vm.iciCaseUsers[index];
                if ((eachICICaseUser.user.userDetails != undefined ?
                    (eachICICaseUser.user.userDetails.country != undefined ?
                    eachICICaseUser.user.userDetails.country.countryName.toLowerCase().search(vm.searchQuery) < 0 : true)
                    && (eachICICaseUser.user.userDetails.companyName != null ?
                    eachICICaseUser.user.userDetails.companyName.toLowerCase().search(vm.searchQuery) < 0 : true) : true)
                    && eachICICaseUser.user.firstName.toLowerCase().search(vm.searchQuery) < 0
                    && eachICICaseUser.user.lastName.toLowerCase().search(vm.searchQuery) < 0
                    && (eachICICaseUser.user.firstName + " " + eachICICaseUser.user.lastName).search(vm.searchQuery) < 0
                    && eachICICaseUser.user.email.toLowerCase().search(vm.searchQuery) < 0 )
                {
                    vm.iciCaseUsers.splice(index, 1);
                    index--;
                }
            }
        }

        function resetSearchResult()
        {
            vm.iciCaseUsers = vm.allICICaseUsers.slice();
            vm.isUserSearchActive = false;
        }

        function getICICaseUsers() {
            vm.case = CaseHeader.case;
            getExistingICIUserCaseAssociationsByCaseId(vm.case.caseId,
                function (response, status) {
                    vm.iciCaseUsers = response;
                    vm.allICICaseUsers = response;
                },
                    function(response,status){
                        ErrorHandlerService.DisplayErrorMessage(response, status);
                        vm.iciCaseUsers = [];
                        vm.allICICaseUsers = [];
                });
        }

        function getExistingICIUserCaseAssociationsByCaseId(caseId, cbSuccess, cbError) {
            $http.get("api/iciUserCaseAssociation/" + caseId)
                .success(function (response, status) { cbSuccess(response, status); })
                .error(function (response, status) { cbError(response, status); });
        }

        function addICIUser() {
            vm.iciCaseUser = {
                isActive: true,
                user: undefined,
            };
            vm.isExistingICIUser = false;
            vm.isICIUserBeingEdited = false;
            openICIUserModal();
        }

        function openICIUserModal()
        {
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/add-iciuser-modal.view.html',
                size: 'lg'
            });
        }

        function sendInvitation(isValid) {
            if (!vm.emailRegex.test(vm.iciCaseUser.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.iciCaseUser.user.role = vm.iciRole;
            vm.iciCaseUser.caseId = vm.case.caseId;
            vm.iciCaseUser.isBeingAdded = true;
            vm.invitation = {
                user: vm.iciCaseUser.user,
                iciCaseUser: vm.iciCaseUser,
                participantUser: {}
            };
            vm.inviteNewUser(vm.invitation, function (response, status) {
                toastr.success("Invitation sent");
                closeModal();
                getICICaseUsers();
                getAllICIUsers();
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function saveICIUser(isValid, iciUserAction) {
            if (!vm.emailRegex.test(vm.iciCaseUser.user.email)) { toastr.error("Please enter email in correct format"); return; }
            if (!isValid) { return; }
            vm.iciCaseUser.user.role = vm.iciRole;
            vm.iciCaseUser.caseId = vm.case.caseId;
            if (!vm.isICIUserBeingEdited) {
                vm.iciCaseUser.isBeingAdded = true;
                saveNewICIUserCaseAssociation(vm.iciCaseUser, function (response, status) {
                    toastr.success("ICI User " + Object.keys(vm.iciUserAction)[iciUserAction].toLowerCase());
                    closeModal();
                    getICICaseUsers();
                    getAllICIUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response,status); });
            }
            else {
                saveEditedICIUserCaseAssociation(vm.iciCaseUser, function (response, status) {
                    toastr.success("ICI User " + Object.keys(vm.iciUserAction)[iciUserAction].toLowerCase());
                    closeModal();
                    getICICaseUsers();
                    getAllICIUsers();
                }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }
        }

        function saveNewICIUserCaseAssociation(iciUserCaseAssociation, cbSuccess, cbError)
        {
            $http.post('api/iciUserCaseAssociation', iciUserCaseAssociation)
                   .success(function (response, status) {
                       cbSuccess(response, status);
                   }).error(function (response, status) {
                       cbError(response, status);
                   });
        }

        function saveEditedICIUserCaseAssociation(iciUserCaseAssociation, cbSuccess, cbError)
        {
            $http.put('api/iciUserCaseAssociation/' + iciUserCaseAssociation.id, iciUserCaseAssociation)
                    .success(function (response, status) {
                        cbSuccess(response, status);
                    }).error(function (response, status) {
                        cbError(response, status);
                    });
        }

        function closeModal() {
            if (vm.modalInstance != undefined)
                vm.modalInstance.dismiss('cancel');
        }

        function getRolesList() {
            var options="?$filter=Name eq 'ICIUser'";
            vm.getExistingRoles(options, function (response, status) {
                if (response.items)
                    vm.iciRole = response.items[0];
                else
                    vm.iciRole = {};
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function editICIUser(iciCaseUser) {            
            vm.isICIUserBeingEdited = true;
            vm.iciCaseUser = angular.copy(iciCaseUser);
            if (vm.iciCaseUser.user.userDetails == undefined)
                vm.iciCaseUser.user.userDetails = {};
            vm.iciCaseUser.user.userDetails.userId = vm.iciCaseUser.user.id;
            openICIUserModal();
        }

        function onICIUserSelected(item) {
            vm.iciCaseUser.user = item;
            vm.iciCaseUser.iciUserId = item.id;
            if (item.id != -1) {
                vm.isExistingICIUser = true;
            }
            else
            {
                vm.isExistingICIUser = false;
            }
        }

        function onCountrySelected(item)
        {
            vm.iciCaseUser.user.userDetails.country = item;
            vm.iciCaseUser.user.userDetails.countryId = item.countryId;
        }

        function refresh($select) {
            var search = $select.search,
              list = angular.copy($select.items),
              flag = -1;
            //remove last user input
            list = list.filter(function (item) {
                return item.id !== flag;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            }
            else {
                //manually add user input and set selection
                var userInputItem = {
                    id: -1,
                    email: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        }

        function getCountriesList()
        {
            vm.getExistingCountries(function (response, status) { vm.countries = response; }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.countries = [];
            });
        }

        function getAllICIUsers()
        {
            var options = "?$skip=0&$filter=Role/Name eq 'ICIUser'";
            vm.getExistingUsers(options, function (response, status) {                
                if (response.items)
                    vm.iciUsers = response.items;
                else
                    vm.iciUsers = [];
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.iciUsers = [];
            });
        }

        function getAllCaseRelatedUsers() {
            vm.case = CaseHeader.case;
            vm.getExistingCaseUsers(vm.case.caseId, function (response, status) { vm.activeUsers = response.items; },
                function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.activeUsers = [];
            });
        }

        vm.init = init;
        vm.emailRegex = $global.regex.email;
        vm.iciUserAction = Enums.ICIUserAction;
        vm.onCountrySelected = onCountrySelected;
        vm.addICIUser = addICIUser;
        vm.getAllICIUsers = getAllICIUsers;
        vm.getICICaseUsers = getICICaseUsers;
        vm.openICIUserModal = openICIUserModal;
        vm.saveICIUser = saveICIUser;
        vm.sendInvitation = sendInvitation;
        vm.closeModal = closeModal;
        vm.getRolesList = getRolesList;
        vm.editICIUser = editICIUser;
        vm.refresh = refresh;
        vm.onICIUserSelected = onICIUserSelected;
        vm.toggleICIUserStatus = toggleICIUserStatus;
        vm.getCountriesList = getCountriesList;
        vm.searchUsers = searchUsers;
        vm.resetSearchResult = resetSearchResult;
        vm.getAllCaseRelatedUsers = getAllCaseRelatedUsers;
        vm.getCaseFromCaseNumber = CaseHeader.getCaseFromCaseNumber;
        vm.inviteNewUser = $global.inviteNewUser;
        vm.getExistingRoles = $global.getExistingRoles;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.getExistingCountries = $global.getExistingCountries;
        vm.getExistingCaseUsers = $global.getExistingCaseUsers;
        vm.roleName = $global.roleName;
        vm.getExistingICIUserCaseAssociationsByCaseId = getExistingICIUserCaseAssociationsByCaseId;
        vm.saveNewICIUserCaseAssociation = saveNewICIUserCaseAssociation;
        vm.saveEditedICIUserCaseAssociation = saveEditedICIUserCaseAssociation;
        return vm;
    }
    ]);
})();