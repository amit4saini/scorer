﻿(function () {
    'use strict';
    angular.module('tasks').factory('Tasks', ['CaseTasks', '$uibModal', '$http', 'toastr', 'ErrorHandlerService', 'Enums', '$window', '$global', '$location',
    function (CaseTasks, $uibModal, $http, toastr, ErrorHandlerService, Enums, $window, $global, $location) {
        var vm = this;
        
        function init() {
            vm.areCompletedTasksHidden = false;
            vm.defaultPageSize = $window.defaultPageSize;
            vm.currentUser = $global.user;
            vm.filterOption = "All";
            vm.filter = "&$filter=IsDeleted eq false";
            getAssignedTasks(0);
            vm.canUserEditTask = $global.user.permissions.indexOf("CASE_READ_ALL") > -1;
            vm.roleName = $global.roleName;
            if (vm.roleName == "Sales")
                vm.isSalesPerson = true;
            else
                vm.isSalesPerson = false;
            vm.isTaskSearchActive = false;
            vm.taskStatus = [
            { 'type': 'Not Started', 'value': 0 },
            { 'type': 'In Progress', 'value': 1 },
            { 'type': 'Completed', 'value': 2 }
            ];
            vm.datePickerOptions = { 'minDate': new Date(), 'showWeeks': false };
        }

        function resetSearchResult()
        {
            vm.isTaskSearchActive = false;
            vm.filter = '&$filter=IsDeleted eq false';
            vm.filterOption = 'All';
            vm.getAssignedTasks(0)
        }

        function searchTasks(searchQuery)
        {
            if(searchQuery == "" || searchQuery == undefined)
            {
                return;
            }
            vm.isTaskSearchActive = true;
            vm.searchQuery = searchQuery;
            getTasksByKeyword(0);
        }

        function getTasksByKeyword(pageIndex)
        {
            vm.allTaskPages = [];
            vm.uncompletedTaskPages = [];
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = "&$top=" + vm.defaultPageSize + "&$skip=" + skipCount + vm.filter + "&$orderby=DueDate,Status,CompletionDate";
            vm.getExistingTasksByKeyword(vm.searchQuery, vm.currentUser.userId, options, function (tasks, response, status) {
                vm.allTasks = tasks;
                vm.totalNumberOfTasks = response.result.count;
                vm.allTaskPages.length = Math.ceil(vm.totalNumberOfTasks / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allTasks = [];
            }, false);
        }

        function getAllUsers()
        {
            var options = "?$skip=0";
            vm.getExistingUsers(options, function (response, status) { vm.users = response.items; }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.users = [];
            });
        }

        function onTaskStatusSelected(item) {
            vm.task.status = item.value;
        }

        function onUserSelected(item) {
            vm.task.assignedTo = item.id;
        }

        function archiveTask(taskId)
        {
            vm.deleteExistingTask(taskId, function (response, status) {
                vm.filter = "&$filter=IsDeleted eq false";
                vm.filterOption = "All";
                vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : getAssignedTasks(vm.pageIndex);
                toastr.success("Task archived");
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function unarchiveTask(task)
        {
            task.isDeleted = false;
            vm.saveEditedTask(task, function (response, status) {
                vm.filter = "&$filter=IsDeleted eq false";
                vm.filterOption = "All";
                vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : getAssignedTasks(vm.pageIndex);
                toastr.success("Task unarchived");
            }, function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
            vm.task = {};
            vm.status = undefined;
            vm.assignToUser = undefined;
            vm.isTaskSearchActive ? getTasksByKeyword(vm.pageIndex) : getAssignedTasks(vm.pageIndex);
        }

        function openEditTaskModal(task) {
            vm.task = task;
            vm.task.dueDate = new Date(vm.task.dueDate);
            for (var i = 0; i < vm.taskStatus.length; i++) {
                if (vm.taskStatus[i].value == task.status) {
                    vm.status = vm.taskStatus[i];
                }
            }
            vm.assignToUser = task.assignedToUserDetails;
            getAllUsers();
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/tasks/views/edit-task-modal.view.html',
                size: 'md'
            });
            vm.modalInstance.result.then(function () {
            }, function () {
                vm.isTaskBeingEdited = false;
            });
        }

        function getAssignedTasks(pageIndex)
        {
            vm.allTaskPages = [];
            vm.uncompletedTaskPages = [];
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options = "?$top=" + vm.defaultPageSize + "&$skip=" + skipCount + vm.filter + "&$orderby=DueDate,Status,CompletionDate";
            vm.getExistingUserAssignedTasks(vm.currentUser.userId, options, function (tasks, response, status) {
                vm.allTasks = tasks;
                vm.totalNumberOfTasks = response.count;
                vm.allTaskPages.length = Math.ceil(vm.totalNumberOfTasks / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allTasks = [];
            });
        }

        function saveTask(isValid)
        {
            if (isValid == false) { return; }
            if (vm.task.dueDate == undefined) { toastr.error("Please select a date"); return; }
            vm.task.assignedToUserDetails = vm.assignToUser;
            if (vm.task.status == 2 && vm.task.completionDate == undefined) { vm.task.completionDate = new Date(); }
            else if (vm.task.status != 2) { vm.task.completionDate = null; }
            vm.saveEditedTask(vm.task, function (response, status) { toastr.success("Task edited"); closeModal(); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getFilterForEachOption(filterOption) {
            switch (filterOption) {
                case Enums.TaskStatus.NotStarted: vm.filter = "&$filter=Status eq 'NOT_STARTED' and IsDeleted eq false"; vm.filterOption = "Not Started"; break;
                case Enums.TaskStatus.InProgress: vm.filter = "&$filter=Status eq 'IN_PROGRESS' and IsDeleted eq false"; vm.filterOption = "In Progress"; break;
                case Enums.TaskStatus.Completed: vm.filter = "&$filter=Status eq 'COMPLETED' and IsDeleted eq false"; vm.filterOption = "Completed"; break;
                case Enums.TaskStatus.Archived: vm.filter = "&$filter=IsDeleted eq true"; vm.filterOption = "Archived"; break;
                case Enums.TaskStatus.All: vm.filter = "&$filter=IsDeleted eq false"; vm.filterOption = "All"; break;
            }
            if (vm.isTaskSearchActive)
                getTasksByKeyword(0);
            else
                getAssignedTasks(0);
        }

        vm.init = init;
        vm.getAssignedTasks = getAssignedTasks;
        vm.archiveTask = archiveTask;
        vm.unarchiveTask = unarchiveTask;
        vm.openEditTaskModal = openEditTaskModal;
        vm.closeModal = closeModal;
        vm.getAllUsers = getAllUsers;
        vm.saveTask = saveTask;
        vm.onTaskStatusSelected = onTaskStatusSelected;
        vm.onUserSelected = onUserSelected;
        vm.getFilterForEachOption = getFilterForEachOption;
        vm.searchTasks = searchTasks;
        vm.getTasksByKeyword = getTasksByKeyword;
        vm.resetSearchResult = resetSearchResult;
        vm.getExistingUsers = $global.getExistingUsers;
        vm.getExistingUserAssignedTasks = $global.getExistingUserAssignedTasks;
        vm.getExistingTasksByKeyword = $global.getExistingTasksByKeyword;
        vm.saveEditedTask = $global.saveEditedTask;
        vm.deleteExistingTask = $global.deleteExistingTask;
        vm.goToTaskComments = CaseTasks.goToTaskComments;
        vm.openSelectedTaskCaseDetails = CaseTasks.openSelectedTaskCaseDetails;
        return vm;
    }
    ]);
})();
