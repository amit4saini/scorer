﻿(function () {
    'use strict';
    angular.module('case').factory('Discussions', ['CaseDiscussions', '$http', 'toastr', '$uibModal', 'Upload', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window','$location','$stateParams',
    function (CaseDiscussions, $http, toastr, $uibModal, Upload, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $stateParams) {
        var vm = this;
        
        function init() {
            vm.defaultPageSize = $window.defaultPageSize;
            vm.allDiscussionPages = [];
            vm.selectedDiscussion = {};
            fetchAndStoreDiscussionsList(0);
        }

        function fetchAndStoreDiscussionsList(pageIndex)
        {
            vm.pageIndex = pageIndex;
            var skipCount = vm.pageIndex * vm.defaultPageSize;
            var options='?$top=' + vm.defaultPageSize + '&$skip=' + skipCount + "&$orderby=LastEditedDateTime desc";
            getMyDiscussions(options, function (response, status) {
                vm.allDiscussions = response.result.items;
                vm.numberOfDiscussions = response.result.count;
                vm.allDiscussionPages.length = Math.ceil(vm.numberOfDiscussions / vm.defaultPageSize);
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allDiscussions = [];
                vm.numberOfDiscussions = 0;
            })
        }

        function getMyDiscussions(options, cbSuccess, cbError) {
            $http.get('api/my/discussions'+ options)
               .success(function (response, status) {
                   cbSuccess(response, status);
               })
               .error(function (response, status) {
                   cbError(response, status);
               });
        }
        
        function goToDiscussionMessages(discussion) {
            vm.selectedDiscussion = discussion;
            $location.path('/case-management/discussions/' + discussion.id);
        }

        vm.init = init;
        vm.fetchAndStoreDiscussionsList = fetchAndStoreDiscussionsList;
        vm.goToDiscussionMessages = goToDiscussionMessages;
        return vm;
    }
    ]);
})();
