﻿'use strict';

// Setting up route
angular.module('discussions').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
        .state('discussions-list', {
            url: '/case-management/discussions',
            templateUrl: '/webapp/modules/discussions/views/assigned-discussions-list.view.html'
        })
      .state('discussions-discussionMessages', {
          url: '/case-management/discussions/:discussionId',
          templateUrl: '/webapp/modules/cases/views/discussion-messages-list.view.html'
      });
  }
]);
