﻿'use strict';

angular.module('discussions').controller('DiscussionController', ['Discussions',
    function (discussions) {
        return discussions;
    }
]);
