﻿
angular.module('core').constant('Enums',
   {
       SubjectType: {
           'Company': 0,
           'Individual': 1,
           'Other': 2,
       },
       DownloadFileType: {
           'CaseFile': 0,
           'ProfilePic': 1,
           'DiscussionFile': 2,
           'TaskFile' : 3
       },
       TaskStatus: {
           'NotStarted': 0,
           'InProgress': 1,
           'Completed': 2,
           'Archived':3,
           'All' : 4
       },
       Actions: {
           'CASE_ADDED': 0,
           'CASE_EDITED': 1,
           'CASE_ARCHIVED':2,
           'CASE_UNARCHIVED':3,
           'CASE_EXPENSE_ADDED': 4,
           'CASE_EXPENSE_EDITED': 5,
           'CASE_EXPENSE_DELETED': 6,
           'CASE_TIMESHEET_ADDED': 7,
           'CASE_TIMESHEET_EDITED': 8,
           'CASE_TIMESHEET_DELETED': 9,
           'FILE_ADDED': 10,
           'FILE_DELETED': 11,
           'CASE_TASK_EDITED': 12,
           'CASE_TASK_ADDED': 13,
           'CASE_TASK_ARCHIVED':14,
           'CASE_TASK_UNARCHIVED':15,
           'CASE_NOTES_ADDED': 16,
           'CASE_NOTES_EDITED': 17,
           'CASE_NOTES_DELETED': 18,
           'CASE_PRINCIPAL_DELETED': 19,
           'CASE_PRINCIPAL_ADDED': 20,
           'CASE_ASSIGNED': 21,
           'CASE_DISCUSSION_ADDED': 22,
           'CASE_DISCUSSION_EDITED': 23,
           'CASE_DISCUSSION_DELETED': 24,
           'CASE_DISCUSSION_ARCHIVED': 25,
           'CASE_DISCUSSION_UNARCHIVED': 26,
           'CASE_DISCUSSION_MESSAGE_ADDED': 27,
           'CASE_DISCUSSION_MESSSAGE_EDITED': 28,
           'CASE_DISCUSSION_MESSAGE_DELETED': 29,
           'CASE_DISCUSSION_MESSAGE_ARCHIVED': 30,
           'CASE_DISCUSSION_MESSAGE_UNARCHIVED': 31,
           'CASE_REPORT_ADDED': 32,
           'CASE_STATUS_CHANGED': 33,
           'CASE_PARTICIPANT_ADDED': 34,
           'CASE_PARTICIPANT_EDITED': 35,
           'CASE_ICI_USER_ADDED':36,
           'CASE_ICI_USER_EDITED':37,
           'CASE_SUBSTATUS_CHANGED':38,
           'CASE_NFC_USER_ADDED':39,
           'CASE_NFC_USER_EDITED':40,
           'CASE_EXPENSE_APPROVED': 41
       },
       Speed: {
           'Normal': 0,
           'Rush': 1,
           'SuperRush': 2
       },
       HistoryFilter:{
           'Task':0,
           'Expense':1,
           'Case_Status':2,
           'Discussion':3,
           'All': 4,
           'File' : 5
       },
       CaseFilter: {
          'New_Case':0,
           'My_Cases': 1,
           'Completed': 2,
           'Archived': 3,
           'All_Cases': 4 
       },
       DiscussionAction: {
           'Added': 0,
           'Edited': 1,
           'Marked as note': 2,
           'Unmarked as note': 3,
           'Archived': 4
       },
       ParticipantAction:{
           'Added': 0,
           'Edited': 1,
           'Status set to active': 2,
           'Status set to inactive': 3,
       },
       ICIUserAction: {
           'Added': 0,
           'Edited': 1,
           'Status set to active': 2,
           'Status set to inactive': 3,
       },
       NFCUserAction: {
           'Added': 0,
           'Edited': 1,
           'Status set to active': 2,
           'Status set to inactive': 3,
       },
       TaskPriority: {           
           'Regular': 0,
           'Low': 1,
           'High' : 2
       },
       DropdownListType: {
           "ExpenseType": 0,
           "Vendor": 1
       },
       AnalyticsType: {
           "Case": 0,
           "Task": 1,
           "Expense": 2,
           "Timesheet": 3
       },
       ExpensePaymentType: {
           "Account" : 0,
           "CreditCardPayment": 1,
           "Invoice" : 2
       }
   });
   