﻿'use strict';

//Menu service used for managing  menus
angular.module('core').factory('Header', ['$location', '$global', '$http', 'toastr', 'ErrorHandlerService', '$window', '$interval', 'Cases', 'Enums', 'CaseDiscussions',
function ($location, $global, $http, $toastr, ErrorHandlerService, $window, $interval, Cases, Enums, CaseDiscussionsService) {
        var vm = this;

    function getActiveSidebarClass(path) {
        if (path == "/case-management/dashboard" && $location.path() == "/") {
            return 'btn-default-focus';
        }
        return ($location.path().indexOf(path) != -1) ? 'btn-default-focus' : '';
    }
    
    function init()
    {        
        vm.numberOfNotifications = 0;
        vm.isGlobalSearchActive = false;
        window.confirm = function () { };
        $window.confirm = function () { };
        vm.viewNotificationNumber = false;
        vm.firstName = $global.user.firstName;
        vm.userProfilePicUrl = $global.user.profilePicUrl;
        vm.roleName = $global.roleName;
        vm.unreadNotifications = [];
        vm.allNotifications = [];
        getUnreadNotifications();
    }

    $interval(function () {
        if ($window.notificationMessages != undefined) {
            for (var i = 0; i < $window.notificationMessages.length; i++) {
                vm.newMessage = { "notificationText": $window.notificationMessages[i], "url": $window.notificationUrls[i] }
                vm.notificationUrl = $window.url;
                vm.unreadNotifications.unshift(vm.newMessage);
                vm.viewNotificationNumber = true;
            }
             vm.numberOfNotifications += 1;
            Cases.fetchAndStoreCasesList(0);
            notificationMessages = undefined;
        }
    }, 100);

    function getAllNotifications() {
        vm.numberOfNotifications = 0;
        getExistingNotificationsByIsReadProperty(true,
            function (response, status) {
                vm.allNotifications = response.map(function (item) { item.createdOn = new Date(item.createdOn); return item; });
                if (vm.allNotifications.length > 0) {
                    for (var i = 0 ; i < vm.allNotifications.length ; i++) {
                        vm.allNotifications[i].createDate = moment.utc(vm.allNotifications[i].createdOn).format("YYYY-MM-DD");
                    }
                }
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.allNotifications = [];
        });
    }

    function getExistingNotificationsByIsReadProperty(isRead, cbSuccess, cbError) {
        $http.get('/api/notification/' + isRead)
             .success(function (response, status) {               
                 cbSuccess(response, status);
             }).error(function (response, status) {
                 cbError(response, status);
             });
    }

    function getUnreadNotifications()
    {
        vm.numberOfNotifications = 0;
        getExistingUnreadNotifications(function (response, status) {
            vm.unreadNotifications = response.map(function (item) { item.createdOn = new Date(item.createdOn); return item; });
            for (var i = 0; i < vm.unreadNotifications.length; i++) {
                if (vm.unreadNotifications[i].isViewed == false) {
                    vm.numberOfNotifications++;
                    vm.viewNotificationNumber = true;
                }
            }
        }, function (response, status) {
            ErrorHandlerService.DisplayErrorMessage(response, status);
            vm.unreadNotifications = [];
        });
    }

    function getExistingUnreadNotifications(cbSuccess,cbError) {
        $http.get('/api/notifications/')
               .success(function (response, status) {                
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
    }

    function markUnreadNotificationsAsRead()
    {
        vm.viewNotificationNumber = false;
        saveNotificationsStatus(function (response, status) { },
            function (response, status) {ErrorHandlerService.DisplayErrorMessage(response, status);});
        vm.numberOfNotifications = 0;
    }

    function saveNotificationsStatus(cbSuccess, cbError) {
        $http.put('/api/notification/')
               .success(function (response, status) {
                   cbSuccess(response, status);
               }).error(function (response, status) {
                   cbError(response, status);
               });
    }

    function logOut(path) {
        window.location.href = path;
    }

    function globalSearch(searchQuery)
    {
        if(searchQuery == undefined || searchQuery == "")
        {
            return;
        }
        vm.searchQuery = searchQuery;
        vm.isGlobalSearchActive = true;
        getSearchResultsByKeyword(0);
        $location.path('/case-management/search-results');
    }

    function getSearchResultsByKeyword(pageIndex) {
        vm.pageIndex = pageIndex;
        var skipCount = vm.pageIndex * vm.defaultPageSize;
        var options = '&$skip=' + skipCount + '&$top=' + vm.defaultPageSize;
        getMatchedResultsByKeyword(options, vm.searchQuery, function (response, status) {          
            vm.cases = response.items;
            vm.totalNumberOfCases = response.count;
            vm.allPages.length = Math.ceil(vm.totalNumberOfCases / vm.defaultPageSize);
        },
            function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                vm.cases = [];              
                vm.totalNumberOfCases = 0;
            });
    }

    function getMatchedResultsByKeyword(options, keyword, cbSuccess, cbError) {      
        $http.get('api/search?keyword=' + keyword + options)
        .success(function (response, status) {            
            cbSuccess(response, status);
        })
        .error(function (response, status) {
            cbError(response, status);
        });
    }

    function resetSearchResult()
    {
        vm.isGlobalSearchActive = false;
        vm.cases = [];
    }

    function downloadFile(file)
    {
        vm.downloadExistingFile(file.fileId, file.type,
            function (response, status) { window.location.href = response; },
            function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
    }

    function goToDiscussionMessages(selectedDiscussion, caseDetails) {
        selectedDiscussion.caseNumber = caseDetails.caseNumber;
        CaseDiscussionsService.goToDiscussionMessages(selectedDiscussion);
    }

    function goToCaseFiles(selectedCase, selectedFile) {       
        $global.searchedFileId = selectedFile.fileId;
        $location.hash("");
        $location.path('/case-management/cases/' + selectedCase.caseNumber + '/files');
    } 

    vm.init = init;
    vm.getActiveSidebarClass = getActiveSidebarClass;
    vm.downloadFile = downloadFile;
    vm.markUnreadNotificationsAsRead = markUnreadNotificationsAsRead;
    vm.getUnreadNotifications = getUnreadNotifications;
    vm.getAllNotifications = getAllNotifications;
    vm.logOut = logOut;
    vm.globalSearch = globalSearch;
    vm.getSearchResultsByKeyword = getSearchResultsByKeyword;
    vm.resetSearchResult = resetSearchResult;
    vm.downloadExistingFile = $global.downloadExistingFile;
    vm.openCaseDetails = Cases.openCaseDetails;   
    vm.goToDiscussionMessages = goToDiscussionMessages;
    vm.defaultPageSize = $window.defaultPageSize;
    vm.getExistingNotificationsByIsReadProperty = getExistingNotificationsByIsReadProperty;
    vm.getExistingUnreadNotifications = getExistingUnreadNotifications;
    vm.saveNotificationsStatus = saveNotificationsStatus;
    vm.getMatchedResultsByKeyword = getMatchedResultsByKeyword;
    vm.goToCaseFiles = goToCaseFiles; 
    vm.allPages = [];

    return vm;
}]);