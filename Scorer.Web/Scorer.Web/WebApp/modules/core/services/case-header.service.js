﻿'use strict';

angular.module('core').factory('CaseHeader', ['$location', '$global', 'ErrorHandlerService', '$uibModal', 'Enums',
    function ($location, $global, ErrorHandlerService, $uibModal, Enums) {
        var vm = this;

        function getCaseFromCaseNumber(caseNumber, cbSuccess, cbSuccessParam) {
            vm.roleName = $global.roleName;
            vm.getExistingCaseFromCaseNumber(caseNumber,
                function (response, status) {
                    vm.case = angular.copy($global.case);
                    vm.case.lastEditedDateTime = new Date(vm.case.lastEditedDateTime);
                    cbSuccess != undefined ? cbSuccessParam == undefined ? cbSuccess() : cbSuccess(cbSuccessParam) : null;
                },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function setActiveTabClass(path) {
            return ($location.path().indexOf(path) != -1) ? 'active' : '';
        }

        function showAllCaseReports() {
            vm.modalInstance = $uibModal.open({
                templateUrl: '/webapp/modules/cases/views/case-reports-list-modal.view.html',
                size: 'md'
            });
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
        }

        vm.getCaseFromCaseNumber = getCaseFromCaseNumber;
        vm.setActiveTabClass = setActiveTabClass;
        vm.getExistingCaseFromCaseNumber = $global.getExistingCaseFromCaseNumber;
        vm.showAllCaseReports = showAllCaseReports;
        vm.closeModal = closeModal;
        vm.speedEnum = Enums.Speed;
        vm.roleName = $global.roleName;
        vm.goToPreviousUrl = $global.goToPreviousUrl;

        return vm;
    }]);