﻿'use strict';

angular.module('core').factory('$global', ['Authentication', '$http', 'Upload', '$window', function (authentication, $http, Upload, $window) {
    var vm = this;
    vm.roleName = authentication.user.role.name;
    vm.user = authentication.user;
    vm.regex = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        decimal: /^\d*[.]?\d*$/,
    }

    function getExistingCaseFromCaseNumber(caseNumber, cbSuccess, cbError)
    {
        $http.get("api/case/" + caseNumber + "/details")
        .success(function (response, status) {
            vm.case = response;          
         cbSuccess(response, status);
        })
        .error(function (response, status) { vm.case = {}; cbError(response, status); });
    }

    function getExistingCases(options, cbSuccess, cbError) {
        $http.get("api/cases" + options)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function saveNewCase(newCase, cbSuccess, cbError) {
        $http.post('/api/case', newCase)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function saveEditedCase(editedCase, cbSuccess, cbError) {
        $http.put('/api/case/' + editedCase.caseId, editedCase)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function getExistingCaseUsers(caseId, cbSuccess, cbError)
    {
        $http.get("api/Case/" + caseId + "/Users")
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function getExistingCaseSubStatusList(cbSuccess, cbError)
    {
        $http.get('/api/subStatus/')
           .success(function (response, status) { cbSuccess(response, status); })
           .error(function (response, status) { cbError(response, status); });
    }

    function getExistingCountries(cbSuccess, cbError) {
        $http.get('api/countries')
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function getExistingStatesByCountryId(countryId, cbSuccess, cbError) {
        $http.get('/api/country/' + countryId + '/states')
              .success(function (response, status) { cbSuccess(response, status); })
              .error(function (response, status) { cbError(response, status); });
    }

    function getExistingIndustries(cbSuccess, cbError) {
        $http.get('/api/industries')
                .success(function (response, status) { cbSuccess(response, status); })
                .error(function (response, status) { cbError(response, status); });
    }

    function getExistingIndustrySubcategories(cbSuccess, cbError) {
        $http.get('/api/industrySubCategories')
              .success(function (response, status) { cbSuccess(response, status); })
              .error(function (response, status) { cbError(response, status); });
    }

    function getExistingUsers(options, cbSuccess, cbError)
    {
        $http.get("api/users" + options)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function saveEditedUser(user, cbSuccess, cbError) {
        $http.put('/api/user/' + user.id, user)
         .success(function (response, status) { cbSuccess(response, status); })
         .error(function (response, status) { cbError(response, status); });
    }

    function getExistingRoles(options, cbSuccess, cbError) {
        $http.get("api/user/getRoles" + options)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function getExistingCaseAssociatedTasks(caseId, options, cbSuccess, cbError)
    {
        $http.get("api/Case/" + caseId + "/Tasks" + options)
        .success(function (response, status) {
            var tasks = processTasks(response.result.items, true);
            vm.case.caseAssociatedTasksCount = response.allTasksCount;
            cbSuccess(tasks, response, status);
        }).error(function (response, status) { cbError(response, status); });
    }

    function getExistingUserAssignedTasks(userId, options, cbSuccess, cbError)
    {
        $http.get("api/Task/" + userId + "/Users" + options)
        .success(function (response, status) {
            var tasks = processTasks(response.items, false);
            cbSuccess(tasks, response, status);
        }).error(function (response, status) { cbError(response, status); });
    }

    function getExistingTasksByKeyword(keyword, id, options, cbSuccess, cbError, isCaseAssociatedSearch)
    {
        $http.get('api/search/tasks?keyword=' + keyword + '&id=' + id + options)
             .success(function (response, status) {
                 var tasks = processTasks(response.result.items, isCaseAssociatedSearch);
                 isCaseAssociatedSearch ? vm.case.caseAssociatedTasksCount = response.allTasksCount : null;
                 cbSuccess(tasks, response, status);
             }).error(function (response, status) { cbError(response, status); });
    }

    function saveEditedTask(task, cbSuccess, cbError) {
        $http.put('api/task/' + task.taskId, task)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function deleteExistingTask(taskId, cbSuccess, cbError) {
        $http.delete('/api/task/' + taskId)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
    }

    function uploadNewFile(file, cbSuccess, cbError)
    {
        Upload.upload({
            url: 'api/File',
            data: { file: file }
        }).then(function (resp) { cbSuccess(resp); },
        function (resp) { toastr.error("File size exceeds 4 Mb"); },
        function (evt) { });
    }

    function downloadExistingFile(fileId, fileType, cbSuccess, cbError)
    {
        $http.get("api/file/" + fileId + "?type=" + fileType)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function inviteNewUser(invitation, cbSuccess, cbError)
    {
        $http.post('api/invitation', invitation)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function getExistingCaseRelatedICIUsers(caseId, cbSuccess, cbError) {
        $http.get("api/ICIUserCaseAssociation/" + caseId)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status); });
    }

    function saveEditedFile(file, cbSuccess, cbError) {
        $http.put("/api/file?fileId=" + file.fileId, file)
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status) });
    }
    function getExistingCaseStatus(cbSuccess, cbError) {
        $http.get("/api/CaseStatus")
        .success(function (response, status) { cbSuccess(response, status); })
        .error(function (response, status) { cbError(response, status) });
    }

    function processTasks(tasks, isRequestCaseAssociated)
    {
        for (var index = 0; index < tasks.length; index++) {
            tasks[index].dueDate = new Date(tasks[index].dueDate);
            isRequestCaseAssociated ? tasks[index].caseNumber = vm.case.caseNumber : null;
            if (tasks[index].completionDate != null)
                tasks[index].completionDate = new Date(tasks[index].completionDate);
        }
        return tasks;
    }

    //validate Currency Inputs
    function validateCurrency(cost) {   
        var currencyCost = cost.toString();
        if (currencyCost.match(/[-!$%^&*()_+|~=`{}\[\]:";'<>?@\/]/))
            return false;
        if (!currencyCost.match(/^-?(?:0|[1-9]\d{0,2}(\d{3})*)(?:\,\d+)*(?:\.\d+)?$/))
            return false;
        return true;
    }

    function goToPreviousUrl() {
        $window.history.back();
    }

    vm.getExistingCaseFromCaseNumber = getExistingCaseFromCaseNumber;
    vm.getExistingCases = getExistingCases;
    vm.saveNewCase = saveNewCase;
    vm.saveEditedCase = saveEditedCase;
    vm.getExistingCaseUsers = getExistingCaseUsers;
    vm.getExistingCaseSubStatusList = getExistingCaseSubStatusList;
    vm.getExistingCountries = getExistingCountries;
    vm.getExistingStatesByCountryId = getExistingStatesByCountryId;
    vm.getExistingIndustries = getExistingIndustries;
    vm.getExistingIndustrySubcategories = getExistingIndustrySubcategories;
    vm.getExistingUsers = getExistingUsers;
    vm.saveEditedUser = saveEditedUser;
    vm.getExistingRoles = getExistingRoles;
    vm.getExistingCaseAssociatedTasks = getExistingCaseAssociatedTasks;
    vm.getExistingUserAssignedTasks = getExistingUserAssignedTasks;
    vm.getExistingTasksByKeyword = getExistingTasksByKeyword;
    vm.saveEditedTask = saveEditedTask;
    vm.deleteExistingTask = deleteExistingTask;
    vm.uploadNewFile = uploadNewFile;
    vm.downloadExistingFile = downloadExistingFile;
    vm.inviteNewUser = inviteNewUser;
    vm.validateCurrency = validateCurrency;
    vm.goToPreviousUrl = goToPreviousUrl;
    vm.getExistingCaseRelatedICIUsers = getExistingCaseRelatedICIUsers;
    vm.saveEditedFile = saveEditedFile;
    vm.getExistingCaseStatus = getExistingCaseStatus;
    vm.isDashboardView = false;
    vm.processTasks = processTasks;
    return vm;
}]);
