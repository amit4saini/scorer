﻿'use strict';

angular.module('core').factory('ErrorHandlerService', ['toastr', function (toastr) {
    var vm = this;
    
    function DisplayErrorMessage(message, status) {
        if (status == 500)
            toastr.error("Something went wrong, we are trying to resolve it");
        else
            toastr.error(message);
    }

    vm.DisplayErrorMessage = DisplayErrorMessage;
    return vm;
}]);
