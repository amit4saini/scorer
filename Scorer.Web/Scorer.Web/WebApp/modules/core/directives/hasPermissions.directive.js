﻿//hasPermission.js
angular.module('core').directive('hasPermission', ['Authentication', function (authentication) {
    return {
        link: function(scope, element, attrs) {
            var value = attrs.hasPermission.trim();
            if (authentication.user.permissions.indexOf(value) > -1) {
                element.css("display", "");
            } else {
                element.css("display", "none");
            }
        }
    };
}]);