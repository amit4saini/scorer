'use strict';

// Setting up route
angular.module('core').factory('angular-spinner-factory', ['$q', '$window', '$rootScope', function ($q, $window, $rootScope) {
    return {
        request: function (config) {
            $rootScope.spinner = { active: true };
            return config || $q.when(config);

        },
        response: function (response) {
            $rootScope.spinner = { active: false };
            return response || $q.when(response);

        },
        responseError: function (response) {
            $rootScope.spinner = { active: false };
            return $q.reject(response);
        }
    };
}
]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$routeProvider',
  function ($stateProvider, $urlRouterProvider, $httpProvider, $routeProvider) {

     $httpProvider.interceptors.push('angular-spinner-factory');
     $httpProvider.interceptors.push('httpInterceptor');
    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise('not-found');

    // Home state routing
    $stateProvider
      .state('home', {
          url: '/',
          templateUrl: '/webapp/modules/dashboard/views/dashboard.html'
      })
        .state('homes', {
            url: '/case-management',
            templateUrl: '/webapp/modules/dashboard/views/dashboard.html'
        })
      .state('not-found', {
          url: '/not-found',
          templateUrl: '/Webapp/modules/core/views/404.view.html'
      })
      .state('notifications', {
          url: '/case-management/notifications',
          templateUrl: '/Webapp/modules/core/views/notification-listing.view.html'
      })
      .state('global-search', {
          url: '/case-management/search-results',
          templateUrl: 'webapp/modules/core/views/global-search-results.view.html'
      });
    $routeProvider
     .when("LogOff", {
         templateUrl: "LogOff",
     });
  }
]);
angular.module('core').factory('httpInterceptor', ['$q', 'usSpinnerService', function ($q, usSpinnerService) {

    var numLoadings = 0;

    return {
        request: function (config) {

            numLoadings++;

            // Show loader
            usSpinnerService.spin('spinner');
            return config || $q.when(config);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
                usSpinnerService.stop('spinner');
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if (!(--numLoadings)) {
                // Hide loader
                usSpinnerService.stop('spinner');
            }

            return $q.reject(response);
        }
    };
}]);
