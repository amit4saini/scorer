﻿(function () {
    'use strict';
    // Users service used for communicating with the users REST endpoint
    angular.module('users').factory('UserSettings', ['$http', 'usSpinnerService', 'ErrorHandlerService', '$uibModal', 'toastr', '$window','Upload','Enums',
      function ($http, usSpinnerService, ErrorHandlerService, $uibModal, toastr, $window,Upload,Enums) {
          var vm = this;
          vm.getUserDetails = getUserDetails;
          vm.saveUserDetails = saveUserDetails;
          vm.uploadProfilePictureToBlobStorage = uploadProfilePictureToBlobStorage;
          vm.getUploadedPictureUrl = getUploadedPictureUrl;
          vm.cancelChanges = cancelChanges;

          function getUploadedPictureUrl()
          {
              $http.get("/api/FileUpload/" + vm.uploadedPicId + "?type=" + Enums.DownloadFileType.ProfilePic)
              .success(function (response, status) {
                  vm.user.profilePicUrl = response;
              })
              .error(function (response, status) {
                  ErrorHandlerService.DisplayErrorMessage(response, status);
              });
          }

          function getUserDetails() {
              $http.get("api/user/getCurrentUser")
              .success(function (response, status) {
                  vm.user = response;
                  vm.isPictureUploaded = false;
                  vm.actualPictureUrl = vm.user.profilePicUrl;
              })
                  .error(function (response, status) {
                      ErrorHandlerService.DisplayErrorMessage(response,status);
                  });

          }

          function saveUserDetails()
          {
              if (vm.isPictureUploaded == false) { toastr.error("Please upload a picture first"); return;}
              vm.user.profilePicKey = vm.uploadedPicId;
              $http.put('api/user/'+vm.user.id,vm.user)
              .success(function(response,status){
                  toastr.success("Profile picture saved.");
              })
              .error(function(response,status){
                  ErrorHandlerService.DisplayErrorMessage(response,status);
              });
              
          }

          function uploadProfilePictureToBlobStorage(file) {
              usSpinnerService.spin('spinner');
              Upload.upload({
                  url: 'api/FileUpload',
                  data: { file: file }
              }).then(function (resp) {
                  vm.isPictureUploaded = true;
                  vm.uploadedPicId = resp.data[0].fileId;
                  usSpinnerService.stop('spinner');
                  toastr.success("Profile picture uploaded.");
                  getUploadedPictureUrl();
              }, function (resp) {
                  usSpinnerService.stop('spinner');
                  ErrorHandlerService.DisplayErrorMessage(resp.data);
              }, function (evt) {
              });
          }

          function cancelChanges()
          {
              vm.user.profilePicUrl = vm.actualPictureUrl;
              vm.isPictureUploaded = false;
          }

        return vm;
      }
    ]);
})();
