'use strict';

angular.module('users').controller('UserSettingsController', ['UserSettings',
  function (UserSettings) {
    return UserSettings;
  }
]);
