﻿angular.module('core').directive('ngReallyClick', ['$uibModal', '$global', function ($uibModal, $global) {
    return {
        restrict: 'A',
        scope: {
                            ngReallyClick: "&",
                            item: "="
        },
        link: function (scope, element, attrs) {            
            element.bind('click', function () {
                var message = attrs.ngReallyMessage || "Are you sure ?";
                var modalTitle = attrs.ngReallyTitle || "Confirmation Box...";
               
                var modalHtml, modalTitle;

                modalHtml = '<button type="button" ng-click="ok()" class="btn btn-theme-yellow" data-dismiss="modal">Yes</button>' +
                            '<button type="button" ng-click="cancel()" class="btn btn-theme-gray md-close" data-dismiss="modal">No</button>';
                message = '<div class="input-group input-group-block">' + message + '</div>';
                modalHtml = '<div ng-controller="reallyDeleteController as reallyDeleteController"><div class="sp-pop-content">' +
                                '<div class="modal-header"><div class="close" data-dismiss="modal">' +
                                '<span ng-click="cancel()" aria-hidden="true">×</span></div>' +
                                '<h4 class="modal-title" id="feedback-label">' + modalTitle + '</h4></div>' +
                                '<div class="modal-body"><div role="tabpanel">' + message +
                                '</div><div class="modal-footer hide_modal_border">' + modalHtml + '</div></div></div></div>';
               
                $global.modalInstance = $uibModal.open({
                    template: modalHtml,
                    size: 'md'
                });
            
                $global.modalInstance.result.then(function () {
                    scope.ngReallyClick({ item: scope.item });                   
                }, function () {
                    //The modal closed by click on on any other part of the screen
                });
              
                scope.$apply(attrs.ngReallyClick);              
            });     
        }
    }
}]);


angular.module('core').controller('reallyDeleteController', ['$scope', '$uibModal', '$global', function ($scope, $uibModal, $global) {    
    $scope.ok = function () {
        $global.modalInstance.close();
    };

    $scope.cancel = function () {
        $global.modalInstance.dismiss('cancel');
    };
}]);