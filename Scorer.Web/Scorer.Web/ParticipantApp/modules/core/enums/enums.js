﻿
angular.module('core').constant('Enums',
   {
       SubjectType: {
           'Company': 0,
           'Individual': 1,
           'Other': 2,
       },
       DownloadFileType: {
           'CaseFile': 0,
           'ProfilePic': 1,
           'DiscussionFile': 2,
       },
       TaskStatus: {
           'NotStarted': 0,
           'InProgress': 1,
           'Completed': 2,
       },
       Actions: {
           'CASE_ADDED': 0,
           'CASE_EDITED': 1,
           'CASE_DELETED': 2,
           'CASE_EXPENSE_ADDED': 3,
           'CASE_EXPENSE_EDITED': 4,
           'CASE_EXPENSE_DELETED': 5,
           'CASE_TIMESHEET_ADDED': 6,
           'CASE_TIMESHEET_EDITED': 7,
           'CASE_TIMESHEET_DELETED': 8,
           'FILE_ADDED': 9,
           'FILE_DELETED': 10,
           'CASE_TASK_EDITED': 11,
           'CASE_TASK_ADDED': 12,
           'CASE_TASK_DELETED': 13,
           'CASE_NOTES_ADDED': 14,
           'CASE_NOTES_EDITED': 15,
           'CASE_NOTES_DELETED': 16,
           'CASE_PRINCIPAL_DELETED': 17,
           'CASE_PRINCIPAL_ADDED': 18,
           'CASE_ASSIGNED': 19,
           'CASE_DISCUSSION_ADDED': 20,
           'CASE_DISCUSSION_EDITED': 21,
           'CASE_DISCUSSION_DELETED': 22,
           'CASE_DISCUSSION_ARCHIVED': 23,
           'CASE_DISCUSSION_UNARCHIVED': 24,
           'CASE_DISCUSSION_MESSAGE_ADDED': 25,
           'CASE_DISCUSSION_MESSSAGE_EDITED': 26,
           'CASE_DISCUSSION_MESSAGE_DELETED': 27,
           'CASE_DISCUSSION_MESSAGE_ARCHIVED': 28,
           'CASE_DISCUSSION_MESSAGE_UNARCHIVED': 29,
           'CASE_REPORT_ADDED': 30,
           'CASE_STATUS_CHANGED':31
       },
       Speed: {
           'Normal': 0,
           ' Rush': 1,
           'SuperRush': 2
       },
       HistoryFilter:{
           'TASK':0,
           'EXPENSES':1,
           'CASE_STATUS':2,
           'DISCUSSION':3,
           'ALL': 4
       }
   });
   