﻿'use strict';

//Menu service used for managing  menus
angular.module('core').factory('Header', ['Authentication', '$location', '$global', '$http', 'toastr', 'ErrorHandlerService', '$window', '$interval', 'Cases', function (authentication, $location, $global, $http, $toastr, ErrorHandlerService, $window, $interval, Cases) {
    var vm = this;
    vm.numberOfNotifications = 0;
    vm.getActiveSidebarClass = getActiveSidebarClass;
    window.confirm = function () { };
    $window.confirm = function () { };

    function getActiveSidebarClass(path) {
        if (path == "/case-management/dashboard" && $location.path() == "/") {
            return 'btn-default-focus';
        }
        return ($location.path().indexOf(path) != -1) ? 'btn-default-focus' : '';
    }

    $interval(function () {
        if ($window.notificationMessages != undefined) {
            for (var i = 0; i < $window.notificationMessages.length; i++) {
                vm.newMessage = { "notificationText": $window.notificationMessages[i] }
                vm.unreadNotifications.unshift(vm.newMessage);
                vm.viewNotificationNumber = true;
            }
            vm.numberOfNotifications += 1;
            Cases.fetchAndStoreCasesList(0);
            notificationMessages = undefined;
        }
    }, 100);

    function getAllNotifications() {
        vm.numberOfNotifications = 0;
        $http.get('/api/notification/' + true)
             .success(function (response, status) {
                 vm.allNotifications = response;
                 if (vm.allNotifications.length > 0) {
                     vm.numberOfNotifications = vm.allNotifications.length;
                     vm.viewNotificationNumber = true;
                 }
             }).error(function (response, status) {
                 ErrorHandlerService.DisplayErrorMessage(response, status);
                 vm.allNotifications = [];
             });
    }

    function getUnreadNotifications() {
        vm.numberOfNotifications = 0;
        $http.get('/api/notifications/')
               .success(function (response, status) {
                   vm.unreadNotifications = response;
                   for (var i = 0; i < vm.unreadNotifications.length; i++) {
                       if (vm.unreadNotifications[i].isViewed == false) {
                           vm.numberOfNotifications++;
                           vm.viewNotificationNumber = true;
                       }
                   }
               }).error(function (response, status) {
                   ErrorHandlerService.DisplayErrorMessage(response, status);
                   vm.unreadNotifications = [];
               });
    }

    function markUnreadNotificationsAsRead() {
        vm.viewNotificationNumber = false;
        $http.put('/api/notification/')
               .success(function (response, status) {
               }).error(function (response, status) {
                   ErrorHandlerService.DisplayErrorMessage(response, status);
               });
        vm.numberOfNotifications = 0;
    }

    function logOut(path) {
        window.location.href = path;
    }

    vm.markUnreadNotificationsAsRead = markUnreadNotificationsAsRead;
    vm.getUnreadNotifications = getUnreadNotifications;
    vm.unreadNotifications = [];
    vm.allNotifications = [];
    vm.getAllNotifications = getAllNotifications;
    vm.userName = authentication.user.Email;
    vm.userProfilePicUrl = authentication.user.ProfilePicUrl;
    vm.logOut = logOut;
    vm.viewNotificationNumber = false;
    return vm;
}]);