﻿'use strict';

angular.module('core').factory('ErrorHandlerService', ['toastr', function (toastr) {
    var vm = this;
    
    function DisplayErrorMessage(message, status) {
        if(status == 500)
        toastr.error(message);
    }

    vm.DisplayErrorMessage = DisplayErrorMessage;
    return vm;
}]);

