﻿(function () {
    'use strict';
    angular.module('case').factory('CaseDetails', ['$http', 'toastr', '$uibModal', 'Upload', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams',
    function ($http, toastr, $uibModal, Upload, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams) {
        var vm = this;
        vm.getCaseDetailsFromCaseNumber = getCaseDetailsFromCaseNumber;
        vm.setActiveTabClass = setActiveTabClass;

        function setActiveTabClass(path) {
            return ($location.path().indexOf(path) != -1) ? 'active' : '';
        }

        function getCaseDetailsFromCaseNumber() {
            $http.get("api/case/" + $stateParams.caseNumber + "/details")
                .success(function (response, status) {
                    vm.case = response;
                    vm.filesUploaded = 0;
                })
                .error(function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                });
        }
        return vm;
    }
    ]);
})();

