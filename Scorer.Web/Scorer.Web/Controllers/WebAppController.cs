﻿using System;
using System.Linq;
using System.Web.Mvc;
using Scorer.Core.Domain;
using Scorer.Web.ViewModels.UserManagement;
using Microsoft.AspNet.Identity;
using System.Web.Configuration;
using Scorer.Core.Services.Implementation;
using Scorer.Common.Database.Enums;
using System.Web.Script.Serialization;

namespace Scorer.Web.Controllers
{
    [Authorize]
    public class WebAppController : Controller
    {
        private IUserService _userService;
        private readonly IAzureFileManager _azureFileManager;    

        public WebAppController(IUserService userService, IAzureFileManager azureFileManager)
        {
            _userService = userService;
            _azureFileManager = azureFileManager;
        }

        [Route("case-management")]
        [Route("")]
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            var userId = new Guid();
            var userViewModel = new UserViewModel();
            var userIdString = User.Identity.GetUserId();
            if (!Guid.TryParse(userIdString, out userId))
            {
                return RedirectToAction("Login", "Account");
            }
            var userDetailsResult = _userService.GetUser(userId);
            if (!userDetailsResult.IsSuccess)
            {
                return RedirectToAction("Login", "Account");
            }
            var hasPasswordExpired = ValidatePasswordExpirationDate(userDetailsResult.Data);
            if (hasPasswordExpired)            
                return RedirectToAction("GeneratePasswordResetCodeAfterExpiration", "Account", new { userId = userDetailsResult.Data.Id });            
            userViewModel.Email = userDetailsResult.Data.Email;
            userViewModel.FirstName = userDetailsResult.Data.FirstName;
            userViewModel.LastName = userDetailsResult.Data.LastName;
            userViewModel.UserId = userDetailsResult.Data.Id;
            userViewModel.Role = userDetailsResult.Data.Role;
            userViewModel.Permissions = userDetailsResult.Data.Permissions.ToList();
            Guid profilePicKey = userDetailsResult.Data.ProfilePicKey ?? Guid.Empty;
            if (profilePicKey != Guid.Empty)
                userViewModel.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey);
            else
                userViewModel.ProfilePicUrl = string.Empty;//Common.Constants.ScorerConstants.PROFILE_PIC_URL;
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            ViewBag.User = javaScriptSerializer.Serialize(userViewModel);
            ViewBag.DefaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) ||
                (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            return View();
        }

        bool ValidatePasswordExpirationDate(Models.Account.User currentUserDetails)
        {
            if (currentUserDetails.LastEditedDateTime.AddDays(90) <= DateTime.Now)
                return true;
            else
                return false;
        }
    }
}