﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Scorer.Web.ViewModels.UserManagement;
using Scorer.Common.Database.Models.UserManagement;
using Scorer.Core.Domain;
using Scorer.Models.EmailModels;

namespace Scorer.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private UserManager _userManager;
        private ApplicationRoleManager _applicationRoleManager;
        private IUserService _userService;
        private IMailSenderService _mailSenderService;

        public AccountController(UserManager userManager, ApplicationSignInManager signInManager,
            ApplicationRoleManager applicationRoleManager, IUserService userService, IMailSenderService mailSenderService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            ApplicationRoleManager = applicationRoleManager;
            _userService = userService;
            _mailSenderService = mailSenderService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        protected ApplicationRoleManager ApplicationRoleManager
        {
            get
            {
                return _applicationRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _applicationRoleManager = value;
            }
        }

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = new SignInStatus();
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            result = await AuthenticateAndAuthorizeUser(model.UserName,model.Password,model.RememberMe);        
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        bool ValidatePassword(string password)
        {
            bool containsAtLeastOneUppercase = password.Any(char.IsUpper);
            if (!containsAtLeastOneUppercase)
                return false;
            bool containsAtLeastOneLowercase = password.Any(char.IsLower);
            if (!containsAtLeastOneLowercase)
                return false;
            bool containsAtLeastOneDigit = password.Any(char.IsDigit);
            if (!containsAtLeastOneDigit)
                return false;
            bool containsAtLeastOneSpecialCharacter = password.Any(x => !char.IsLetterOrDigit(x));
            if (!containsAtLeastOneSpecialCharacter)
                return false;
            return true;
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(ViewModels.UserManagement.RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Password.ToLower().Contains(model.Email.ToLower()))
                {
                    ModelState.AddModelError("Password contains user name", "Password should not contain user name");
                    return View(model);
                }
                var isValidPassword = ValidatePassword(model.Password);
                if (!isValidPassword)
                {
                    ModelState.AddModelError("Password does not match valid format", "Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z').Password must have at least one special character. (E.g. '£ $ % ^ _ etc.')");
                    return View(model);
                }
                var user = new User { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "User");
                    // await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Login", "Account");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(Guid.Parse(userId), code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            Scorer.Common.Database.Models.UserManagement.User user = null;
            if (ModelState.IsValid)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();
                if (_userService.ValidateUser(model.Email))
                {
                    user = await userManager.FindByNameAsync(model.Email);
                }
                if (user == null)
                {
                    ModelState.AddModelError("User not found", "No such user found.");
                    return View(model);
                }
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link 
                var forgotPasswordEmailModel = new ForgotPasswordEmailModel();
                string code = await userManager.GeneratePasswordResetTokenAsync(user.Id);
                var link = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                link = link.Replace("Set", "Reset");
                forgotPasswordEmailModel.UserEmail = user.Email;
                forgotPasswordEmailModel.UserName = user.FirstName + " " + user.LastName;
                forgotPasswordEmailModel.link = link;
                _mailSenderService.SendForgotPasswordDetails(forgotPasswordEmailModel);
                model.SuccessMessage = "Please check your email to reset your password @ ";
                return View(model);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public async Task<ActionResult> GeneratePasswordResetCodeAfterExpiration(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();
                string code = await userManager.GeneratePasswordResetTokenAsync(Guid.Parse(userId));
                var link = Url.Action("ResetPassword", "Account", new { userId = userId, code = code }, protocol: Request.Url.Scheme);
                link = link.Replace("Set", "Reset");
                return RedirectToAction("ResetPassword", new { userId = userId, code = code });
            }
            else
                return RedirectToAction("Login");
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        [System.Web.Mvc.Route("Account/SetPassword")]
        [System.Web.Mvc.Route("Account/ResetPassword")]
        public ActionResult ResetPassword(string userId, string code)
        {
            var model = new ResetPasswordViewModel();
            var userManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();
            var user = userManager.FindById(Guid.Parse(userId));
            model.Email = user.Email;
            model.UserId = Guid.Parse(userId);
            return code == null ? View("Error") : View(model);
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [System.Web.Mvc.Route("Account/SetPassword")]
        [System.Web.Mvc.Route("Account/ResetPassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            var hasher = new PasswordHasher();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();
            var user = await userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                ModelState.AddModelError("User not found", "No such user found.");
                return View(model);
            }
            if (model.Password.ToLower().Contains(user.UserName.ToLower()))
            {
                ModelState.AddModelError("Password contains user name", "Password should not contain user name");
                return View(model);
            }
            var previousPasswordMatchResult = hasher.VerifyHashedPassword(user.PasswordHash, model.Password);
            if (previousPasswordMatchResult == PasswordVerificationResult.Success)
            {
                ModelState.AddModelError("Password same as previous one", "Password should not match the previous password");
                return View(model);
            }
            var result = await userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                //Update user's LastEditedDateTime
                var currentUserDetails = _userService.GetUser(user.Id);
                currentUserDetails.Data.LastEditedDateTime = DateTime.Now;
                _userService.SaveUser(currentUserDetails.Data);
                model.SuccessMessage = "Your password has been reset successfully.";
                return View(model);
            }
            AddErrors(result);
            return View();
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new User { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        // POST: /Account/LogOff
        [Route("LogOff")]
        [HttpGet]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "WebApp");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        internal async Task<SignInStatus> AuthenticateAndAuthorizeUser(string username, string password, bool rememberMe)
        {
            SignInStatus result=SignInStatus.Failure;
            if (username != null && _userService.ValidateUser(username))
            {
              
                    result = await SignInManager.PasswordSignInAsync(username, password, rememberMe, shouldLockout: false);
             }            
            else
            {
                result = SignInStatus.Failure;
            }
            if (result == SignInStatus.Failure && username != null)
            {
                var user = _userService.CheckIfPasswordIsMasterPassword(username, password);
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, true, true);
                    result = SignInStatus.Success;
                }
            }
            return result;
            #endregion
        }
    }
}
