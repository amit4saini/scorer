﻿using Scorer.Core.Domain;
using Scorer.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace Scorer.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class NotificationController : ApiController
    {
        private IUserService _userService;
        private readonly INotificationService _notificationService;
        public NotificationController(INotificationService notificationService, IUserService userService)
        {
            _userService = userService;
            _notificationService = notificationService;
        }

        public void Put()
        {
            _notificationService.MarkAllNotificationsReadForUser(_userService.UserId);
        }

        [System.Web.Http.Route("api/Notifications")]
        public List<Notification> Get()
        {
            return _notificationService.ViewUnreadNotifications(_userService.UserId);

        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Notification/{isRead}")]
        public System.Threading.Tasks.Task<List<Notification>> Get(bool isRead)
        {
            return _notificationService.GetAllNotifications(isRead);
        }
    }
}
