﻿using System.Collections.Generic;
using System.Web.Http;
using Scorer.Core.Domain;

namespace Scorer.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CountryController : ApiController
    {
        private readonly ICountryService _countryService;

        public CountryController(ICountryService countryService)
        {
            _countryService = countryService;
        }

        // GET api/<controller>    
        [Route("api/Countries")]     
        public List<Scorer.Models.Country> Get()
        {
            var getAllCountriesResult = _countryService.GetAllCountries();
            return getAllCountriesResult;
        }

        // GET api/<controller>/5   
        [Route("api/Region/{id}/Countries")]
        public List<Scorer.Models.Country> Get(string id)
        {
            var getAllCountriesResult = _countryService.GetAllCountriesByRegion(id);
            return getAllCountriesResult;
        }
    }
}