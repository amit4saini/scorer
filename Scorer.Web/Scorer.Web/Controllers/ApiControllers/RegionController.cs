﻿using System.Collections.Generic;
using System.Web.Http;
using Scorer.Core.Domain;

namespace Scorer.Web.Controllers.ApiControllers
{
    [Authorize]
    public class RegionController : ApiController
    {
        private readonly IRegionService _regionService;
        public RegionController(IRegionService regionService)
        {
            _regionService = regionService;
        }

        // GET api/<controller>
        [System.Web.Http.Route("api/Regions")]
        public List<Scorer.Models.Region> Get()
        {
            var getAllRegionsResult = _regionService.GetAllRegions();
            return getAllRegionsResult;
        }

    }
}