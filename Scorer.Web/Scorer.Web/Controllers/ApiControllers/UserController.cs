﻿using Scorer.Models;
using Scorer.Models.Account;
using Scorer.Web.Attributes;
using Scorer.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Scorer.Core.Services.Implementation;
using System.Threading.Tasks;
using Scorer.Core.Domain;

namespace Scorer.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAzureFileManager _azureFileManager;
        public UserController(IUserService userService, IAzureFileManager azureFileManager)
        {
            _userService = userService;
            _azureFileManager = azureFileManager;
        }

        // [Queryable]
         [System.Web.Http.Route("api/Users")]    
        public PageResult<User> Get(ODataQueryOptions<User> options)
        {
            return _userService.GetAllUsers(options, Request);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/{id}")]
        public Result<User> GetUser(Guid id)
        {
            return _userService.GetUser(id);
        }
   
        public User Post(User userData)
        {
            return _userService.SaveUser(userData);
        }

        [System.Web.Http.Route("api/User/{id}")]
        //[HasPermission(new Permission[] { Permission.USER_UPDATE })]
        public User Put(Guid id, User userData)
        {
            userData.Id = id;
            return _userService.SaveUser(userData);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/GetRoles")]  
        public PageResult<Role> GetRoles(ODataQueryOptions<Role> options)
        {
            return _userService.GetAllRoles(options, Request);
        }

        [System.Web.Mvc.HttpDelete]
        [System.Web.Http.Route("api/User/{id}")]
        public Result Delete(Guid id)
        {
            return _userService.Delete(id);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/GetCurrentUser")]
        public async Task<User> GetCurrentUser()
        {
            var result = new User();
            result = _userService.GetCurrentUser();
            result.ProfilePicUrl = string.Empty;
            Guid profilePicKey = result.ProfilePicKey ?? Guid.Empty;
            if (profilePicKey != Guid.Empty)
                result.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey);
            else
                result.ProfilePicUrl = string.Empty;//Common.Constants.ScorerConstants.PROFILE_PIC_URL;
            return result;
        }
        
        [HttpGet]
        [Route("api/Search/Users")]
        public PageResult<User> SearchUsers(ODataQueryOptions<Models.Account.User> options, string keyword)
        {
            return _userService.SearchUsersByKeyword(options, Request, keyword);
        }
    }
}