﻿using Scorer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Scorer.Core.Domain;
using Scorer.Core.Services.Implementation;
using Scorer.Web.Attributes;
using Scorer.Common.Database.Enums;

namespace Scorer.Web.Controllers
{
    [System.Web.Http.Authorize]
    public class FileController : ApiController
    {
        private IAzureFileManager _azureFileManager;
        public IFileService _fileUploadService;
        private IErrorHandlerService _errorHandlerService;
        public FileController(IAzureFileManager azureFileManager, IFileService fileUploadService, IErrorHandlerService errorHandlerService)
        {
            _azureFileManager = azureFileManager;
            _fileUploadService = fileUploadService;
            _errorHandlerService = errorHandlerService;
        }

        [System.Web.Mvc.HttpPost]
        public async Task<List<FileModel>> Post()
        {
            var result = new List<FileModel>();
            var uploadedFiles = await _azureFileManager.Add(Request);
            var saveFileResult = _fileUploadService.Save(uploadedFiles.ToList());
            result = new List<FileModel>();
            result = saveFileResult;
            return result;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/File/{id}/{type}")]
        public async Task<string> Get(Guid id)
        {
            return await _azureFileManager.DownloadFile(id);
        }

        [HttpPut]
        public void RenameFile(Guid fileId,FileModel file)
        {
            _fileUploadService.RenameFile(file);
        }
    }
}
