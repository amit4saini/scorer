﻿using System.Collections.Generic;
using System.Web.Http;
using Scorer.Core.Domain;

namespace Scorer.Web.Controllers.ApiControllers
{
    [Authorize]
    public class StateController : ApiController
    {
        private readonly IStateService _stateService;
        public StateController(IStateService stateService)
        {
            _stateService = stateService;
        }

        // GET api/<controller>/5    
        [Route("api/Country/{id}/States")]
        public List<Scorer.Models.State> Get(string id)
        {
            var listOfStatesBasedOnCountryIdResult = _stateService.ListOfStatesBasedOnCountryId(id);
            return listOfStatesBasedOnCountryIdResult;
        }
    }
}