// Karma configuration
// Generated on Fri Apr 22 2016 12:16:24 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
       "../Scorer.Web/Scripts/textAngular-rangy.min.js",
      '../Scorer.Web.Tests/UI/Scripts/jasmine/angular.js',
      "../Scorer.Web/Scripts/textAngular-sanitize.min.js",
      "../Scorer.Web/Scripts/textAngularSetup.js",
      "../Scorer.Web/Scripts/textAngular.min.js",
      '../Scorer.Web.Tests/UI/Scripts/jasmine/angular-mocks.js',
	  '../Scorer.Web.Tests/UI/Scripts/jasmine/angular-resource.js',	
	  "../Scorer.Web/Scripts/jquery-2.2.0.min.js",                 
     "../Scorer.Web/Scripts/toastr.min.js",
      "../Scorer.Web/Scripts/jquery-ui.js",
      "../Scorer.Web/Scripts/angular-resource.js",
      "../Scorer.Web/Scripts/angular-animate.js",
      "../Scorer.Web/Scripts/AngularUI/ui-router.js",
      "../Scorer.Web/Scripts/angular-ui/ui-bootstrap.js",
      "../Scorer.Web/Scripts/angular-ui/ui-bootstrap-tpls.js",
      "../Scorer.Web/Scripts/angular-file-upload.min.js",
      "../Scorer.Web/Scripts/angular-toastr.tpls.min.js",
      "../Scorer.Web/Scripts/angular-cookies.js",
      "../Scorer.Web/Scripts/angular-route.js",
      "../Scorer.Web/Scripts/angular-route.min.js",
      "../Scorer.Web/Scripts/spin.min.js",
      "../Scorer.Web/Scripts/angular-spinner.min.js",    
      "../Scorer.Web/Scripts/ng-file-upload-master/dist/ng-file-upload.min.js",
      "../Scorer.Web/Scripts/select.min.js",
      "../Scorer.Web/Scripts/moment.min.js",
      "../Scorer.Web/Scripts/angular-bootstrap-calendar-tpls.min.js",
      "../Scorer.Web/WebApp/modules/cases/Directives/checkbox-model.directive.js",
      "../Scorer.Web/Scripts/angular-filter.min.js",
      "../Scorer.Web/Scripts/tinymce/jquery.tinymce.min.js",
      "../Scorer.Web/Scripts/tinymce/tinymce.min.js",
       "../Scorer.Web/Scripts/AngularUI/tinymce.js",
      
       "../Scorer.Web/Scripts/ment.io/dist/mentio.js",
       "../Scorer.Web/Scripts/ment.io/dist/templates.js",
	  '../Scorer.Web/WebApp/config.js',
	  '../Scorer.Web/WebApp/init.js',

	    '../Scorer.Web/WebApp/modules/users/users.client.module.js',
	  "../Scorer.Web/WebApp/modules/users/services/authentication.client.service.js",	
	  '../Scorer.Web/WebApp/modules/cases/cases.module.js',
	  '../Scorer.Web/WebApp/modules/core/core.module.js',
      '../Scorer.Web/WebApp/modules/calendar/calendar.module.js',
	  '../Scorer.Web/WebApp/modules/clients/clients.module.js',
      '../Scorer.Web/WebApp/modules/tasks/tasks.module.js',
      '../Scorer.Web/WebApp/modules/dashboard/dashboard.client.module.js',
     '../Scorer.Web/WebApp/modules/cases/services/cases.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/caseSpec.js',
      '../Scorer.Web/WebApp/modules/calendar/services/calendar.service.js',
      '../Scorer.Web.Tests/UI/Test/spec/calendarSpec.js',
       '../Scorer.Web/WebApp/modules/users/services/users.client.service.js',
       '../Scorer.Web.Tests/UI/Test/spec/userSpec.js',
       '../Scorer.Web/WebApp/modules/clients/services/clients.service.js',
       '../Scorer.Web.Tests/UI/Test/spec/clientSpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/case-discussions.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/caseDiscussionSpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/expenses.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/expenseSpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/timesheets.service.js',
    '../Scorer.Web.Tests/UI/Test/spec/timeSheetSpec.js',
      '../Scorer.Web/WebApp/modules/cases/services/casetasks.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/caseTaskSpec.js',
      '../Scorer.Web/WebApp/modules/cases/services/casehistory.service.js',
    '../Scorer.Web.Tests/UI/Test/spec/caseHistorySpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/case-details.service.js',
    '../Scorer.Web.Tests/UI/Test/spec/caseDetailsSpec.js',
     '../Scorer.Web/WebApp/modules/core/services/header.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/headerSpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/participants.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/participantSpec.js',
      '../Scorer.Web/WebApp/modules/core/services/global.variables.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/globalSpec.js',
     '../Scorer.Web/WebApp/modules/cases/services/case-users.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/caseUserSpec.js',
      '../Scorer.Web/WebApp/modules/cases/services/case-nfcusers.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/caseNFCUsersSpec.js',
     '../Scorer.Web/WebApp/modules/dashboard/services/dashboard.service.js',
     '../Scorer.Web.Tests/UI/Test/spec/dashboardSpec.js',
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        '../Scorer.Web/WebApp/modules/cases/services/cases.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/clients/services/clients.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/users/services/users.client.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/calendar/services/calendar.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/discussions.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/expenses.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/timesheets.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/casetasks.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/casehistory.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/case-details.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/users/services/usersettings.client.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/core/services/header.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/cases/services/participants.service.js': ['coverage'],
        '../Scorer.Web/WebApp/modules/tasks/services/tasks.service.js': ['coverage'],
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
