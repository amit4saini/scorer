﻿using Scorer.Common.Infrastructure.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Scorer.Web.Tests.Common.Infrastructure.Email
{
    [TestClass]
    public class EmailServiceTestClass
    {
        IEmailService _emailService = new Scorer.Common.Infrastructure.Email.EmailService();
        [TestInitialize]
        public void TestInitialize()
        {
            _emailService.SendTo = new List<string>();
            _emailService.SendTo.Add("shubham.rathi@innostax.com");
            _emailService.SendTo.Add("abc@gmail.com");
            _emailService.EmailFrom = "dummy@innostax.com";
            _emailService.AutoText = true;

            _emailService.Bcc = new List<string>();
            _emailService.Bcc.Add("bcc1@innostax.com");            
            _emailService.Cc = new List<string>();
            _emailService.Cc.Add("cc1@innostax.com");          
            _emailService.Template = "CASE_ADDED";
            _emailService.Subject = "Testing Subject............";
            _emailService.MailContent = "Test Content";
            _emailService.EmailFromName = "It's From me.";           
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("User", "TestUser");
            _emailService.TokenData.Add("Admin", "Testadmin");
            
        }
        [TestMethod]
        public void GivenCallingSendEmail_IfSuccessful_ThenReturnTrue()
        {
             _emailService.SendEmail();
        }

        [TestMethod] 
        public void GivenCallingSendEmail_IfIncorrectDetails_Then_CompareErrorMessage()
        {                  
            _emailService.SendTo = null;
            try
            {
                _emailService.SendEmail();
            }
            catch (Exception e)
            {
                Assert.IsNotNull(e);
            }
                    

        }
    }
}
