﻿'use strict';
describe('CaseTasksController', function () {
    var CaseHeader;
    var tasks;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1,
                taskId: 1,
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: "1000",
                    caseNumber: "2000"
                }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $provide.value('CaseHeader', CaseHeader);
            $global = {
                getExistingCaseAssociatedTasks: function () { return; }
            };
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            tasks = $injector.get('CaseTasks', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
        tasks.modalInstance = {
            dismiss: function () { return; }
        };
        tasks.allPages = {
            length: 0
        };
        tasks.case = {
            caseId: 1
        };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("getExistingTaskComments", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { tasks.taskComments = response; };
            cbError = function (response, status) { tasks.taskComments = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('/api/taskcomment?TaskId=1').respond(200, [{ caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            tasks.getExistingTaskComments(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.taskComments).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('/api/taskcomment?TaskId=1').respond(500, 'Some problem has occured');
            tasks.getExistingTaskComments(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.taskComments).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveNewTaskComment", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Comment added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            tasks.newTaskComment = { name: "dummy" };
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST('/api/taskcomment/', tasks.newTaskComment).respond(200, 'Success');
            tasks.saveNewTaskComment(tasks.newTaskComment, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Comment added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST('/api/taskcomment/', tasks.newTaskComment).respond(500, 'Some problem has occured');
            tasks.saveNewTaskComment(tasks.newTaskComment, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveEditedTaskComment", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Comment edited"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            tasks.newTaskComment = { name: "dummy", id: 1 };
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT('/api/taskcomment/1', tasks.newTaskComment).respond(200, 'Success');
            tasks.saveEditedTaskComment(tasks.newTaskComment, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Comment edited");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT('/api/taskcomment/1', tasks.newTaskComment).respond(500, 'Some problem has occured');
            tasks.saveEditedTaskComment(tasks.newTaskComment, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("deleteExistingTaskComment", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Comment archived"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectDELETE('/api/taskcomment?id=1').respond(200, 'Success');
            tasks.deleteExistingTaskComment(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Comment archived");
        });
        it('Negative Test', function () {
            $httpBackend.expectDELETE('/api/taskcomment?id=1').respond(500, 'Some problem has occured');
            tasks.deleteExistingTaskComment(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveNewTask", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Task added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            tasks.task = { name: "dummy" };
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST('/api/task/', tasks.task).respond(200, 'Success');
            tasks.saveNewTask(tasks.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST('/api/task/', tasks.task).respond(500, 'Some problem has occured');
            tasks.saveNewTask(tasks.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("getSelectedTask", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { tasks.task = response; };
            cbError = function (response, status) { tasks.task = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/task/1').respond(200, [{ taskName: "dummyText", id: 1 }]);
            tasks.getSelectedTask(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.task).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/task/1').respond(500, 'Some problem has occured');
            tasks.getSelectedTask(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.task).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("getSelectedTaskFiles", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { tasks.taskFiles = response; };
            cbError = function (response, status) { tasks.taskFiles = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/TaskFiles/1').respond(200, [{ fileName: "dummyText", id: 1 }, { fileName: "dummyText1", id: 2 }]);
            tasks.getSelectedTaskFiles(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.taskFiles.length).toEqual(2);
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/TaskFiles/1').respond(500, 'Some problem has occured');
            tasks.getSelectedTaskFiles(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(tasks.taskFiles).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("deletExistingTaskFile", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("File Deleted") };
            cbError = function (response, status) { tasks.taskFiles = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectDELETE('api/TaskFiles/1/File/1').respond(200, 'Success');
            tasks.deletExistingTaskFile(1, 1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("File Deleted");
        });
        it('Negative Test', function () {
            $httpBackend.expectDELETE('api/TaskFiles/1/File/1').respond(500, 'Some problem has occured');
            tasks.deletExistingTaskFile(1, 1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("postTaskFiles", function () {
        beforeEach(function () {
            tasks.task = { name: "dummy" };
            cbSuccess = function (response, status) { toastr.success("File Deleted") };
            cbError = function (response, status) { tasks.taskFiles = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST('api/TaskFiles',tasks.task).respond(200, 'Success');
            tasks.postTaskFiles(tasks.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("File Deleted");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST('api/TaskFiles',tasks.task).respond(500, 'Some problem has occured');
            tasks.postTaskFiles(tasks.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});