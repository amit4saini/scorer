﻿'use strict';
describe('CaseController', function () {
    var authentication;
    var Cases;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var Upload;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            $provide.value('Upload', Upload);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $provide.value('Authentication', authentication);
            $global = {
                getExistingUsers: function () {
                    return;
                }
            }
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Cases = $injector.get('Cases', { Authentication: authentication, toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService, Enums: Enums, $location: $location, $anchorScroll: $anchorScroll, Upload: Upload, $stateParams: $stateParams, $global: $global });
        });
        Cases.modalInstance = {
            dismiss: function () { return; }
        };
        Cases.allPages = {
            length: 0
        };
        Cases.allDiscussionPages = {
            length: 0
        };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("assign case to user", function () {
        beforeEach(function () {
            Cases.case = {
                caseId: "1000",
                assignedToUser:{id:"2000", firstName:"John", lastName:"Smith"},
            };
            cbSuccess = function (response, status) {
                toastr.success("Case assigned to " + Cases.case.assignedToUser.firstName + " " + Cases.case.assignedToUser.lastName);
                closeModal();
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST('/api/case/1000/2000/user').respond(200, { data: [{ Name: "test1" }, { Name: "test2" }] });
            Cases.assignCaseToUser(Cases.case.caseId, Cases.case.assignedToUser.id, cbSuccess,cbError);
            $httpBackend.flush();
            expect(Cases.result).not.toBeNull();
            expect(toastr.success).toHaveBeenCalledWith("Case assigned to John Smith");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST('/api/case/1000/2000/user').respond(500, 'Some problem has occured');
            Cases.assignCaseToUser(true);                
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Some problem has occured", 500);
        });
    });
    describe("getExistingReportTypes", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Cases.reports = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Cases.reports = []; };
        });
        it('Positive Test', function () {
            $httpBackend.expectGET("/api/reports").respond(200, { data: [{ reportName: "abc" }, { reportName: "xyz" }] });
            Cases.getExistingReportTypes(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.reports.data.length).toEqual(2);
            expect(Cases.reports).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/reports").respond(500, 'Some problem has occured');
            Cases.getExistingReportTypes(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.reports.length).toEqual(0);
        });
    });
    describe("getExistingClients", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Cases.clients = response; } ;
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Cases.clients = []; } ;
        });
        it('Positive Test', function () {
            $httpBackend.expectGET("/api/getAllClients").respond(200, { data: [{ clientname: "dummyText" }, { clientname: "Akshat" }] });
            Cases.getExistingClients(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.clients.data.length).toEqual(2);
            expect(Cases.clients).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/getAllClients").respond(404, { data: [] });
            Cases.getExistingClients(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.clients.length).toEqual(0);
        });
    });
    describe("getExistingDiscounts", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Cases.discountOptions = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Cases.discountOptions = []; };
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/discounts").respond(200, [{ discountAmount: 2.0 }, { discountAmount: 3.55 }]);
            Cases.getExistingDiscounts(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.discountOptions.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/discounts").respond(500, "Error Message");
            Cases.getExistingDiscounts(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("getExistingCaseByCaseId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                Cases.case = response;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
                
                it('Positive Test', function () {
                    $httpBackend.expectGET('/api/case/1').respond(200, { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] });
                    Cases.getExistingCaseByCaseId(1, cbSuccess, cbError);
                    $httpBackend.flush();
                    expect(Cases.case).not.toBeNull();
                });
                it('Negative Test', function () {
                    $httpBackend.expectGET('/api/case/1').respond(500, 'Some problem has occured');
                    Cases.getExistingCaseByCaseId(1, cbSuccess, cbError);
                    $httpBackend.flush();
                    expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
                });
    });
    describe("getExistingCasesByKeyword", function () {
        beforeEach(function () {
            Cases.keyword = "Dummy";
            Cases.options = '&$top=1&$skip=0&$orderby=LastEditedDateTime desc';
            cbSuccess = function (response, status) {
                Cases.case = response.items;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Cases.cases = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/search/cases?keyword=' + Cases.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(200, { items: { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] } });
            Cases.getExistingCasesByKeyword(Cases.keyword, Cases.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.case).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/search/cases?keyword=' + Cases.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(500, 'Some problem has occured');
            Cases.getExistingCasesByKeyword(Cases.keyword, Cases.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Cases.case).toBeUndefined();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
       describe("deleteExistingCase", function () {
                beforeEach(function(){
                    Cases.pageIndex = 1;
                    Cases.cases = [];
                    Cases.caseId = 1;
                    Cases.defaultPageSize = 10;
                    cbSuccess=function (response, status) {
                        toastr.success("Case archived");
                    };
                    cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
                });
                it("Postive", function () {
                    $httpBackend.expectDELETE('api/case/1').respond(200, 'success');
                    Cases.deleteExistingCase(Cases.caseId, cbSuccess, cbError);
                    $httpBackend.flush();
                    expect(toastr.success).toHaveBeenCalledWith("Case archived");
                });
                it("Negative", function () {
                    $httpBackend.expectDELETE('api/case/1').respond(500, 'invalid Case');
                    Cases.deleteExistingCase(Cases.caseId, cbSuccess, cbError);
                    $httpBackend.flush();
                    expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('invalid Case', 500);
                });
            });
    });

