﻿'use strict'
describe('CaseDetailsController', function () {
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var CaseDetails;
    var CaseTasks;
    var CaseHeader;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: null
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                DownloadFileType: {
                    'CaseFile': 0,
                    'ProfilePic': 1,
                    'DiscussionFile': 2,
                },
            };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            CaseTasks = {
                openTaskCommentsModal: function () { return; }
            }
            $provide.value('CaseTasks', CaseTasks);
            $global = {
                downloadExistingFile: function () { return; }
            }
            $provide.value('$global', $global);
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: 1
                }
            };
            $provide.value('CaseHeader', CaseHeader);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            CaseDetails = $injector.get('CaseDetails', { toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService, Enums: Enums, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    
    describe("saveCaseFileAssociations", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("File added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            CaseDetails.case = {name:"Dummy", caseId:1};
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT("/api/caseFiles/1",CaseDetails.case).respond(200, 'Success' );
            CaseDetails.saveCaseFileAssociations(CaseDetails.case, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("File added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT("/api/caseFiles/1",CaseDetails.case).respond(500, 'Some problem has occured');
            CaseDetails.saveCaseFileAssociations(CaseDetails.case, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
   
    describe("saveEditedFile", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("File renamed"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            CaseDetails.file = { name: "Dummy", fileId: 1 };
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT("/api/file?fileId=1", CaseDetails.file).respond(200, 'Success');
            CaseDetails.saveEditedFile(CaseDetails.file, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("File renamed");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT("/api/file?fileId=1", CaseDetails.file).respond(500, 'Some problem has occured');
            CaseDetails.saveEditedFile(CaseDetails.file, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
   
    describe("getExistingCaseAssociatedFiles", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { CaseDetails.allFilesList = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET("api/caseAllFiles/1").respond(200, [{ name: 'dummy1' }, { name: 'dummy2' }]);
            CaseDetails.getExistingCaseAssociatedFiles(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.allFilesList.length).toEqual(2);
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("api/caseAllFiles/1").respond(500, 'Some problem has occured');
            CaseDetails.getExistingCaseAssociatedFiles(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("getTaskDetailsByTaskId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { CaseDetails.tasks = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET("api/task/1").respond(200, [{ name: 'dummy1' }, { name: 'dummy2' }]);
            CaseDetails.getTaskDetailsByTaskId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.tasks.length).toEqual(2);
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("api/task/1").respond(500, 'Some problem has occured');
            CaseDetails.getTaskDetailsByTaskId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("saveAssignedCase", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Case assigned to Dummy Name"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST("/api/case/1/1/user").respond(200, 'Success' );
            CaseDetails.saveAssignedCase(1,1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case assigned to Dummy Name");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST("/api/case/1/1/user").respond(500, 'Some problem has occured');
            CaseDetails.saveAssignedCase(1,1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
   
    describe("getExistingReportTypes", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { CaseDetails.reports = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status);CaseDetails.reports = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET("/api/reports").respond(200, [{ name: 'dummy1' }, { name: 'dummy2' }]);
            CaseDetails.getExistingReportTypes(cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.reports.length).toEqual(2);
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/reports").respond(500, 'Some problem has occured');
            CaseDetails.getExistingReportTypes(cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.reports.length).toEqual(0);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("getExistingDiscounts", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { CaseDetails.discountOptions = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status);CaseDetails.discountOptions = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET("/api/discounts").respond(200, [{ name: 'dummy1' }, { name: 'dummy2' }]);
            CaseDetails.getExistingDiscounts(cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.discountOptions.length).toEqual(2);
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/discounts").respond(500, 'Some problem has occured');
            CaseDetails.getExistingDiscounts(cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseDetails.discountOptions.length).toEqual(0);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});
