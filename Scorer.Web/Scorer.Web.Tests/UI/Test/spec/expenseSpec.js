﻿'use strict';
describe('ExpenseController', function () {
    var CaseHeader;
    var Expenses;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var Upload;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1
            };
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: "1000",
                    caseNumber: "2000"
                }
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            $provide.value('Upload', Upload);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $provide.value('CaseHeader', CaseHeader);
            $global = {
                validateCurrency: function () { return; }
            }
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Expenses = $injector.get('Expenses', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, Upload: Upload, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
        Expenses.modalInstance = {
            dismiss: function () { return; }
        };
        Expenses.allPages = {
            length: 0
        };
        Expenses.allDiscussionPages = {
            length: 0
        };
        Expenses.currentCase = { caseId: 5000 };
        Expenses.case = { caseId: 5000 };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    
    describe("getExistingVendors", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Expenses.vendors = response; };
            cbError = function (response, status) { Expenses.vendors = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/Vendor').respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            Expenses.getExistingVendors(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Expenses.vendors).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/Vendor').respond(500, 'Some problem has occured');
            Expenses.getExistingVendors(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Expenses.vendors).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("getExistingExpenses", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Expenses.allExpenses = response; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Expenses.allExpenses = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET("api/case/1/caseExpenses").respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            Expenses.getExistingExpenses(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(Expenses.allExpenses).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET("api/case/1/caseExpenses").respond(500, 'Some problem has occured');
            Expenses.getExistingExpenses(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(Expenses.allExpenses).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("deleteExistingExpense", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Case expense removed"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectDELETE("/api/CaseExpense?id=1").respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            Expenses.deleteExistingExpense(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case expense removed");
        });
        it('Negative Test', function () {
            $httpBackend.expectDELETE("/api/CaseExpense?id=1").respond(500, 'Some problem has occured');
            Expenses.deleteExistingExpense(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("saveNewExpense", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Case expense added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            Expenses.caseExpense = {name:"Dummy"};
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST("/api/CaseExpense",Expenses.caseExpense).respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            Expenses.saveNewExpense(Expenses.caseExpense,cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case expense added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST("/api/CaseExpense",Expenses.caseExpense).respond(500, 'Some problem has occured');
            Expenses.saveNewExpense(Expenses.caseExpense,cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveEditedExpense", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Case expense edited"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            Expenses.caseExpense = {name:"Dummy",id:1};
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT("/api/CaseExpense/1",Expenses.caseExpense).respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            Expenses.saveEditedExpense(Expenses.caseExpense,cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case expense edited");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT("/api/CaseExpense/1",Expenses.caseExpense).respond(500, 'Some problem has occured');
            Expenses.saveEditedExpense(Expenses.caseExpense,cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});
