﻿'use strict';
describe('TimeSheetController', function () {
    var CaseHeader;
    var TimeSheets;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1
            };
            CaseHeader = {
                getCaseHeaderFromCaseNumber: function () { return true; },
                case: {
                    caseId: "1000",
                    caseNumber: "2000"
                }
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $provide.value('CaseHeader', CaseHeader);
            $global = {
                getExistingUsers : function(){return;}
            }
            $provide.value("$global", $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            TimeSheets = $injector.get('Timesheets', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
        TimeSheets.modalInstance = {
            dismiss: function () { return; }
        };
        TimeSheets.allPages = {
            length: 0
        };
        TimeSheets.allDiscussionPages = {
            length: 0
        };
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("getExistingTimeSheets", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { TimeSheets.allTimeSheets = response; };
            cbError = function (response, status) { TimeSheets.allTimeSheets = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/case/1/timesheets').respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            TimeSheets.getExistingTimesheets(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(TimeSheets.allTimeSheets).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/case/1/timesheets').respond(500, 'Some problem has occured');
            TimeSheets.getExistingTimesheets(1,cbSuccess, cbError);
            $httpBackend.flush();
            expect(TimeSheets.allTimeSheets).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });//
    });
    
    describe("getExistingTimesheetsDescriptions", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { TimeSheets.allTimesheetDescriptions = response; };
            cbError = function (response, status) { TimeSheets.allTimesheetDescriptions = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('/api/timesheetDescription').respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            TimeSheets.getExistingTimesheetsDescriptions(cbSuccess, cbError);
            $httpBackend.flush();
            expect(TimeSheets.allTimesheetDescriptions).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('/api/timesheetDescription').respond(500, 'Some problem has occured');
            TimeSheets.getExistingTimesheetsDescriptions(cbSuccess, cbError);
            $httpBackend.flush();
            expect(TimeSheets.allTimesheetDescriptions).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

describe("saveNewTimesheet", function () {
    beforeEach(function () {
        cbSuccess = function (response, status) {  toastr.success("Timesheet added"); };
        cbError = function (response, status) {  ErrorHandlerService.DisplayErrorMessage(response, status); };
        TimeSheets.timesheet = { name : "dummy" };
    });

    it('Positive Test', function () {
        $httpBackend.expectPOST('/api/timesheet/',TimeSheets.timesheet).respond(200, 'Success');
        TimeSheets.saveNewTimesheet(TimeSheets.timesheet, cbSuccess, cbError);
        $httpBackend.flush();
        expect(toastr.success).toHaveBeenCalledWith("Timesheet added");
    });
    it('Negative Test', function () { 
        $httpBackend.expectPOST('/api/timesheet/',TimeSheets.timesheet).respond(500, 'Some problem has occured');
        TimeSheets.saveNewTimesheet(TimeSheets.timesheet, cbSuccess, cbError);
        $httpBackend.flush();
        expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
    });
});

describe("saveEditedTimesheet", function () {
    beforeEach(function () {
        cbSuccess = function (response, status) {  toastr.success("Timesheet edited"); };
        cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        TimeSheets.timesheet = { name : "dummy", id:1 };
    });

    it('Positive Test', function () {
        $httpBackend.expectPUT('/api/timesheet/1',TimeSheets.timesheet).respond(200, 'Success');
        TimeSheets.saveEditedTimesheet(TimeSheets.timesheet, cbSuccess, cbError);
        $httpBackend.flush();
        expect(toastr.success).toHaveBeenCalledWith("Timesheet edited");
    });
    it('Negative Test', function () { 
        $httpBackend.expectPUT('/api/timesheet/1',TimeSheets.timesheet).respond(500, 'Some problem has occured');
        TimeSheets.saveEditedTimesheet(TimeSheets.timesheet, cbSuccess, cbError);
        $httpBackend.flush();
        expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
    });
});

    describe("deleteExistingTimesheet", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {  toastr.success("Timesheet removed"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectDELETE('/api/timesheet/1').respond(200,'Success');
            TimeSheets.deleteExistingTimesheet(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Timesheet removed");
        });
        it('Negative Test', function () { 
            $httpBackend.expectDELETE('/api/timesheet/1').respond(500, 'Some problem has occured');
            TimeSheets.deleteExistingTimesheet(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});
