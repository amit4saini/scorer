﻿'use strict';
describe('CaseHistoryController', function () {
    var CaseHeader;
    var caseHistory;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var cbSuccess;
    var cbError;
    var $global;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1
            };
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: "1000",
                    caseNumber: "2000"
                }
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                Actions: {
                    'CASE_ADDED': 0,
                    'CASE_EDITED': 1,
                    'CASE_DELETED': 2,
                    'CASE_EXPENSE_ADDED': 3,
                    'CASE_EXPENSE_EDITED': 4,
                    'CASE_EXPENSE_DELETED': 5,
                    'CASE_TIMESHEET_ADDED': 6,
                    'CASE_TIMESHEET_EDITED': 7,
                    'CASE_TIMESHEET_DELETED': 8,
                    'FILE_ADDED': 9,
                    'FILE_DELETED': 10,
                    'CASE_TASK_EDITED': 11,
                    'CASE_TASK_ADDED': 12,
                    'CASE_TASK_DELETED': 13,
                    'CASE_NOTES_ADDED': 14,
                    'CASE_NOTES_EDITED': 15,
                    'CASE_NOTES_DELETED': 16,
                    'CASE_PRINCIPAL_DELETED': 17,
                    'CASE_PRINCIPAL_ADDED': 18,
                    'CASE_ASSIGNED': 19,
                    'CASE_DISCUSSION_ADDED': 20,
                    'CASE_DISCUSSION_EDITED': 21,
                    'CASE_DISCUSSION_DELETED': 22,
                    'CASE_DISCUSSION_ARCHIVED': 23,
                    'CASE_DISCUSSION_UNARCHIVED': 24,
                    'CASE_DISCUSSION_MESSAGE_ADDED': 25,
                    'CASE_DISCUSSION_MESSSAGE_EDITED': 26,
                    'CASE_DISCUSSION_MESSAGE_DELETED': 27,
                    'CASE_DISCUSSION_MESSAGE_ARCHIVED': 28,
                    'CASE_DISCUSSION_MESSAGE_UNARCHIVED': 29,
                    'CASE_REPORT_ADDED': 30
                },
                HistoryFilter: {
                    'TASK': 0,
                    'EXPENSES': 1,
                    'CASE_STATUS': 2,
                    'DISCUSSION': 3,
                    'ALL': 4
                },
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $provide.value('CaseHeader', CaseHeader);
            $global = {
                roleName: 'Dummy'
            };
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            caseHistory = $injector.get('CaseHistory', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
        caseHistory.case = { caseId: 1 };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("getExistingCaseHistory", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { caseHistory.allCaseHistories = response.items; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };

        });
            it("Positive Test", function () {
                $httpBackend.expectGET('api/casehistory/1/case/4').respond(200, { items: [{ caseId: 1 }], count: 5 });
                caseHistory.getExistingCaseHistory(1, Enums.HistoryFilter.ALL,cbSuccess, cbError );
                $httpBackend.flush();
                expect(caseHistory.allCaseHistories.length).toEqual(1);
            });
            it("Negative Test", function () {
                $httpBackend.expectGET('api/casehistory/1/case/4').respond(500, "Error Message");
                caseHistory.getExistingCaseHistory(1, Enums.HistoryFilter.ALL, cbSuccess, cbError);
                $httpBackend.flush();
                expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            });
    });
});