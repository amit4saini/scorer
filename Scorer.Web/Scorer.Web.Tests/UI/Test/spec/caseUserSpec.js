﻿'use strict'
describe('CaseUsersController', function () {
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var CaseUser;
    var CaseHeader;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: null
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                DownloadFileType: {
                    'CaseFile': 0,
                    'ProfilePic': 1,
                    'DiscussionFile': 2,
                },
            };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $global = {
                downloadExistingFile: function () { return; },
                regex: {
                    email:"Dummy"
                }
            }
            $provide.value('$global', $global);
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: 1
                }
            };
            $provide.value('CaseHeader', CaseHeader);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            CaseUser = $injector.get('CaseUsers', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService, Enums: Enums, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("getExistingICIUserCaseAssociationsByCaseId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                CaseUser.iciCaseUsers = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseUser.iciCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/iciUserCaseAssociation/1").respond(200, [{ userName: 'dummy', id: 1000, caseId: 1 }, { userName: 'dummy1', caseId: 2, id: 1001 }]);
            CaseUser.getExistingICIUserCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseUser.iciCaseUsers.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/iciUserCaseAssociation/1").respond(500, "Error Message");
            CaseUser.getExistingICIUserCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(CaseUser.iciCaseUsers.length).toEqual(0);
        });
    });

    describe("saveNewICIUserCaseAssociation", function () {
        beforeEach(function () {
            CaseUser.iciUserCaseAssociation = { name: "dummy" };
            cbSuccess = function (response, status) {
                toastr.success("ICIUser Added")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseUser.iciCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("api/iciUserCaseAssociation", CaseUser.iciUserCaseAssociation).respond(200, 'Success');
            CaseUser.saveNewICIUserCaseAssociation(CaseUser.iciUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("ICIUser Added");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("api/iciUserCaseAssociation", CaseUser.iciUserCaseAssociation).respond(500, "Error Message");
            CaseUser.saveNewICIUserCaseAssociation(CaseUser.iciUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });

    describe("saveEditedICIUserCaseAssociation", function () {
        beforeEach(function () {
            CaseUser.iciUserCaseAssociation = { name: "dummy", id: 1 };
            cbSuccess = function (response, status) {
                toastr.success("ICIUser Edited")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseUser.iciCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("api/iciUserCaseAssociation/1", CaseUser.iciUserCaseAssociation).respond(200, 'Success');
            CaseUser.saveEditedICIUserCaseAssociation(CaseUser.iciUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("ICIUser Edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("api/iciUserCaseAssociation/1", CaseUser.iciUserCaseAssociation).respond(500, "Error Message");
            CaseUser.saveEditedICIUserCaseAssociation(CaseUser.iciUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });

});