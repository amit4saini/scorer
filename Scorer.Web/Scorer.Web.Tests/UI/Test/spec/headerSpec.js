﻿'use strict';
describe('HeaderController', function () {
    var Header;
    var Cases;
    var toastr;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var $location;
    var $interval;
    var $global;
    var Enums;
    var CaseTasks;
    var CaseDiscussions;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('core');
        module('core', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            $interval = function () { };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $provide.value('document', document);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $global = {
                downloadExistingFile: function () { return; }
            }
            $provide.value('$global', $global);
            $provide.value('$interval', $interval);
            Cases = {
                openCaseDetails: function () {
                    return;
                }
            }
            $provide.value('Cases', Cases);
            CaseTasks = {
                openTaskCommentsModal: function () { return; }
            }
            $provide.value('CaseTasks', CaseTasks);
            $provide.value('CaseDiscussions', CaseDiscussions);
            $provide.value('Enums', Enums);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Header = $injector.get('Header', { toastr: toastr, ErrorHandlerService: ErrorHandlerService, $window: $window, $location: $location, Cases: Cases, $global: $global, $interval: $interval, CaseDiscussions: CaseDiscussions, CaseTasks: CaseTasks, Enums: Enums });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
   
    describe("getExistingNotificationsByIsReadProperty", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Header.allNotifications = response; };
            cbError = function (response, status) { Header.allNotifications = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('/api/notification/true').respond(200,  [ { notificationText: "dummyText", url:"/dummy/text" }]);
            Header.getExistingNotificationsByIsReadProperty(true, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.allNotifications).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('/api/notification/true').respond(500, 'Some problem has occured');
            Header.getExistingNotificationsByIsReadProperty(true, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.allNotifications).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("getExistingUnreadNotifications", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Header.unreadNotifications = response; };
            cbError = function (response, status) { Header.unreadNotifications = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('/api/notifications/').respond(200,  [ { notificationText: "dummyText", url:"/dummy/text" }]);
            Header.getExistingUnreadNotifications(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.unreadNotifications).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('/api/notifications/').respond(500, 'Some problem has occured');
            Header.getExistingUnreadNotifications(cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.unreadNotifications).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("saveNotificationsStatus", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Notification Saved") };
            cbError = function (response, status) { Header.unreadNotifications = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT('/api/notification/').respond(200,  [ { notificationText: "dummyText", url:"/dummy/text" }]);
            Header.saveNotificationsStatus(cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Notification Saved");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT('/api/notification/').respond(500, 'Some problem has occured');
            Header.saveNotificationsStatus(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    describe("getMatchedResultsByKeyword", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Header.cases = response; };
            cbError = function (response, status) { Header.cases = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/search?keyword=a&$skip=0').respond(200,  [ { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] } ]);
            Header.getMatchedResultsByKeyword("&$skip=0","a",cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.cases).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/search?keyword=a&$skip=0').respond(500, 'Some problem has occured');
            Header.getMatchedResultsByKeyword("&$skip=0","a",cbSuccess, cbError);
            $httpBackend.flush();
            expect(Header.cases).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});