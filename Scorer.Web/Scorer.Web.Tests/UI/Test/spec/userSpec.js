﻿'use strict';
describe('UserController', function () {
    var Users;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var usSpinnerService;
    var $window;
    var ErrorHandlerService;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('users');
        module('users', function ($provide) {
            toastr = {
                success: function (message) { }
            };
            spyOn(toastr, 'success');
            $provide.value('toastr', toastr);
            $provide.value('$uibModal', $uibModal);
            $provide.value('data', 'length');
            $provide.value('');
            $provide.value('usSpinnerService', usSpinnerService);
            $window = {
                document: document,
                defaultPageSize: 5
            }
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            }
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $global = {
                getExistingUsers: function () { return;}
            }
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Users = $injector.get('Users', { toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService });
        });
        Users.modalInstance = {
            dismiss: function () { return; }
        };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getUserByUserId', function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Users.user = response.data; };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); Users.user = {}; };
        });
            it('Positive Test', function () {
                $httpBackend.when('GET', 'api/user/1').respond(200, { data: { username: "TestUser", role: { data: { rolename: "TestRole" } } } });
                Users.getUserByUserId(1, cbSuccess, cbError);
                $httpBackend.flush();
                expect(Users.user).not.toBeNull();
                expect(Users.role).not.toBeNull();

            });

            it('Negative Test', function () {
                $httpBackend.when('GET', 'api/user/1').respond(404, { data: [] });
                Users.getUserByUserId(1, cbSuccess, cbError);
                $httpBackend.flush();
                expect(Users.user).toEqual({});

            });
        });
    describe('deleteExistingUser', function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                toastr.success("User removed");
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response); };
        });
            it('Positive Test', function () {
                $httpBackend.expectDELETE('/api/user/1').respond(200);
                Users.deleteExistingUser(1, cbSuccess, cbError);
                $httpBackend.flush();
                expect(toastr.success).toHaveBeenCalledWith("User removed");
            });

            it('Negative Test', function () {
                var userId = 1;
                Users.defaultPageSize = 5;
                $httpBackend.expectDELETE('/api/user/1').respond(500, 'invalid user');
                Users.deleteExistingUser(1, cbSuccess, cbError);
                $httpBackend.flush();
                expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('invalid user');
            });
    });
        describe("getExistingUsersByKeyword", function () {
            beforeEach(function () {
                Users.keyword = "Dummy";
                Users.options = '&$top=1&$skip=0&$orderby=LastEditedDateTime desc';
                cbSuccess = function (response, status) {
                    Users.users = response.items;
                };
                cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Users.users = []; };
            });

            it('Positive Test', function () {
                $httpBackend.expectGET('api/search/users?keyword=' + Users.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(200, { items: { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] } });
                Users.getExistingUsersByKeyword(Users.keyword, Users.options, cbSuccess, cbError);
                $httpBackend.flush();
                expect(Users.users).not.toBeNull();
            });
            it('Negative Test', function () {
                $httpBackend.expectGET('api/search/users?keyword=' + Users.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(500, 'Some problem has occured');
                Users.getExistingUsersByKeyword(Users.keyword, Users.options, cbSuccess, cbError);
                $httpBackend.flush();
                expect(Users.user).toBeUndefined();
                expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
            });
        });
    });