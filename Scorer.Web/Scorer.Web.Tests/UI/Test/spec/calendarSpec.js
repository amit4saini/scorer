﻿'use strict'
describe('CalendarController', function () {
    var Calendar;
    var CaseTasks;
    var CaseHeader;
    var toastr;
    var $httpBackend;
    var ErrorHandlerService;
    var document;
    var Enums;
    var $uibModal;
    var calendarConfig;
    var $global;
    var $stateParams;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('calendar');
        module('calendar', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $provide.value('document', document);
            CaseTasks = {
                openTaskCommentsModal : function(){}
            };
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
                TaskStatus: {
                    'NotStarted': 0,
                    'InProgress': 1,
                    'Completed': 2,
                },
            };
            $provide.value('Enums', Enums);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('CaseTasks', CaseTasks);
            CaseHeader = {
                getCaseFromCaseNumber: function () {
                    return;
                }
            }
            $provide.value('CaseHeader', CaseHeader);
            $provide.value('$uibModal', $uibModal);
            calendarConfig = {
                templates:
                    {calendarMonthCell : 'customMonthCell.html'}
            };
            $provide.value('calendarConfig', calendarConfig);
            $global = {
                getCaseFromCaseNumber: function () { return; }
            };
            $provide.value('$global', $global);
            $provide.value('$stateParams', $stateParams);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Calendar = $injector.get('Calendar', { CaseTasks: CaseTasks, CaseHeader: CaseHeader, toastr: toastr, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $uibModal: $uibModal, calendarConfig: calendarConfig, $global: $global, $stateParams: $stateParams });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("getExistingTasksByCaseIdAndUserId", function () {
        beforeEach(function () {
            Calendar.casesAndUsersIdList = [{ caseId: 1000, isSelected: true }, { caseId: 2000, isSelected: false }];
            cbSuccess = function (response, status) {
                Calendar.tasks = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                Calendar.tasks = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("/api/task/getTasksForCasesAndUsers", Calendar.casesAndUsersIdList).respond(200, [{ title: "Task1", status: 0, caseId: 1000, dueDate: new Date("2016-01-01 00:00:00") }, { title: "Task2", status: 2, assignedToUser: 1000, dueDate: new Date("2016-01-02 00:00:00") }]);
            Calendar.getExistingTasksByCaseIdAndUserId(Calendar.casesAndUsersIdList, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Calendar.tasks.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("/api/task/getTasksForCasesAndUsers").respond(500, "Error Message");
            Calendar.getExistingTasksByCaseIdAndUserId(Calendar.casesAndUsersIdList, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(Calendar.tasks.length).toEqual(0);
        });
    });
});
