﻿'use strict';
describe('Client Service Test Cases', function () {
    var Clients;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var usSpinnerService;
    var $window;
    var document;
    var ErrorHandlerService;
    var Upload;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('client');
        module('client', function ($provide) {
            $stateParams = {
                clientId: 1
            };
            toastr = {
                success: function (message) {

                },
            error: function (message) {

            }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) { return status; }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('$uibModal', $uibModal);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$window', $window);
            $provide.value('toastr', toastr);
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$anchorScroll', $anchorScroll);
            $provide.value('Upload', Upload);

            $window = {
                document: document,
                defaultPageSize: 1
            }
            $location = {
                hash: function () { return; }
            }
            $provide.value('$window', $window);
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $global = {
                getExistingCountries: function () { return;}
            }
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            $window = $injector.get('$window');        
            Clients = $injector.get('Clients', { toastr: toastr, $uibModal: $uibModal, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, $location: $location, $anchorScroll: $anchorScroll, Upload: Upload, $stateParams: $stateParams, $global: $global });
        });
        Clients.allPages = {
            length: 0
        };
    });
    
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
   
    describe("getExistingClients", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                Clients.clients = response.items;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it('Positive Test', function () {
            $httpBackend.expectGET("/api/clients?$skip=1&$orderby=LastEditedDateTime desc").respond(200, { items: [{ clientname: "testing" }, { clientname: "testing2" }], count: 10 });
            Clients.getExistingClients("?$skip=1&$orderby=LastEditedDateTime desc", cbSuccess, cbError);
            $httpBackend.flush();
            expect(Clients.clients.length).toEqual(2);
            expect(Clients.clients).not.toBeNull();
        });
   
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/clients?$skip=1&$orderby=LastEditedDateTime desc").respond(404, { items: [] });
            Clients.getExistingClients("?$skip=1&$orderby=LastEditedDateTime desc", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalled();
        });
    });
    describe("getExistingClientsByKeyword", function () {
        beforeEach(function () {
            Clients.keyword = "Dummy";
            Clients.options = '&$top=1&$skip=0&$orderby=LastEditedDateTime desc';
            cbSuccess = function (response, status) {
                Clients.clients = response.items;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Clients.clients = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('/api/search/clients?keyword=' + Clients.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(200, { items: { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] } });
            Clients.getExistingClientsByKeyword(Clients.keyword, Clients.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Clients.case).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('/api/search/clients?keyword=' + Clients.keyword + '&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(500, 'Some problem has occured');
            Clients.getExistingClientsByKeyword(Clients.keyword, Clients.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Clients.case).toBeUndefined();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
    
    describe("deleteExistingClient", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                toastr.success("Client removed");
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it('Positive Test', function () {
            Clients.pageIndex = 1;
            $httpBackend.expectDELETE("/api/client/1").respond(200, 'success');
            Clients.deleteExistingClient(1,cbSuccess,cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Client removed");
        });

        it('Negative Test', function () {
            $httpBackend.expectDELETE("/api/client/1").respond(404, []);
            Clients.deleteExistingClient(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalled();
        });
    });

    describe("saveNewClient", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Client added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it('Positive Test', function () {
            $httpBackend.expectPOST('/api/client').respond(200, 'success');
            Clients.saveNewClient({ clientId: 1 }, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Client added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST("/api/client").respond(500, 'Save Error');
            Clients.saveNewClient({ clientId:1 }, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Save Error', 500);
        });
    });
    describe("saveEditedClient", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Client edited"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it('Positive Test', function () {
            $httpBackend.expectPUT('/api/client/1').respond(200, 'success');
            Clients.saveEditedClient({ clientId: 1 }, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Client edited");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT("/api/client/1").respond(500, 'Save Error');
            Clients.saveEditedClient({ clientId: 1 }, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Save Error', 500);
        });
    });
    describe("getClientByClientId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                Clients.client = response;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it('Positive Test', function () {
            $httpBackend.expectGET("/api/client/1").respond(200, {
                clientname: "testing", clientId: "1"
            });
            Clients.getClientByClientId(1,cbSuccess,cbError);
            $httpBackend.flush();
            expect(Clients.client.clientId).toEqual("1");
        });
    
        it('Negative Test', function () {
            $httpBackend.expectGET("/api/client/1").respond(404, { data: [{}] });
            Clients.getClientByClientId(1,cbSuccess,cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalled();
        });
    });
    describe("getCasesByClientId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                Clients.clientCases = response.items;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Clients.clientCases = []; };
        });
        it('Positive Test', function () {
            $httpBackend.expectGET("/api/client/1/cases?$skip=0&$filter=IsDeleted eq false&$orderby=LastEditedDateTime desc").respond(200, { items: [{ clientname: "testing", clientId: "1" }] });
            Clients.getCasesByClientId(1, "?$skip=0&$filter=IsDeleted eq false&$orderby=LastEditedDateTime desc", cbSuccess, cbError);
            $httpBackend.flush();
            expect(Clients.clientCases[0].clientId).toEqual("1");
        });

        it('Negative Test', function () {
            $httpBackend.expectGET("/api/client/1/cases?$skip=0&$filter=IsDeleted eq false&$orderby=LastEditedDateTime desc").respond(404, { items: [{}] });
            Clients.getCasesByClientId(1, "?$skip=0&$filter=IsDeleted eq false&$orderby=LastEditedDateTime desc", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalled();
        });
    });
    
});
