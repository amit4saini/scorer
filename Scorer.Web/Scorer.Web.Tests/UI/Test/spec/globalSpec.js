﻿'use strict'
describe('GlobalVariableService', function () {
    var global;
    var Authentication;
    var Upload;
    var cbSuccess;
    var cbError;
    var document;
    var ErrorHandlerService;
    var toastr;
    var $httpBackend;
    var $window;
    beforeEach(function () {
        module('core');
        module('core', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            Authentication = {
                user: {
                    role: {
                        name:'dummy'
                    }
                }
            }
            $provide.value('Authentication', Authentication);
            $provide.value('Upload', Upload);
            $provide.value('document', document);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $provide.value('$window', $window);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            global = $injector.get('$global', { Authentication: Authentication, Upload: Upload, $window: $window });
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("getExistingCaseFromCaseNumber", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.case = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.case = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/case/1/details").respond(200, [{ caseNickName: 'dummy', caseId: 1000,caseNumber:1 }]);
            global.getExistingCaseFromCaseNumber(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.case.length).toEqual(1);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/case/1/details").respond(500, "Error Message");
            global.getExistingCaseFromCaseNumber(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.case.length).toEqual(0);
        });
    });

    describe("getExistingCases", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.cases = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.cases = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/cases?$skip=0").respond(200, [{ caseNickName: 'dummy', caseId: 1000, caseNumber: 1 }, { caseNickName: 'dummy1', caseId: 1001, caseNumber: 2 }]);
            global.getExistingCases("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.cases.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/cases?$skip=0").respond(500, "Error Message");
            global.getExistingCases("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.cases.length).toEqual(0);
        });
    });
    
    describe("saveNewCase", function () {
        beforeEach(function () {
            global.case = { caseNickName: 'dummy', caseId: 1000, caseNumber: 1 };
            cbSuccess = function (response, status) {
                toastr.success("Case added")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("/api/case", global.case).respond(200, 'Success');
            global.saveNewCase(global.case, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case added");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("/api/case", global.case).respond(500, "Error Message");
            global.saveNewCase(global.case, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    
    describe("saveEditedCase", function () {
        beforeEach(function () {
            global.editedCase = { caseNickName: 'dummy', caseId: 1, caseNumber: 1 };
            cbSuccess = function (response, status) {
                toastr.success("Case edited")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("/api/case/1", global.editedCase).respond(200, 'Success');
            global.saveEditedCase(global.editedCase, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Case edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("/api/case/1", global.editedCase).respond(500, "Error Message");
            global.saveEditedCase(global.editedCase, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
   
    describe("getExistingCaseUsers", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.users = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.users = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/Case/1/Users").respond(200, [{ userName: 'dummy', id: 1000, caseId: 1 }, { userName: 'dummy1', caseId: 2, id: 1001 }]);
            global.getExistingCaseUsers(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.users.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/Case/1/Users").respond(500, "Error Message");
            global.getExistingCaseUsers(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.users.length).toEqual(0);
        });
    });
    
    describe("getExistingCaseSubStatusList", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.subStatus = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.subStatus = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/subStatus/").respond(200, [{ subStatus: 'dummy', id: 1000}, { subStatus: 'dummy1', id: 1001 }]);
            global.getExistingCaseSubStatusList( cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.subStatus.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/subStatus/").respond(500, "Error Message");
            global.getExistingCaseSubStatusList(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.subStatus.length).toEqual(0);
        });
    });
   
    describe("getExistingCountries", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.countries = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.countries = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/countries").respond(200, [{ countryName: 'dummy', id: 1000 }, { countryName: 'dummy1', id: 1001 }]);
            global.getExistingCountries(cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.countries.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/countries").respond(500, "Error Message");
            global.getExistingCountries(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.countries.length).toEqual(0);
        });
    });
    
    describe("getExistingStatesByCountryId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.states = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.states = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/country/1/states").respond(200, [{ stateName: 'dummy', id: 1000, countryId: 1 }, { stateName: 'dummy1', id: 1001, countryId: 1 }]);
            global.getExistingStatesByCountryId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.states.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/country/1/states").respond(500, "Error Message");
            global.getExistingStatesByCountryId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.states.length).toEqual(0);
        });
    });
    
    describe("getExistingIndustries", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.industries = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.industries = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/industries").respond(200, [{ industryName: 'dummy', id: 1000 }, { industryName: 'dummy1', id: 1001 }]);
            global.getExistingIndustries(cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.industries.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/industries").respond(500, "Error Message");
            global.getExistingIndustries(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.industries.length).toEqual(0);
        });
    });
    
    describe("getExistingIndustrySubcategories", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.industrySubCategories = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.industrySubCategories = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/industrySubCategories").respond(200, [{ industrySubName: 'dummy', id: 1000 }, { industrySubName: 'dummy1', id: 1001 }]);
            global.getExistingIndustrySubcategories(cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.industrySubCategories.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/industrySubCategories").respond(500, "Error Message");
            global.getExistingIndustrySubcategories(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.industrySubCategories.length).toEqual(0);
        });
    });
    
    describe("getExistingUsers", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.users = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.users = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/users?$skip=0").respond(200, [{ userName: 'dummy', id: 1000}, { userName: 'dummy1', id: 1001 }]);
            global.getExistingUsers("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.users.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/users?$skip=0").respond(500, "Error Message");
            global.getExistingUsers("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.users.length).toEqual(0);
        });
    });
    
    describe("saveEditedUser", function () {
        beforeEach(function () {
            global.user = { userName: 'dummy', id: 1};
            cbSuccess = function (response, status) {
                toastr.success("User edited")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("/api/user/1", global.user).respond(200, 'Success');
            global.saveEditedUser(global.user, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("User edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("/api/user/1", global.user).respond(500, "Error Message");
            global.saveEditedUser(global.user, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    
    describe("getExistingRoles", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.roles = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.roles = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/user/getRoles?$skip=0").respond(200, [{ roleName: 'dummy', id: 1000 }, { roleName: 'dummy1', id: 1001 }]);
            global.getExistingRoles("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.roles.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/user/getRoles?$skip=0").respond(500, "Error Message");
            global.getExistingRoles("?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.roles.length).toEqual(0);
        });
    });

    describe("getExistingCaseAssociatedTasks", function () {
        beforeEach(function () {
            global.case = {
                caseNumber: 1
            };
            cbSuccess = function (response, status) {
                global.tasks = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.tasks = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/Case/1/Tasks?$skip=0").respond(200, { result: { items: [{ taskName: 'dummy', id: 1000, caseNumber: 1 }, { taskName: 'dummy1', id: 1001, caseNumber: 2 }] } });
            global.getExistingCaseAssociatedTasks(1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.tasks.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/Case/1/Tasks?$skip=0").respond(500, "Error Message");
            global.getExistingCaseAssociatedTasks(1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.tasks.length).toEqual(0);
        });
    });
    
    describe("getExistingUserAssignedTasks", function () {
        beforeEach(function () {
            global.case = {
                caseNumber: 1
            };
            cbSuccess = function (response, status) {
                global.tasks = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.tasks = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/Task/1/Users?$skip=0").respond(200, { items: [{ taskName: 'dummy', id: 1000, caseNumber: 1 }, { taskName: 'dummy1', id: 1001, caseNumber: 2 }] });
            global.getExistingUserAssignedTasks(1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.tasks.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/Task/1/Users?$skip=0").respond(500, "Error Message");
            global.getExistingUserAssignedTasks(1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.tasks.length).toEqual(0);
        });
    });
    
    describe("getExistingTasksByKeyword", function () {
        beforeEach(function () {
            global.case = {
                caseNumber: 1
            };
            cbSuccess = function (response, status) {
                global.tasks = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.tasks = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/search/tasks?keyword=a&id=1?$skip=0").respond(200, { result: { items: [{ taskName: 'dummy', id: 1000, caseNumber: 1 }, { taskName: 'dummy1', id: 1001, caseNumber: 2 }] } });
            global.getExistingTasksByKeyword("a",1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.tasks.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/search/tasks?keyword=a&id=1?$skip=0").respond(500, "Error Message");
            global.getExistingTasksByKeyword("a",1, "?$skip=0", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.tasks.length).toEqual(0);
        });
    });

    describe("saveEditedTask", function () {
        beforeEach(function () {
            global.task = { taskName: 'dummy', taskId: 1 };
            cbSuccess = function (response, status) {
                toastr.success("Task edited")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("api/task/1", global.task).respond(200, 'Success');
            global.saveEditedTask(global.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("api/task/1", global.task).respond(500, "Error Message");
            global.saveEditedTask(global.task, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });

    describe("deleteExistingTask", function () {
        beforeEach(function () {
            global.task = { taskName: 'dummy', taskId: 1 };
            cbSuccess = function (response, status) {
                toastr.success("Task archived")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectDELETE("/api/task/1").respond(200, 'Success');
            global.deleteExistingTask(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task archived");
        });
        it("Negative Test", function () {
            $httpBackend.expectDELETE("/api/task/1").respond(500, "Error Message");
            global.deleteExistingTask(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });

    describe("downloadExistingFile", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                global.file = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                global.file = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/file/1?type=text").respond(200, [{ fileName: 'dummy', id: 1000 }]);
            global.downloadExistingFile(1,"text",cbSuccess, cbError);
            $httpBackend.flush();
            expect(global.file.length).toEqual(1);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/file/1?type=text").respond(500, "Error Message");
            global.downloadExistingFile(1, "text", cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(global.file.length).toEqual(0);
        });
    });

    describe("inviteNewUser", function () {
        beforeEach(function () {
            global.invitation = { userName: 'dummy',email:"xyz@xyz.com" };
            cbSuccess = function (response, status) {
                toastr.success("User added")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("api/invitation", global.invitation).respond(200, 'Success');
            global.inviteNewUser(global.invitation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("User added");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("api/invitation", global.invitation).respond(500, "Error Message");
            global.inviteNewUser(global.invitation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    
});
