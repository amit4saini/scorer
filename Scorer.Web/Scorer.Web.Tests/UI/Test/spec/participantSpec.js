﻿'use strict';
describe('participantController', function () {
    var participants;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums
    var Upload;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var CaseHeader;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: 1,
                participantId:1,
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
                ParticipantAction: {
                    'Added': 0,
                    'Edited': 1,
                    'Status set to active': 2,
                    'Status set to inactive': 3,
                }
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            CaseHeader = {
                getCaseFromCaseNumber:function(){return true;},
                case: {
                    caseId: 1
                }
            };
            $provide.value('CaseHeader', CaseHeader);
            $global = {
                inviteNewUser: function () { return; }
            }
            $provide.value('$global', $global);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            participants = $injector.get('Participants', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
            participants.participantAction = Enums.ParticipantAction;
        });
        participants.modalInstance = {
            dismiss: function () { return; }
        };
        participants.allPages = {
            length: 0
        };
        participants.case = {
            caseId: 1
        };
        participants.promise = CaseHeader.getCaseFromCaseNumber();
        participants.promise = {
            status: 1,
            then: function () {
                return true;
            }
        }
        
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("getExistingParticipantCaseAssociationsByCaseId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {  participants.participantsList = response; };
            cbError = function (response, status) { participants.participantsList = []; ErrorHandlerService.DisplayErrorMessage(response, status); };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/participator/1').respond(200, [{ caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] }]);
            participants.getExistingParticipantCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(participants.participantsList).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/participator/1').respond(500, 'Some problem has occured');
            participants.getExistingParticipantCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(participants.participantsList).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveNewParticipantCaseAssociation", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Participant added"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            participants.participantCaseAssociation = {name:"Dummy"};
        });

        it('Positive Test', function () {
            $httpBackend.expectPOST("api/participator",participants.participantCaseAssociation).respond(200, 'Success' );
            participants.saveNewParticipantCaseAssociation(participants.participantCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Participant added");
        });
        it('Negative Test', function () {
            $httpBackend.expectPOST("api/participator",participants.participantCaseAssociation).respond(500, 'Some problem has occured');
            participants.saveNewParticipantCaseAssociation(participants.participantCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });

    describe("saveEditedParticipantCaseAssociation", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Participant edited"); };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            participants.participantCaseAssociation = {name:"Dummy",id:1};
        });

        it('Positive Test', function () {
            $httpBackend.expectPUT("api/participator/1",participants.participantCaseAssociation).respond(200, 'Success');
            participants.saveEditedParticipantCaseAssociation(participants.participantCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Participant edited");
        });
        it('Negative Test', function () {
            $httpBackend.expectPUT("api/participator/1",participants.participantCaseAssociation).respond(500, 'Some problem has occured');
            participants.saveEditedParticipantCaseAssociation(participants.participantCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});