﻿'use strict';
describe('UserSettingsController', function () {
    var Users;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var usSpinnerService;
    var $window;
    var Upload;
    var ErrorHandlerService;
    var Enums;
    var Header;
    beforeEach(function () {
        module('users');
        module('users', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $provide.value('$uibModal', $uibModal);
            $provide.value('data', 'length');
            $provide.value('usSpinnerService', usSpinnerService);
            $window = {
                document: document,
                defaultPageSize: 5
            }
            $provide.value('$window', $window);
            Enums = {
                DownloadFileType: {
                    'CaseFile': 0,
                    'ProfilePic': 1,
                    'DiscussionFile': 2,
                },
            };
            $provide.value('Enums', Enums);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            }
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            Header = {
                userProfilePicUrl:"",
            };
            $provide.value('Header', Header);
            $provide.value('Upload', Upload);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Users = $injector.get('UserSettings', { Header: Header, toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService, Upload: Upload ,Enums:Enums});
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("getUploadedPictureUrl", function () {
        beforeEach(function () {
            Users.uploadedPicId = "1000";
            Users.user = { profilePicUrl: "" };
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/File/1000?type=1").respond(200, "Sample-url");
            Users.getUploadedPictureUrl();
            $httpBackend.flush();
            expect(Users.user.profilePicUrl).toEqual("Sample-url");
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/File/1000?type=1").respond(500, "Error Message");
            Users.getUploadedPictureUrl();
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("getUserDetails", function () {
        it("Positive Test", function () {
            $httpBackend.expectGET("api/user/getCurrentUser").respond(200, { id: "1000", profilePicUrl: "Sample-url" });
            Users.getUserDetails();
            $httpBackend.flush();
            expect(Users.user.id).toEqual("1000");
            expect(Users.isPictureUploaded).toBeFalsy();
            expect(Users.actualPictureUrl).toEqual("Sample-url");
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/user/getCurrentUser").respond(500, "Error Message");
            Users.getUserDetails();
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("saveUserDetails", function () {
        beforeEach(function () {
            Users.uploadedPicId = "1000";
            Users.user = { id: "2000", profilePicUrl: "demo-url" };
        });
        it("Positive Test", function () {
            Users.isPictureUploaded = true;
            $httpBackend.expectPUT('api/user/2000', Users.user).respond(200, 'success');
            Users.saveUserDetails();
            $httpBackend.flush();
            expect(Header.userProfilePicUrl).toEqual("demo-url");
            expect(Users.user.profilePicKey).toEqual("1000");
            expect(toastr.success).toHaveBeenCalledWith("Profile picture saved");
        });
        it("Negative Test-Validation Fail", function () {
            Users.isPictureUploaded = false;
            Users.saveUserDetails();
            expect(toastr.error).toHaveBeenCalledWith("Please upload a picture first");
            expect(Users.user.profilePicKey).toBeUndefined();
        });
        it("Negative Test-Http Fail", function () {
            Users.isPictureUploaded = true;
            $httpBackend.expectPUT('api/user/2000', Users.user).respond(500, 'Error Message');
            Users.saveUserDetails();
            $httpBackend.flush();
            expect(Users.user.profilePicKey).toEqual("1000");
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        })
    });
});