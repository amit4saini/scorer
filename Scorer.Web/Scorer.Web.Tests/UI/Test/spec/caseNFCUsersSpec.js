﻿'use strict'
describe('CaseNFCUsersController', function () {
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var CaseNFCUsers;
    var CaseHeader;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            $stateParams = {
                caseNumber: null
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Enums = {
                DownloadFileType: {
                    'CaseFile': 0,
                    'ProfilePic': 1,
                    'DiscussionFile': 2,
                },
            };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $global = {
                downloadExistingFile: function () { return; },
                regex: {
                    email: "Dummy"
                }
            }
            $provide.value('$global', $global);
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: 1
                }
            };
            $provide.value('CaseHeader', CaseHeader);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            CaseNFCUsers = $injector.get('CaseNFCUsers', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, $window: $window, usSpinnerService: usSpinnerService, Enums: Enums, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, $global: $global });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("getExistingNFCUserCaseAssociationsByCaseId", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                CaseNFCUsers.nfcCaseUsers = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseNFCUsers.nfcCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/nfcUserCaseAssociation/1").respond(200, [{ userName: 'dummy', id: 1000, caseId: 1 }, { userName: 'dummy1', caseId: 2, id: 1001 }]);
            CaseNFCUsers.getExistingNFCUserCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(CaseNFCUsers.nfcCaseUsers.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/nfcUserCaseAssociation/1").respond(500, "Error Message");
            CaseNFCUsers.getExistingNFCUserCaseAssociationsByCaseId(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(CaseNFCUsers.nfcCaseUsers.length).toEqual(0);
        });
    });
    describe("saveNewNFCUserCaseAssociation", function () {
        beforeEach(function () {
            CaseNFCUsers.nfcUserCaseAssociation = { name: "dummy" };
            cbSuccess = function (response, status) {
                toastr.success("ICIUser Added")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseNFCUsers.iciCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("api/nfcUserCaseAssociation", CaseNFCUsers.nfcUserCaseAssociation).respond(200, 'Success');
            CaseNFCUsers.saveNewNFCUserCaseAssociation(CaseNFCUsers.nfcUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("ICIUser Added");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("api/nfcUserCaseAssociation", CaseNFCUsers.nfcUserCaseAssociation).respond(500, "Error Message");
            CaseNFCUsers.saveNewNFCUserCaseAssociation(CaseNFCUsers.nfcUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });

    describe("saveEditedNFCUserCaseAssociation", function () {
        beforeEach(function () {
            CaseNFCUsers.nfcUserCaseAssociation = { name: "dummy", id: 1 };
            cbSuccess = function (response, status) {
                toastr.success("ICIUser Edited")
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                CaseNFCUsers.iciCaseUsers = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("api/nfcUserCaseAssociation/1", CaseNFCUsers.nfcUserCaseAssociation).respond(200, 'Success');
            CaseNFCUsers.saveEditedNFCUserCaseAssociation(CaseNFCUsers.nfcUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("ICIUser Edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("api/nfcUserCaseAssociation/1", CaseNFCUsers.nfcUserCaseAssociation).respond(500, "Error Message");
            CaseNFCUsers.saveEditedNFCUserCaseAssociation(CaseNFCUsers.nfcUserCaseAssociation, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
});