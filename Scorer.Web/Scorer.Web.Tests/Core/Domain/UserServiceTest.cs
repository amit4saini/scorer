﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Scorer.Core.Domain;
using Scorer.Common.Database.Database;
using Scorer.Common.Database.Models;
using Moq;
using Scorer.Common.Database.Models.UserManagement;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using AutoMapper;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData.Builder;

namespace Scorer.Web.Tests.Core.Domain
{
    [TestClass]
    public class UserServiceTest
    {
        private Mock<ScorerDb> scorerDbMock;
        private UserService sut;
        private ErrorHandlerService _errorHandlerService;
        private Mock<IUserService> _userService;
        private Mock<RolePermission> _rolePermission;
        private Mock<Role> _role;
        ODataQueryOptions<Models.Account.User> options;

        Guid userId;
        Guid roleId;

        [TestInitialize]
        public void TestInitialize()
        {
            scorerDbMock = new Mock<ScorerDb>();
            _errorHandlerService = new ErrorHandlerService();
            userId = Guid.NewGuid();
            roleId = Guid.NewGuid();
            _userService = new Mock<IUserService>();
            _rolePermission = new Mock<RolePermission>();
            var userPermission = new List<Scorer.Common.Database.Enums.Permission> { Scorer.Common.Database.Enums.Permission.ADD_VENDOR};
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            sut = new UserService(scorerDbMock.Object, _errorHandlerService);
            _role = new Mock<Role>();           
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Account.User>("User");
            options = new ODataQueryOptions<Models.Account.User>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Account.User)), new HttpRequestMessage());
        }
        private List<User> GetDummyUsers()
        {
            var dummyUsers = new List<User>
            {
                new User
                {
                    Id = userId,
                    UserName ="dummyUser@abc.com",
                    Email = "MyEmail@123.com",
                    FirstName = "first",
                    LastName = "last"
                   

                },
                new User
                {
                    Id = Guid.NewGuid(),
                     UserName ="dummyUser@xyz.com",
                     Email = "Test@1234.com",
                     FirstName = "fname",
                     LastName = "lname"
                }
            };
            return dummyUsers;
        }
        private List<Role> GetDummyRoles()
        {
            var dummyRoles = new List<Role>
            {
                new Role
                {
                    Id = roleId,
                    Name = "Executive",   
                },
                new Role
                {
                    Id = roleId,
                    Name = "Ceo"
                }
            };
            return dummyRoles;
        }
        private List<UserRole> GetDummyUserRoles()
        {
            var dummyUserRoles = new List<UserRole>
            {
                new UserRole
                {
                    RoleId = roleId,
                    UserRoleId = Guid.NewGuid(),
                    UserId = Guid.NewGuid()
                }
            };
            return dummyUserRoles;
        }
        private List<RolePermission> GetDummyRolePermissions()
        {
            var dummyRolePermissions = new List<RolePermission>
            {
                new RolePermission
                {
                    RoleId = roleId,
                    RolePermissionId = Guid.NewGuid(),
                    Permission=0,
                    Role = new Role { Id = roleId, Name = "Ceo" }

                },
                new RolePermission
                {
                    RoleId = roleId,
                    RolePermissionId = Guid.NewGuid(),
                    Permission=0,
                    Role = new Role { Id = roleId, Name = "Ceo"}
                }
            };
            return dummyRolePermissions;
        }
        [TestMethod]
        public void GivenCallingGetUser_WhenUserIdIsNotFound_ThenTestPasses()
        {
            // Arrange

            var dummyUsers = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            try
            {
                sut.GetUser(Guid.NewGuid());
            }
            catch(Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }
        [TestMethod]
        public void GivenCallingGetUserPermissions_WhenUserIdIsNotFound_ThenTestPasses()
        {
            // Arrange
            var dummyUsers = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            try
            {
                sut.GetUserPermissions(Guid.NewGuid());
            }
            catch(HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
            }
        }
        [TestMethod]
        public void GivenCallingGetRolePermissions_ThenTestPasses()
        {
            // Arrange

            var dummyRolePermissions = GetDummyRolePermissions();
            var dummyRoleId = Guid.NewGuid();
            var dummyRolePermission = new RolePermission
            {
                RoleId = dummyRoleId,
                Permission = 0
            };
            dummyRolePermissions.Add(dummyRolePermission);
            scorerDbMock.Setup(c => c.RolePermissions).ReturnsDbSet(dummyRolePermissions);
            var result = sut.GetRolePermissions(dummyRoleId);
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCall_GetAllUsers_WhenNoUserExists_ThenThrowNotFound()
        {
            try
            {
                var dummyUser = new List<Scorer.Common.Database.Models.UserManagement.User>();
                scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUser);                
                sut.GetAllUsers(options,new HttpRequestMessage());
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCall_GetAllUsers_WhenUserExists_ThenReturnMappedResult()
        {
            var dummyUser = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUser);           
            scorerDbMock.Setup(c => c.RolePermissions).ReturnsDbSet(GetDummyRolePermissions());        
            scorerDbMock.Setup(c => c.UserRoles).ReturnsDbSet(new UserRole[0]);
            scorerDbMock.Setup(c => c.Roles).ReturnsDbSet(new Scorer.Common.Database.Models.RolesAndPermissionManagement.Role[0]);
            var actual=sut.GetAllUsers(options, new HttpRequestMessage());            // Assert
            Assert.IsNotNull(actual.Items);
        }
        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public void GivenCalling_GetAllRoles_WhenRolesNotFound_ThenThrowNotFound()
        //{
        //    try {
        //        var dummyRoles = new List<Role>();
        //        scorerDbMock.Setup(c => c.Roles).ReturnsDbSet(dummyRoles);
        //        sut.GetAllRoles();
        //    }
        //    catch (HttpResponseException e)
        //    {
        //        // Assert
        //        Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
        //        throw;
        //    }
        //}
        //[TestMethod]
        //public void GivenCalling_GetAllRoles_WhenRolesFound_ThenReturnMappedResult()
        //{
        //    var dummyRoles = GetDummyRoles();
        //    scorerDbMock.Setup(c => c.Roles).ReturnsDbSet(dummyRoles);
        //    var actual = sut.GetAllRoles();
        //    Assert.IsNotNull(actual.Data);
        //}

        public void GivenCalling_Delete_WhenDataIsValid_MappedResultReturn()
        {

            var dummyUser = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUser);
            try
            {
                var actual = sut.Delete(userId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_SearchUserByKeyword_WhenKeywordPresentInData_MappedResultReturn()
        {
            var dummyUser = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUser);
            scorerDbMock.Setup(c => c.RolePermissions).ReturnsDbSet(GetDummyRolePermissions());
            scorerDbMock.Setup(c => c.Roles).ReturnsDbSet(GetDummyRoles());
            scorerDbMock.Setup(c => c.UserRoles).ReturnsDbSet(GetDummyUserRoles());
            try
            {
                var result = sut.SearchUsersByKeyword(options, new HttpRequestMessage(), "fname");
                Assert.IsNotNull(result.Items);
                Assert.IsTrue(result.Count > 0);
            }
            catch (Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_SearchUserByKeyword_WhenKeywordNotPresentInData_MappedResultReturn()
        {
            var dummyUser = GetDummyUsers();
            scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUser);
            scorerDbMock.Setup(c => c.RolePermissions).ReturnsDbSet(GetDummyRolePermissions());
            scorerDbMock.Setup(c => c.Roles).ReturnsDbSet(GetDummyRoles());
            scorerDbMock.Setup(c => c.UserRoles).ReturnsDbSet(GetDummyUserRoles());
            try
            {
                var result = sut.SearchUsersByKeyword(options, new HttpRequestMessage(), "jhbffh");
                Assert.IsNotNull(result.Items);
                Assert.IsTrue(result.Count == 0);
            }
            catch (Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public void GivenCalling_SearchUserByKeyword_WhenKeywordNotPresentInData_ThenThrowNotFound()
        //{
        //    var dummyUsers = GetDummyUsers();
        //    dummyUsers[0].UserName = "dummyUser@test.com";
        //    dummyUsers[1].UserName = "test@test.com";
        //    scorerDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
        //    try
        //    {
        //       var result = sut.SearchUserByKeyword(new HttpRequestMessage(), "dummyUser@dummy.com");
        //    }
        //    catch (HttpResponseException e)
        //    {
        //        // Assert
        //        Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
        //        throw;
        //    }
        //}

    }
}
