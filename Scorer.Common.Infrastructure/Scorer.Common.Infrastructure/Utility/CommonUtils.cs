﻿using System;

namespace Scorer.Common.Infrastructure.Utility
{
    public interface ICommonUtils
    {
        DateTime CurrentDateTime();
        
    }
    public class CommonUtils : ICommonUtils
    {
        public DateTime CurrentDateTime()
        {
            return DateTime.Now;
        }
        
    }
}
