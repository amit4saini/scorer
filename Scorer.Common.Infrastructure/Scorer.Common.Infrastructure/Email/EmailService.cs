﻿using System.Collections.Generic;
using System.Linq;
using Mandrill.Model;
using System.IO;
using Scorer.Models;
using System.Web.Configuration;

namespace Scorer.Common.Infrastructure.Email
{
    public interface IEmailService
    {
        void SendEmail();     
        string MailContent { get; set; }
        string EmailFrom { get; set; }
        string EmailFromName { get; set; }
        List<string> SendTo { get; set; }
        string Subject { get; set; }
        bool AutoHTML { get; set; }
        bool AutoText { get; set; }
        IDictionary<string, string> TokenData { get; set; }
        string Template { get; set; }
        IDictionary<string, string> AttachmentFile { get; set; }
        List<string> Cc { get; set; }
        List<string> Bcc { get; set; }
    }
  
    public class EmailService : IEmailService
    {
        private static readonly string AccountKey = WebConfigurationManager.AppSettings["MandrillApiKey"];

        private string mailContent;
        private string emailFrom;
        private string emailFromName;
        private List<string> sendTo;
        private string subject;
        private bool autoHTML;
        private bool autoText;
        private IDictionary<string, string> tokenData;
        private string template;
        private IDictionary<string, string> attachmentFile;
        private List<string> cc;
        private List<string> bcc;
        
        IDictionary<string, string> IEmailService.AttachmentFile
        {
            get
            {
                return attachmentFile;
            }

            set
            {
                attachmentFile = value;
            }
        }

        bool IEmailService.AutoHTML
        {
            get
            {
                return autoHTML;
            }

            set
            {
                autoHTML = value;
            }
        }

        bool IEmailService.AutoText
        {
            get
            {
                return autoText;
            }

            set
            {
                autoText = value;
            }
        }

       IDictionary<string, string> IEmailService.TokenData
        {
            get
            {
                return tokenData;
            }

            set
            {
                tokenData = value;
            }
        }

        string IEmailService.EmailFrom
        {
            get
            {
                return emailFrom;
            }

            set
            {
                emailFrom = value;
            }
        }

        string IEmailService.EmailFromName
        {
            get
            {
                return emailFromName;
            }

            set
            {
                emailFromName = value;
            }
        }

        string IEmailService.MailContent
        {
            get
            {
                return mailContent;
            }

            set
            {
                mailContent = value;
            }
        }

        List<string> IEmailService.SendTo
        {
            get
            {
                return sendTo;
            }

            set
            {
                sendTo = value;
            }
        }

        string IEmailService.Subject
        {
            get
            {
                return subject;
            }

            set
            {
                subject = value;
            }
        }

        string IEmailService.Template
        {
            get
            {
                return template;
            }

            set
            {
                template = value;
            }
        }

        public List<string> Cc
        {
            get
            {
                return cc;
            }

            set
            {
                cc = value;
            }
        }

        public List<string> Bcc
        {
            get
            {
                return bcc;
            }

            set
            {
                bcc = value;
            }
        }

        public void SendEmail()
        {          
            var result = new Result();
            var api = new Mandrill.MandrillApi(AccountKey);
            bool async = true;                      
            var message = new MandrillMessage()
            {   Subject = subject,
                FromEmail = "info@innostax.com",                                                             
            };   
            if (tokenData != null)            
             tokenData.All(p => { message.GlobalMergeVars.Add(new MandrillMergeVar() { Name = p.Key, Content = p.Value }); return true; });
            
            sendTo.All(p => { message.AddTo(p); return true; });

            if (cc != null)
                cc.All(p => { message.To.Add(new MandrillMailAddress() { Email = p, Type = MandrillMailAddressType.Cc }); return true; });

            if (bcc != null)
                bcc.All(p => { message.To.Add(new MandrillMailAddress() { Email = p, Type = MandrillMailAddressType.Bcc }); return true; });

            if (attachmentFile != null)
                attachmentFile.All(p => { message.Attachments.Add(new MandrillAttachment(p.Key, Path.GetFileNameWithoutExtension(p.Value), File.ReadAllBytes(p.Value))); return true; });

            var sendMailResponse = new List<MandrillSendMessageResponse>();
            sendMailResponse = template == null ? api.Messages.Send(message, async).ToList() : api.Messages.SendTemplate(message, template, null, async).ToList();
        }           
    }
}
