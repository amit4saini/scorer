﻿using System;
using System.Net.Mail;
using Scorer.Models;

namespace Scorer.Common.Infrastructure
{
    public interface ISmtpService
    {
        bool SendMailUsingSmtp(Mail userMail);
    }
    public class SmtpService : ISmtpService
    {      
        public bool SendMailUsingSmtp(Mail userMail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ankita.agarwal@innostax.com");
                mail.To.Add(userMail.EmailAddress);
                mail.Subject = userMail.Subject;
                mail.Body = userMail.Body;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("ankita.agarwal@innostax.com", "Ankita@14129");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception )
            {
                return false;
            }
            return true;
         }       
        }
    }

