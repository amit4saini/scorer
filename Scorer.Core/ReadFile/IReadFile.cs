﻿namespace Scorer.Core.ReadFile
{
    public interface IReadFile
    {
        string ReadFile(string filePath);
    }
}
