﻿using Spire.Doc;
using Spire.Doc.Documents;
using System.Text;

namespace Scorer.Core.ReadFile
{
    class ReadDocFile:IReadFile
    {
        public string ReadFile(string filePath)
        {
            StringBuilder text = new StringBuilder();         
                Document document = new Document();
                document.LoadFromFile(filePath);

                foreach (Section section in document.Sections)
                {
                    foreach (Paragraph paragraph in section.Paragraphs)
                    {
                        text.AppendLine(paragraph.Text);
                    }
                }
            return text.ToString();
            }
        }
        
    }
