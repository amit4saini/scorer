﻿using System.Text;

namespace Scorer.Core.ReadFile
{
    class ReadTxtFile : IReadFile
    {
        public string ReadFile(string filePath)
        {
            StringBuilder text = new StringBuilder();
            string[] lines = System.IO.File.ReadAllLines(filePath);
            foreach (var line in lines)
            {
                text = text.Append(line + " ");
            }

           return text.ToString();
        }
    }
}
