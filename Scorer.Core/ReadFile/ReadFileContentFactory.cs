﻿namespace Scorer.Core.ReadFile
{
    public class ReadFileContentFactory
    { 
     
        public IReadFile ReadFileContent(string fileExtension)
        {
            IReadFile readFile = null;
            switch (fileExtension)
            {
                case ".txt":readFile = new ReadTxtFile();break;
                case ".pdf":readFile = new ReadPdfFile();break;
                case ".doc":readFile = new ReadDocFile();break;
                case ".docx": readFile = new ReadDocFile(); break;      
            }
            return readFile;
        }
    }
}
