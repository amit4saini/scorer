﻿using System.Text;

namespace Scorer.Core.ReadFile
{
    class ReadPdfFile : IReadFile
    {
        public string ReadFile(string filePath)
        {
            StringBuilder text = new StringBuilder();
            using (var pdfReader = new iTextSharp.text.pdf.PdfReader(filePath))
            {
                for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    iTextSharp.text.pdf.parser.ITextExtractionStrategy strategy = new iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy();

                    var currentText = iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(
                        pdfReader,
                        page,
                        strategy);

                    currentText =
                        Encoding.UTF8.GetString(Encoding.Convert(
                            Encoding.Default,
                            Encoding.UTF8,
                            Encoding.Default.GetBytes(currentText)));
                    text.Append(currentText);
                }
                return text.ToString();
            }
        }
    }
}
