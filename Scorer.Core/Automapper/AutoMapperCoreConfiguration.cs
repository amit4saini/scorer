﻿using AutoMapper;
using Scorer.Models;

namespace Scorer.Core.Automapper
{
    public class AutoMapperCoreConfiguration : Profile
    {

        protected override void Configure()
        {   
            CreateMap<Scorer.Models.State, Scorer.Common.Database.Models.State>();
            CreateMap<Scorer.Common.Database.Models.State,Scorer.Models.State>();
            CreateMap<Scorer.Models.Country, Scorer.Common.Database.Models.Country>();
            CreateMap<Scorer.Common.Database.Models.Country, Scorer.Models.Country>();
            CreateMap<Scorer.Common.Database.Models.Region, Scorer.Models.Region>();                 
            CreateMap<Scorer.Common.Database.Models.RolesAndPermissionManagement.Role, Scorer.Models.Role>();
            CreateMap<Scorer.Models.Role, Scorer.Common.Database.Models.RolesAndPermissionManagement.Role>();
            CreateMap<Scorer.Common.Database.Models.UserManagement.User, Scorer.Models.Account.User>();
            CreateMap<Scorer.Models.Account.User, Scorer.Common.Database.Models.UserManagement.User>();
            CreateMap<Notification, Common.Database.Models.Notification>()
                .ForMember(y => y.CreatedOn, z => z.Ignore())
                .ForMember(y => y.CreatedBy, z => z.Ignore());
            CreateMap<Common.Database.Models.Notification, Notification>();       
        }
    }
}
