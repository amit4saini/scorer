﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Scorer.Models;
using System.Configuration;
using Scorer.Common.Database.Database;
using System.Web.Http;
using Scorer.Core.Domain;
using Scorer.Core.ReadFile;
using Scorer.Common.Database.Enums;

namespace Scorer.Core.Services.Implementation
{
    public interface IAzureFileManager
    {
        Task<IEnumerable<FileModel>> Add(HttpRequestMessage request);
        Task<String> DownloadFile(Guid fileId);
    }

    [Authorize]
    public class AzureFileManager : IAzureFileManager
    {
        private CloudStorageAccount StorageAccount { get; set; }
        private string ContainerName { get; set; }
        private string ThumbnailsContainerName { get; set; }
        private string AssetsContainerName { get; set; }
        private string DeletedContainerName { get; set; }
        private IScorerDb _scorerDb;     
        private readonly IUserService _userService;

        public AzureFileManager(IScorerDb scorerDb, IUserService userService)
        {
            this.StorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["AzureFileStorage"]);
            this.ContainerName = "krellerfileupload";
            this.AssetsContainerName = "assets";
            this.DeletedContainerName = "deleted-archive";
            _scorerDb = scorerDb;    
            _userService = userService;

        }

        public async Task<IEnumerable<FileModel>> Add(HttpRequestMessage request)
        {
            var fileContent = await GetFileContent(request);
            CloudBlobClient blobClient = this.StorageAccount.CreateCloudBlobClient();
            CloudBlobContainer fileContainer = blobClient.GetContainerReference(this.ContainerName);
            fileContainer.CreateIfNotExists();
            fileContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Off });
            var provider = new AzureBlobMultipartFormDataStreamProvider(fileContainer);
            Stream stream = await request.Content.ReadAsStreamAsync();
            stream.Seek(0, SeekOrigin.Begin);
            await request.Content.ReadAsMultipartAsync(provider);
            var files = new List<FileModel>();
            foreach (var file in provider.FileData)
            {
                var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
                var key = file.LocalFileName.Replace(fileName, "");
                var name = file.Headers.ContentDisposition.Name.Replace("\"", "");                
                var blob = await fileContainer.GetBlobReferenceFromServerAsync(file.LocalFileName);
                CloudBlockBlob tBlob = fileContainer.GetBlockBlobReference(file.LocalFileName);
                await blob.FetchAttributesAsync();
                files.Add(new FileModel
                {
                    FileName = fileName,
                    CreateDateTime = blob.Metadata["Created"] == null ? DateTime.Now : DateTime.Parse(blob.Metadata["Created"]),
                    ModifiedDateTime = ((DateTimeOffset)blob.Properties.LastModified).DateTime,
                    FileUrl = blob.Uri.AbsoluteUri,
                    AzureKey = blob.Name,
                    Key = key,
                    FileContent = fileContent,
                    FileType = System.IO.Path.GetExtension(fileName)
                });
            }

            return files;
        }

        public async Task<String> GetFileContent(HttpRequestMessage request)
        {
            string fileContent = String.Empty;
            var extensions = new List<string> { ".doc", ".docx", ".pdf", ".txt" };
            var provider = await request.Content.ReadAsMultipartAsync();          
            foreach (var content in provider.Contents)
            {
                if (!string.IsNullOrEmpty(content.Headers.ContentDisposition.FileName))
                {
                    var fileName = content.Headers.ContentDisposition.FileName.Replace("\"", "");
                    var fileExtension = System.IO.Path.GetExtension(fileName);
                    
                    if (!(extensions.Contains(fileExtension)))
                    {
                        return fileContent;
                    }
                    var file = await content.ReadAsByteArrayAsync();
                    var fileKey = System.Guid.NewGuid();
                    var filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, (fileKey.ToString() + fileName));
                    File.WriteAllBytes(filePath, file);
                    ReadFileContentFactory readFileContentFactory = new ReadFileContentFactory();
                    var fileReaderObject = readFileContentFactory.ReadFileContent(fileExtension);
                    fileContent= fileReaderObject.ReadFile(filePath);           
                        if (File.Exists(filePath))
                        {                         
                           File.Delete(filePath);                     
                        }
                    }                                                        
            }
            return fileContent;
        }

        public async Task<String> DownloadFile(Guid fileId)
        {
            var errorResponse = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent(string.Empty)
            };                 
                CloudBlobClient blobClient = this.StorageAccount.CreateCloudBlobClient();
                CloudBlobContainer fileContainer = blobClient.GetContainerReference(this.ContainerName);
                string sasUrl = String.Empty;
                var uploadedFile = _scorerDb.UploadFiles.FirstOrDefault(x => x.FileId == fileId);
                if (uploadedFile != null)
                {
                    var blob = await fileContainer.GetBlobReferenceFromServerAsync(uploadedFile.AzureKey);
                    var builder = new UriBuilder(blob.Uri);
                    builder.Query = blob.GetSharedAccessSignature(
                    new SharedAccessBlobPolicy
                    {
                        Permissions = SharedAccessBlobPermissions.Read,
                        SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                        SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(15))
                    },
                   new SharedAccessBlobHeaders
                   {
                       ContentDisposition = "attachment; filename="
                        + uploadedFile.FileName,
                       ContentType = blob.Properties.ContentType
                   }).TrimStart('?');
                    return builder.Uri.AbsoluteUri;
                }
                else
                {
                    throw new HttpResponseException(errorResponse);
                }
            }
        }
}

