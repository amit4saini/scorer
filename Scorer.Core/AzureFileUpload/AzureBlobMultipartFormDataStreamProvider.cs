﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Scorer.Core.Services
{
    public class AzureBlobMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        private CloudBlobContainer BlobContainer { get; set; }

    public AzureBlobMultipartFormDataStreamProvider(CloudBlobContainer blobContainer): base("azure")
        {
        this.BlobContainer = blobContainer;
    }

    public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
    {
        if (parent == null)
        {
            throw new ArgumentNullException("parent");
        }
        if (headers == null)
        {
            throw new ArgumentNullException("headers");
        }

        var fileName = this.GetLocalFileName(headers);

        CloudBlockBlob blob = this.BlobContainer.GetBlockBlobReference(Guid.NewGuid() + fileName);
        blob.Metadata["Created"] = DateTime.Now.ToString();

        if (headers.ContentType != null)
        {
            blob.Properties.ContentType = headers.ContentType.MediaType;
        }

        this.FileData.Add(new MultipartFileData(headers, blob.Name));           
               var openWrite=blob.OpenWrite();
                return openWrite;           
        }

    public override Task ExecutePostProcessingAsync()
    {

        return base.ExecutePostProcessingAsync();
    }

    public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
    {
        //Make the file name URL safe and then use it & is the only disallowed url character allowed in a windows filename            
        var name = !string.IsNullOrWhiteSpace(headers.ContentDisposition.FileName) ? headers.ContentDisposition.FileName : "NoName";

        name = name.Trim(new char[] { '"' })
                    .Replace("&", "and");

        //IE sets the full path as the file name 
        name = Path.GetFileName(name);

        return name;
    }
}
}