﻿using Scorer.Models;
using Scorer.Models.Account;
using Scorer.Models.EmailModels;
using Scorer.Common.Database.Database;
using System.Linq;
using System;
using AutoMapper;
using System.Data.Entity.Migrations;

namespace Scorer.Core.Domain
{
    public interface IInvitationService
    {
        void SendInvitationToUser(User user, string link); 
    }

    public class InvitationService : IInvitationService
    {
        public IScorerDb _scorerDb;
        private IMailSenderService _mailSenderService;
        private IUserService _userService;

        public InvitationService(IScorerDb scorerDb, IMailSenderService mailSenderService,
            IUserService userService)
        {
            _scorerDb = scorerDb;
            _mailSenderService = mailSenderService;
            _userService = userService;      
        }

        public void SendInvitationToUser(User user,string link)
        {
            var inviteUserEmailModel = new InviteUserEmailModel();
            inviteUserEmailModel.InviteeName = user.FirstName + " " + user.LastName;
            inviteUserEmailModel.InviteeEmail = user.Email;
            inviteUserEmailModel.link = link;
            var inviterDetails = _scorerDb.Users.FirstOrDefault(x => x.Id == _userService.UserId);
            inviteUserEmailModel.InviterName = inviterDetails.FirstName + " " + inviterDetails.LastName;
            _mailSenderService.SendInviteUserDetails(inviteUserEmailModel);
        }
        }
    }
