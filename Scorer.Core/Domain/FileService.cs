﻿using System;
using System.Collections.Generic;
using Scorer.Models;
using AutoMapper;
using System.Linq;
using Scorer.Common.Database.Database;

namespace Scorer.Core.Domain
{
    public interface IFileService
    {
        List<FileModel> Save(List<FileModel> files);
        void RenameFile(FileModel file);
    }

    public class FileService : IFileService
    {
        public IScorerDb _scorerDb;
        private readonly IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;

        public FileService(IScorerDb scorerDb, IUserService userService, IErrorHandlerService errorHandlerService)
        {
            _scorerDb = scorerDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;        
        }

        public List<FileModel> Save(List<FileModel> files)
        {
            var result = new List<FileModel>();
            var userId = _userService.UserId;
            if (userId == new Guid())
            {
                string userNotAuthorizeError = string.Empty;//Common.Constants.ErrorMessages.Common.FileUploadError.USER_NOT_AUTHORIZE;
                _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizeError);
            }
            var fileList = new List<Scorer.Common.Database.Models.UploadedFile>();
            foreach (var file in files)
            {
                fileList.Add(new Scorer.Common.Database.Models.UploadedFile
                {
                    FileName = file.FileName,
                    FileUrl = file.FileUrl,
                    CreateDateTime = DateTime.UtcNow,
                    DeleteDateTime = null,
                    UserId = userId,
                    AzureKey = file.AzureKey,
                    FileId = Guid.Parse(file.Key),
                    FileContent = file.FileContent,
                    FileType = file.FileType
                });
            }          
                _scorerDb.UploadFiles.AddRange(fileList);
                _scorerDb.SaveChangesWithErrors();             
            result = new List<FileModel>();

            foreach (var file in fileList)
            {
                var uploadedFile = Mapper.Map<FileModel>(file);
                result.Add(uploadedFile);
            }
            return result;
        }

        public void RenameFile(FileModel file)
        {
            var existingFile = _scorerDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId && x.IsDeleted != true);
            if(existingFile != null)
            {
                existingFile.FileName = file.FileName;
                _scorerDb.SaveChangesWithErrors();
            }
        }
    }
}

