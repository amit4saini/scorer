﻿using Scorer.Common.Database.Database;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Scorer.Core.Domain
{
    public interface IErrorHandlerService
    {
        string ThrowInternalServerError(string errorMessage);
        string ThrowBadRequestError(string errorMessage);
        string ThrowResourceNotFoundError(string errorMessage);
        string ThrowForbiddenRequestError(string errorMessage);
    }

    public class ErrorHandlerService : IErrorHandlerService
    {        
        public ErrorHandlerService()
        {
        }

        public string ThrowInternalServerError(string errorMessage)
        {
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(errorMessage)
            };
            throw new HttpResponseException(response);
        }

        public string ThrowBadRequestError(string errorMessage)
        {
            var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(errorMessage)
            };
            throw new HttpResponseException(response);
        }

        public string ThrowResourceNotFoundError(string errorMessage)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent(errorMessage)
            };
            throw new HttpResponseException(response);
        }

        public string ThrowForbiddenRequestError(string errorMessage)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Forbidden)
            {
                Content = new StringContent(errorMessage)
            };
            throw new HttpResponseException(response);
        }
    }
}
