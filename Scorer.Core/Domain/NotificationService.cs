﻿using AutoMapper;
using Scorer.Models;
using Scorer.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Scorer.Models.Account;
using Scorer.Common.Database.Enums;
using Scorer.Core.Services.Implementation;

namespace Scorer.Core.Domain
{
    public interface INotificationService
    {
        void MarkAllNotificationsReadForUser(Guid id);
        System.Threading.Tasks.Task<List<Notification>> GetAllNotifications(bool isRead);

        //Getting top 5 unread notification
        List<Notification> ViewUnreadNotifications(Guid id);
        List<Guid> SaveNotification(string notificationText, List<Guid> listOfUserId, string url);
        List<string> GetMessageToNotifyUser(Guid userId);
        List<Guid> SaveWebJobNotification(string notificationText, List<Guid> listOfUserId, string url);
    }

    public class NotificationService : INotificationService
    {
        private IScorerDb _scorerDb;
        private IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;      
        private readonly IAzureFileManager _azureFileManager;
          
        public NotificationService(IScorerDb scorerDb, IUserService userService, IErrorHandlerService errorHandlerService, IAzureFileManager azureFileManager)
        {
            _scorerDb = scorerDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _azureFileManager = azureFileManager;
        }

        public async System.Threading.Tasks.Task<List<Notification>> GetAllNotifications(bool isRead)
        {
            var result = new List<Notification>();
            var allNotifications = _scorerDb.Notifications.Include(x => x.CreatedByUser).Where(x => x.UserId == _userService.UserId && x.IsViewed == isRead).OrderByDescending(x => x.CreatedOn).ToList();
            if (allNotifications == null || allNotifications.Count == 0)
            {
                string notificationNotFoundError = Common.Constants.ErrorMessages.NotificationManagement.NotificationError.NOTIFICATION_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(notificationNotFoundError);
            }
            result = new List<Notification>();
            foreach (var notification in allNotifications)
            {
                var notifications = Mapper.Map<Notification>(notification);
                notifications.CreatedByUser = Mapper.Map<User>(notification.CreatedByUser);
                Guid profilePicKey = notifications.CreatedByUser.ProfilePicKey ?? Guid.Empty;
                if (profilePicKey != Guid.Empty)
                    notifications.CreatedByUser.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey);
                else
                    notifications.CreatedByUser.ProfilePicUrl = string.Empty;//Common.Constants.TelarixConstants.PROFILE_PIC_URL;
                result.Add(notifications);
            }
            return result;
        }

        public void MarkAllNotificationsReadForUser(Guid id)
        {
            var unSeenNotifications = _scorerDb.Notifications.Where(x => x.UserId == id && x.IsViewed == false).ToList();

            if (unSeenNotifications != null && unSeenNotifications.Count >= 0)
            {
                unSeenNotifications.All(p => { p.IsViewed = true; return true; });              
                    _scorerDb.SaveChangesWithErrors();        
            }
        }

        public List<Notification> ViewUnreadNotifications(Guid id)
        {
            var result = new List<Notification>();
            var top5Notifications = _scorerDb.Notifications.OrderByDescending(p => p.CreatedOn).Where(x => x.UserId == id).Take(5).ToList();
            result = new List<Notification>();
            foreach (var notification in top5Notifications)
            {
                var notifications = Mapper.Map<Notification>(notification);
                result.Add(notifications);
            }
            return result;
        }

        public List<Guid> SaveWebJobNotification(string notificationText, List<Guid> listOfUserId, string url)
        {
            var notificationModel = new Notification();
            notificationModel.NotificationText = notificationText;
            notificationModel.ListOfUserId = listOfUserId;
            notificationModel.URL = url;
            var notification = Mapper.Map<Common.Database.Models.Notification>(notificationModel);
            var adminRole = _scorerDb.Roles.FirstOrDefault(y => y.Name == "Admin");
            var adminId = _scorerDb.UserRoles.FirstOrDefault(y => y.RoleId == adminRole.Id).UserId;
            notification.CreatedBy = adminId;
            notification.CreatedOn = DateTime.UtcNow;
            notification.IsViewed = false;
            foreach (var userId in notificationModel.ListOfUserId)
            {
                notification.UserId = userId;
                _scorerDb.Notifications.Add(notification);
                _scorerDb.SaveChangesWithErrors();
            }
            return notificationModel.ListOfUserId;
        }

        public List<Guid> SaveNotification(string notificationText, List<Guid> listOfUserId, string url)
        {
            var notificationModel = new Notification();       
            notificationModel.NotificationText = notificationText;
            listOfUserId.Remove(_userService.UserId);
            notificationModel.ListOfUserId = listOfUserId;
            notificationModel.URL = url;
            return Save(notificationModel);
        }

        internal List<Guid> Save(Notification model)
        {          
            var notificationModel = Mapper.Map<Common.Database.Models.Notification>(model);                     
            notificationModel.CreatedBy = _userService.UserId;
            notificationModel.CreatedOn = DateTime.UtcNow;
            notificationModel.IsViewed = false;
            model.ListOfUserId = model.ListOfUserId.Distinct().ToList();
            foreach (var userId in model.ListOfUserId)
            {
                notificationModel.UserId = userId;
                _scorerDb.Notifications.Add(notificationModel);
                _scorerDb.SaveChangesWithErrors();
            }                   
            return model.ListOfUserId;
        }

        //TO DO: message to be sent as a list.
        public List<string> GetMessageToNotifyUser(Guid userId)
        {
            var notifications = _scorerDb.Notifications.Where(x => x.UserId == userId && x.IsViewed == false).ToList();
            var messageToSend = new List<string>();
            if (notifications != null && notifications.Count > 0)
            {
                foreach (var eachNotification in notifications)
                {
                    messageToSend.Add(eachNotification.NotificationText + "#&#" + eachNotification.URL);
                }
            }
            return messageToSend;
        }
    }
}
