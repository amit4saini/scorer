﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Scorer.Common.Database.Database;

namespace Scorer.Core.Domain
{
    public interface IStateService
    {
        List<Scorer.Models.State> ListOfStatesBasedOnCountryId(string id);
    }

    public class StateService : IStateService
    {
        private IScorerDb _scorerDb;
        private readonly IErrorHandlerService _errorHandlerService;
        public StateService(IScorerDb scorerDb, IErrorHandlerService errorHandlerService)
        {
            _scorerDb = scorerDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Scorer.Models.State> ListOfStatesBasedOnCountryId(string id)
        {
            var result = new List<Scorer.Models.State>();
            int countryId;
            if (!int.TryParse(id, out countryId))
            {
                string countryIdNotValidError = string.Empty;//Common.Constants.ErrorMessages.Common.StateError.COUNTRYID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(countryIdNotValidError);
            }
            var existingStates = _scorerDb.States.Where(x => x.CountryId == countryId).ToList();
            if (existingStates.Count == 0)
            {
                string stateNotFoundError = string.Empty;//Common.Constants.ErrorMessages.Common.StateError.STATE_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(stateNotFoundError);
            }
            result = new List<Scorer.Models.State>();
            foreach (var eachState in existingStates)
            {
                var state = Mapper.Map<Scorer.Models.State>(eachState);
                result.Add(state);
            }
            return result;
        }
    }
}
