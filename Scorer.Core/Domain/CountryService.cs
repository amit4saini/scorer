﻿using System.Collections.Generic;
using System.Linq;
using Scorer.Common.Database.Database;
using AutoMapper;

namespace Scorer.Core.Domain
{
    public interface ICountryService
    {
        List<Scorer.Models.Country> GetAllCountries();
        List<Scorer.Models.Country> GetAllCountriesByRegion(string regionId);
    }

    public class CountryService : ICountryService
    {
        private IScorerDb _scorerDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public CountryService(IScorerDb scorerDb, IErrorHandlerService errorHandlerService)
        {
            _scorerDb = scorerDb;
            _errorHandlerService = errorHandlerService;

        }

        public List<Scorer.Models.Country> GetAllCountries()
        {
            var result = new List<Scorer.Models.Country>();
            var existingCountries = _scorerDb.Countries.ToList();
            if (existingCountries.Count == 0)
            {
                string countryNotFoundError = string.Empty;//Common.Constants.ErrorMessages.Common.CountryError.COUNTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(countryNotFoundError);
            }
            result = new List<Scorer.Models.Country>();
            foreach (var eachCountry in existingCountries)
            {
                var country = Mapper.Map<Scorer.Models.Country>(eachCountry);
                result.Add(country);
            }
            return result;
        }

        public List<Scorer.Models.Country> GetAllCountriesByRegion(string id)
        {
            var result = new List<Scorer.Models.Country>();
            int regionId;
            if (!int.TryParse(id, out regionId))
            {
                string invalidRegionIdError = string.Empty;//Common.Constants.ErrorMessages.Common.RegionError.INVALID_REGION_ID;
                _errorHandlerService.ThrowResourceNotFoundError(invalidRegionIdError);
            }
            var existingCountriesbyRegion = _scorerDb.Countries.Where(t => t.RegionId == regionId).ToList();
            if (existingCountriesbyRegion.Count == 0)
            {
                string countryNotFoundError = string.Empty;//Common.Constants.ErrorMessages.Common.CountryError.COUNTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(countryNotFoundError);

            }
            result = new List<Scorer.Models.Country>();
            foreach (var eachCountry in existingCountriesbyRegion)
            {
                var country = Mapper.Map<Scorer.Models.Country>(eachCountry);
                result.Add(country);
            }
            return result;
        }
    }
}
