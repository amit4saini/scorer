﻿using System;
using System.Collections.Generic;
using Scorer.Models;
using Scorer.Common.Infrastructure.Email;
using Scorer.Common.Database.Enums;
using Scorer.Models.EmailModels;

namespace Scorer.Core.Domain
{
    public interface IMailSenderService
    {
        void SendNewCaseDetails(NewCaseEmailModel newCaseEmailModel);
        void SendAssignedCaseDetails(AssignCaseEmailModel assignCaseEmailModel);
        void SendNewDiscussionDetails(NewDiscussionEmailModel newDiscussionEmailModel);
        void SendNewDiscussionMessageDetails(NewDiscussionMessageEmailModel newDiscussionMessageEmailModel);
        void SendAssignedTaskDetails(AssignTaskEmailModel assignedTaskEmailModel);
        void SendInviteUserDetails(InviteUserEmailModel inviteUserEmailModel);
        void SendForgotPasswordDetails(ForgotPasswordEmailModel forgotPasswordEmailModel);
        void SendNotificationToSubscriber(string caseNumber, string status, List<string> listOfEmailId);
        void SendTaskCommentDetails(TaskCommentEmailModel taskCommentEmailDetails);
    }

    public class MailSenderService : IMailSenderService
    {        
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public MailSenderService(IUserService userService, IEmailService emailService)
        {
            _userService = userService;
            _emailService = emailService;
        }

        public void SendNotificationToSubscriber(string caseNumber,string status,List<string> listOfEmailId)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", caseNumber);
            _emailService.TokenData.Add("status", status);
          //  _emailService.Template = EmailTemplates.CASE_STATUS_CHANGED.ToString();            
            _emailService.SendTo= listOfEmailId;            
            _emailService.SendEmail();
        }

        public void SendNewCaseDetails(NewCaseEmailModel newCaseEmailModel)
        {                      
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", newCaseEmailModel.CaseNumber);           
            _emailService.TokenData.Add("user", newCaseEmailModel.User.FirstName + " " + newCaseEmailModel.User.LastName);
        //    _emailService.Template = EmailTemplates.CASE_ADDED.ToString();                      
            _emailService.SendTo.Add(newCaseEmailModel.User.Email);          
            _emailService.SendEmail();
        }     

        internal void SendErrorMessage(string errorMessage,string errorCause)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("error_cause", errorCause);
            _emailService.TokenData.Add("error_message", errorMessage);
            _emailService.SendTo.Add("ashish.sehra@vicesoftware.com");
          //  _emailService.Template = EmailTemplates.ERROR_TEMPLATE.ToString();
            _emailService.SendEmail();
        }

        public void SendAssignedCaseDetails(AssignCaseEmailModel assignCaseEmailModel)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", assignCaseEmailModel.CaseNumber);
            _emailService.TokenData.Add("user", assignCaseEmailModel.AssignedUserName);
            _emailService.TokenData.Add("assignee_user_name", assignCaseEmailModel.AssigneeName);
          //  _emailService.Template = EmailTemplates.CASE_ASSIGNED.ToString();
            _emailService.SendTo.Add(assignCaseEmailModel.AssignedUserMail);
            _emailService.SendEmail();
        }

        public void SendNewDiscussionDetails(NewDiscussionEmailModel newDiscussionEmailModel)
        {
            
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", newDiscussionEmailModel.CaseNumber);
            _emailService.TokenData.Add("title", newDiscussionEmailModel.DiscussionTitle);
            _emailService.TokenData.Add("started_by", newDiscussionEmailModel.StartedByUser);
        //    _emailService.Template = EmailTemplates.DISCUSSION_STARTED.ToString();
            for(var index = 0; index < newDiscussionEmailModel.RecipientMails.Count; index++)
            {
                _emailService.SendTo = new List<string>();
                _emailService.SendTo.Add(newDiscussionEmailModel.RecipientMails[index]);
                _emailService.TokenData["recipient"] = newDiscussionEmailModel.RecipientNames[index];
                _emailService.SendEmail();
            }
        }

        public void SendNewDiscussionMessageDetails(NewDiscussionMessageEmailModel newDiscussionMessageEmailModel)
        {
            
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", newDiscussionMessageEmailModel.CaseNumber);
            _emailService.TokenData.Add("title", newDiscussionMessageEmailModel.DiscussionTitle);
            _emailService.TokenData.Add("message", newDiscussionMessageEmailModel.Message);
            _emailService.TokenData.Add("posted_by", newDiscussionMessageEmailModel.AddedByUser);
          //  _emailService.Template = EmailTemplates.ADDED_DISCUSSION_MESSAGE.ToString();
            for (var index = 0; index < newDiscussionMessageEmailModel.RecipientMails.Count; index++)
            {
                _emailService.SendTo = new List<string>();
                _emailService.SendTo.Add(newDiscussionMessageEmailModel.RecipientMails[index]);
                _emailService.TokenData["recipient"] = newDiscussionMessageEmailModel.RecipientNames[index];
                _emailService.SendEmail();
            }
        }

        public void SendAssignedTaskDetails(AssignTaskEmailModel assignedTaskEmailModel)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("case_number", assignedTaskEmailModel.CaseNumber);
            _emailService.TokenData.Add("recipient", assignedTaskEmailModel.AssignedUserName);
            _emailService.TokenData.Add("assignee_user_name", assignedTaskEmailModel.AssigneeName);
            _emailService.TokenData.Add("task_title", assignedTaskEmailModel.TaskTitle);
            _emailService.TokenData.Add("task_description", assignedTaskEmailModel.TaskDescription);
           // _emailService.Template = EmailTemplates.TASK_ASSIGNED.ToString();
            _emailService.SendTo.Add(assignedTaskEmailModel.AssignedUserMail);         
            _emailService.SendEmail();
        }

        public void SendInviteUserDetails(InviteUserEmailModel inviteUserEmailModel)
        {
            _emailService.Subject = inviteUserEmailModel.InviterName + " has invited you to join the Scorer Business Information Group case management system.";
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("inviter_user", inviteUserEmailModel.InviterName);
            _emailService.TokenData.Add("resetPasswordLink", inviteUserEmailModel.link);
          //  _emailService.Template = EmailTemplates.INVITE_USER.ToString();
            _emailService.SendTo.Add(inviteUserEmailModel.InviteeEmail);
            _emailService.SendEmail();
        }

        public void SendForgotPasswordDetails(ForgotPasswordEmailModel forgotPasswordEmailModel)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("recipient", forgotPasswordEmailModel.UserName);
            _emailService.TokenData.Add("resetPasswordLink", forgotPasswordEmailModel.link);
          //  _emailService.Template = EmailTemplates.FORGOT_PASSWORD.ToString();
            _emailService.SendTo.Add(forgotPasswordEmailModel.UserEmail);
            _emailService.SendEmail();
        }

        public void SendTaskCommentDetails(TaskCommentEmailModel taskCommentEmailModel)
        {
            _emailService.SendTo = new List<string>();
            _emailService.TokenData = new Dictionary<string, string>();
            _emailService.TokenData.Add("taskTitle", taskCommentEmailModel.TaskTitle);
            _emailService.TokenData.Add("caseNumber", taskCommentEmailModel.CaseNumber);
            _emailService.TokenData.Add("commentedBy", taskCommentEmailModel.CommentedBy);
            _emailService.TokenData.Add("taskCommentAction", taskCommentEmailModel.TaskCommentAction.ToLower());
            _emailService.TokenData.Add("time", taskCommentEmailModel.Time);
           // _emailService.Template = EmailTemplates.TASK_COMMENT.ToString();
            _emailService.SendTo = taskCommentEmailModel.TaggedUserEmailList;
            _emailService.SendTo.Add(taskCommentEmailModel.AssignedToUser);
            _emailService.Subject = string.Concat("A task comment ", taskCommentEmailModel.TaskCommentAction.ToLower() );
            _emailService.SendEmail();
        }
    }
}