﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scorer.Models;
using Scorer.Models.Account;
using Scorer.Common.Database.Database;
using Scorer.Common.Database.Enums;
using Microsoft.AspNet.Identity;
using AutoMapper;
using System.Net.Http;
using System.Data.Entity.Migrations;
using Scorer.Common.Database.Models.RolesAndPermissionManagement;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;
using System.Web.Configuration;
using System.Data.Entity;

namespace Scorer.Core.Domain
{
    public interface IUserService
    {
        Guid UserId { get; }
        List<Permission> UserPermissions { get; }
        Result<User> GetUser(Guid userId);
        PageResult<User> GetAllUsers(ODataQueryOptions<User> options, HttpRequestMessage request);
        Result<List<Permission>> GetRolePermissions(Guid roleId);
        Result<List<Permission>> GetUserPermissions(Guid userId);
        User SaveUser(User model);
        PageResult<Models.Role> GetAllRoles(ODataQueryOptions<Models.Role> options, HttpRequestMessage request);
        Result Delete(Guid userId);
        bool ValidateUser(string userName);
        User GetCurrentUser();
        PageResult<User> SearchUsersByKeyword(ODataQueryOptions<Models.Account.User> options, HttpRequestMessage request, string keyword);
        User GetUserByEmail(string email);
        List<User> GetUserByUserName(List<string> listOfUserName);
        Common.Database.Models.UserManagement.User CheckIfPasswordIsMasterPassword(string username, string password);
    }

    public class UserService : IUserService
    {
        private IScorerDb _scorerDb;
        private List<Permission> _userPermissions; 
        private readonly IErrorHandlerService _errorHandlerService;

        public UserService(IScorerDb scorerDb, IErrorHandlerService errorHandlerService)
        {
            _errorHandlerService = errorHandlerService;
            _scorerDb = scorerDb;
        }

        public Guid UserId
        {
            get
            {
                return Guid.Parse(HttpContext.Current.User.Identity.GetUserId());
            }
        }

        public List<Permission> UserPermissions
        {
            get
            {
                _userPermissions = GetUserPermissions(UserId).Data;
                return _userPermissions;
            }

        }

        public Result<User> GetUser(Guid userId)
        {
            var result = new Result<User>();
            result.Data = new User();
            var user = _scorerDb.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
            {
                return result;
            }           
            var userRoles = user.Roles.Select(x => x.RoleId).ToList();
            var roleId = _scorerDb.UserRoles.FirstOrDefault(x => x.UserId == userId).RoleId;
            var role = _scorerDb.Roles.FirstOrDefault(x => x.Id == roleId);
            var userPermissions =
              _scorerDb.RolePermissions.Where(rolePermissions => userRoles.Contains(rolePermissions.RoleId)).Select(rolePermissions => rolePermissions.Permission.ToString()).ToList();
            result.Data = Mapper.Map<Models.Account.User>(user);          
            result.Data.Email = user.Email;
            result.Data.Role = Mapper.Map<Models.Role>(role);
            result.Data.Permissions = userPermissions;
            result.IsSuccess = true;
            return result;
        }

        public Result<List<Permission>> GetUserPermissions(Guid userId)
        {
            var result = new Result<List<Permission>>();
            result.Data = new List<Permission>();
            var user = _scorerDb.Users.FirstOrDefault(x => x.Id == userId && x.IsDeleted == false);
            if (user == null)
            {
                string userNotFoundError = Common.Constants.ErrorMessages.UserManagement.UserError.USER_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(userNotFoundError);
            }

            var userRoles = user.Roles.ToList().Select(x => x.RoleId);
            var userPermissions =
                _scorerDb.RolePermissions.Where(rolePermissions => userRoles.Contains(rolePermissions.RoleId)).ToList().Select(rolePermissions => rolePermissions.Permission);

            foreach (var permission in userPermissions)
            {
                result.Data.Add(permission);
            }
            return result;
        }

        public Result<List<Permission>> GetRolePermissions(Guid roleId)
        {
            var result = new Result<List<Permission>>();
            var permissions =
                _scorerDb.RolePermissions.Where(rolePermissions => rolePermissions.RoleId == roleId).Select(permission => permission.Permission).ToList();
            result.Data = new List<Permission>();
            foreach (var permission in permissions)
            {
                result.Data.Add(permission);
            }
            return result;
        }

        public PageResult<User> GetAllUsers(ODataQueryOptions<User> options, HttpRequestMessage request)
        {
            var defaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) || (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            var result = new Result<List<User>>();
            var allUsers = _scorerDb.Users.Where(x => x.IsDeleted == false).ToList();
            if (allUsers == null || allUsers.Count == 0)
            {
                string userNotFoundError = Common.Constants.ErrorMessages.UserManagement.UserError.USER_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(userNotFoundError);
            }
            result.Data = new List<User>();
            foreach (var eachuser in allUsers)
            {
                var user = Mapper.Map<User>(eachuser);                
                var userRoles = eachuser.Roles.Select(x => x.RoleId).ToList();
                var userPermissions =
                  _scorerDb.RolePermissions.Where(rolePermissions => userRoles.Contains(rolePermissions.RoleId)).Select(rolePermissions => rolePermissions.Permission.ToString()).ToList();
                user.Permissions = userPermissions;
                user.Role = Mapper.Map<Models.Role>(_scorerDb.Roles.FirstOrDefault(x => x.Id == (_scorerDb.UserRoles.FirstOrDefault(y => y.UserId == eachuser.Id).RoleId)));
                result.Data.Add(user);
            }
            int selectedCount = result.Data.Count;
            if (options.Filter != null)
            {
                selectedCount = (options.Filter.ApplyTo(result.Data.AsQueryable(), new ODataQuerySettings()) as IEnumerable<User>).ToList().Count;
            }
            result.Data = result.Data.OrderBy(y => y.Role.Name).ToList();
            IQueryable results = options.ApplyTo(result.Data.AsQueryable());           
            return new PageResult<User>(
            results as IEnumerable<User>, request.ODataProperties().NextLink,
            selectedCount);
        }

        public User SaveUser(User model)
        {        
            var result = new User();
            var hasher = new PasswordHasher();
            var UserModel = new Common.Database.Models.UserManagement.User();
            var existingUser = _scorerDb.Users.FirstOrDefault(x => x.Id == model.Id && x.IsDeleted != true);
            if (existingUser != null)
            {
                if (existingUser.Email == model.Email && model.ProfilePicKey == null && model.Id == Guid.Empty)
                {
                    var errorMessage = Common.Constants.ErrorMessages.AccountManagement.UserRegistrationErrors.USER_NAME_ALREADY_EXISTS;
                    _errorHandlerService.ThrowBadRequestError(errorMessage);
                }

                Guid? profilePicKey = null;
                if (model.ProfilePicKey != null)
                {
                    profilePicKey = model.ProfilePicKey;
                }
                UserModel = new Common.Database.Models.UserManagement.User
                {
                    Id = model.Id,
                    PasswordHash = existingUser.PasswordHash,
                    Email = model.Email,
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    SecurityStamp = existingUser.SecurityStamp,
                    CreationDateTime = existingUser.CreationDateTime,
                    ProfilePicKey = profilePicKey
                };
                //Update LastEditedDateTime only when password is changed
                if (model.LastEditedDateTime != DateTime.MinValue)
                    UserModel.LastEditedDateTime = model.LastEditedDateTime;
                else
                    UserModel.LastEditedDateTime = existingUser.LastEditedDateTime;                  
            }
            else
            {
                var existingUserName = _scorerDb.Users.FirstOrDefault(x => x.Email == model.Email);
                if (existingUserName != null)
                {
                    var errorMessage = Common.Constants.ErrorMessages.AccountManagement.UserRegistrationErrors.USER_NAME_ALREADY_EXISTS;
                    _errorHandlerService.ThrowBadRequestError(errorMessage);
                }
                if (model.PasswordHash == null)
                {
                    model.PasswordHash = "dummy123";
                }
                UserModel = new Common.Database.Models.UserManagement.User
                {

                    PasswordHash = hasher.HashPassword(model.PasswordHash),
                    Email = model.Email,
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    CreationDateTime = DateTime.Now,
                    LastEditedDateTime = DateTime.Now
            };
            }        
            _scorerDb.Users.AddOrUpdate(UserModel);
            _scorerDb.SaveChangesWithErrors();
            AssignUserRole(UserModel.Id, model);
            result = Mapper.Map<User>(UserModel);
            return result;
        }

        Result AssignUserRole(Guid Id, User user)
        {
            Result result = new Result();
            var userRole = _scorerDb.UserRoles.FirstOrDefault(x => x.UserId == Id);
            if (userRole == null)
            {
                _scorerDb.UserRoles.Add(new UserRole
                {
                    RoleId = user.Role.Id,
                    UserId = Id
                });
            }
            else {
                _scorerDb.UserRoles.AddOrUpdate(new UserRole
                {
                    UserRoleId = userRole.UserRoleId,
                    RoleId = user.Role.Id,
                    UserId = Id
                });
            }
            _scorerDb.SaveChangesWithErrors();
            return result;
        }

        public PageResult<Models.Role> GetAllRoles(ODataQueryOptions<Models.Role> options, HttpRequestMessage request)
        {
            var result = new List<Models.Role>();
            var allRoles = _scorerDb.Roles.ToList();
            if (allRoles == null || allRoles.Count == 0)
            {
                string rolesNotFoundError = Common.Constants.ErrorMessages.UserManagement.UserError.ROLES_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(rolesNotFoundError);
            }
            result = new List<Models.Role>();
            foreach (var role in allRoles)
            {
                var roles = Mapper.Map<Models.Role>(role);
                result.Add(roles);
            }
            IQueryable results = options.ApplyTo(result.AsQueryable());
            return new PageResult<Models.Role>(results as IEnumerable<Models.Role>, request.ODataProperties().NextLink, result.Count);
        }

        public Result Delete(Guid userId)
        {
            Result result = new Result();
            var existingUser = _scorerDb.Users.FirstOrDefault(x => x.Id == userId && (x.IsDeleted != true));
            if (existingUser != null)
            {
                existingUser.IsDeleted = true;
                existingUser.DeletedBy = UserId;
                existingUser.DeletedOn = DateTime.Now;
                _scorerDb.SaveChangesWithErrors();
            }
            return result;
        }

        public bool ValidateUser(string userName)
        {
            var existingUser = _scorerDb.Users.FirstOrDefault(x => x.UserName == userName);
            if (existingUser != null && existingUser.IsDeleted == true)
            {
                return false;
            }
            return true;
        }

        public User GetCurrentUser()
        {
            var result = new User();
            var currentUser = _scorerDb.Users.Include(x => x.Roles).FirstOrDefault(x => x.Id == UserId);
            if (currentUser != null)
            {
                result = Mapper.Map<User>(currentUser);
                var roleId = currentUser.Roles.FirstOrDefault().RoleId;
                var role = _scorerDb.Roles.FirstOrDefault(x => x.Id == roleId);
                result.Role = Mapper.Map<Models.Role>(role);
                result.Permissions = _scorerDb.RolePermissions.Include(y => y.Permission).Where(x => x.RoleId == roleId).Select(x => x.Permission.ToString()).ToList();
            }
            return result;
        }

        public PageResult<User> SearchUsersByKeyword(ODataQueryOptions<Models.Account.User> options, HttpRequestMessage request, string keyword)
        {
            var result = new List<Scorer.Models.Account.User>();
            var defaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) || (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            var searchUsersResult = _scorerDb.Users.Where(x => ((x.FirstName ?? "").Contains(keyword) || (x.LastName ?? "").Contains(keyword) || (x.Email).Contains(keyword) || (x.UserName).Contains(keyword)) && (x.IsDeleted == false)).ToList();
            foreach (var eachuser in searchUsersResult)
            {
                var user = Mapper.Map<User>(eachuser);
                var userRoles = eachuser.Roles.Select(x => x.RoleId).ToList();
                var userPermissions =
                  _scorerDb.RolePermissions.Where(rolePermissions => userRoles.Contains(rolePermissions.RoleId)).Select(rolePermissions => rolePermissions.Permission.ToString()).ToList();
                user.Permissions = userPermissions;
                var role = _scorerDb.UserRoles.FirstOrDefault(y => y.UserId == eachuser.Id);
                if (role != null)
                {
                    var roleId = role.RoleId;
                    user.Role = Mapper.Map<Models.Role>(_scorerDb.Roles.FirstOrDefault(x => x.Id == roleId));
                }
                result.Add(user);
            }
            int selectedCount = result.Count;
            if (options.Filter != null)
            {
                selectedCount = (options.Filter.ApplyTo(result.AsQueryable(), new ODataQuerySettings()) as IEnumerable<User>).ToList().Count;
            }
            IQueryable results = options.ApplyTo(result.AsQueryable());
            return new PageResult<User>(
            results as IEnumerable<User>, request.ODataProperties().NextLink,
           selectedCount);
        }

        public List<User> GetUserByUserName(List<string> listOfUserName)
        {
            var existingUser = _scorerDb.Users.Where(x => listOfUserName.Contains(string.Concat(x.FirstName, " ", x.LastName)));
            var mappedUsersList = Mapper.Map<List<Models.Account.User>>(existingUser);
            return mappedUsersList;
        }

        public User GetUserByEmail(string email)
        {
            var existingUser = _scorerDb.Users.FirstOrDefault(x => x.Email == email);
            var mappedUser = Mapper.Map<Models.Account.User>(existingUser);
            return mappedUser;
        }

        public Common.Database.Models.UserManagement.User CheckIfPasswordIsMasterPassword(string username, string password)
        {      
            var existingUser = _scorerDb.Users.FirstOrDefault(x => x.UserName == username);
            if (existingUser != null)
            {
                var masterPassword = WebConfigurationManager.AppSettings["masterPassword"].ToString();
               var comparePassword = masterPassword.Equals(password);
                if (comparePassword == true)
                {
                    return existingUser;
                }
                else { return null; }
            }
            return null;
        }
    }
}

