﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scorer.Core.Domain
{
    public class CustomPasswordValidator : PasswordValidator
    {
        public override async Task<IdentityResult> ValidateAsync(string password)
        {
            var requireNonLetterOrDigit = base.RequireNonLetterOrDigit;
            base.RequireNonLetterOrDigit = false;
            var result = await base.ValidateAsync(password);

            if (!requireNonLetterOrDigit)
                return result;

            if (!Enumerable.All<char>((IEnumerable<char>)password, new Func<char, bool>(this.IsLetterOrDigit)))
                return result;

            // Build a new list of errors so that the custom 'PasswordRequireNonLetterOrDigit' could be added. 
            List<string> list = new List<string>();
            foreach (var error in result.Errors)
            {
                list.Add(error);
            }
            // Add our own message: (The default by MS is: 'Passwords must have at least one non letter or digit character.')
            list.Add("Password must have at least one special character. (E.g. '£ $ % ^ _ etc.')");
            result = await Task.FromResult<IdentityResult>(IdentityResult.Failed(string.Join(" ", (IEnumerable<string>)list)));
            return result;
        }
    }
}
