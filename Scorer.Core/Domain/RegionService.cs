﻿using System.Collections.Generic;
using System.Linq;
using Scorer.Common.Database.Database;
using AutoMapper;

namespace Scorer.Core.Domain
{

    public interface IRegionService
    {
        List<Scorer.Models.Region> GetAllRegions();
    }

    public class RegionService : IRegionService
    {
        private IScorerDb _scorerDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public RegionService(IScorerDb scorerDb, IErrorHandlerService errorHandlerService)
        {
            _scorerDb = scorerDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Scorer.Models.Region> GetAllRegions()
        {

            var result = new List<Scorer.Models.Region>();
            var regionList = _scorerDb.Regions.ToList();
            if (regionList.Count == 0)
            {
                string regionNotFoundError = string.Empty;//Common.Constants.ErrorMessages.Common.RegionError.REGION_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(regionNotFoundError);
            }
            result = new List<Scorer.Models.Region>();
            foreach (var eachregion in regionList)
            {
                var region = Mapper.Map<Scorer.Models.Region>(eachregion);
                result.Add(region);
            }
            return result;
        }

    }
}
