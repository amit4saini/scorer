﻿using System;

namespace Scorer.Models
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
