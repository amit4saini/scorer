﻿using System;

namespace Scorer.Models
{
    public class RegisterViewModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }   
        public string Password { get; set; }       
        public string ConfirmPassword { get; set; }
      
    }
}
