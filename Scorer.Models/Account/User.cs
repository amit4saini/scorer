﻿using System;
using System.Collections.Generic;

namespace Scorer.Models.Account
{
    public class User
    {
        public User()
        {
            Permissions = new List<string>();
        }
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string ConfirmPassword { get; set; }
        public Role Role { get; set; }
        public IList<string> Permissions { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? ProfilePicKey { get; set; }
        public string ProfilePicUrl { get; set; }
        public DateTime LastEditedDateTime { get; set; }
    }
}
