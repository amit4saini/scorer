﻿using Scorer.Models.Account;
using System;
using System.Collections.Generic;

namespace Scorer.Models
{
    public class Notification
    {
        public Guid Id { get; set; }
        public List<Guid> ListOfUserId { get; set; }
        public string NotificationText { get; set; }
        public bool IsViewed { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public string URL { get; set; }
        public User CreatedByUser { get; set; }
    }
}
