﻿namespace Scorer.Models
{
    public class Mail
    {
      public  string EmailAddress { get; set; }
      public  string Subject { get; set; }
      public string Body { get; set; }
    }
}
