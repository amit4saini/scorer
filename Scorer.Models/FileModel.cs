﻿using System;
using Scorer.Common.Database.Enums;

namespace Scorer.Models
{
    public class FileModel
    {
        public string FileName { get; set; }       
        public DateTime CreateDateTime { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string FileUrl { get; set; }
        public Guid FileId { get; set; }
        public string Key { get; set; }
        public string AzureKey { get; set; } 
        public string FileType { get; set; }   
        public string FileContent { get; set; }   
        public string UserName { get; set; } 
    }
}
