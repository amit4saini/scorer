﻿using Scorer.Models.Account;

namespace Scorer.Models.EmailModels
{
    public class NewCaseEmailModel
    {
        public string CaseNumber { get; set; }
        public User User { get; set; }        
    }
}
