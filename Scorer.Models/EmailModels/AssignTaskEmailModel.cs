﻿namespace Scorer.Models.EmailModels
{
    public class AssignTaskEmailModel
    {        
        public string CaseNumber { get; set; }
        public string AssigneeName { get; set; }
        public string AssignedUserMail { get; set; }
        public string AssignedUserName { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }  
    }
}
