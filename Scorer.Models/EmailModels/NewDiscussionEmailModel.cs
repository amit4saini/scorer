﻿using System.Collections.Generic;

namespace Scorer.Models.EmailModels
{
    public class NewDiscussionEmailModel
    {
        public string CaseNumber { get; set; }
        public string DiscussionTitle { get; set; }
        public string StartedByUser { get; set; }
        public List<string> RecipientMails { get; set; }
        public List<string> RecipientNames { get; set; }
    }
}
