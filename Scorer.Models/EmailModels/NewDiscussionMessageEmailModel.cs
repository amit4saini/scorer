﻿using System.Collections.Generic;

namespace Scorer.Models.EmailModels
{
    public class NewDiscussionMessageEmailModel
    {
        public string CaseNumber { get; set; }
        public string DiscussionTitle { get; set; }
        public string Message { get; set; }
        public string AddedByUser { get; set; }
        public List<string> RecipientMails { get; set; }
        public List<string> RecipientNames { get; set; }
    }
}
