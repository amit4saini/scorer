﻿using System.Collections.Generic;

namespace Scorer.Models.EmailModels
{
    public class TaskCommentEmailModel
    {
        public string CaseNumber { get; set; }
        public string TaskTitle { get; set; }
        public string CommentedBy { get; set; }
        public string TaskCommentAction { get; set; }
        public string Time { get; set; }
        public List<string> TaggedUserEmailList { get; set; }
        public string AssignedToUser { get; set; }
    }
}
