﻿namespace Scorer.Models.EmailModels
{
    public class AssignCaseEmailModel
    {        
        public string CaseNumber { get; set; }
        public string AssigneeName { get; set; }
        public string AssignedUserMail { get; set; }
        public string AssignedUserName { get; set; }          
    }
}
