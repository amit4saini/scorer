﻿namespace Scorer.Models.EmailModels
{
    public class InviteUserEmailModel
    {
        public string InviterName { get; set; }
        public string InviteeName { get; set; }
        public string InviteeEmail { get; set; }
        public string link { get; set; }
    }
}
