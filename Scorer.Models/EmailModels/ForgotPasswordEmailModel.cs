﻿namespace Scorer.Models.EmailModels
{
    public class ForgotPasswordEmailModel
    {
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string link { get; set; }
    }
}
