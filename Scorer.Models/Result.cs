﻿using System;
using System.Collections.Generic;

namespace Scorer.Models
{
    // TODO: Prashanth - we need to move to using status
    public class Result<T>
    {
        public Result()
        {
            ArgumentErrors = new List<string>();
        }
        public T Data { get; set; }
        public List<string> ArgumentErrors { get; set; }
        public string ErrorCode { get; set; }
        public String ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class Result
    {
        public Result()
        {
            ArgumentErrors = new List<string>();
        }
        public List<string> ArgumentErrors { get; set; }
        public string ErrorCode { get; set; }
        public String ErrorMessage { get; set; }       
        public bool IsSuccess { get; set; }
    }
}